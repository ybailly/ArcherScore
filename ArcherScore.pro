
QT += core gui widgets svg concurrent

android-* {
    QT += multimedia multimediawidgets
}

CONFIG += c++14

TARGET = ArcherScore
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000
DEFINES *= QT_USE_QSTRINGBUILDER

INCLUDEPATH += src
SOURCES += \
    src/main.cpp \
    src/score_unit.cpp \
    src/main_win.cpp \
    src/target_scene.cpp \
    src/hit_location_editor.cpp \
    src/hit_locations_viewer.cpp \
    src/target_view.cpp \
    src/style.cpp \
    src/sessions_list.cpp \
    src/sessions_list_model.cpp \
    src/sessions_list_delegate.cpp \
    src/ends_round.cpp \
    src/session.cpp \
    src/ends_round_model.cpp \
    src/ends_round_delegate.cpp \
    src/single_end.cpp \
    src/session_setup.cpp \
    src/ends_round_editor.cpp \
    src/front_page.cpp \
    src/tournament_editor.cpp \
    src/tournament_rounds_model.cpp \
    src/tournament_rounds_delegate.cpp \
    src/generic_page.cpp \
    src/generic_overlay.cpp \
    src/overlay_message.cpp \
    src/overlay_choose_location.cpp \
    src/overlay_get_arrow_size_spec.cpp \
    src/settings.cpp \
    src/units_ui.cpp \
    src/bow_editor.cpp \
    src/prev_sessions_page.cpp \
    src/bows.cpp \
    src/edit_bows_page.cpp \
    src/bow_accessories_editor.cpp \
    src/bow_sight_editor.cpp \
    src/bow_brands_models_editor.cpp \
    src/overlay_choose_bow_type.cpp \
    src/bow_stabs_editor.cpp \
    src/generic_number_input.cpp \
    src/overlay_generic_number_input.cpp \
    src/overlay_tagged_value_pair_input.cpp \
    src/keep_ratio_svg_widget.cpp \
    src/button_label.cpp \
    src/overlay_choose_session_type.cpp \
    src/color_chooser.cpp \
    src/qt_utils.cpp \
    src/input_button_controller.cpp \
    src/bows_list_widget.cpp \
    src/bow_push_button.cpp \
    src/overlay_choose_score.cpp \
    src/arrow_size_spec.cpp \
    src/common_types_utils.cpp \
    src/geometry_utils.cpp \
    src/ui_utils.cpp \
    src/arrows.cpp \
    src/overlay_choose_target_min_score.cpp \
    src/arrow_set_editor.cpp \
    src/arrow_sets_list_widget.cpp \
    src/arrow_set_push_button.cpp \
    src/edit_arrow_sets_page.cpp \
    src/color_button.cpp

HEADERS  += \
    src/score_unit.h \
    src/miniball.h \
    src/main_win.h \
    src/target_scene.h \
    src/hit_location_editor.h \
    src/hit_locations_viewer.h \
    src/target_view.h \
    src/style.h \
    src/sessions_list.h \
    src/sessions_list_model.h \
    src/sessions_list_delegate.h \
    src/single_end.h \
    src/ends_round.h \
    src/session.h \
    src/ends_round_model.h \
    src/ends_round_delegate.h \
    src/session_setup.h \
    src/ends_round_editor.h \
    src/front_page.h \
    src/tournament_editor.h \
    src/tournament_rounds_model.h \
    src/tournament_rounds_delegate.h \
    src/generic_page.h \
    src/generic_overlay.h \
    src/overlay_message.h \
    src/overlay_choose_location.h \
    src/overlay_get_arrow_size_spec.h \
    src/settings.h \
    src/bows.h \
    src/units.h \
    src/units_ui.h \
    src/bow_editor.h \
    src/prev_sessions_page.h \
    src/edit_bows_page.h \
    src/tagged_value.h \
    src/cpp_utils.h \
    src/bow_accessories_editor.h \
    src/bow_stabs_editor.h \
    src/bow_sight_editor.h \
    src/bow_brands_models_editor.h \
    src/overlay_choose_bow_type.h \
    src/generic_number_input.h \
    src/overlay_generic_number_input.h \
    src/overlay_tagged_value_input.h \
    src/overlay_tagged_value_pair_input.h \
    src/input_button_controller.h \
    src/overlay_value_input_common.h \
    src/keep_ratio_svg_widget.h \
    src/button_label.h \
    src/overlay_choose_session_type.h \
    src/color_chooser.h \
    src/tagged_value_pair.h \
    src/qt_utils.h \
    src/bows_list_widget.h \
    src/bow_push_button.h \
    src/overlay_choose_score.h \
    src/arrows.h \
    src/arrow_size_spec.h \
    src/common_types.h \
    src/units_pairs.h \
    src/common_types_utils.h \
    src/geometry_utils.h \
    src/ui_utils.h \
    src/overlay_choose_target_min_score.h \
    src/cpp_enum_utils.h \
    src/arrow_set_editor.h \
    src/arrow_sets_list_widget.h \
    src/arrow_set_push_button.h \
    src/edit_arrow_sets_page.h \
    src/color_button.h

FORMS += \
    src/main_win.ui \
    src/hit_location_editor.ui \
    src/hit_locations_viewer.ui \
    src/session_setup.ui \
    src/ends_round_editor.ui \
    src/front_page.ui \
    src/tournament_editor.ui \
    src/overlay_choose_location.ui \
    src/overlay_get_arrow_size_spec.ui \
    src/bow_editor.ui \
    src/prev_sessions_page.ui \
    src/edit_bows_page.ui \
    src/bow_accessories_editor.ui \
    src/bow_brands_models_editor.ui \
    src/bow_stabs_editor.ui \
    src/bow_sight_editor.ui \
    src/overlay_choose_bow_type.ui \
    src/generic_number_input.ui \
    src/overlay_generic_number_input.ui \
    src/overlay_tagged_value_pair_input.ui \
    src/overlay_choose_session_type.ui \
    src/overlay_choose_score.ui \
    src/overlay_choose_target_min_score.ui \
    src/arrow_set_editor.ui \
    src/edit_arrow_sets_page.ui


android-* {
    QT += androidextras
    HEADERS += \
        src/android_sms.h \
#        src/camera_capture.h \
#        src/camera_target.h

    SOURCES += \
        src/android_sms.cpp \
#        src/camera_capture.cpp \
#        src/camera_target.cpp

    FORMS += \
#        src/camera_target.ui

}

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    ui/Session_Setup_Form.ui.qml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

RESOURCES += \
    resources.qrc

STATECHARTS +=

