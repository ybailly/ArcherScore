
#include "android_sms.h"

#include <QtDebug>
#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>

bool sendSMS(QString const& message,
             QStringList const& numbers)
{
   QAndroidJniObject activity =
         QAndroidJniObject::callStaticObjectMethod(
            "org/qtproject/qt5/android/QtNative",
            "activity",
            "()Landroid/app/Activity;");
   if ( not activity.isValid() )
   {
      qDebug() << __FILE__ << __LINE__ << "Failed to get Android activity";
      return false;
   }
   // get the default SmsManager
   QAndroidJniObject sms_manager =
         QAndroidJniObject::callStaticObjectMethod(
            "android/telephony/SmsManager",
            "getDefault",
            "()Landroid/telephony/SmsManager;" );
   if ( not sms_manager.isValid() )
   {
      qDebug() << __FILE__ << __LINE__ << "Failed to get SMS manager";
      return false;
   }
   QAndroidJniObject sc_address = NULL;
#if 0
   QAndroidJniObject sent_intent = NULL;
   QAndroidJniObject delivery_intent = NULL;
#endif
   QAndroidJniObject text_message = QAndroidJniObject::fromString(message);
   for(QString const& number: numbers)
   {
      QAndroidJniObject phone_number = QAndroidJniObject::fromString(number);
      sms_manager.callMethod<void>(
               "sendTextMessage",
               "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
               phone_number.object<jstring>(),
               sc_address.object<jstring>(),
               text_message.object<jstring>(),
               NULL,
               NULL );
   }
   return true;
}

