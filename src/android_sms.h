
#pragma once

#include <QtCore/QString>
#include <QtCore/QStringList>

bool sendSMS(QString const& message,
             QStringList const& numbers);
