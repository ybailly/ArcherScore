
#include "arrow_set_editor.h"

#include "qt_utils.h"

namespace
{
   template<typename T>
   void create_controller(T*& p, QObject* parent)
   {
      p = new T(parent);
   }
}

Arrow_Set_Editor::Arrow_Set_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);
   qt::adjustToolButtonsIconSize(this->contents(), 4.0);

   create_controller(this->count_controller, this);
   this->count_controller->setButton(this->bt_count);
   this->count_controller->setOverlayMessage(tr("Nombre de flèches :"));

   create_controller(this->diameter_controller, this);
   this->diameter_controller->setButton(this->bt_diameter);
   this->diameter_controller->setOverlayMessage(tr("Diamètre des flèches :"));

   create_controller(this->thickness_controller, this);
   this->thickness_controller->setButton(this->bt_thickness);
   this->diameter_controller->setOverlayMessage(tr("Épaisseur des flèches :"));

   create_controller(this->spine_controller, this);
   this->spine_controller->setButton(this->bt_spine);
   this->spine_controller->setOverlayMessage(tr("<i>Spine</i> des flèches :"));

   create_controller(this->length_controller, this);
   this->length_controller->setButton(this->bt_length);
   this->length_controller->setOverlayMessage(tr("Longueur des flèches :"));

   create_controller(this->weight_controller, this);
   this->weight_controller->setButton(this->bt_weight);
   this->weight_controller->setOverlayMessage(tr("Poids des flèches :"));
}

void Arrow_Set_Editor::setCurrentArrowSet(Arrow_Set const& as)
{
   this->le_name->setText(as.name);
   this->count_controller->setValue(as.count);
   this->bt_body_color->setColor(as.color);
   this->bt_feather_0_color->setColor(as.fletching.colors[0]);
   this->bt_feather_1_color->setColor(as.fletching.colors[1]);
   this->bt_feather_2_color->setColor(as.fletching.colors[2]);
   this->diameter_controller->setValue(as.diameter);
   this->thickness_controller->setValue(as.thickness);
   this->spine_controller->setValue(as.spine);
   this->length_controller->setValue(as.length);
   this->weight_controller->setValue(as.weight);
}

Arrow_Set Arrow_Set_Editor::currentArrowSet() const
{
   Arrow_Set as;
   as.name = this->le_name->text();
   as.count = this->count_controller->value();
   as.color = this->bt_body_color->color();
   as.fletching.colors[0] = this->bt_feather_0_color->color();
   as.fletching.colors[1] = this->bt_feather_1_color->color();
   as.fletching.colors[2] = this->bt_feather_2_color->color();
   as.diameter = this->diameter_controller->value();
   as.thickness = this->thickness_controller->value();
   as.spine = this->spine_controller->value();
   as.length = this->length_controller->value();
   as.weight = this->weight_controller->value();
   return as;
}
