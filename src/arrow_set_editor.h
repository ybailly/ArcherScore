
#pragma once

#include "generic_page.h"

#include "ui_arrow_set_editor.h"

#include "input_button_controller.h"
#include "arrows.h"

class Arrow_Set_Editor: public Generic_Page, private Ui::Arrow_Set_Editor
{
      Q_OBJECT
   public:
      explicit Arrow_Set_Editor(QWidget* parent = nullptr);

      void setCurrentArrowSet(Arrow_Set const& as);
      Arrow_Set currentArrowSet() const;

   private:
      Input_Button_Controller<decltype(Arrow_Set::count)>*     count_controller{nullptr};
      Input_Button_Controller<decltype(Arrow_Set::diameter)>*  diameter_controller{nullptr};
      Input_Button_Controller<decltype(Arrow_Set::thickness)>* thickness_controller{nullptr};
      Input_Button_Controller<decltype(Arrow_Set::spine)>*     spine_controller{nullptr};
      Input_Button_Controller<decltype(Arrow_Set::length)>*    length_controller{nullptr};
      Input_Button_Controller<decltype(Arrow_Set::weight)>*    weight_controller{nullptr};

}; // class Arrow_Set_Editor
