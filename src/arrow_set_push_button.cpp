
#include "arrow_set_push_button.h"

#include "arrows.h"
#include "units_ui.h"
#include "ui_utils.h"
#include "qt_utils.h"

Arrow_Set_Push_Button::Arrow_Set_Push_Button(QWidget* parent):
   Button_Label(parent)
{
   this->setWordWrap(true);
   this->setSpacing(qRound(0.5*qt::computeEX(this)));
   this->setArrowSetIndex(Invalid_Arrow_Set_Index);
}

Arrow_Set_Index Arrow_Set_Push_Button::arrowSetIndex() const
{
   return this->arrow_set_index;
}

bool Arrow_Set_Push_Button::haveArrowSet() const
{
   return this->have_arrow_set;
}

void Arrow_Set_Push_Button::setArrowSetIndex(Arrow_Set_Index asi)
{
   this->arrow_set_index = asi;
   if ( this->arrow_set_index == Invalid_Arrow_Set_Index )
   {
      this->loadLeftSvg(generateSvgFromFletching(undefinedFletching()));
      this->setText(tr("Flèches<br/>indéterminées..."));
      this->have_arrow_set = false;
   }
   else
   {
      Arrow_Set const& arrow_set = arrowSets().arrowSet(this->arrow_set_index);
      this->internalDisplayArrowSet(arrow_set);
      this->have_arrow_set = true;
   }
}

void Arrow_Set_Push_Button::displayArrowSet(Arrow_Set const& as)
{
   this->arrow_set_index = Invalid_Arrow_Set_Index;
   this->internalDisplayArrowSet(as);
   this->have_arrow_set = true;
}

void Arrow_Set_Push_Button::internalDisplayArrowSet(Arrow_Set const& as)
{
   this->loadLeftSvg(generateSvgFromFletching(as.fletching));
   this->setText(
            QStringLiteral("<b>%1</b><br/>%2 flèches | %5%3 | %4 | %5")
            .arg(as.name)
            .arg(as.count)
            .arg(toShortString(as.diameter))
            .arg(toShortString(as.length))
            .arg(toShortString<void>(as.spine))
            .arg(QChar(0x2300)));
}
