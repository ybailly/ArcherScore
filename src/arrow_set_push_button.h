
#pragma once

#include "common_types.h"
#include "button_label.h"

class Arrow_Set;

class Arrow_Set_Push_Button: public Button_Label
{
      Q_OBJECT
   public:
      explicit Arrow_Set_Push_Button(QWidget* parent = nullptr);

      Arrow_Set_Index arrowSetIndex() const;
      bool haveArrowSet() const;

   public slots:
      void setArrowSetIndex(Arrow_Set_Index asi);
      void displayArrowSet(Arrow_Set const& as);

   protected:
      void internalDisplayArrowSet(Arrow_Set const& as);

   private:
      Arrow_Set_Index arrow_set_index{Invalid_Arrow_Set_Index};
      bool have_arrow_set{false};

}; // class Arrow_Set_Push_Button
