
#include "arrow_sets_list_widget.h"

#include "button_label.h"
#include "arrows.h"
#include "arrow_set_push_button.h"
#include "overlay_message.h"
#include "qt_utils.h"

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QScrollArea>

Arrow_Sets_List_Widget::Arrow_Sets_List_Widget(QWidget* parent):
   QWidget(parent)
{
   QVBoxLayout* vbox = new QVBoxLayout(this);
   vbox->setMargin(0);
   vbox->setSpacing(2);
   this->setLayout(vbox);

   this->scroll = new QScrollArea(this);
   this->scroll->setWidgetResizable(true);
   this->scroll->setAlignment(Qt::AlignTop bitor Qt::AlignHCenter);
   this->scroll->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   vbox->addWidget(this->scroll);

   Button_Label* bt_new = new Button_Label(this);
   bt_new->setText(tr("Nouveau jeu de flèches..."));
   bt_new->loadLeftSvg(QStringLiteral(":/icons/add_arrow_set"));
   bt_new->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
   bt_new->setSpacing(qt::computeEM(this));
   bt_new->setHorizontalMargin(qt::computeEM(this));
   QObject::connect(
            bt_new, &Button_Label::clicked,
            this, &Arrow_Sets_List_Widget::requestNewArrowSet);
   vbox->addWidget(bt_new);
}

void Arrow_Sets_List_Widget::refreshArrowSetList()
{
   this->scroll->setWidget(nullptr);
   QWidget* w = new QWidget(this->scroll);
   this->scroll->setWidget(w);

   if ( arrowSets().empty() )
   {
      return;
   }

   QGridLayout* grid = new QGridLayout(w);
   grid->setMargin(2);
   grid->setHorizontalSpacing(2);
   grid->setVerticalSpacing(4);
   w->setLayout(grid);

   int row = 0;
   for(Arrow_Set const& arrow_set: arrowSets())
   {
      Arrow_Set_Push_Button* arrow_set_button = new Arrow_Set_Push_Button(w);
      arrow_set_button->setArrowSetIndex(Arrow_Set_Index(row));
      arrow_set_button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
      QObject::connect(
               arrow_set_button, &Button_Label::clicked,
               [this,row]()
      {
         emit this->selectedArrowSet(Arrow_Set_Index(row));
      });
      grid->addWidget(arrow_set_button, row, 0);
      //
      Button_Label* bt_delete = new Button_Label(w);
      bt_delete->loadLeftSvg(QStringLiteral(":/icons/delete_arrow_set"));
      bt_delete->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
      bt_delete->setHorizontalMargin(qRound(0.5*qt::computeEX(this)));
      QObject::connect(
               bt_delete, &Button_Label::clicked,
               [this,row]()
      {
         this->onRequestDeleteArrowSet(Arrow_Set_Index(row));
      });
      grid->addWidget(bt_delete, row, 1);
      //
      ++row;
   }
   grid->setRowStretch(row, 1);
}

void Arrow_Sets_List_Widget::onRequestDeleteArrowSet(Arrow_Set_Index asi)
{
   if ( asi == Invalid_Arrow_Set_Index )
   {
      return;
   }
   Overlay_Message* msg = new Overlay_Message(this);
   Arrow_Set const& arrow_set = arrowSets().arrowSet(asi);
   QString const& arrow_set_name = arrow_set.name;
   msg->setQuestion(
            tr("Suppression d'un jeu de flèches"),
            tr("Voulez-vous vraiment supprimer le jeu de flèches <b>%1</b> ?<br/>"
               "Cette opération est définitive.")
            .arg(arrow_set_name));
   QObject::connect(
            msg, &Overlay_Message::clickedButton,
            [this,msg,asi](Overlay_Message::Button button)
   {
      if ( button == Overlay_Message::Button::Yes )
      {
         eraseArrowSet(asi);
         this->refreshArrowSetList();
      }
      msg->discard();
   });
   msg->run();
}
