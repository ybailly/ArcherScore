
#pragma once

#include <QtWidgets/QWidget>

#include "arrows.h"

class QScrollArea;

class Arrow_Sets_List_Widget: public QWidget
{
      Q_OBJECT
   public:
      explicit Arrow_Sets_List_Widget(QWidget* parent = nullptr);

   public slots:
      void refreshArrowSetList();

   signals:
      void requestNewArrowSet();
      void selectedArrowSet(Arrow_Set_Index asi);

   protected:
      void onRequestDeleteArrowSet(Arrow_Set_Index asi);

   private:
      QScrollArea* scroll{nullptr};

}; // class Arrow_Sets_List_Widget
