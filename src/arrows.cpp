
#include "arrows.h"
#include "units_ui.h"

#include <QtDebug>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QSaveFile>

namespace
{
   struct Arrow_Set_Json
   {
         static QString const version;
         static QString const name;
         static QString const diameter;
         static QString const thickness;
         static QString const count;
         static QString const spine;
         static QString const length;
         static QString const weight;
         static QString const color;
         static QString const fletching;
         struct Fletching
         {
               static QString const colors;
         };
   };
   QString const Arrow_Set_Json::version{QStringLiteral("version")};
   QString const Arrow_Set_Json::name{QStringLiteral("name")};
   QString const Arrow_Set_Json::diameter{QStringLiteral("diameter")};
   QString const Arrow_Set_Json::thickness{QStringLiteral("thickness")};
   QString const Arrow_Set_Json::count{QStringLiteral("count")};
   QString const Arrow_Set_Json::spine{QStringLiteral("spine")};
   QString const Arrow_Set_Json::length{QStringLiteral("length")};
   QString const Arrow_Set_Json::weight{QStringLiteral("weight")};
   QString const Arrow_Set_Json::color{QStringLiteral("color")};
   QString const Arrow_Set_Json::fletching{QStringLiteral("fletching")};
   QString const Arrow_Set_Json::Fletching::colors{QStringLiteral("colors")};

   struct Arrow_Sets_Json
   {
         static QString const version;
         static QString const sets;
   };
   QString const Arrow_Sets_Json::version{QStringLiteral("version")};
   QString const Arrow_Sets_Json::sets{QStringLiteral("sets")};
}

Fletching const& undefinedFletching()
{
   static Fletching undefined{{Qt::transparent,Qt::transparent,Qt::transparent}};
   return undefined;
}

QJsonObject Arrow_Set::toJson() const
{
   QJsonObject obj;
   obj.insert(Arrow_Set_Json::version, 1);
   obj.insert(Arrow_Set_Json::name, this->name);
   obj.insert(Arrow_Set_Json::diameter, toLongString(this->diameter));
   obj.insert(Arrow_Set_Json::thickness, toLongString(this->thickness));
   obj.insert(Arrow_Set_Json::count, this->count);
   obj.insert(Arrow_Set_Json::spine, this->spine);
   obj.insert(Arrow_Set_Json::length, toLongString(this->length));
   obj.insert(Arrow_Set_Json::weight, toLongString(this->weight));
   obj.insert(Arrow_Set_Json::color, this->color.name(QColor::HexArgb));
   QJsonArray fletching_colors;
   for(QColor const& color: this->fletching.colors)
   {
      fletching_colors.append(color.name(QColor::HexArgb));
   }
   QJsonObject fletching_object;
   fletching_object.insert(Arrow_Set_Json::Fletching::colors, fletching_colors);
   obj.insert(Arrow_Set_Json::fletching, fletching_object);
   return obj;
}

Arrow_Set& Arrow_Set::fromJson(QJsonObject obj)
{
   int const version = obj[Arrow_Set_Json::version].toInt();
   if ( version > 1 )
   {
      qDebug() << "*** Unknown version" << version << "while reading arrows";
      return *this;
   }
   this->name = obj[Arrow_Set_Json::name].toString();
   this->diameter = fromString<decltype(this->diameter)>(obj[Arrow_Set_Json::diameter].toString());
   this->thickness = fromString<decltype(this->thickness)>(obj[Arrow_Set_Json::thickness].toString());
   this->count = static_cast<uint8_t>(obj[Arrow_Set_Json::count].toInt());
   this->spine = obj[Arrow_Set_Json::spine].toDouble();
   this->length = fromString<decltype(this->length)>(obj[Arrow_Set_Json::length].toString());
   this->weight = fromString<decltype(this->weight)>(obj[Arrow_Set_Json::weight].toString());
   this->color = QColor(obj[Arrow_Set_Json::color].toString());
   QJsonObject fletching_object = obj[Arrow_Set_Json::fletching].toObject();
   QJsonArray fletching_colors = fletching_object[Arrow_Set_Json::Fletching::colors].toArray();
   size_t color_index = 0;
   for(QJsonValue color_value: fletching_colors)
   {
      this->fletching.colors[color_index] = QColor(color_value.toString());
      ++color_index;
   }
   return *this;
}

Arrow_Set const& Arrow_Sets::arrowSet(Arrow_Set_Index asi) const
{
   return this->arrow_sets.at(asi.value());
}

void Arrow_Sets::add(Arrow_Set new_arrow_set)
{
   this->arrow_sets.push_back(std::move(new_arrow_set));
}

void Arrow_Sets::replace(Arrow_Set_Index asi,
                         Arrow_Set upd_arrow_set)
{
   this->arrow_sets[asi.value()] = std::move(upd_arrow_set);
}

void Arrow_Sets::erase(Arrow_Set_Index asi)
{
   this->arrow_sets.erase(this->begin()+asi.value());
}

bool Arrow_Sets::empty() const
{
   return this->arrow_sets.empty();
}

size_t Arrow_Sets::size() const
{
   return this->arrow_sets.size();
}

Arrow_Sets::const_iterator Arrow_Sets::begin() const
{
   return this->arrow_sets.begin();
}

Arrow_Sets::const_iterator Arrow_Sets::end() const
{
   return this->arrow_sets.end();
}

Arrow_Sets::const_iterator Arrow_Sets::cbegin() const
{
   return this->arrow_sets.cbegin();
}

Arrow_Sets::const_iterator Arrow_Sets::cend() const
{
   return this->arrow_sets.cend();
}

namespace
{
   Arrow_Sets& internal_arrow_sets()
   {
      static Arrow_Sets the_arrow_sets;
      return the_arrow_sets;
   }
}

Arrow_Sets const& arrowSets()
{
   return internal_arrow_sets();
}

void addArrowSet(Arrow_Set new_arrow_set)
{
   internal_arrow_sets().add(std::move(new_arrow_set));
}

void replaceArrowSet(Arrow_Set_Index asi,
                     Arrow_Set upd_arrow_set)
{
   internal_arrow_sets().replace(asi, std::move(upd_arrow_set));
}

void eraseArrowSet(Arrow_Set_Index asi)
{
   internal_arrow_sets().erase(asi);
}

void readArrowSets()
{
   internal_arrow_sets() = Arrow_Sets();
   QJsonDocument json = qt::readJsonDocument(QStringLiteral("arrows"));
   QJsonObject root = json.object();
   int const version = root[Arrow_Sets_Json::version].toInt();
   if ( version > 1 )
   {
      qDebug() << "*** Unknown version" << version << "while reading bows";
      return;
   }
   QJsonArray as_array = root[Arrow_Sets_Json::sets].toArray();
   for(QJsonValue val: as_array)
   {
      Arrow_Set new_arrow_set;
      new_arrow_set.fromJson(val.toObject());
      addArrowSet(std::move(new_arrow_set));
   }
}

void writeArrowSets()
{
   QJsonArray as_array;
   for(Arrow_Set const& as: arrowSets())
   {
      as_array.append(as.toJson());
   }
   QJsonObject root;
   root.insert(Arrow_Sets_Json::version, 1);
   root.insert(Arrow_Sets_Json::sets, as_array);
   QJsonDocument json;
   json.setObject(root);
   qt::writeJsonDocument(json, QStringLiteral("arrows"));
}

