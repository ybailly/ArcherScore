
#pragma once

#include "common_types.h"
#include "units_pairs.h"
#include "arrow_size_spec.h"

#include <QtCore/QJsonObject>
#include <QtGui/QColor>

#include <array>
#include <vector>

struct Fletching
{
      std::array<QColor, 3> colors;
}; // struct Fletching

Fletching const& undefinedFletching();

struct Arrow_Set
{
      QString name;
      Diameter_Mm_64thIn diameter;
      Thickness_Mm_1KthIn thickness;
      uint8_t count{0};
      double spine{0.0};
      Length_Cm_In length;
      Weight_G_Oz weight;
      QColor color;
      Fletching fletching;

      QJsonObject toJson() const;
      Arrow_Set& fromJson(QJsonObject obj);
}; // struct Arrow_Set

class Arrow_Sets
{
   public:

      Arrow_Set const& arrowSet(Arrow_Set_Index asi) const;

      void add(Arrow_Set new_arrow_set);
      void replace(Arrow_Set_Index asi,
                   Arrow_Set upd_arrow_set);
      void erase(Arrow_Set_Index asi);

      bool empty() const;
      size_t size() const;

      using Arrow_Sets_Container = std::vector<Arrow_Set>;
      using const_iterator = Arrow_Sets_Container::const_iterator;
      const_iterator begin() const;
      const_iterator end() const;
      const_iterator cbegin() const;
      const_iterator cend() const;

   private:
      Arrow_Sets_Container arrow_sets;
}; // Arrow_Sets

Arrow_Sets const& arrowSets();
void addArrowSet(Arrow_Set new_arrow_set);
void replaceArrowSet(Arrow_Set_Index asi,
                     Arrow_Set upd_arrow_set);
void eraseArrowSet(Arrow_Set_Index asi);

void readArrowSets();
void writeArrowSets();

