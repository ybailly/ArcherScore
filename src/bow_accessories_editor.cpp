
#include "bow_accessories_editor.h"
#include "bows.h"

Bow_Accessories_Editor::Bow_Accessories_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setBackVisible(true);
   this->setTitle(tr("Accessoires de l'arc"));

   this->string_length_controller = new Input_Button_Controller<Length_Cm_In>(this);
   this->string_length_controller->setButton(this->bt_string_length);
   this->string_length_controller->setOverlayMessage(tr("Longueur de base de la corde :"));

   this->string_twists_count_controller = new Input_Button_Controller<int>(this);
   this->string_twists_count_controller->setButton(this->bt_string_twists_count);
   this->string_twists_count_controller->setOverlayMessage(tr("Nombre de vrilles de la corde :"));

   this->string_fibers_count_controller = new Input_Button_Controller<int>(this);
   this->string_fibers_count_controller->setButton(this->bt_string_fibers_count);
   this->string_fibers_count_controller->setOverlayMessage(tr("Nombre de fibres de la corde :"));

   QObject::connect(
            this->bt_have_clicker, &QPushButton::toggled,
            [this](bool on)
   {
      this->bt_have_clicker->setText(on ? tr("Oui") : tr("Non"));
   });
   QObject::connect(
            this->bt_have_berger_button, &QPushButton::toggled,
            [this](bool on)
   {
      this->bt_have_berger_button->setText(on ? tr("Oui") : tr("Non"));
   });
}

void Bow_Accessories_Editor::fillFromBow(Bow const& b)
{
   this->string_length_controller->setValue(b.string_length);
   this->string_twists_count_controller->setValue(b.string_nb_twists);
   this->string_fibers_count_controller->setValue(b.string_nb_fibers);
   this->bt_have_clicker->setChecked(b.has_clicker);
   this->bt_have_berger_button->setChecked(b.has_berger_button);
}

Length_Cm_In Bow_Accessories_Editor::stringLength() const
{
   return this->string_length_controller->value();
}

uint16_t Bow_Accessories_Editor::stringTwistsCount() const
{
   return this->string_twists_count_controller->value();
}

uint16_t Bow_Accessories_Editor::stringFibersCount() const
{
   return this->string_fibers_count_controller->value();
}

bool Bow_Accessories_Editor::haveClicker() const
{
   return this->bt_have_clicker->isChecked();
}

bool Bow_Accessories_Editor::haveBergerButton() const
{
   return this->bt_have_berger_button->isChecked();
}
