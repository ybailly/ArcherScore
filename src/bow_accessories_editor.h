
#pragma once

#include "generic_page.h"
#include "input_button_controller.h"

#include "ui_bow_accessories_editor.h"

class Bow;

class Bow_Accessories_Editor: public Generic_Page, private Ui::Bow_Accessories_Editor
{
      Q_OBJECT
   public:
      explicit Bow_Accessories_Editor(QWidget* parent = nullptr);

      void fillFromBow(Bow const& b);

      Length_Cm_In stringLength() const;
      uint16_t stringTwistsCount() const;
      uint16_t stringFibersCount() const;
      bool haveClicker() const;
      bool haveBergerButton() const;

   private:
      Input_Button_Controller<Length_Cm_In>* string_length_controller{nullptr};
      Input_Button_Controller<int>* string_twists_count_controller{nullptr};
      Input_Button_Controller<int>* string_fibers_count_controller{nullptr};
}; // class Bow_Accessories_Editor
