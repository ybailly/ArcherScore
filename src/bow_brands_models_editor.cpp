
#include "bow_brands_models_editor.h"

Bow_Brands_Models_Editor::Bow_Brands_Models_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setBackVisible(true);
   this->setTitle(tr("Marques et modèles"));
}
