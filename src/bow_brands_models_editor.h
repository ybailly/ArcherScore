
#pragma once

#include "generic_page.h"

#include "ui_bow_brands_models_editor.h"

class Bow_Brands_Models_Editor: public Generic_Page, private Ui::Bow_Brands_Models_Editor
{
      Q_OBJECT
   public:
      explicit Bow_Brands_Models_Editor(QWidget* parent = nullptr);

}; // class Bow_Brands_Models_Editor
