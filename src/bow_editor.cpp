
#include "bow_editor.h"
#include "bow_sight_editor.h"
#include "bow_stabs_editor.h"
#include "bow_accessories_editor.h"
#include "bow_brands_models_editor.h"
#include "common_types_utils.h"
#include "ui_utils.h"

#include "overlay_choose_bow_type.h"

#include <QtDebug>

Bow_Editor::Bow_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setBackVisible(true);
   this->setTitle(tr("Édition d'un arc"));
   {
      struct buttons_controls
      {
            Input_In_Cm_Controller*& controller;
            QPushButton* button;
            QString message;
      };
      buttons_controls ctrl_btns[] = {
         {this->draw_length_controller, this->bt_draw_length, tr("Allonge pour cet arc :")},
         {this->riser_size_controller, this->bt_riser_size, tr("Taille de la poignée :")},
         {this->limbs_length_controller, this->bt_limbs_length, tr("Taille des branches :")},
         {this->top_tiller_length_controller, this->bt_top_tiller_length, tr("Donnez le <i>tiller</i> haut :")},
         {this->middle_tiller_length_controller, this->bt_middle_tiller_length, tr("Donnez le band :")},
         {this->bottom_tiller_length_controller, this->bt_bottom_tiller_length, tr("Donnez le <i>tiller</i> bas :")},
      };
      for(auto& p: ctrl_btns)
      {
         p.controller = new Input_In_Cm_Controller(this);
         p.controller->setButton(p.button);
         p.controller->setOverlayMessage(p.message);
      }
   }
   {
      struct buttons_controls
      {
            Input_Lb_Kg_Controller*& controller;
            QPushButton* button;
            QString message;
      };
      buttons_controls ctrl_btns[] = {
         {this->power_at_draw_length_controller, this->bt_power_at_draw_length, tr("Puissance à l'allonge :")},
         {this->limbs_power_controller, this->bt_limbs_power, tr("Puissance des branches :")}
      };
      for(auto& p: ctrl_btns)
      {
         p.controller = new Input_Lb_Kg_Controller(this);
         p.controller->setButton(p.button);
         p.controller->setOverlayMessage(p.message);
      }
   }

   QObject::connect(
            this->bt_edit_sight, &QPushButton::clicked,
            this, &Bow_Editor::requestEditBowSight);
   QObject::connect(
            this->bt_edit_stabs, &QPushButton::clicked,
            this, &Bow_Editor::requestEditBowStabs);
   QObject::connect(
            this->bt_edit_accessories, &QPushButton::clicked,
            this, &Bow_Editor::requestEditBowAccessories);

   QObject::connect(
            this->bt_type, &QToolButton::clicked,
            [this]()
   {
      Overlay_Choose_Bow_Type* ocbt = new Overlay_Choose_Bow_Type(this->topLevelWidget());
      QObject::connect(
               ocbt, &Overlay_Choose_Bow_Type::bowTypeChoosen,
               [this,ocbt](Bow_Type bow_type)
      {
         this->current_bow.type = bow_type;
         this->updateFromBowType();
         ocbt->discard();
      });
      ocbt->run();
   });

   QObject::connect(
            this->top_tiller_length_controller, &Input_In_Cm_Controller::valueChanged,
            this, &Bow_Editor::checkTillers);
   QObject::connect(
            this->bottom_tiller_length_controller, &Input_In_Cm_Controller::valueChanged,
            this, &Bow_Editor::checkTillers);
}

void Bow_Editor::setCurrentBow(Bow const& b)
{
   this->current_bow = b;
   this->setSubTitle(b.name);
   this->le_name->setText(b.name);
   {
      std::pair<Input_In_Cm_Controller*, Length_Cm_In Bow::*> ctrl_btns[] = {
         {this->draw_length_controller, &Bow::draw_length},
         {this->riser_size_controller, &Bow::riser_size},
         {this->limbs_length_controller, &Bow::limbs_size},
         {this->top_tiller_length_controller, &Bow::tiller_top},
         {this->middle_tiller_length_controller, &Bow::tiller_middle},
         {this->bottom_tiller_length_controller, &Bow::tiller_bottom},
      };
      for(auto& p: ctrl_btns)
      {
         p.first->setValue(b.*(p.second));
      }
   }
   {
      std::pair<Input_Lb_Kg_Controller*, Weight_Kg_Lb Bow::*> ctrl_btns[] = {
         {this->power_at_draw_length_controller, &Bow::power_at_draw},
         {this->limbs_power_controller, &Bow::limbs_power}
      };
      for(auto& p: ctrl_btns)
      {
         p.first->setValue(b.*(p.second));
      }
   }
   this->updateFromBowType();
   this->checkTillers();
}

Bow const& Bow_Editor::currentBow() const
{
   this->current_bow.name = this->le_name->text();
   this->current_bow.power_at_draw = this->power_at_draw_length_controller->value();
   this->current_bow.draw_length = this->draw_length_controller->value();
   this->current_bow.riser_size = this->riser_size_controller->value();
   this->current_bow.limbs_size = this->limbs_length_controller->value();
   this->current_bow.limbs_power = this->limbs_power_controller->value();
   this->current_bow.tiller_top = this->top_tiller_length_controller->value();
   this->current_bow.tiller_middle = this->middle_tiller_length_controller->value();
   this->current_bow.tiller_bottom = this->bottom_tiller_length_controller->value();
   return this->current_bow;
}

void Bow_Editor::updateBowSight(Bow_Sight_Editor const* bse)
{
   this->current_bow.sights = bse->sights();
}

void Bow_Editor::updateBowStabs(Bow_Stabs_Editor const* bse)
{
   this->current_bow.front_stabiliser_weight = bse->frontStabWeight();
   this->current_bow.left_reverse_stabiliser_weight = bse->leftReverseStabWeight();
   this->current_bow.right_reverse_stabiliser_weight = bse->rightReverseStabWeight();
   this->current_bow.top_stabiliser_weight = bse->topStabWeight();
   this->current_bow.bottom_stabiliser_weight = bse->bottomStabWeight();
}

void Bow_Editor::updateBowAccessories(Bow_Accessories_Editor const* bae)
{
   this->current_bow.string_length = bae->stringLength();
   this->current_bow.string_nb_twists = bae->stringTwistsCount();
   this->current_bow.string_nb_fibers = bae->stringFibersCount();
   this->current_bow.has_clicker = bae->haveClicker();
   this->current_bow.has_berger_button = bae->haveBergerButton();
}

void Bow_Editor::updateBowBrandsModels(Bow_Brands_Models_Editor const* bbme)
{

}

void Bow_Editor::updateFromBowType()
{
   this->bt_type->setIcon(iconForBowType(this->current_bow.type));
   this->bt_type->setText(buttonTextForBowType(this->current_bow.type));
   this->bt_edit_sight->setVisible(bowTypeHasSight(this->current_bow.type));
   this->bt_edit_stabs->setVisible(bowTypeHasStabiliser(this->current_bow.type));
}

void Bow_Editor::checkTillers()
{
   Length_Cm_In const top_tiller = this->top_tiller_length_controller->value();
   Length_Cm_In const bot_tiller = this->bottom_tiller_length_controller->value();
   double const diff = std::abs(top_tiller.v2().value()-bot_tiller.v2().value());
   QPalette pal = this->bt_edit_accessories->palette();
   if ( (diff < 0.3) or (diff > 0.5) )
   {
      pal.setBrush(QPalette::Button, QColor(255,160,160));
   }
   this->bt_top_tiller_length->setPalette(pal);
   this->bt_bottom_tiller_length->setPalette(pal);
}
