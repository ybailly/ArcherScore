
#pragma once

#include "bows.h"

#include "generic_page.h"
#include "input_button_controller.h"

#include "ui_bow_editor.h"

class Bow_Sight_Editor;
class Bow_Stabs_Editor;
class Bow_Accessories_Editor;
class Bow_Brands_Models_Editor;

class Bow_Editor: public Generic_Page, private Ui::Bow_Editor
{
      Q_OBJECT
   public:
      explicit Bow_Editor(QWidget* parent = nullptr);

      void setCurrentBow(Bow const& b);
      Bow const& currentBow() const;

      void updateBowSight(Bow_Sight_Editor const* bse);
      void updateBowStabs(Bow_Stabs_Editor const* bse);
      void updateBowAccessories(Bow_Accessories_Editor const* bae);
      void updateBowBrandsModels(Bow_Brands_Models_Editor const* bbme);

   signals:
      void requestEditBowSight();
      void requestEditBowStabs();
      void requestEditBowAccessories();
      void requestEditBowBrandsModels();

   protected:
      void updateFromBowType();
      void checkTillers();

   private:
      mutable Bow current_bow;

      using Input_In_Cm_Controller = Input_Button_Controller<Length_Cm_In>;
      using Input_Lb_Kg_Controller = Input_Button_Controller<Weight_Kg_Lb>;

      Input_In_Cm_Controller* draw_length_controller{nullptr};
      Input_Lb_Kg_Controller* power_at_draw_length_controller{nullptr};
      Input_In_Cm_Controller* riser_size_controller{nullptr};
      Input_In_Cm_Controller* limbs_length_controller{nullptr};
      Input_Lb_Kg_Controller* limbs_power_controller{nullptr};
      Input_In_Cm_Controller* top_tiller_length_controller{nullptr};
      Input_In_Cm_Controller* middle_tiller_length_controller{nullptr};
      Input_In_Cm_Controller* bottom_tiller_length_controller{nullptr};
}; // class Bow_Editor
