
#include "bow_push_button.h"

#include "bows.h"
#include "units_ui.h"
#include "ui_utils.h"

Bow_Push_Button::Bow_Push_Button(QWidget* parent):
   Button_Label(parent)
{
   this->setWordWrap(true);
   this->setSpacing(qRound(0.5*qt::computeEX(this)));
   this->setBowIndex(Invalid_Bow_Index);
}

Bow_Index Bow_Push_Button::bowIndex() const
{
   return this->bow_index;
}

bool Bow_Push_Button::haveBow() const
{
   return this->have_bow;
}

void Bow_Push_Button::setBowIndex(Bow_Index bi)
{
   this->bow_index = bi;
   if ( this->bow_index == Invalid_Bow_Index )
   {
      this->loadLeftSvg(svgResourceForBowType(Bow_Type::Unknown));
      this->setText(tr("Arc<br/>indéterminé..."));
      this->have_bow = false;
   }
   else
   {
      Bow const& bow = bows().bow(this->bow_index);
      this->internalDisplayBow(bow);
      this->have_bow = true;
   }
}

void Bow_Push_Button::displayBow(Bow const& bow)
{
   this->bow_index = Invalid_Bow_Index;
   this->internalDisplayBow(bow);
   this->have_bow = true;
}

void Bow_Push_Button::internalDisplayBow(Bow const& bow)
{
   this->loadLeftSvg(svgResourceForBowType(bow.type));
   if ( bow.name.isEmpty() )
   {
      this->setText(
               QStringLiteral("<b>%1</b><br/>%3 / %4")
               .arg(textForBowType(bow.type))
               .arg(toShortString(bow.draw_length))
               .arg(toShortString(bow.power_at_draw)));
   }
   else
   {
      this->setText(
               QStringLiteral("<b>%1</b><br/>%2 / %3 / %4")
               .arg(bow.name)
               .arg(textForBowType(bow.type))
               .arg(toShortString(bow.draw_length))
               .arg(toShortString(bow.power_at_draw)));
   }
}
