
#pragma once

#include "common_types.h"
#include "button_label.h"

class Bow;

class Bow_Push_Button: public Button_Label
{
      Q_OBJECT
   public:
      explicit Bow_Push_Button(QWidget* parent = nullptr);

      Bow_Index bowIndex() const;
      bool haveBow() const;

   public slots:
      void setBowIndex(Bow_Index bi);
      void displayBow(Bow const& bow);

   protected:
      void internalDisplayBow(Bow const& bow);

   private:
      Bow_Index bow_index{Invalid_Bow_Index};
      bool have_bow{false};

}; // class Bow_Push_Button
