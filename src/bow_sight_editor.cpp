
#include "bow_sight_editor.h"

#include "bows.h"
#include "input_button_controller.h"

using Distance_Controller = Input_Button_Controller<decltype(Sight::for_distance)>;
using Depth_Controller = Input_Button_Controller<decltype(Sight::depth_position)>;
using Vertical_Controller = Input_Button_Controller<decltype(Sight::vertical_position)>;
using Lateral_Controller = Input_Button_Controller<decltype(Sight::lateral_position)>;

Bow_Sight_Editor::Bow_Sight_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setBackVisible(true);
   this->setTitle(tr("Viseur de l'arc"));

   QFont fnt = this->lbl_title_distance->font();
   fnt = qt::adjustFontSize(fnt, 0.8);
   this->lbl_title_distance->setFont(fnt);
   this->lbl_title_depth->setFont(fnt);
   this->lbl_title_lateral->setFont(fnt);
   this->lbl_title_vertical->setFont(fnt);
   this->lbl_title_vertical_eyepiece->setFont(fnt);

   this->svg_title_distance->setSizeBuddy(this->bt_new_sight);
   this->svg_title_distance->setFillOrientation(Qt::Vertical);
   this->svg_title_distance->load(QStringLiteral(":/icons/distance_archer_target"));

   this->svg_title_depth->setSizeBuddy(this->bt_new_sight);
   this->svg_title_depth->setFillOrientation(Qt::Vertical);
   this->svg_title_depth->load(QStringLiteral(":/icons/sight_depth"));

   this->svg_title_lateral->setSizeBuddy(this->bt_new_sight);
   this->svg_title_lateral->setFillOrientation(Qt::Vertical);
   this->svg_title_lateral->load(QStringLiteral(":/icons/sight_lateral"));

   this->svg_title_vertical->setSizeBuddy(this->bt_new_sight);
   this->svg_title_vertical->setFillOrientation(Qt::Vertical);
   this->svg_title_vertical->load(QStringLiteral(":/icons/sight_vertical"));

   this->svg_title_vertical_eyepiece->setSizeBuddy(this->bt_new_sight);
   this->svg_title_vertical_eyepiece->setFillOrientation(Qt::Vertical);
   this->svg_title_vertical_eyepiece->load(QStringLiteral(":/icons/sight_vertical_eyepiece"));

   for(int c = 0; c < 5; ++c)
   {
      this->sights_list_layout->itemAtPosition(0, c)->setAlignment(Qt::AlignCenter);
   }

   QObject::connect(
            this->bt_new_sight, &QPushButton::clicked,
            [this]() { this->addSight({}); });
}

void Bow_Sight_Editor::fillFromBow(Bow const& b)
{
   for(Sight_Buttons& sight_buttons: this->buttons)
   {
      delete sight_buttons.distance;
      delete sight_buttons.depth;
      delete sight_buttons.lateral;
      delete sight_buttons.vertical;
   }
   this->buttons.clear();
   this->controllers.clear();
   for(auto const& sight_pair: b.sights)
   {
      Sight const& sight = sight_pair.second;
      this->addSight(sight);
   }
}

std::map<Distance_M_Yd, Sight> Bow_Sight_Editor::sights() const
{
   std::map<Distance_M_Yd, Sight> map;
   for(Sight_Controllers const& sight_controls: this->controllers)
   {
      Sight s;
      s.for_distance = sight_controls.distance->value();
      s.depth_position = sight_controls.depth->value();
      s.vertical_position = sight_controls.vertical->value();
      s.lateral_position = sight_controls.lateral->value();
      s.vertical_eyepiece_position = sight_controls.vertical_eyepiece->value();
      map[s.for_distance] = s;
   }
   return map;
}

void Bow_Sight_Editor::addSight(Sight const& sight)
{
   int const row = this->sights_list_layout->rowCount();

   Sight_Buttons sight_buttons;
   Sight_Controllers sight_controllers;

   QSizePolicy size_policy(QSizePolicy::Expanding, QSizePolicy::Fixed);
   size_policy.setHorizontalStretch(0);
   size_policy.setVerticalStretch(0);

   sight_buttons.distance = new QPushButton(this->w_sights_list);
   sight_buttons.distance->setSizePolicy(size_policy);
   sight_controllers.distance = new Distance_Controller(sight_buttons.distance);
   sight_controllers.distance->setButton(sight_buttons.distance);
   sight_controllers.distance->setValue(sight.for_distance);
   this->sights_list_layout->addWidget(sight_buttons.distance, row, 0, 1, 3);

   sight_buttons.depth = new QPushButton(this->w_sights_list);
   sight_buttons.depth->setSizePolicy(size_policy);
   sight_controllers.depth = new Depth_Controller(sight_buttons.depth);
   sight_controllers.depth->setButton(sight_buttons.depth);
   sight_controllers.depth->setValue(sight.depth_position);
   this->sights_list_layout->addWidget(sight_buttons.depth, row, 3, 1, 3);

   sight_buttons.lateral = new QPushButton(this->w_sights_list);
   sight_buttons.lateral->setSizePolicy(size_policy);
   sight_controllers.lateral = new Lateral_Controller(sight_buttons.lateral);
   sight_controllers.lateral->setButton(sight_buttons.lateral);
   sight_controllers.lateral->setValue(sight.lateral_position);
   this->sights_list_layout->addWidget(sight_buttons.lateral, row, 6, 1, 3);

   sight_buttons.vertical = new QPushButton(this->w_sights_list);
   sight_buttons.vertical->setSizePolicy(size_policy);
   sight_controllers.vertical = new Vertical_Controller(sight_buttons.vertical);
   sight_controllers.vertical->setButton(sight_buttons.vertical);
   sight_controllers.vertical->setValue(sight.vertical_position);
   this->sights_list_layout->addWidget(sight_buttons.vertical, row, 9, 1, 3);

   sight_buttons.vertical_eyepiece = new QPushButton(this->w_sights_list);
   sight_buttons.vertical_eyepiece->setSizePolicy(size_policy);
   sight_controllers.vertical_eyepiece = new Vertical_Eyepiece_Controller(sight_buttons.vertical_eyepiece);
   sight_controllers.vertical_eyepiece->setButton(sight_buttons.vertical_eyepiece);
   sight_controllers.vertical_eyepiece->setValue(sight.vertical_eyepiece_position);
   this->sights_list_layout->addWidget(sight_buttons.vertical_eyepiece, row, 12, 1, 3);

   this->controllers.push_back(sight_controllers);
   this->buttons.push_back(sight_buttons);
}
