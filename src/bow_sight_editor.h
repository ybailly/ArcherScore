
#pragma once

#include "generic_page.h"
#include "bows.h"
#include "input_button_controller.h"

#include "ui_bow_sight_editor.h"

class Bow_Sight_Editor: public Generic_Page, private Ui::Bow_Sight_Editor
{
      Q_OBJECT
   public:
      explicit Bow_Sight_Editor(QWidget* parent = nullptr);

      void fillFromBow(Bow const& b);

      std::map<Distance_M_Yd, Sight> sights() const;

   protected:
      void addSight(Sight const& s);

   private:
      using Distance_Controller           = Input_Button_Controller<decltype(Sight::for_distance)>;
      using Depth_Controller              = Input_Button_Controller<decltype(Sight::depth_position)>;
      using Lateral_Controller            = Input_Button_Controller<decltype(Sight::lateral_position)>;
      using Vertical_Controller           = Input_Button_Controller<decltype(Sight::vertical_position)>;
      using Vertical_Eyepiece_Controller  = Input_Button_Controller<decltype(Sight::vertical_eyepiece_position)>;

      struct Sight_Controllers
      {
            Distance_Controller* distance{nullptr};
            Depth_Controller* depth{nullptr};
            Lateral_Controller* lateral{nullptr};
            Vertical_Controller* vertical{nullptr};
            Vertical_Eyepiece_Controller* vertical_eyepiece{nullptr};
      };
      struct Sight_Buttons
      {
            QPushButton* distance{nullptr};
            QPushButton* depth{nullptr};
            QPushButton* lateral{nullptr};
            QPushButton* vertical{nullptr};
            QPushButton* vertical_eyepiece{nullptr};
      };

      std::vector<Sight_Controllers> controllers;
      std::vector<Sight_Buttons> buttons;

}; // class Bow_Sight_Editor
