
#include "bow_stabs_editor.h"

#include "bows.h"

Bow_Stabs_Editor::Bow_Stabs_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setBackVisible(true);
   this->setTitle(tr("Stabilisateurs de l'arc"));
   this->background_image->load(QStringLiteral(":/images/bow_stabs"));

   {
      std::pair<Input_Oz_G_Controller*&, QPushButton*> ctrl_btns[] = {
         {this->front_stab_weight_controller, this->bt_front_stab_weight},
         {this->left_rev_stab_weight_controller, this->bt_left_rev_stab_weight},
         {this->right_rev_stab_weight_controller, this->bt_right_rev_stab_weight},
         {this->top_stab_weight_controller, this->bt_top_stab_weight},
         {this->bottom_stab_weight_controller, this->bt_bottom_stab_weight},
      };
      for(auto& p: ctrl_btns)
      {
         p.first = new Input_Oz_G_Controller(this);
         p.first->setButton(p.second);
      }
   }
}

void Bow_Stabs_Editor::fillFromBow(Bow const& b)
{
   std::pair<Input_Oz_G_Controller*, Weight_G_Oz Bow::*> ctrl_btns[] = {
      {this->front_stab_weight_controller, &Bow::front_stabiliser_weight},
      {this->left_rev_stab_weight_controller, &Bow::left_reverse_stabiliser_weight},
      {this->right_rev_stab_weight_controller, &Bow::right_reverse_stabiliser_weight},
      {this->top_stab_weight_controller, &Bow::top_stabiliser_weight},
      {this->bottom_stab_weight_controller, &Bow::bottom_stabiliser_weight},
   };
   for(auto& p: ctrl_btns)
   {
      p.first->setValue(b.*(p.second));
   }
}

Weight_G_Oz Bow_Stabs_Editor::frontStabWeight() const
{
   return this->front_stab_weight_controller->value();
}

Weight_G_Oz Bow_Stabs_Editor::leftReverseStabWeight() const
{
   return this->left_rev_stab_weight_controller->value();
}

Weight_G_Oz Bow_Stabs_Editor::rightReverseStabWeight() const
{
   return this->right_rev_stab_weight_controller->value();
}

Weight_G_Oz Bow_Stabs_Editor::topStabWeight() const
{
   return this->top_stab_weight_controller->value();
}

Weight_G_Oz Bow_Stabs_Editor::bottomStabWeight() const
{
   return this->bottom_stab_weight_controller->value();
}
