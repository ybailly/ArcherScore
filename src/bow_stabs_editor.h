
#pragma once

#include "generic_page.h"

#include "ui_bow_stabs_editor.h"

#include "input_button_controller.h"

class Bow;

class Bow_Stabs_Editor: public Generic_Page, private Ui::Bow_Stabs_Editor
{
      Q_OBJECT
   public:
      explicit Bow_Stabs_Editor(QWidget* parent = nullptr);

      void fillFromBow(Bow const& b);

      Weight_G_Oz frontStabWeight() const;
      Weight_G_Oz leftReverseStabWeight() const;
      Weight_G_Oz rightReverseStabWeight() const;
      Weight_G_Oz topStabWeight() const;
      Weight_G_Oz bottomStabWeight() const;

   private:
      using Input_Oz_G_Controller = Input_Button_Controller<Weight_G_Oz>;
      Input_Oz_G_Controller* front_stab_weight_controller{nullptr};
      Input_Oz_G_Controller* left_rev_stab_weight_controller{nullptr};
      Input_Oz_G_Controller* right_rev_stab_weight_controller{nullptr};
      Input_Oz_G_Controller* top_stab_weight_controller{nullptr};
      Input_Oz_G_Controller* bottom_stab_weight_controller{nullptr};
}; // class Stabs_Editor
