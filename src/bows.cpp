
#include "bows.h"

#include "cpp_utils.h"
#include "units_ui.h"
#include "qt_utils.h"
#include "common_types_utils.h"

#include <QtDebug>
#include <QtCore/QCoreApplication>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QFileInfo>
#include <QtCore/QSaveFile>
#include <QtCore/QDir>
#include <QtCore/QVariant>

namespace
{
   struct Sight_Json
   {
         static QString const distance;
         static QString const depth_position;
         static QString const vertical_position;
         static QString const lateral_position;
         static QString const vertical_eyepiece_position;
   };
   QString const Sight_Json::distance{QStringLiteral("distance")};
   QString const Sight_Json::depth_position{QStringLiteral("depth_position")};
   QString const Sight_Json::vertical_position{QStringLiteral("vertical_position")};
   QString const Sight_Json::lateral_position{QStringLiteral("lateral_position")};
   QString const Sight_Json::vertical_eyepiece_position{QStringLiteral("vertical_eyepiece_position")};
}

QJsonObject Sight::toJson() const
{
   QJsonObject obj;
   obj.insert(Sight_Json::distance, toLongString(this->for_distance));
   obj.insert(Sight_Json::depth_position, this->depth_position);
   obj.insert(Sight_Json::vertical_position, this->vertical_position);
   obj.insert(Sight_Json::lateral_position, this->lateral_position);
   obj.insert(Sight_Json::vertical_eyepiece_position, this->vertical_eyepiece_position);
   return obj;
}

Sight& Sight::fromJson(QJsonObject obj, int version)
{
   if ( version > 1 )
   {
      qDebug() << "*** Unknown verison" << version << "while reading bow sights";
      return *this;
   }
   fillFromString(this->for_distance, obj[Sight_Json::distance].toString());
   this->depth_position = obj[Sight_Json::depth_position].toDouble();
   this->vertical_position = obj[Sight_Json::vertical_position].toDouble();
   this->lateral_position = obj[Sight_Json::lateral_position].toDouble();
   this->vertical_eyepiece_position = obj[Sight_Json::vertical_eyepiece_position].toDouble();
   return *this;
}

namespace
{
   struct Bow_Json
   {
         static QString const version;
         static QString const name;
         static QString const type;
         static QString const power_at_draw;
         static QString const draw_length;
         static QString const tiller_top;
         static QString const tiller_middle;
         static QString const tiller_bottom;
         static QString const riser_size;
         static QString const limbs_size;
         static QString const limbs_power;
         static QString const string_length;
         static QString const string_nb_twists;
         static QString const string_nb_fibers;
         static QString const front_stabiliser_weight;
         static QString const left_reverse_stabiliser_weight;
         static QString const right_reverse_stabiliser_weight;
         static QString const top_stabiliser_weight;
         static QString const bottom_stabiliser_weight;
         static QString const sights;
   };
   QString const Bow_Json::version{QStringLiteral("version")};
   QString const Bow_Json::name{QStringLiteral("name")};
   QString const Bow_Json::type{QStringLiteral("type")};
   QString const Bow_Json::power_at_draw{QStringLiteral("power_at_draw")};
   QString const Bow_Json::draw_length{QStringLiteral("draw_length")};
   QString const Bow_Json::tiller_top{QStringLiteral("tiller_top")};
   QString const Bow_Json::tiller_middle{QStringLiteral("tiller_middle")};
   QString const Bow_Json::tiller_bottom{QStringLiteral("tiller_bottom")};
   QString const Bow_Json::riser_size{QStringLiteral("riser_size")};
   QString const Bow_Json::limbs_size{QStringLiteral("limbs_size")};
   QString const Bow_Json::limbs_power{QStringLiteral("limbs_power")};
   QString const Bow_Json::string_length{QStringLiteral("string_length")};
   QString const Bow_Json::string_nb_twists{QStringLiteral("string_nb_twists")};
   QString const Bow_Json::string_nb_fibers{QStringLiteral("string_nb_fibers")};
   QString const Bow_Json::front_stabiliser_weight{QStringLiteral("front_stabiliser_weight")};
   QString const Bow_Json::left_reverse_stabiliser_weight{QStringLiteral("left_reverse_stabiliser_weight")};
   QString const Bow_Json::right_reverse_stabiliser_weight{QStringLiteral("right_reverse_stabiliser_weight")};
   QString const Bow_Json::top_stabiliser_weight{QStringLiteral("top_stabiliser_weight")};
   QString const Bow_Json::bottom_stabiliser_weight{QStringLiteral("bottom_stabiliser_weight")};
   QString const Bow_Json::sights{QStringLiteral("sights")};
}

QJsonObject Bow::toJson() const
{
   QJsonObject obj;
   obj.insert(Bow_Json::version, 1);
   obj.insert(Bow_Json::name, this->name);
   obj.insert(Bow_Json::type, bowTypeToString(this->type));
   obj.insert(Bow_Json::power_at_draw, toLongString(this->power_at_draw));
   obj.insert(Bow_Json::draw_length, toLongString(this->draw_length));
   obj.insert(Bow_Json::tiller_top, toLongString(this->tiller_top));
   obj.insert(Bow_Json::tiller_middle, toLongString(this->tiller_middle));
   obj.insert(Bow_Json::tiller_bottom, toLongString(this->tiller_bottom));
   obj.insert(Bow_Json::riser_size, toLongString(this->riser_size));
   obj.insert(Bow_Json::limbs_size, toLongString(this->limbs_size));
   obj.insert(Bow_Json::limbs_power, toLongString(this->limbs_power));
   obj.insert(Bow_Json::string_length, toLongString(this->string_length));
   obj.insert(Bow_Json::string_nb_twists, this->string_nb_twists);
   obj.insert(Bow_Json::string_nb_fibers, this->string_nb_fibers);
   obj.insert(Bow_Json::front_stabiliser_weight, toLongString(this->front_stabiliser_weight));
   obj.insert(Bow_Json::left_reverse_stabiliser_weight, toLongString(this->left_reverse_stabiliser_weight));
   obj.insert(Bow_Json::right_reverse_stabiliser_weight, toLongString(this->right_reverse_stabiliser_weight));
   obj.insert(Bow_Json::top_stabiliser_weight, toLongString(this->top_stabiliser_weight));
   obj.insert(Bow_Json::bottom_stabiliser_weight, toLongString(this->bottom_stabiliser_weight));
   if ( not this->sights.empty() )
   {
      QJsonArray sights_array;
      for(auto const& sight_pair: this->sights)
      {
         sights_array.append(sight_pair.second.toJson());
      }
      obj.insert(Bow_Json::sights, sights_array);
   }
   return obj;
}

Bow& Bow::fromJson(QJsonObject obj)
{
   int const version = obj[Bow_Json::version].toInt();
   if ( version > 1 )
   {
      qDebug() << "*** Unknown version" << version << "while reading bows";
      return *this;
   }
   this->name = obj[Bow_Json::name].toString();
   this->type = bowTypeFromString(obj[Bow_Json::type].toString());
   fillFromString(this->power_at_draw, obj[Bow_Json::power_at_draw].toString());
   fillFromString(this->draw_length, obj[Bow_Json::draw_length].toString());
   fillFromString(this->tiller_top, obj[Bow_Json::tiller_top].toString());
   fillFromString(this->tiller_middle, obj[Bow_Json::tiller_middle].toString());
   fillFromString(this->tiller_bottom, obj[Bow_Json::tiller_bottom].toString());
   fillFromString(this->riser_size, obj[Bow_Json::riser_size].toString());
   fillFromString(this->limbs_size, obj[Bow_Json::limbs_size].toString());
   fillFromString(this->limbs_power, obj[Bow_Json::limbs_power].toString());
   fillFromString(this->string_length, obj[Bow_Json::string_length].toString());
   this->string_nb_twists = obj[Bow_Json::string_nb_twists].toVariant().toUInt();
   this->string_nb_fibers = obj[Bow_Json::string_nb_fibers].toVariant().toUInt();
   fillFromString(this->front_stabiliser_weight, obj[Bow_Json::front_stabiliser_weight].toString());
   fillFromString(this->left_reverse_stabiliser_weight, obj[Bow_Json::left_reverse_stabiliser_weight].toString());
   fillFromString(this->right_reverse_stabiliser_weight, obj[Bow_Json::right_reverse_stabiliser_weight].toString());
   fillFromString(this->top_stabiliser_weight, obj[Bow_Json::top_stabiliser_weight].toString());
   fillFromString(this->bottom_stabiliser_weight, obj[Bow_Json::bottom_stabiliser_weight].toString());
   QJsonArray sights_array = obj[Bow_Json::sights].toArray();
   if ( not sights_array.isEmpty() )
   {
      this->sights.clear();
      for(QJsonValue const& sight_val: sights_array)
      {
         Sight new_sight;
         new_sight.fromJson(sight_val.toObject(), version);
         this->sights[new_sight.for_distance] = std::move(new_sight);
      }
   }
   return *this;
}

Bow const& Bows::bow(Bow_Index bi) const
{
   return this->bows_[bi.value()];
}

void Bows::add(Bow new_bow)
{
   this->bows_.push_back(std::move(new_bow));
}

void Bows::replace(Bow_Index bi,
                      Bow upd_bow)
{
   this->bows_[bi.value()] = std::move(upd_bow);
}

void Bows::erase(Bow_Index bi)
{
   this->bows_.erase(this->begin()+bi.value());
}

bool Bows::empty() const
{
   return this->bows_.empty();
}

size_t Bows::size() const
{
   return this->bows_.size();
}

void Bows::sort()
{
   std::sort(
            this->bows_.begin(),
            this->bows_.end(),
            [](Bow const& b1, Bow const& b2)
   { return QString::compare(b1.name, b2.name, Qt::CaseInsensitive) < 0; });
}

Bows::const_iterator Bows::begin() const
{
   return this->bows_.cbegin();
}

Bows::const_iterator Bows::end() const
{
   return this->bows_.cend();
}

Bows::const_iterator Bows::cbegin() const
{
   return this->bows_.cbegin();
}

Bows::const_iterator Bows::cend() const
{
   return this->bows_.cend();
}

namespace
{
   Bows& internal_bows()
   {
      static Bows the_bows;
      return the_bows;
   }
}

Bows const& bows()
{
   return internal_bows();
}

void addBow(Bow new_bow)
{
   internal_bows().add(std::move(new_bow));
}

void replaceBow(Bow_Index bi,
                Bow upd_bow)
{
   if ( bi == Invalid_Bow_Index )
      return;
   internal_bows().replace(bi, std::move(upd_bow));
}

void eraseBow(Bow_Index bi)
{
   if ( bi == Invalid_Bow_Index )
      return;
   internal_bows().erase(bi);
}

void readBows()
{
   internal_bows() = Bows();
   QJsonDocument json = qt::readJsonDocument(QStringLiteral("bows"));
   QJsonObject root = json.object();
   int const version = root[QStringLiteral("version")].toInt();
   if ( version > 1 )
   {
      qDebug() << "*** Unknown version" << version << "while reading bows";
      return;
   }
   QJsonArray bows_array = root[QStringLiteral("bows")].toArray();
   for(auto const& val: bows_array)
   {
      Bow new_bow;
      new_bow.fromJson(val.toObject());
      addBow(std::move(new_bow));
   }
   internal_bows().sort();
}

void writeBows()
{
   QJsonArray bows_array;
   for(Bow const& bow: bows())
   {
      bows_array.append(bow.toJson());
   }
   QJsonObject root;
   root.insert(QStringLiteral("version"), 1);
   root.insert(QStringLiteral("bows"), bows_array);
   QJsonDocument json;
   json.setObject(root);
   qt::writeJsonDocument(json, QStringLiteral("bows"));
}
