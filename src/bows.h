
#pragma once

#include "common_types.h"
#include "units_pairs.h"

#include <QtCore/QString>

#include <vector>
#include <map>

struct Brand_Model
{
      QString brand;
      QString model;
}; // struct Brand_Model

struct Sight
{
      Sight() = default;
      Sight(Sight const&) = default;
      Sight(Sight&&) = default;
      Sight& operator=(Sight const&) = default;
      Sight& operator=(Sight&&) = default;

      Distance_M_Yd for_distance;
      double depth_position{0.0};
      double vertical_position{0.0};
      double lateral_position{0.0};
      double vertical_eyepiece_position{0.0};

      QJsonObject toJson() const;
      Sight& fromJson(QJsonObject obj, int version);
}; // struct Sight

struct Bow
{
      Bow() = default;
      Bow(Bow const&) = default;
      Bow(Bow&&) = default;
      Bow& operator=(Bow const&) = default;
      Bow& operator=(Bow&&) = default;

      // main data
      QString name;
      Bow_Type type{Bow_Type::Unknown};
      Weight_Kg_Lb power_at_draw;
      Length_Cm_In draw_length;
      Length_Cm_In riser_size;
      Length_Cm_In limbs_size;
      Weight_Kg_Lb limbs_power;
      Length_Cm_In tiller_top;
      Length_Cm_In tiller_middle;
      Length_Cm_In tiller_bottom;
      // accessories
      Length_Cm_In string_length;
      uint16_t string_nb_twists{0};
      uint16_t string_nb_fibers{1};
      bool has_clicker{true};
      bool has_berger_button{true};
      // stabilisers
      Weight_G_Oz front_stabiliser_weight;
      Weight_G_Oz left_reverse_stabiliser_weight;
      Weight_G_Oz right_reverse_stabiliser_weight;
      Weight_G_Oz top_stabiliser_weight;
      Weight_G_Oz bottom_stabiliser_weight;

      struct
      {
            Brand_Model riser;
            Brand_Model limbs;
            Brand_Model string;
            Brand_Model front_stabiliser;
            Brand_Model left_reverse_stabiliser;
            Brand_Model right_reverse_stabiliser;
      } brands_models;

      std::map<Distance_M_Yd, Sight> sights;

      QJsonObject toJson() const;
      Bow& fromJson(QJsonObject obj);

}; // struct Bow

class Bows
{
   public:

      Bow const& bow(Bow_Index bi) const;
      void add(Bow new_bow);
      void replace(Bow_Index bi,
                      Bow upd_bow);
      void erase(Bow_Index bi);

      bool empty() const;
      size_t size() const;
      void sort();

      using Bows_Container = std::vector<Bow>;
      using const_iterator = Bows_Container::const_iterator;
      const_iterator begin() const;
      const_iterator end() const;
      const_iterator cbegin() const;
      const_iterator cend() const;

   private:
      Bows_Container bows_;
}; // class Bows

Bows const& bows();
void addBow(Bow new_bow);
void replaceBow(Bow_Index bi,
                Bow upd_bow);
void eraseBow(Bow_Index bi);

void readBows();
void writeBows();
