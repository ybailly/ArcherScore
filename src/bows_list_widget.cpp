
#include "bows_list_widget.h"
#include "units_ui.h"
#include "button_label.h"
#include "overlay_message.h"
#include "bow_push_button.h"
#include "ui_utils.h"

#include <QtDebug>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>

Bows_List_Widget::Bows_List_Widget(QWidget* parent):
   QWidget(parent)
{
   QVBoxLayout* vbox = new QVBoxLayout(this);
   vbox->setMargin(0);
   vbox->setSpacing(2);
   this->setLayout(vbox);

   this->scroll = new QScrollArea(this);
   this->scroll->setWidgetResizable(true);
   this->scroll->setAlignment(Qt::AlignTop bitor Qt::AlignHCenter);
   this->scroll->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   vbox->addWidget(this->scroll);

   Button_Label* bt_new = new Button_Label(this);
   bt_new->setText(tr("Nouvel arc..."));
   bt_new->loadLeftSvg(QStringLiteral(":/icons/add_bow"));
   bt_new->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
   bt_new->setSpacing(qt::computeEM(this));
   bt_new->setHorizontalMargin(qt::computeEM(this));
   QObject::connect(
            bt_new, &Button_Label::clicked,
            this, &Bows_List_Widget::requestNewBow);
   vbox->addWidget(bt_new);
}

void Bows_List_Widget::refreshBowList()
{
   this->scroll->setWidget(nullptr);
   QWidget* w = new QWidget(this->scroll);
   this->scroll->setWidget(w);

   if ( bows().empty() )
   {
      return;
   }

   QGridLayout* grid = new QGridLayout(w);
   grid->setMargin(2);
   grid->setHorizontalSpacing(2);
   grid->setVerticalSpacing(4);
   w->setLayout(grid);

   int row = 0;
   for(Bow const& b: bows())
   {
      Bow_Push_Button* bow_button = new Bow_Push_Button(w);
      bow_button->setBowIndex(Bow_Index(row));
      bow_button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
      QObject::connect(
               bow_button, &Button_Label::clicked,
               [this,row]()
      {
         emit this->selectedBow(Bow_Index(row));
      });
      grid->addWidget(bow_button, row, 0);
      //
      Button_Label* bt_delete = new Button_Label(w);
      bt_delete->loadLeftSvg(QString(":/icons/delete_arc"));
      bt_delete->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
      bt_delete->setHorizontalMargin(qRound(0.5*qt::computeEX(this)));
      QObject::connect(
               bt_delete, &Button_Label::clicked,
               [this,row]()
      {
         this->onRequestDeleteBow(Bow_Index(row));
      });
      grid->addWidget(bt_delete, row, 1);
      //
      ++row;
   }
   grid->setRowStretch(row, 1);
}

void Bows_List_Widget::onRequestDeleteBow(Bow_Index bi)
{
   if ( bi == Invalid_Bow_Index )
   {
      return;
   }
   Overlay_Message* msg = new Overlay_Message(this);
   Bow const& bow = bows().bow(bi);
   QString const& bow_name =
         bow.name.isEmpty() ?
            textForBowType(bow.type) :
            bow.name;
   msg->setQuestion(
            tr("Suppression d'un arc"),
            tr("Voulez-vous vraiment supprimer l'arc <b>%1</b> ?<br/>"
               "Cette opération est définitive.")
            .arg(bow_name));
   QObject::connect(
            msg, &Overlay_Message::clickedButton,
            [this,msg,bi](Overlay_Message::Button button)
   {
      if ( button == Overlay_Message::Button::Yes )
      {
         eraseBow(bi);
         this->refreshBowList();
      }
      msg->discard();
   });
   msg->run();
}
