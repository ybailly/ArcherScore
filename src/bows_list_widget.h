
#pragma once

#include <QtWidgets/QWidget>

#include "bows.h"

class QScrollArea;

class Bows_List_Widget: public QWidget
{
      Q_OBJECT
   public:
      explicit Bows_List_Widget(QWidget* parent = nullptr);

   public slots:
      void refreshBowList();

   signals:
      void requestNewBow();
      void selectedBow(Bow_Index bi);

   protected:
      void onRequestDeleteBow(Bow_Index bi);

   private:
      QScrollArea* scroll{nullptr};

}; // class Bows_List_Widget
