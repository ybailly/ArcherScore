
#include "button_label.h"
#include "keep_ratio_svg_widget.h"
#include "qt_utils.h"

#include <QtDebug>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QEvent>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QLabel>

Button_Label::Button_Label(QWidget* parent):
   QPushButton(parent)
{
   QHBoxLayout* hbox = new QHBoxLayout(this);
   hbox->setMargin(qRound(0.5*qt::computeEM(this)));
   hbox->setSpacing(0);
   this->setLayout(hbox);

   this->left_svg = new Keep_Ratio_Svg_Widget(this);
   hbox->addWidget(this->left_svg);
   this->left_svg->hide();

   this->label_ = new QLabel(this);
   this->label_->setFixedSize(
            0,
            this->label_->minimumSizeHint().height());
   hbox->addWidget(this->label_);
   this->label_->hide();

   this->left_svg->setSizeBuddy(this->label_);
   this->left_svg->setFillOrientation(Qt::Vertical);

   this->right_svg = new Keep_Ratio_Svg_Widget(this);
   hbox->addWidget(this->right_svg);
   this->right_svg->hide();

   this->spacing = qRound(0.5*qt::computeEX(this));

   this->label_->installEventFilter(this);
}

void Button_Label::setText(QString const& txt)
{
   this->label_->setText(txt);
   if ( txt.isEmpty() )
   {
      this->label_->setFixedSize(
               0,
               this->label_->minimumSizeHint().height());
      this->layout()->setSpacing(0);
      this->label_->hide();
   }
   else
   {
      this->label_->show();
      this->label_->setFixedSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
      this->layout()->setSpacing(this->spacing);
   }
}

void Button_Label::setWordWrap(bool ww)
{
   this->label_->setWordWrap(ww);
}

void Button_Label::setTextAlignment(Qt::Alignment a)
{
   this->label_->setAlignment(a);
}

void Button_Label::setFont(QFont const& fnt)
{
   this->label_->setFont(fnt);
}

void Button_Label::loadLeftSvg(QString const& s)
{
   if ( not s.isEmpty() )
      this->left_svg->load(s);
   this->left_svg->setVisible(not s.isEmpty());
}

void Button_Label::loadRightSvg(QString const& s)
{
   if ( not s.isEmpty() )
      this->right_svg->load(s);
   this->right_svg->setVisible(not s.isEmpty());
}

void Button_Label::loadLeftSvg(QByteArray const& ba)
{
   if ( not ba.isEmpty() )
      this->left_svg->load(ba);
   this->left_svg->setVisible(not ba.isEmpty());
}

void Button_Label::loadRightSvg(QByteArray const& ba)
{
   if ( not ba.isEmpty() )
      this->right_svg->load(ba);
   this->right_svg->setVisible(not ba.isEmpty());
}

void Button_Label::setSpacing(int s)
{
   this->spacing = s;
   if ( this->label_->text().isEmpty() )
      this->layout()->setSpacing(0);
   else
      this->layout()->setSpacing(this->spacing);
}

void Button_Label::setHorizontalMargin(int s)
{
   QMargins m = this->layout()->contentsMargins();
   m.setLeft(s);
   m.setRight(s);
   this->layout()->setContentsMargins(m);
}

void Button_Label::setVerticalMargin(int s)
{
   QMargins m = this->layout()->contentsMargins();
   m.setTop(s);
   m.setBottom(s);
   this->layout()->setContentsMargins(m);
}

QSize Button_Label::sizeHint() const
{
   return this->layout()->sizeHint();
}

QSize Button_Label::minimumSizeHint() const
{
   int min_width = 0;
   if ( this->left_svg->isVisible() ) min_width += 2*this->left_svg->size().width();
   if ( this->right_svg->isVisible() ) min_width += 2*this->right_svg->size().width();
   QSize const msh = this->QPushButton::minimumSizeHint();
   int const extra = this->layout()->contentsMargins().left() +
                     this->layout()->contentsMargins().right() +
                     (this->label_->text().isEmpty() ? 0 : this->layout()->spacing());
   if ( min_width > 0 )
   {
      return QSize(min_width+extra, msh.height());
   }
   else
   {
      return QSize(this->label_->minimumSizeHint().width()+extra, msh.height());
   }
}

bool Button_Label::eventFilter(QObject* w, QEvent* e)
{
   QMouseEvent* qme = dynamic_cast<QMouseEvent*>(e);
   if ( qme == nullptr )
   {
      return false;
   }
   w->event(e);
   e->setAccepted(false);
   return true;
}
