
#pragma once

#include <QtWidgets/QPushButton>

class QLabel;
class Keep_Ratio_Svg_Widget;

class Button_Label: public QPushButton
{
      Q_OBJECT
   public:
      explicit Button_Label(QWidget* parent = nullptr);

      void setText(QString const& txt);
      void setWordWrap(bool ww);
      void setTextAlignment(Qt::Alignment a);
      void setFont(QFont const& fnt);

      void loadLeftSvg(QString const& s);
      void loadRightSvg(QString const& s);
      void loadLeftSvg(QByteArray const& ba);
      void loadRightSvg(QByteArray const& ba);
      void setSpacing(int s);
      void setHorizontalMargin(int s);
      void setVerticalMargin(int s);

      virtual QSize sizeHint() const override;
      virtual QSize minimumSizeHint() const override;
      virtual bool eventFilter(QObject* w, QEvent* e) override;

   private:
      QLabel* label_{nullptr};
      Keep_Ratio_Svg_Widget* left_svg{nullptr};
      Keep_Ratio_Svg_Widget* right_svg{nullptr};
      int spacing{0};
}; // class Button_Label
