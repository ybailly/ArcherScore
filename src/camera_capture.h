
#pragma once

#include <QtGui/QImage>
#include <QtMultimedia/QAbstractVideoSurface>
#include <QtMultimedia/QVideoProbe>
#include <QtMultimedia/QCameraInfo>
#include <QtMultimedia/QCamera>

class Camera_Capture: public QAbstractVideoSurface
{
      Q_OBJECT
   public:
      explicit Camera_Capture(QObject* parent = nullptr);

      void setSource(QCamera* cam);

      virtual QList<QVideoFrame::PixelFormat>
      supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const override;

      virtual bool present(QVideoFrame const& frame) override;

      QImage const& image() const;

   signals:
      void imageAvailable();

   private:
      QImage image_;
      QVideoProbe probe;
      QCameraInfo camera_info;
      QCamera* camera{nullptr};

}; // class Camera_Capture
