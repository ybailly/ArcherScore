
#include "camera_target.h"
#include "camera_capture.h"
#include "single_end.h"
#include "ends_round.h"
#include "session.h"

#include <QtWidgets/QGraphicsScene>
#include <QtMultimedia/QCamera>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QtMultimedia/QCameraExposure>
#include <QtMultimedia/QCameraImageProcessing>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtGui/QBackingStore>
#include <QtGui/QScreen>
#include <QtCore/QTime>

Camera_Target::Camera_Target(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);

   this->gv_target_camera->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   this->gv_target_camera->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

   QGraphicsScene* scene = new QGraphicsScene(this->gv_target_camera);
   scene->setBackgroundBrush(QBrush(QColor(Qt::black)));
   // group for rings items
   this->rings_group = new QGraphicsItemGroup;
   this->rings_group->setZValue(1.0);
   this->rings_group->setVisible(true);
   scene->addItem(this->rings_group);
   // group for axis items
   this->axis_group = new QGraphicsItemGroup;
   this->axis_group->setZValue(1.0);
   this->axis_group->setVisible(false);
   scene->addItem(this->axis_group);
   // group for distorsion (control points)
   this->distorsion_group = new QGraphicsItemGroup;
   this->distorsion_group->setZValue(1.0);
   this->distorsion_group->setVisible(true);
   scene->addItem(this->distorsion_group);
   // group for distorsion (frame)
   this->distorsion_frame_group = new QGraphicsItemGroup;
   this->distorsion_frame_group->setZValue(1.0);
   this->distorsion_frame_group->setVisible(true);
   scene->addItem(this->distorsion_frame_group);
   //
   this->gv_target_camera->setScene(scene);
   std::fill(this->rings_for_mark.begin(), this->rings_for_mark.end(), nullptr);
   this->hit_buttons.resize(1);
   this->hit_buttons[0] = this->bt_hit_0;
   QObject::connect(
            this->bt_hit_0, &QPushButton::clicked,
            this, &Camera_Target::hitButtonClicked);

   QObject::connect(
            this->bt_distort_decrease, &QPushButton::clicked,
            [this]()
   {
      this->distorsion_transform =
            QTransform(1.0, 0.0, 0.0,
                       0.0, 1.0, this->distorsion_transform.m23()-0.005,
                       0.0, 0.0, 1.0);
      this->applyTransform();
   });
   QObject::connect(
            this->bt_distort_increase, &QPushButton::clicked,
            [this]()
   {
      this->distorsion_transform =
            QTransform(1.0, 0.0, 0.0,
                       0.0, 1.0, this->distorsion_transform.m23()+0.005,
                       0.0, 0.0, 1.0);
      this->applyTransform();
   });
   QObject::connect(
            this->bt_zoom_decrease, &QPushButton::clicked,
            [this]()
   {
      double const new_scale = this->zoom_transform.m11()-0.05;
      this->zoom_transform = QTransform::fromScale(new_scale, new_scale);
      this->applyTransform();
   });
   QObject::connect(
            this->bt_zoom_increase, &QPushButton::clicked,
            [this]()
   {
      double const new_scale = this->zoom_transform.m11()+0.05;
      this->zoom_transform = QTransform::fromScale(new_scale, new_scale);
      this->applyTransform();
   });
   QObject::connect(
            this->bt_tx_decrease, &QPushButton::clicked,
            [this]()
   {
      this->translate_transform =
            QTransform::fromTranslate(
               this->translate_transform.dx()-0.1,
               this->translate_transform.dy());
      this->applyTransform();
   });
   QObject::connect(
            this->bt_tx_increase, &QPushButton::clicked,
            [this]()
   {
      this->translate_transform =
            QTransform::fromTranslate(
               this->translate_transform.dx()+0.1,
               this->translate_transform.dy());
      this->applyTransform();
   });
   QObject::connect(
            this->bt_ty_decrease, &QPushButton::clicked,
            [this]()
   {
      this->translate_transform =
            QTransform::fromTranslate(
               this->translate_transform.dx(),
               this->translate_transform.dy()-0.1);
      this->applyTransform();
   });
   QObject::connect(
            this->bt_ty_increase, &QPushButton::clicked,
            [this]()
   {
      this->translate_transform =
            QTransform::fromTranslate(
               this->translate_transform.dx(),
               this->translate_transform.dy()+0.1);
      this->applyTransform();
   });

   QObject::connect(
            this->bt_fix, &QPushButton::clicked,
            [this]()
   {
      this->camera->stop();
      this->rings_group->setVisible(true);
      this->axis_group->setVisible(false);
      this->distorsion_group->setVisible(true);
      this->distorsion_frame_group->setVisible(true);
      this->applyDistorsion();
      this->functions_stack->setCurrentWidget(this->adjust_image_page);
   });
   QObject::connect(
            this->bt_capture, &QPushButton::clicked,
            [this]()
   {
      this->camera->start();
      this->rings_group->setVisible(false);
      this->axis_group->setVisible(false);
      this->distorsion_group->setVisible(false);
      this->distorsion_frame_group->setTransform(QTransform());
      this->distorsion_frame_group->setVisible(true);
      this->functions_stack->setCurrentWidget(this->center_image_page);
   });
   QObject::connect(
            this->bt_adjust_ok, &QPushButton::clicked,
            [this]()
   {
      this->camera->stop();
      this->rings_group->setVisible(true);
      this->axis_group->setVisible(false);
      this->distorsion_group->setVisible(false);
      this->distorsion_frame_group->setVisible(false);
      this->functions_stack->setCurrentWidget(this->edit_page);
   });
   scene->installEventFilter(this);
}

void Camera_Target::setEnd(Session const* sess,
                           Round_Index round_idx,
                           End_Index end_idx)
{
   this->session_ = sess;
   this->round_index = round_idx;
   this->end_index = end_idx;
   if ( (this->session_ == nullptr) or
        (this->round_index == Invalid_Round_Index) or
        (this->end_index == Invalid_End_Index) )
   {
      this->hit_locs.clear();
      this->session_ = nullptr;
      this->round = nullptr;
      this->end = nullptr;
      this->round_index = Invalid_Round_Index;
      this->end_index = Invalid_End_Index;
      this->rings_group->setVisible(false);
      this->axis_group->setVisible(false);
      this->distorsion_group->setVisible(false);
      this->distorsion_frame_group->setVisible(false);
      return;
   }
   this->round = &(this->session_->endsRound(this->round_index));
   this->end = &(this->round->singleEnd(end_idx));
   this->hit_locs.resize(this->end->nbShots());
   std::copy(this->end->begin(), this->end->end(), this->hit_locs.begin());

   // creating rings items
   QGraphicsScene* scene = this->gv_target_camera->scene();
   QPen green_pen(QColor(64,192,64), 0.005);
   for(Mark m = 0; m < this->session_->targetMinimumMark(); ++m)
   {
      if ( this->rings_for_mark[m] != nullptr )
      {
         this->rings_for_mark[m]->setVisible(false);
      }
   }
   for(Mark m = this->session_->targetMinimumMark(); m < 11; ++m)
   {
      if ( this->rings_for_mark[m] == nullptr )
      {
         double const ref_radius = static_cast<double>(11-m)*0.1;
         this->rings_for_mark[m] =
               new QGraphicsEllipseItem(
                  -ref_radius, -ref_radius,
                  2.0*ref_radius, 2.0*ref_radius,
                  this->rings_group);
         this->rings_for_mark[m]->setPen(green_pen);
         this->rings_for_mark[m]->setBrush(Qt::NoBrush);
      }
      this->rings_for_mark[m]->setVisible(true);
   }
   if ( this->ring_10plus == nullptr )
   {
      this->ring_10plus = new QGraphicsEllipseItem(-0.05, -0.05, 0.1, 0.1, this->rings_group);
      this->ring_10plus->setPen(QPen(QColor(64,255,64), 0.003));
      this->ring_10plus->setBrush(Qt::NoBrush);
      this->ring_10plus->setVisible(true);
   }
   double const max_radius = static_cast<double>(11-this->session_->targetMinimumMark())*0.1;
   QGraphicsRectItem* rect_rings =
         new QGraphicsRectItem(
            -max_radius, -max_radius,
            2.0*max_radius, 2.0*max_radius,
            this->rings_group);
   rect_rings->setPen(green_pen);
   rect_rings->setVisible(true);

   // creating axis items
   if ( this->h_line == nullptr )
   {
      this->h_line = new QGraphicsLineItem(-max_radius, 0.0, max_radius, 0.0, this->axis_group);
      this->h_line->setPen(green_pen);
      this->h_line->setVisible(true);
   }
   else
   {
      this->h_line->setLine(-max_radius, 0.0, max_radius, 0.0);
   }
   if ( this->v_line == nullptr )
   {
      this->v_line = new QGraphicsLineItem(0.0, -max_radius, 0.0, max_radius, this->axis_group);
      this->v_line->setPen(green_pen);
      this->v_line->setVisible(true);
   }
   else
   {
      this->v_line->setLine(0.0, -max_radius, 0.0, max_radius);
   }
   QGraphicsRectItem* rect_axis =
         new QGraphicsRectItem(
            -max_radius, -max_radius,
            2.0*max_radius, 2.0*max_radius,
            this->axis_group);
   rect_axis->setPen(green_pen);
   rect_axis->setVisible(true);

   // creating distorsion frame items
   if ( this->distorsion_points[0] == nullptr )
   {
      int point_index = 0;
      for(QGraphicsEllipseItem*& distorsion_point: this->distorsion_points)
      {
         distorsion_point = new QGraphicsEllipseItem(-0.075, -0.075, 0.15, 0.15, this->distorsion_group);
         distorsion_point->setPen(green_pen);
         distorsion_point->setBrush(QColor(64,255,64,128));
         distorsion_point->setVisible(true);
         distorsion_point->setZValue(1.0);
         distorsion_point->setData(0, point_index);
         ++point_index;
      }
      this->distorsion_frame_rect = new QGraphicsRectItem(this->distorsion_frame_group);
      this->distorsion_frame_rect->setPen(green_pen);
      this->distorsion_frame_rect->setBrush(Qt::NoBrush);
      this->distorsion_frame_rect->setVisible(true);
      this->distorsion_frame_h_line = new QGraphicsLineItem(this->distorsion_frame_group);
      this->distorsion_frame_h_line->setPen(green_pen);
      this->distorsion_frame_h_line->setVisible(true);
      this->distorsion_frame_v_line = new QGraphicsLineItem(this->distorsion_frame_group);
      this->distorsion_frame_v_line->setPen(green_pen);
      this->distorsion_frame_v_line->setVisible(true);
   }
   this->distorsion_points[0]->setPos(0.0,0.0); // center
   this->distorsion_points[1]->setPos(-max_radius, -max_radius); // top left
   this->distorsion_points[2]->setPos( max_radius, -max_radius); // top right
   this->distorsion_points[3]->setPos( max_radius,  max_radius); // bottom right
   this->distorsion_points[4]->setPos(-max_radius,  max_radius); // bottom left
   this->distorsion_points[5]->setPos(0.0,  -max_radius); // top
   this->distorsion_points[6]->setPos(max_radius,  0.0); // right
   this->distorsion_points[7]->setPos(0.0,  max_radius); // bottom
   this->distorsion_points[8]->setPos(-max_radius,  0.0); // left
   this->distorsion_frame_rect->setRect(-max_radius, -max_radius, 2.0*max_radius, 2.0*max_radius);
   this->distorsion_frame_h_line->setLine(-max_radius, 0.0, max_radius, 0.0);
   this->distorsion_frame_v_line->setLine(0.0, -max_radius, 0.0, max_radius);
   this->distorsion_frame_group->setTransform(QTransform());
   this->distorsion_group->setPos(0.0,0.0);

   // finishing scene
   scene->setSceneRect(-max_radius-0.15, -max_radius-0.15, 2.0*max_radius+0.3, 2.0*max_radius+0.3);
   this->gv_target_camera->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
   // hits buttons
   std::for_each(this->hit_buttons.begin(),
                 this->hit_buttons.end(),
                 [](QPushButton* bt) { bt->hide(); });
   this->hit_buttons.resize(end->nbShots(), nullptr);
   for(uint8_t hit_idx = 0; hit_idx < end->nbShots(); ++hit_idx)
   {
      QPushButton*& hit_button = this->hit_buttons[hit_idx];
      if ( hit_button == nullptr )
      {
         hit_button = new QPushButton(this->w_hit_buttons);
         hit_button->setAutoFillBackground(true);
         hit_button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
         hit_button->setProperty("hit_index", hit_idx);
         this->w_hit_buttons->layout()->addWidget(hit_button);
         QObject::connect(
                  hit_button, &QPushButton::clicked,
                  this, &Camera_Target::hitButtonClicked);
      }
      this->updateHitButton(hit_button, end->score(Score_Index(hit_idx)));
      hit_button->show();
   }
   // resetting transforms
   this->distorsion_transform = QTransform();
   this->zoom_transform = QTransform();
   this->translate_transform = QTransform();
   this->applyTransform();
}

void Camera_Target::slidingStart()
{
   if ( this->camera != nullptr )
   {
      this->stopCamera();
   }
}

void Camera_Target::slidingEnd()
{
   if ( this->isVisible() )
   {
      if ( this->camera == nullptr )
      {
         this->camera = new QCamera(QCamera::BackFace, this);
         QObject::connect(
                  this->camera, &QCamera::stateChanged,
                  this, &Camera_Target::cameraStateChanged);
         this->camera->load();
      }
      else
      {
         this->camera->start();
      }
      this->gv_target_camera->fitInView(this->gv_target_camera->sceneRect(), Qt::KeepAspectRatio);
      this->functions_stack->setCurrentWidget(this->center_image_page);
      this->distorsion_transform = QTransform();
      this->zoom_transform = QTransform();
      this->translate_transform = QTransform();
      this->applyTransform();
      this->rings_group->setVisible(false);
      this->axis_group->setVisible(false);
      this->distorsion_group->setVisible(false);
      this->distorsion_frame_group->setVisible(true);
   }
}

bool Camera_Target::eventFilter(QObject* obj, QEvent* qe)
{
   QGraphicsScene* scene = dynamic_cast<QGraphicsScene*>(obj);
   if ( scene != this->gv_target_camera->scene() )
   {
      return false;
   }
   QGraphicsSceneMouseEvent* qgsme = dynamic_cast<QGraphicsSceneMouseEvent*>(qe);
   if ( qgsme == nullptr )
   {
      return false;
   }
   if ( (qgsme->button() == Qt::NoButton) and
        (qgsme->buttons() != Qt::NoButton) )
   {
      // mouse move
      if ( this->moving_distorsion_item != nullptr )
      {
         if ( this->moving_distorsion_item == this->distorsion_points[0] )
         {
            // we're moving the center: moving everything
            this->distorsion_group->setPos(qgsme->scenePos()+this->mouse_offset);
         }
         else
         {
            int const point_index = this->moving_distorsion_item->data(0).toInt();
            QPointF const old_pos = this->moving_distorsion_item->scenePos() - this->distorsion_group->scenePos();
            QPointF const new_pos = qgsme->scenePos()+this->mouse_offset - this->distorsion_group->scenePos();
            QPointF const delta = new_pos - old_pos;
            if ( point_index < 5)
            {
               // moving a corner: moving opposite corners with the opposit amount
               int const prev_corner_index = ((point_index+2)%4)+1;
               int const next_corner_index = (point_index%4)+1;
               this->distorsion_points[prev_corner_index]->setPos(
                        this->distorsion_points[prev_corner_index]->pos()-delta);
               this->distorsion_points[next_corner_index]->setPos(
                        this->distorsion_points[next_corner_index]->pos()-delta);
            }
            else
            {
               // moving a border: moving both corners with the same amount
               int const prev_corner_index = (point_index-4);
               int const next_corner_index = ((point_index-4)%4)+1;
               this->distorsion_points[prev_corner_index]->setPos(
                        this->distorsion_points[prev_corner_index]->pos()+delta);
               this->distorsion_points[next_corner_index]->setPos(
                        this->distorsion_points[next_corner_index]->pos()+delta);
            }
            this->moving_distorsion_item->setPos(new_pos);
         }
         this->applyDistorsion();
      }
   }
   else if ( (qgsme->button() != Qt::NoButton) and
             (qgsme->buttons() == Qt::NoButton) )
   {
      // mouse release
      this->moving_distorsion_item = nullptr;
   }
   else if ( (qgsme->button() != Qt::NoButton) and
             (qgsme->buttons() != Qt::NoButton) )
   {
      // computing mouse offset
      QScreen* scr = this->backingStore()->window()->screen();
      if ( scr != nullptr )
      {
         // offset upward
         double const dpi_y = std::min(static_cast<double>(scr->logicalDotsPerInchY()),
                                       static_cast<double>(scr->physicalDotsPerInchY()));
         QPointF const origin = this->gv_target_camera->mapToScene(QPoint{0,0});
         QPointF const dpi_y_pos = this->gv_target_camera->mapToScene({0, qRound(dpi_y)});
         QPointF const vect = dpi_y_pos - origin;
         double const vect_length = std::sqrt(QPointF::dotProduct(vect,vect));
         this->mouse_offset = QPointF(0.0,-vect_length*0.55);
      }
      else
      {
         this->mouse_offset = QPointF(0.0,0.0);
      }
qDebug() << mouse_offset;
      // mouse press
      QList<QGraphicsItem*> items = scene->items(qgsme->scenePos());
      this->moving_distorsion_item = nullptr;
      for(QGraphicsItem* item: items)
      {
         QGraphicsEllipseItem* ellipse = dynamic_cast<QGraphicsEllipseItem*>(item);
         auto const distorsion_point_iter = std::find(this->distorsion_points.begin(),
                                                      this->distorsion_points.end(),
                                                      ellipse);
         if ( distorsion_point_iter != this->distorsion_points.end() )
         {
            this->moving_distorsion_item = *distorsion_point_iter;
            break;
         }
      }
   }
   return false;
}

void Camera_Target::cameraStateChanged(QCamera::State state)
{
   if ( state != QCamera::LoadedState )
   {
      return;
   }
   if ( this->camera_capture != nullptr )
   {
      return;
   }
   // finding a "good" resolution
   QList<QSize> resolutions = this->camera->supportedViewfinderResolutions();
   auto good_resolution_iter = resolutions.end();
   int good_resolution_max_dim = 0;
   auto resolution_iter = resolutions.begin();
   auto const resolution_end = resolutions.end();
   while ( resolution_iter != resolution_end )
   {
      QSize const& sz = *resolution_iter;
      int const max_dim = std::max(sz.width(), sz.height());
      if ( (max_dim < 512) and (max_dim > good_resolution_max_dim) )
      {
         good_resolution_iter = resolution_iter;
         good_resolution_max_dim = max_dim;
      }
      ++resolution_iter;
   }
   QSize const& good_resolution = *good_resolution_iter;
   this->camera_capture = new Camera_Capture(this);
   QObject::connect(
            this->camera_capture, &Camera_Capture::imageAvailable,
            this, &Camera_Target::cameraNewImage);
   this->camera_capture->setSource(this->camera);
   QCameraViewfinderSettings vf_settings;
   vf_settings.setResolution(good_resolution);
   this->camera->setViewfinderSettings(vf_settings);
   this->camera->setViewfinder(this->camera_capture);
   this->startCamera();
}

void Camera_Target::cameraNewImage()
{
   static size_t frames_count = 0;
   static QTime chrono;
   if ( this->is_handling_image )
   {
      return;
   }
   if ( frames_count == 0 )
   {
      chrono.restart();
   }
   ++frames_count;
   this->is_handling_image = true;
   QImage const& img = this->camera_capture->image();
   QGraphicsScene* const scene = this->gv_target_camera->scene();
   QRectF const scene_rect = scene->sceneRect();
   if ( this->image_item == nullptr )
   {
      this->image_item = new QGraphicsPixmapItem();
      this->image_item->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
      this->gv_target_camera->scene()->addItem(this->image_item);
      this->image_item->setPos(0.0, 0.0);
      this->image_item->setZValue(0.0);
   }
   QPixmap pix = QPixmap::fromImage(img);
   // fitting image in scene rect width
   double const scaling =
         (pix.width() > pix.height()) ?
            scene_rect.width() / static_cast<double>(pix.height()) :
            scene_rect.width() / static_cast<double>(pix.width());
   this->image_item->setTransform(QTransform::fromScale(-scaling, -scaling));
   this->image_item->setPixmap(pix);
   QRectF const brect = this->image_item->sceneBoundingRect();
   this->image_item->setPos(brect.width()*0.5, brect.height()*0.5);
   this->is_handling_image = false;
   if ( chrono.elapsed() > 1000 )
   {
      emit this->cameraFps(qRound((frames_count*1000.0) / chrono.elapsed()));
      frames_count = 0;
   }
}

void Camera_Target::flashToggled(bool on)
{
   if ( (this->camera == nullptr) or
        (this->camera->state() != QCamera::ActiveState) )
   {
      return;
   }
   QCameraExposure* exposure = this->camera->exposure();
   if ( on )
   {
      exposure->setFlashMode(QCameraExposure::FlashManual bitor QCameraExposure::FlashTorch);
   }
   else
   {
      exposure->setFlashMode(QCameraExposure::FlashManual bitor QCameraExposure::FlashOff);
   }
}

void Camera_Target::hitButtonClicked()
{

}

void Camera_Target::resizeEvent(QResizeEvent* qre)
{
   this->Generic_Page::resizeEvent(qre);
   this->gv_target_camera->fitInView(
            this->gv_target_camera->scene()->sceneRect(),
            Qt::KeepAspectRatio);
}

void Camera_Target::startCamera()
{
   if ( this->camera == nullptr )
   {
      return;
   }
   this->camera->searchAndLock(QCamera::NoLock);
   QCameraImageProcessing* processing = this->camera->imageProcessing();
   processing->setColorFilter(QCameraImageProcessing::ColorFilterNone);
   processing->setWhiteBalanceMode(QCameraImageProcessing::WhiteBalanceManual);
   QCameraExposure* exposure = this->camera->exposure();
   exposure->setExposureMode(QCameraExposure::ExposureManual);

   this->camera->setCaptureMode(QCamera::CaptureVideo);
   this->camera->start();
}

void Camera_Target::stopCamera()
{
   if ( this->camera == nullptr )
   {
      return;
   }
   QCameraExposure* exposure = this->camera->exposure();
   exposure->setFlashMode(QCameraExposure::FlashOff);
   this->camera->stop();
}

void Camera_Target::updateHitButton(QPushButton* bt,
                                    Score_Unit const& su)
{
   if ( su.isDefined() )
   {
      bt->setText(QString("%1").arg(su.score()));
      QPalette pal = bt->palette();
      pal.setBrush(QPalette::Button, textBackgroundColorForMark(su.score()));
      pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(su.score()));
      bt->setPalette(pal);
   }
   else
   {
      bt->setText("?");
      QPalette pal = bt->palette();
      pal.setBrush(QPalette::Button, QColor(Qt::lightGray));
      pal.setBrush(QPalette::ButtonText, QColor(Qt::black));
      bt->setPalette(pal);
   }
}

void Camera_Target::applyTransform()
{
   QTransform const t = this->distorsion_transform *
                        this->zoom_transform *
                        this->translate_transform;
   this->rings_group->setTransform(t);
   this->axis_group->setTransform(t);
}

void Camera_Target::applyDistorsion()
{
   QRectF const orig_rect = this->distorsion_frame_rect->rect();
   QTransform const new_transform =
         distorsionFromCorners(
   {orig_rect.topLeft(),
    orig_rect.topRight(),
    orig_rect.bottomRight(),
    orig_rect.bottomLeft()},
   {this->distorsion_points[1]->scenePos(),
    this->distorsion_points[2]->scenePos(),
    this->distorsion_points[3]->scenePos(),
    this->distorsion_points[4]->scenePos()});
   this->distorsion_frame_group->setTransform(new_transform);
   this->rings_group->setTransform(new_transform);
   // adjusting the frame's center
   this->distorsion_points[0]->setPos(
            this->distorsion_frame_group->mapToScene(orig_rect.center()) -
            this->distorsion_group->pos());
}
