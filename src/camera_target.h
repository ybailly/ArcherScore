
#pragma once

#include "generic_page.h"

#include "ui_camera_target.h"

#include "score_unit.h"

#include <QtMultimedia/QCamera>

class Camera_Capture;
class QGraphicsPixmapItem;
class Session;
class Ends_Round;
class Single_End;

class Camera_Target: public Generic_Page, private Ui::Camera_Target
{
      Q_OBJECT
   public:
      explicit Camera_Target(QWidget* parent = nullptr);

      void setEnd(Session const* sess,
                  Round_Index round_idx,
                  End_Index end_idx);

      Session const* session() const;
      Round_Index roundIndex() const;
      End_Index endIndex() const;
      std::vector<Score_Unit> const& hitLocations() const;

      virtual void slidingStart() override;
      virtual void slidingEnd() override;

      virtual bool eventFilter(QObject* obj, QEvent* qe) override;

   protected slots:
      void cameraStateChanged(QCamera::State state);
      void cameraNewImage();
      void flashToggled(bool on);
      void hitButtonClicked();

   protected:
      virtual void resizeEvent(QResizeEvent* qre) override;
      void startCamera();
      void stopCamera();
      void updateHitButton(QPushButton* bt,
                           Score_Unit const& su);
      void applyTransform();
      void applyDistorsion();

   signals:
      void cameraFps(int);

   private:
      std::array<QGraphicsEllipseItem*, 11> rings_for_mark;
      std::array<QGraphicsEllipseItem*, 9> distorsion_points;
      QTransform distorsion_transform;
      QTransform zoom_transform;
      QTransform translate_transform;
      std::vector<Score_Unit> hit_locs;
      std::vector<QPushButton*> hit_buttons;
      QPointF mouse_offset{0.0,0.0};
      QCamera* camera{nullptr};
      Camera_Capture* camera_capture{nullptr};
      QGraphicsPixmapItem* image_item{nullptr};
      QGraphicsEllipseItem* ring_10plus{nullptr};
      QGraphicsItemGroup* rings_group{nullptr};
      QGraphicsItemGroup* axis_group{nullptr};
      QGraphicsItemGroup* distorsion_group{nullptr};
      QGraphicsItemGroup* distorsion_frame_group{nullptr};
      QGraphicsRectItem* distorsion_frame_rect{nullptr};
      QGraphicsLineItem* distorsion_frame_h_line{nullptr};
      QGraphicsLineItem* distorsion_frame_v_line{nullptr};
      QGraphicsEllipseItem* moving_distorsion_item{nullptr};
      Session const* session_{nullptr};
      Ends_Round const* round{nullptr};
      Single_End const* end{nullptr};
      QGraphicsLineItem* h_line{nullptr};
      QGraphicsLineItem* v_line{nullptr};
      Round_Index round_index{Invalid_Round_Index};
      End_Index end_index{Invalid_End_Index};
      bool is_handling_image{false};

}; // class Camera_Target
