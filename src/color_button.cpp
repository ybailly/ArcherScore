
#include "color_button.h"

#include "generic_overlay.h"
#include "color_chooser.h"

Color_Button::Color_Button(QWidget* parent):
   QPushButton(parent)
{
   QObject::connect(
            this, &Color_Button::clicked,
            [this]()
   {
      Generic_Overlay* overlay = new Generic_Overlay(this->topLevelWidget());
      Color_Chooser* color_chooser = new Color_Chooser;
      overlay->setContents(color_chooser);
      QObject::connect(
               color_chooser, &Color_Chooser::choosenColor,
               [overlay,this](QColor col)
      {
         this->setColor(col);
         overlay->discard();
      });
      overlay->run();
   });
}

void Color_Button::setColor(QColor col)
{
   this->color_ = col;
   QPalette pal = this->palette();
   pal.setBrush(QPalette::Button, this->color_);
   this->setPalette(pal);
}

QColor Color_Button::color() const
{
   return this->color_;
}
