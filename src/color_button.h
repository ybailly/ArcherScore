
#pragma once

#include <QtWidgets/QPushButton>

class Color_Button: public QPushButton
{
      Q_OBJECT
   public:
      explicit Color_Button(QWidget* parent = nullptr);

      void setColor(QColor col);
      QColor color() const;

   private:
      QColor color_{Qt::transparent};
}; // class Color_Button
