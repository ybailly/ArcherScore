
#include "color_chooser.h"

#include <QtWidgets/QPushButton>
#include <QtWidgets/QGridLayout>
#include <QtCore/QVariant>

Color_Chooser::Color_Chooser(QWidget* parent):
   QWidget(parent)
{
   QGridLayout* grid = new QGridLayout(this);
   grid->setMargin(0);
   grid->setSpacing(0);

   int const Nb_Hues = 6;
   int const Nb_Saturations = 2;
   int const Nb_Values = 2;
   double const hue_ival = 1.0 / static_cast<double>(Nb_Hues);
   double const saturation_ival = 1.0 / static_cast<double>(Nb_Saturations+1);
   double const value_ival = 1.0 / static_cast<double>(Nb_Values+1);

   auto prepare_button = [this](QColor color)
   {
      QPushButton* bt = new QPushButton(this);
      QPalette pal = bt->palette();
      pal.setBrush(QPalette::Button, color);
      bt->setPalette(pal);
      bt->setProperty("feather_color", color);
      bt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
      return bt;
   }; // auto prepare_button

   for(int hue_base = 0; hue_base < Nb_Hues; ++hue_base)
   {
      int const row = hue_base;
      double const hue = static_cast<double>(hue_base) * hue_ival;
      int column = 0;
      for(int value_base = 0; value_base < Nb_Values; ++value_base)
      {
         double const value = static_cast<double>(value_base+1)*value_ival;
         grid->addWidget(prepare_button(QColor::fromHsvF(hue,1.0,value)), row, column);
         ++column;
      }
      grid->addWidget(prepare_button(QColor::fromHsvF(hue,1.0,1.0)), row, column);
      ++column;
      for(int saturation_base = 0; saturation_base < Nb_Saturations; ++saturation_base)
      {
         double const saturation = 1.0 - static_cast<double>(saturation_base+1)*saturation_ival;
         grid->addWidget(prepare_button(QColor::fromHsvF(hue,saturation,1.0)), row, column);
         ++column;
      }
   }
   int const nb_grays = Nb_Saturations + Nb_Values;
   double const gray_ival = 1.0 / static_cast<double>(nb_grays);
   int const row = Nb_Hues;
   int column = 0;
   for(int value_base = 0; value_base < nb_grays; ++value_base)
   {
      double const value = static_cast<double>(value_base)*gray_ival;
      grid->addWidget(prepare_button(QColor::fromHsvF(1.0,0.0,value)), row, column);
      ++column;
   }
   QList<QPushButton*> buttons = this->findChildren<QPushButton*>();
   for(QPushButton* bt: buttons)
   {
      QObject::connect(
               bt, &QPushButton::clicked,
               this, &Color_Chooser::onClickedColor);
   }
}

void Color_Chooser::onClickedColor()
{
   QPushButton* bt = dynamic_cast<QPushButton*>(this->sender());
   emit this->choosenColor(bt->palette().brush(QPalette::Button).color());
}
