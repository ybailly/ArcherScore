
#pragma once

#include <QtWidgets/QWidget>

class Color_Chooser: public QWidget
{
      Q_OBJECT
   public:
      explicit Color_Chooser(QWidget* parent = nullptr);

   signals:
      void choosenColor(QColor);

   private:
      void onClickedColor();

}; // class Color_Chooser
