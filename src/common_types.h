
#pragma once

#include "tagged_value.h"

#include <QtGui/QVector2D>

using Hit_Location = QVector2D;

enum class Mark: int8_t
{
   Undefined   = -1,
   Zero     = 0,
   One      = 1,
   Two      = 2,
   Three    = 3,
   Four     = 4,
   Five     = 5,
   Six      = 6,
   Seven    = 7,
   Height   = 8,
   Nine     = 9,
   Ten      = 10,
   TenPlus  = 11
}; // enum class Mark

enum class Session_Type
{
   Unknown,
   Simple,
   Tournament,
   Mixed
}; // enum class Session_Type

enum class Session_Location
{
   Indoor,
   Outdoor
}; // enum class Session_Location

enum class Round_Status
{
   Unknown,
   Irrelevant,
   Won,
   Lost
}; // enum class Round_Status

enum class Bow_Type
{
   Unknown,
   Traditional,
   Traditional_Hunt,
   Recurve,
   Recurve_Hunt,
   Recurve_Modern,
   Compound
}; // enum class Bow_Type

using Session_Index = Tagged_Value<uint16_t, class Index_Tag, class Session>;
using Round_Index = Tagged_Value<uint8_t, class Index_Tag, class Ends_Round>;
using End_Index = Tagged_Value<uint8_t, class Index_Tag, class Single_End>;
using Score_Index = Tagged_Value<uint8_t, class Index_Tag, class Score_Unit>;
using Bow_Index = Tagged_Value<uint8_t, class Index_Tag, class Bow>;
using Arrow_Set_Index = Tagged_Value<uint8_t, class Index_Tag, class Arrow_Set>;

constexpr Session_Index Invalid_Session_Index{0xFFFF};
constexpr Round_Index Invalid_Round_Index{0xFF};
constexpr End_Index Invalid_End_Index(0xFF);
constexpr Score_Index Invalid_Score_Index{0xFF};
constexpr Bow_Index Invalid_Bow_Index{0xFF};
constexpr Arrow_Set_Index Invalid_Arrow_Set_Index{0xFF};

using Concentration_Coefficient = Tagged_Value<double, class Concentration_Tag, No_Unit_Tag>;
