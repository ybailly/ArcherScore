
#include "common_types_utils.h"

#include "cpp_enum_utils.h"

std::array<Mark, 13> const& marks()
{
   static auto const marks_ =
         cpp::make_array(
            Mark::Undefined, Mark::Zero,
            Mark::One, Mark::Two, Mark::Three, Mark::Four, Mark::Five,
            Mark::Six, Mark::Seven, Mark::Height, Mark::Nine, Mark::Ten,
            Mark::TenPlus);
   return marks_;
}

namespace
{
   cpp::Array_Of_Enum_Strings<Mark, cpp::array_size_from_return_type(marks)> const&
      marks_strings()
   {
      static auto const marks_strings_ =
            cpp::make_array(
               cpp::make_enum_string(Mark::Undefined, QStringLiteral("?")),
               cpp::make_enum_string(Mark::Zero,      QStringLiteral("0")),
               cpp::make_enum_string(Mark::One,       QStringLiteral("1")),
               cpp::make_enum_string(Mark::Two,       QStringLiteral("2")),
               cpp::make_enum_string(Mark::Three,     QStringLiteral("3")),
               cpp::make_enum_string(Mark::Four,      QStringLiteral("4")),
               cpp::make_enum_string(Mark::Five,      QStringLiteral("5")),
               cpp::make_enum_string(Mark::Six,       QStringLiteral("6")),
               cpp::make_enum_string(Mark::Seven,     QStringLiteral("7")),
               cpp::make_enum_string(Mark::Height,    QStringLiteral("8")),
               cpp::make_enum_string(Mark::Nine,      QStringLiteral("9")),
               cpp::make_enum_string(Mark::Ten,       QStringLiteral("10")),
               cpp::make_enum_string(Mark::TenPlus,   QStringLiteral("10+"))
               );
      return marks_strings_;
   }
}

Mark markFromString(QString const& s)
{
   return cpp::stringToEnum(s, marks_strings(), Mark::Undefined);
}

QString markToString(Mark m)
{
   return cpp::enumToString(m, marks_strings());
}

QPointF toQPointF(Hit_Location const& hl)
{
   return {static_cast<qreal>(hl.x()),
            static_cast<qreal>(hl.y())};
}

Hit_Location toHitLocation(QPointF const& p)
{
   return {static_cast<float>(p.x()),
            static_cast<float>(p.y())};
}

std::array<Session_Type, 4> const& sessionTypes()
{
   static auto const session_types_ =
         cpp::make_array(Session_Type::Unknown,
                         Session_Type::Simple,
                         Session_Type::Tournament,
                         Session_Type::Mixed);
   return session_types_;
}

namespace
{
   cpp::Array_Of_Enum_Strings<Session_Type, cpp::array_size_from_return_type(sessionTypes)> const&
      session_types_strings()
   {
      static auto const sessions_types_strings_ =
            cpp::make_array(
               cpp::make_enum_string(Session_Type::Unknown, QString()),
               cpp::make_enum_string(Session_Type::Simple, QStringLiteral("simple")),
               cpp::make_enum_string(Session_Type::Tournament, QStringLiteral("tournament")),
               cpp::make_enum_string(Session_Type::Mixed, QStringLiteral("mixed"))
               );
      return sessions_types_strings_;
   }
}

Session_Type sessionTypeFromString(QString const& s)
{
   return cpp::stringToEnum(s, session_types_strings(), Session_Type::Unknown);
}

QString const& sessionTypeToString(Session_Type t)
{
   return cpp::enumToString(t, session_types_strings());
}

std::array<Session_Location, 2> const& sessionLocations()
{
   static auto const session_locations_ =
         cpp::make_array(Session_Location::Indoor, Session_Location::Outdoor);
   return session_locations_;
}

namespace
{
   cpp::Array_Of_Enum_Strings<Session_Location, cpp::array_size_from_return_type(sessionLocations)> const&
      session_loc_strings()
   {
      static auto const sess_loc_strings =
            cpp::make_array(
               cpp::make_enum_string(Session_Location::Indoor, QStringLiteral("indoor")),
               cpp::make_enum_string(Session_Location::Outdoor, QStringLiteral("outdor"))
               );
      return sess_loc_strings;
   }
}

Session_Location sessionLocationFromString(QString const& s)
{
   return cpp::stringToEnum(s, session_loc_strings(), Session_Location::Indoor);
}

QString const& sessionLocationToString(Session_Location t)
{
   return cpp::enumToString(t, session_loc_strings());
}

std::array<Round_Status, 4> const& roundStatuses()
{
   static auto const round_statuses_ =
         cpp::make_array(Round_Status::Unknown, Round_Status::Irrelevant,
                         Round_Status::Won, Round_Status::Lost);
   return round_statuses_;
}

namespace
{
   cpp::Array_Of_Enum_Strings<Round_Status, cpp::array_size_from_return_type(roundStatuses)> const&
      round_status_strings()
   {
      static auto const round_stats_strings =
            cpp::make_array(
               cpp::make_enum_string(Round_Status::Unknown, QStringLiteral("Unknown")),
               cpp::make_enum_string(Round_Status::Irrelevant, QStringLiteral("Irrelevant")),
               cpp::make_enum_string(Round_Status::Won, QStringLiteral("Won")),
               cpp::make_enum_string(Round_Status::Lost, QStringLiteral("Lost"))
               );
      return round_stats_strings;
   }
}

Round_Status roundStatusFromString(QString const& s)
{
   return cpp::stringToEnum(s, round_status_strings(), Round_Status::Unknown);
}

QString const& roundStatusToString(Round_Status rs)
{
   return cpp::enumToString(rs, round_status_strings());
}

std::array<Bow_Type, 7> const& bowTypes()
{
   static auto const bow_types =
         cpp::make_array(
            Bow_Type::Unknown, Bow_Type::Traditional, Bow_Type::Traditional_Hunt,
            Bow_Type::Recurve, Bow_Type::Recurve_Hunt, Bow_Type::Recurve_Modern,
            Bow_Type::Compound);
   return bow_types;
}

namespace
{
   cpp::Array_Of_Enum_Strings<Bow_Type, cpp::array_size_from_return_type(bowTypes)> const&
      bow_types_strings()
   {
      static auto const bow_types_strings_ =
            cpp::make_array(
               cpp::make_enum_string(Bow_Type::Unknown, QString()),
               cpp::make_enum_string(Bow_Type::Traditional, QStringLiteral("traditional")),
               cpp::make_enum_string(Bow_Type::Traditional_Hunt, QStringLiteral("traditional_hunt")),
               cpp::make_enum_string(Bow_Type::Recurve, QStringLiteral("recurve")),
               cpp::make_enum_string(Bow_Type::Recurve_Hunt, QStringLiteral("recurve_hunt")),
               cpp::make_enum_string(Bow_Type::Recurve_Modern, QStringLiteral("recurve_modern")),
               cpp::make_enum_string(Bow_Type::Compound, QStringLiteral("compound"))
               );
      return bow_types_strings_;
   }
}

Bow_Type bowTypeFromString(QString const& s)
{
   return cpp::stringToEnum(s, bow_types_strings(), Bow_Type::Unknown);
}

QString const& bowTypeToString(Bow_Type bt)
{
   return cpp::enumToString(bt, bow_types_strings());
}

bool bowTypeHasSight(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Recurve_Modern:
      case Bow_Type::Compound:
         return true;
         break;
      default:
         return false;
   }
   return true;
}

bool bowTypeHasStabiliser(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Recurve_Modern:
      case Bow_Type::Compound:
         return true;
         break;
      default:
         return false;
   }
   return true;
}

Concentration_Coefficient normalizedDiameterToConcentrationCoefficient(Diameter diam)
{
   // the full target is considered as being a circle having radius 1.0
   // hence diameter 2.0
   // the "10" ring has a diameter of 0.2
   // the concentration coefficient is a given diameter divided by
   // 1.5 times the diameter of the "10" ring
   return Concentration_Coefficient((diam.value() > 0.0) ? diam.value() / (1.5*0.2) : -1.0);
}

