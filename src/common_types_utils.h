
#pragma once

#include "common_types.h"
#include "units.h"
#include "cpp_utils.h"

#include <QtCore/QString>
#include <QtCore/QPointF>
#include <QtGui/QColor>
#include <QtGui/QIcon>

#include <algorithm>

constexpr int8_t markToScore(Mark m)
{
   return cpp::clamp(static_cast<int8_t>(m), {0}, {10});
}

constexpr Mark scoreToMark(int8_t s)
{
   return static_cast<Mark>(cpp::clamp(s, {0}, {10}));
}

std::array<Mark, 13> const& marks();
Mark markFromString(QString const& s);
QString markToString(Mark m);

QPointF toQPointF(Hit_Location const& hl);
Hit_Location toHitLocation(QPointF const& p);

std::array<Session_Type, 4> const& sessionTypes();
Session_Type sessionTypeFromString(QString const& s);
QString const& sessionTypeToString(Session_Type t);

std::array<Session_Location, 2> const& sessionLocations();
Session_Location sessionLocationFromString(QString const& s);
QString const& sessionLocationToString(Session_Location t);

std::array<Round_Status, 4> const& roundStatuses();
Round_Status roundStatusFromString(QString const& s);
QString const& roundStatusToString(Round_Status rs);

std::array<Bow_Type, 7> const& bowTypes();
Bow_Type bowTypeFromString(QString const& s);
QString const& bowTypeToString(Bow_Type bt);
bool bowTypeHasSight(Bow_Type bt);
bool bowTypeHasStabiliser(Bow_Type bt);

constexpr uint64_t makeId(Session_Index si, Round_Index ri, End_Index ei, Score_Index sui)
{
   return
         (uint64_t(si.value()) << 24) bitor
         (uint64_t(ri.value()) << 16) bitor
         (uint64_t(ei.value()) << 8) bitor
         uint64_t(sui.value());
}

Concentration_Coefficient normalizedDiameterToConcentrationCoefficient(Diameter diam);
