
#pragma once

namespace cpp
{
   template<typename ENUM_TYPE>
   struct Enum_String
   {
         ENUM_TYPE enum_value;
         QString string_value;
   }; // struct Enum_String<>

   template<typename ENUM_TYPE>
   Enum_String<ENUM_TYPE> make_enum_string(ENUM_TYPE e, QString const& s)
   { return {e, s}; }

   template<typename ENUM_TYPE, size_t NB_VALUES>
   using Array_Of_Enum_Values = std::array<ENUM_TYPE, NB_VALUES>;

   template<typename ENUM_TYPE, size_t NB_VALUES>
   using Array_Of_Enum_Strings = std::array<Enum_String<ENUM_TYPE>, NB_VALUES>;

   template<typename ENUM_TYPE, size_t NB_VALUES>
   QString const& enumToString(ENUM_TYPE enum_value,
                               Array_Of_Enum_Strings<ENUM_TYPE, NB_VALUES> const& mapping)
   {
      static QString const empty_str;
      for(auto const& enum_string: mapping)
         if (enum_string.enum_value == enum_value)
            return enum_string.string_value;
      return empty_str;
   }

   template<typename ENUM_TYPE, size_t NB_VALUES>
   ENUM_TYPE stringToEnum(QString const& string_value,
                          Array_Of_Enum_Strings<ENUM_TYPE, NB_VALUES> const& mapping,
                          ENUM_TYPE default_value)
   {
      for(auto const& enum_string: mapping)
         if (enum_string.string_value == string_value)
            return enum_string.enum_value;
      return default_value;
   }

} // namespace cpp
