
#pragma once

#include <QtCore/QString>

#include <array>
#include <type_traits>

namespace cpp
{
   template<typename T>
   constexpr T const& clamp(T const& v, T const& low, T const& high)
   {
      return (v < low) ? low : ((high < v) ? high : v);
   }

   template <class D, class...> struct make_array_return_type_helper { using type = D; };
   template <class... Types>
   struct make_array_return_type_helper<void, Types...> : std::common_type<Types...> { };

   template <class D, class... Types>
   using make_array_return_type = std::array<typename make_array_return_type_helper<D, Types...>::type, sizeof...(Types)>;

   template < class D = void, class... Types>
   constexpr make_array_return_type<D, Types...> make_array(Types&&... t)
   {
      return {std::forward<Types>(t)... };
   }

   template<typename RET_TYPE, typename... ARGS>
   constexpr size_t array_size_from_return_type(RET_TYPE (*)(ARGS...))
   {
      return std::tuple_size< typename std::decay<RET_TYPE>::type >::value;
   }

} // namespace cpp
