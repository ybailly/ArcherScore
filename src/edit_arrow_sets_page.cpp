
#include "edit_arrow_sets_page.h"

#include "overlay_message.h"
#include "arrows.h"

Edit_Arrow_Sets_Page::Edit_Arrow_Sets_Page(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);
   this->setTitle(tr("Jeux de flèches enregistrés"));

   QObject::connect(
            this->arrow_sets_list, &Arrow_Sets_List_Widget::requestNewArrowSet,
            this, &Edit_Arrow_Sets_Page::requestNewArrowSetEditor);
   QObject::connect(
            this->arrow_sets_list, &Arrow_Sets_List_Widget::selectedArrowSet,
            this, &Edit_Arrow_Sets_Page::requestArrowSetEditor);
}

void Edit_Arrow_Sets_Page::slidingStart()
{
   this->arrow_sets_list->refreshArrowSetList();
}


