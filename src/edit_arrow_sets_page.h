
#pragma once

#include "generic_page.h"

#include "ui_edit_arrow_sets_page.h"

class Edit_Arrow_Sets_Page: public Generic_Page, private Ui::Edit_Arrow_Sets_Page
{
      Q_OBJECT
   public:
      explicit Edit_Arrow_Sets_Page(QWidget* parent = nullptr);

      virtual void slidingStart() override;

   signals:
      void requestNewArrowSetEditor();
      void requestArrowSetEditor(Arrow_Set_Index asi);

}; // class Edit_Arrow_Sets_Page
