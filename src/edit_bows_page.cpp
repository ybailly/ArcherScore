
#include "edit_bows_page.h"

#include "overlay_message.h"
#include "bows.h"

#include <QtDebug>

Edit_Bows_Page::Edit_Bows_Page(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);
   this->setTitle(tr("Arcs enregistrés"));

   QObject::connect(
            this->bows_list, &Bows_List_Widget::requestNewBow,
            this, &Edit_Bows_Page::requestNewBowEditor);
   QObject::connect(
            this->bows_list, &Bows_List_Widget::selectedBow,
            this, &Edit_Bows_Page::requestBowEditor);
}

void Edit_Bows_Page::slidingStart()
{
   this->bows_list->refreshBowList();
}

