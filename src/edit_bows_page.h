
#pragma once

#include "generic_page.h"

#include "ui_edit_bows_page.h"

class Edit_Bows_Page: public Generic_Page, private Ui::Edit_Bows_Page
{
      Q_OBJECT
   public:
      explicit Edit_Bows_Page(QWidget* parent = nullptr);

      virtual void slidingStart() override;

   signals:
      void requestNewBowEditor();
      void requestBowEditor(Bow_Index bi);

}; // class Edit_Bows_Page
