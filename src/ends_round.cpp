
#include "ends_round.h"
#include "session.h"
#include "common_types_utils.h"

#include <iterator>
#include <algorithm>
#include <numeric>

#include <QtDebug>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

Ends_Round::Ends_Round(uint8_t sc):
   shot_count(sc)
{
}

Ends_Round::Ends_Round(QJsonObject const& json,
                       int version)
{
   if ( version > 3 )
   {
      qDebug() << "*** Unknown version" << version << "while reading round";
   }
   this->cut_ = static_cast<uint16_t>(json["cut"].toInt());
   QJsonArray ends_array = json["ends"].toArray();
   this->ends.reserve(ends_array.count());
   for(QJsonValue const& end_value: ends_array)
   {
      this->ends.emplace_back(end_value.toObject(), version);
   }
   if (not this->ends.empty())
   {
      this->shot_count = this->ends.front().nbShots();
   }
}

void Ends_Round::appendEnd()
{
   this->ends.push_back(Single_End{this->shot_count});
   // as all shots in the new end are undefined, the stats
   // remain unchanged
}

Single_End const& Ends_Round::singleEnd(End_Index end_) const
{
   return this->ends.at(end_.value());
}

void Ends_Round::setScore(End_Index end_,
                          Score_Index shot,
                          Score_Unit const& su)
{
   Score_Unit old_score_unit = this->ends[end_.value()].scoreUnit(shot);
   bool const was_mark_defined = old_score_unit.mark() != Mark::Undefined;
   bool const was_loc_defined = old_score_unit.isLocationDefined();
   bool const was_enabled = old_score_unit.isEnabledForGroup();
   this->ends[end_.value()].setScore(shot, su);
   if ( (su.mark() != Mark::Undefined) or was_mark_defined )
   {
      this->stats->mark_stats_dirty = true;
   }
   if ( su.isLocationDefined() or su.isEnabledForGroup() or was_enabled or was_loc_defined )
   {
      this->stats->group_stats_dirty = true;
   }
}

void Ends_Round::setScoreEnabledForGroup(End_Index end_,
                                         Score_Index shot,
                                         bool is_enabled_for_group)
{
   this->ends[end_.value()].setScoreEnabledForGroup(shot, is_enabled_for_group);
}

Score_Unit const& Ends_Round::scoreUnit(End_Index end_,
                                        Score_Index shot) const
{
   return this->ends[end_.value()].scoreUnit(shot);
}

uint16_t Ends_Round::scoreSum(End_Index end_) const
{
   return this->ends[end_.value()].scoreSum();
}

double Ends_Round::scoreAverage(End_Index end_) const
{
   return this->ends[end_.value()].scoreAverage();
}

Diameter Ends_Round::groupDiameter(End_Index end_) const
{
   return this->ends[end_.value()].groupDiameter();
}

QPointF Ends_Round::groupCenter(End_Index end_) const
{
   return this->ends[end_.value()].groupCenter();
}

Concentration_Coefficient Ends_Round::concentrationCoefficent(End_Index end_) const
{
   return this->ends[end_.value()].concentrationCoefficient();
}

uint8_t Ends_Round::endsCount() const
{
   return static_cast<uint8_t>(this->ends.size());
}

uint8_t Ends_Round::shotCountByEnd() const
{
   return this->shot_count;
}

uint16_t Ends_Round::scoreSum() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->mark_sum;
}

double Ends_Round::scoreAverage() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->mark_average;
}

uint16_t Ends_Round::nbDefinedShots() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->nb_defined;
}

uint16_t Ends_Round::n10() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->nb_10;
}

uint16_t Ends_Round::n10Plus() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->nb_10_plus;
}

Diameter Ends_Round::groupDiameterAverage() const
{
   if ( this->stats->group_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->average_group_diameter;
}

Diameter Ends_Round::groupDiameterGlobal() const
{
   if ( this->stats->group_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->global_group_diameter;
}

QPointF Ends_Round::groupCenterGlobal() const
{
   if ( this->stats->group_stats_dirty )
   {
      this->updateStats();
   }
   return this->stats->global_group_center;
}

Concentration_Coefficient Ends_Round::concentrationCoefficientAverage() const
{
   return normalizedDiameterToConcentrationCoefficient(this->groupDiameterAverage());
}

Concentration_Coefficient Ends_Round::concentrationCoefficentGlobal() const
{
   return normalizedDiameterToConcentrationCoefficient(this->groupDiameterGlobal());
}

uint16_t Ends_Round::cut() const
{
   return this->cut_;
}

void Ends_Round::setCut(uint16_t c)
{
   this->cut_ = c;
}

Round_Status Ends_Round::status() const
{
   if ( this->cut_ == 0 )
   {
      return Round_Status::Unknown;
   }
   if ( this->scoreSum() < this->cut_ )
   {
      return Round_Status::Lost;
   }
   return Round_Status::Won;
}

QJsonObject Ends_Round::toJson() const
{
   QJsonObject round_object;
   round_object.insert("cut", this->cut_);
   QJsonArray ends_array;
   for(Single_End const& se: this->ends)
   {
      ends_array.append(se.toJson());
   }
   round_object.insert("ends", ends_array);
   return round_object;
}

Ends_Round::iterator Ends_Round::begin()
{
   return this->ends.begin();
}

Ends_Round::iterator Ends_Round::end()
{
   return this->ends.end();
}

Ends_Round::const_iterator Ends_Round::begin() const
{
   return this->ends.begin();
}

Ends_Round::const_iterator Ends_Round::end() const
{
   return this->ends.end();
}

Ends_Round::const_iterator Ends_Round::cbegin() const
{
   return this->ends.cbegin();
}

Ends_Round::const_iterator Ends_Round::cend() const
{
   return this->ends.cend();
}

void Ends_Round::updateStats() const
{
   if ( this->stats->mark_stats_dirty )
   {
      this->stats->mark_sum = 0;
      this->stats->mark_average = 0.0;
      this->stats->nb_defined = 0;
      this->stats->nb_10 = 0;
      this->stats->nb_10_plus = 0;
      if ( not this->ends.empty() )
      {
         for(Single_End const& se: this->ends)
         {
            this->stats->nb_defined += se.nbDefinedShots();
            this->stats->nb_10 += se.n10();
            this->stats->nb_10_plus += se.n10plus();
            this->stats->mark_sum += se.scoreSum();
         }
         if ( this->stats->nb_defined > 0 )
         {
            this->stats->mark_average =
                  static_cast<double>(this->stats->mark_sum) /
                  static_cast<double>(this->stats->nb_defined);
         }
         else
         {
            this->stats->mark_average = 0.0;
         }
      }
      this->stats->mark_stats_dirty = false;
   }
   if ( this->stats->group_stats_dirty )
   {
      this->stats->global_group_center = QPointF{0.0,0.0};
      this->stats->global_group_diameter = -1.0_diameter;
      this->stats->average_group_diameter = -1.0_diameter;
      std::vector<Pt2d> pts;
      Diameter diam_sum = 0.0_diameter;
      size_t nb_valid_groups = 0;
      for(Single_End const& se: this->ends)
      {
         Diameter const se_diam = se.groupDiameter();
         if ( se_diam >= 0.0 )
         {
            ++nb_valid_groups;
            diam_sum += se_diam;
            se.appendToPointSet(&pts);
         }
      }
      if ( nb_valid_groups > 0 )
      {
         this->stats->average_group_diameter = diam_sum / static_cast<double>(nb_valid_groups);
      }
      double global_group_diameter;
      computeGroup(pts,
                   &this->stats->global_group_center,
                   &global_group_diameter);
      this->stats->global_group_diameter = Diameter(global_group_diameter);
      this->stats->group_stats_dirty = false;
   }
}
