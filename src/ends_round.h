
#pragma once

#include <vector>
#include <memory>

#include "single_end.h"

class Session;

class Ends_Round
{
   public:
      Ends_Round() = delete;
      Ends_Round(Ends_Round const&) = default;
      Ends_Round(Ends_Round&&) = default;

      Ends_Round& operator=(Ends_Round const&) = default;
      Ends_Round& operator=(Ends_Round&&) = default;

      explicit Ends_Round(uint8_t shot_count);
      explicit Ends_Round(QJsonObject const& json,
                          int version);

      void appendEnd();
      Single_End const& singleEnd(End_Index end_) const;

      // shortcuts to ends

      void setScore(End_Index end_,
                    Score_Index shot,
                    Score_Unit const& su);

      void setScoreEnabledForGroup(End_Index end_,
                                   Score_Index shot,
                                   bool is_enabled_for_group);

      Score_Unit const& scoreUnit(End_Index end_,
                                  Score_Index shot) const;

      uint16_t scoreSum(End_Index end_) const;
      double scoreAverage(End_Index end_) const;
      Diameter groupDiameter(End_Index end_) const;
      QPointF groupCenter(End_Index end_) const;
      Concentration_Coefficient concentrationCoefficent(End_Index end_) const;

      // queries

      uint8_t endsCount() const;
      uint8_t shotCountByEnd() const;

      uint16_t scoreSum() const;
      double scoreAverage() const;
      uint16_t nbDefinedShots() const;
      uint16_t n10() const;
      uint16_t n10Plus() const;
      Diameter groupDiameterAverage() const;
      Diameter groupDiameterGlobal() const;
      QPointF groupCenterGlobal() const;
      Concentration_Coefficient concentrationCoefficientAverage() const;
      Concentration_Coefficient concentrationCoefficentGlobal() const;

      uint16_t cut() const;
      void setCut(uint16_t c);
      Round_Status status() const;

      QJsonObject toJson() const;

      // iterators
      using Single_Ends_Container = std::vector<Single_End>;
      using iterator = Single_Ends_Container::iterator;
      using const_iterator = Single_Ends_Container::const_iterator;
      iterator begin();
      iterator end();
      const_iterator begin() const;
      const_iterator end() const;
      const_iterator cbegin() const;
      const_iterator cend() const;

   private:
      std::vector<Single_End> ends;

      struct Stats
      {
            double mark_average{0.0};
            QPointF global_group_center{0.0,0.0};
            Diameter global_group_diameter{-1.0};
            Diameter average_group_diameter{-1.0};
            uint16_t mark_sum{0};
            uint16_t nb_defined{0};
            uint16_t nb_10{0};
            uint16_t nb_10_plus{0};
            bool mark_stats_dirty{true};
            bool group_stats_dirty{true};
      }; // struct Stats
      std::unique_ptr<Stats> stats{std::make_unique<Stats>()};
      void updateStats() const;

      uint16_t cut_{0};
      Round_Status status_{Round_Status::Unknown};
      uint8_t shot_count{0};
}; // class Score_Table
