
#include "ends_round_delegate.h"

#include "ends_round_model.h"
#include "session.h"
#include "hit_location_editor.h"
#include "hit_locations_viewer.h"
#include "qt_utils.h"
#include "ui_utils.h"

#include <QtDebug>

Ends_Round_Delegate::Ends_Round_Delegate(QObject* parent):
   QStyledItemDelegate(parent)
{
   this->labelForStats();
}

void Ends_Round_Delegate::setEndsRound(Session const* sd,
                                       Round_Index ends_round_idx)
{
   this->session_ = sd;
   this->ends_round_index = ends_round_idx;
   this->ends_round =
         (this->session_ != nullptr) ?
            this->session_->mutableEndsRound(this->ends_round_index) :
            nullptr;
}

Session const* Ends_Round_Delegate::session() const
{
   return this->session_;
}

Round_Index Ends_Round_Delegate::endsRoundIndex() const
{
   return this->ends_round_index;
}

QLabel* Ends_Round_Delegate::labelForStats() const
{
   static QLabel* lbl = nullptr;
   if ( lbl == nullptr )
   {
      lbl = new QLabel;
      lbl->setTextFormat(Qt::RichText);
      lbl->setAutoFillBackground(false);
      lbl->setAlignment(Qt::AlignCenter);
   }
   return lbl;
}

void Ends_Round_Delegate::paint(QPainter* painter,
                                 QStyleOptionViewItem const& option,
                                 QModelIndex const& index) const
{
   Ends_Round_Model const* const model = dynamic_cast<Ends_Round_Model const*>(index.model());
   if ( (model == nullptr) or (this->session_ == nullptr) )
   {
      this->QStyledItemDelegate::paint(painter, option, index);
      return;
   }
   auto paint_view_icon = [&option,painter,this]()
   {
      QIcon const& icon = iconForTargetMinimumScore(this->session_->targetMinimumScore());
      painter->save();
      painter->setClipRect(option.rect);
      icon.paint(painter, option.rect, Qt::AlignCenter);
      painter->restore();
   };
   if ( index.row() == (model->rowCount()-1) )
   {
      if ( index.column() == 0 )
      {
         this->labelForStats()->setText(model->data(index).toString());
         this->labelForStats()->adjustSize();
         this->labelForStats()->setGeometry(option.rect);
         painter->save();
         painter->translate(option.rect.topLeft());
         this->labelForStats()->render(painter);
         painter->restore();
      }
      else if ( model->isStatsColumn(index.column()) )
      {
         QIcon const& icon = qt::icon("add_end");
         QSize const view_size = this->sizeHint(option, model->index(index.row(), index.column()+1));
         int const wanted_size = std::min(
               std::min(view_size.width(), option.rect.width()),
               std::min(view_size.height(), option.rect.height()));
         QPoint const center = option.rect.center();
         QRect const rect(center.x()-wanted_size/2, center.y()-wanted_size/2, wanted_size, wanted_size);
         painter->save();
         painter->setClipRect(rect);
         icon.paint(painter, rect, Qt::AlignCenter);
         painter->restore();
      }
      else if ( model->isViewColumn(index.column()) )
      {
         paint_view_icon();
      }
   }
   else if ( model->isStatsColumn(index.column()) )
   {
      this->labelForStats()->setText(model->data(index).toString());
      QFont fnt = this->adjustedFontForStats(option.font);
      this->labelForStats()->setFont(fnt);
      this->labelForStats()->adjustSize();
      this->labelForStats()->setGeometry(option.rect);
      painter->save();
      painter->translate(option.rect.topLeft());
      this->labelForStats()->render(painter);
      painter->restore();
   }
   else if ( model->isViewColumn(index.column()) )
   {
      paint_view_icon();
   }
   else
   {
      this->QStyledItemDelegate::paint(painter, option, index);
   }
}

QFont Ends_Round_Delegate::adjustedFontForStats(QFont const& fnt_src) const
{
   QFont fnt = fnt_src;
   if ( fnt.pointSizeF() > 1.0 )
   {
      fnt.setPointSizeF(fnt.pointSizeF()*0.8);
   }
   else if ( fnt.pointSize() > 1 )
   {
      fnt.setPointSize(qRound(fnt.pointSize()*0.8));
   }
   else
   {
      fnt.setPixelSize(qRound(fnt.pixelSize()*0.85));
   }
   return fnt;
}

QSize Ends_Round_Delegate::sizeHint(QStyleOptionViewItem const& option,
                                     QModelIndex const& index) const
{
   QSize const base_size = this->QStyledItemDelegate::sizeHint(option, index);
   int const m_height = option.fontMetrics.boundingRect('M').height();
   return QSize(std::max(base_size.width(), 2*m_height), 4*m_height);
}
