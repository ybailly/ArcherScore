
#pragma once

#include <QtWidgets/QStyledItemDelegate>
#include <QtWidgets/QLabel>

#include "session.h"

class Hit_Location_Editor;
class Hit_Locations_Viewer;

class Ends_Round_Delegate: public QStyledItemDelegate
{
      Q_OBJECT
   public:
      explicit Ends_Round_Delegate(QObject* parent = nullptr);

      void setEndsRound(Session const* sd,
                        Round_Index ends_round_idx);
      Session const* session() const;
      Round_Index endsRoundIndex() const;

      virtual void paint(QPainter* painter,
                         QStyleOptionViewItem const& option,
                         QModelIndex const& index) const override;
      virtual QSize sizeHint(QStyleOptionViewItem const& option,
                             QModelIndex const& index) const;

   private:
      QLabel* labelForStats() const;
      QFont adjustedFontForStats(QFont const& fnt) const;
      Session const* session_{nullptr};
      Round_Index ends_round_index{0};
      Ends_Round* ends_round{nullptr};

}; // class Score_Table_Delegate
