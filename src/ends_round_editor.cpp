
#include "ends_round_editor.h"

#include "session.h"
#include "ends_round_model.h"
#include "ends_round_delegate.h"
#include "overlay_choose_score.h"

#include <QtDebug>

Ends_Round_Editor::Ends_Round_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);

   QObject::connect(
            this, &Generic_Page::backRequested,
            this, &Ends_Round_Editor::done);
   QObject::connect(
            this->tv_ends, &QAbstractItemView::clicked,
            this, &Ends_Round_Editor::itemClicked);
}

void Ends_Round_Editor::setEndsRound(Session const* sess,
                                     Round_Index rindex)
{
   if ( this->ends_round_model == nullptr )
   {
      this->ends_round_model = new Ends_Round_Model(this);
      this->tv_ends->setModel(this->ends_round_model);
   }
   if ( this->ends_round_delegate == nullptr )
   {
      this->ends_round_delegate = new Ends_Round_Delegate(this);
      this->tv_ends->setItemDelegate(this->ends_round_delegate);
   }
   if ( (sess == this->session_) and
        (rindex == this->round_index) )
   {
      this->tv_ends->reset();
      return;
   }
   this->tv_ends->clearSpans();
   this->session_ = sess;
   this->round_index = rindex;
   if ( this->session_ == nullptr )
   {
      this->ends_round_model->setEndsRound(nullptr, {});
      this->ends_round_delegate->setEndsRound(nullptr, {});
      return;
   }
   this->ends_round = this->session_->mutableEndsRound(this->round_index);
   this->ends_round_model->setEndsRound(this->session_, this->round_index);
   this->ends_round_delegate->setEndsRound(this->session_, this->round_index);
   this->tv_ends->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
   this->tv_ends->horizontalHeader()->setSectionResizeMode(this->ends_round_model->columnCount()-1, QHeaderView::ResizeToContents);
   this->tv_ends->setSpan(
            this->ends_round_model->rowCount()-1, 0, 1, this->session_->shotCount());
   this->tv_ends->clearSelection();
   this->tv_ends->resizeColumnsToContents();
   this->tv_ends->resizeRowsToContents();
}

Session const* Ends_Round_Editor::session() const
{
   return this->session_;
}

Ends_Round* Ends_Round_Editor::endsRound() const
{
   return this->ends_round;
}

Round_Index Ends_Round_Editor::endsRoundIndex() const
{
   return this->round_index;
}

void Ends_Round_Editor::appendEnd()
{
   this->session_->saveToFile();
   this->ends_round_model->appendEnd();
   this->tv_ends->clearSelection();
   this->tv_ends->resizeColumnsToContents();
   this->tv_ends->resizeRowsToContents();
   this->tv_ends->scrollToBottom();
}

void Ends_Round_Editor::itemClicked(QModelIndex const& index)
{
   if ( index.row() == this->ends_round_model->rowCount()-1 )
   {
      if ( this->ends_round_model->isStatsColumn(index.column()) )
      {
         this->appendEnd();
      }
      else if ( this->ends_round_model->isViewColumn(index.column()) )
      {
         emit this->requestViewRound();
      }
   }
   else
   {
      if ( this->ends_round_model->isScoreColumn(index.column()) )
      {
         //emit this->requestEditScore(End_Index(index.row()), Score_Index(index.column()));
         End_Index const end_idx(index.row());
         Score_Index const score_idx(index.column());
         Overlay_Choose_Score* choose_score = new Overlay_Choose_Score(this->topLevelWidget());
         QObject::connect(
                  choose_score, &Overlay_Choose_Score::scoreChoosen,
                  [choose_score,end_idx,score_idx,this](Mark m)
         {
            this->ends_round->setScore(end_idx, score_idx, Score_Unit(m));
            this->session_->saveToFile();
            choose_score->discard();
         });
         choose_score->run();
      }
      else if ( this->ends_round_model->isViewColumn(index.column()) )
      {
         emit this->requestViewEditEnd(End_Index(index.row()));
      }
   }
   this->tv_ends->clearSelection();
}
