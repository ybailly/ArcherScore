
#pragma once

#include "generic_page.h"

#include "common_types.h"

#include "ui_ends_round_editor.h"

class Session;
class Ends_Round;
class Ends_Round_Model;
class Ends_Round_Delegate;

class Ends_Round_Editor: public Generic_Page, private Ui::Ends_Round_Editor
{
      Q_OBJECT
   public:
      explicit Ends_Round_Editor(QWidget* parent = nullptr);

      void setEndsRound(Session const* sess,
                        Round_Index rindex);

      Session const* session() const;
      Ends_Round* endsRound() const;
      Round_Index endsRoundIndex() const;

   public slots:
      void appendEnd();

   signals:
      void requestEditScore(End_Index end_idx, Score_Index score_idx);
      void requestViewEditEnd(End_Index end_idx);
      void requestViewRound();
      void done();

   protected slots:
      void itemClicked(QModelIndex const& index);

   private:
      Session const* session_{nullptr};
      Round_Index round_index{0};
      Ends_Round* ends_round{nullptr};
      Ends_Round_Model* ends_round_model{nullptr};
      Ends_Round_Delegate* ends_round_delegate{nullptr};

}; // class Ends_Round_Editor
