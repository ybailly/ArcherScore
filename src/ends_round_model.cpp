
#include "ends_round_model.h"

#include "session.h"
#include "ui_utils.h"
#include "common_types_utils.h"

#include <QtDebug>
#include <QtGui/QFont>
#include <QtGui/QBrush>

Ends_Round_Model::Ends_Round_Model(QObject* parent):
   QAbstractItemModel(parent)
{
}

void Ends_Round_Model::setEndsRound(Session const* sd,
                                    Round_Index ends_round_idx)
{
   this->beginResetModel();
   this->session_ = sd;
   this->ends_round_index = ends_round_idx;
   if ( this->session_ != nullptr )
   {
      this->ends_round = this->session_->mutableEndsRound(this->ends_round_index);
      // | shot1 | shot2 | shot3 | stats | view |
      this->nb_shot_columns = static_cast<int>(this->session_->shotCount());
      this->stats_column = this->nb_shot_columns;
      this->view_column = this->stats_column + 1;
   }
   this->endResetModel();
}

Session const* Ends_Round_Model::session() const
{
   return this->session_;
}

Round_Index Ends_Round_Model::endsRoundIndex() const
{
   return this->ends_round_index;
}

Ends_Round* Ends_Round_Model::endsRound() const
{
   return this->ends_round;
}

void Ends_Round_Model::appendEnd()
{
   if ( this->session_ == nullptr )
   {
      return;
   }
   this->beginInsertRows({}, this->rowCount()-1, this->rowCount()-1);
   this->ends_round->appendEnd();
   this->endInsertRows();
}

bool Ends_Round_Model::isScoreColumn(int col) const
{
   return not (this->isStatsColumn(col) or this->isViewColumn(col));
}

bool Ends_Round_Model::isStatsColumn(int col) const
{
   return col == this->stats_column;
}

bool Ends_Round_Model::isViewColumn(int col) const
{
   return col == this->view_column;
}

int Ends_Round_Model::columnCount(QModelIndex const& /*parent*/) const
{
   if ( this->session_ == nullptr )
   {
      return 2;
   }
   return static_cast<int>(this->session_->shotCount())
         + 1 // for the stats
         + 1 // for the view
         ;
}
QVariant Ends_Round_Model::data(QModelIndex const& index,
                                 int role) const
{
   if ( this->session_ == nullptr )
   {
      return QVariant();
   }
   if ( not index.isValid() )
   {
      return QVariant();
   }
   auto format_stats = [&](uint16_t mark_sum, Concentration_Coefficient group_ratio) -> QString
   {
      QString const group_ratio_str =
            (group_ratio < 0.0) ?
               QString("×") :
               QString("%1").arg(group_ratio.value(), 0, 'f', 2);
      return
            QString(
               "Σ=%1<br>"
               "c=<span style=\"color:%2; font-weight:%3;\">%4</span>")
            .arg(mark_sum)
            .arg(colorForConcentrationCoefficient(group_ratio).name())
            .arg(group_ratio < 0.55 ? "bold" : "normal")
            .arg(group_ratio_str);

   }; // format_stats

   if ( static_cast<size_t>(index.row()) == this->ends_round->endsCount() )
   {
      // we're on the last line: showing sums and stats
      switch ( role )
      {
         case Qt::DisplayRole:
         {
            if ( index.column() == 0 )
            {
               QString const group_ratio_str =
                     (this->ends_round->concentrationCoefficientAverage() < 0.0) ?
                        QString("×") :
                        QString("%1").arg(this->ends_round->concentrationCoefficientAverage().value(), 0, 'f', 2);
               return QString(
                        "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"
                        "  <tr>"
                        "     <th>n10</th>"
                        "     <th>n10+</th>"
                        "     <th>c̅</th>"
                        "     <th>Σ</th>"
                        "  </tr>"
                        "  <tr>"
                        "     <td align=\"center\">%1</td>"
                        "     <td align=\"center\">%2</td>"
                        "     <td align=\"center\">%3</td>"
                        "     <td align=\"center\">%4</td>"
                        "  </tr>"
                        "</table>"
                        )
                     .arg(this->ends_round->n10())
                     .arg(this->ends_round->n10Plus())
                     .arg(group_ratio_str)
                     .arg(this->ends_round->scoreSum());
            }
         }
            break;
         case Qt::TextAlignmentRole:
            return Qt::AlignCenter;
            break;
         default:
            return QVariant();
      }
      return QVariant();
   }
   else
   {
      // other lines: scores
      Single_End const& the_end = this->ends_round->singleEnd(End_Index(index.row()));
      Score_Unit const& score_unit = the_end.scoreUnit(Score_Index(index.column()));
      switch ( role )
      {
         case Qt::DisplayRole:
         {
            if ( this->isStatsColumn(index.column()) )
            {
               return format_stats(
                        the_end.scoreSum(),
                        the_end.concentrationCoefficient());
            }
            if ( this->isScoreColumn(index.column()) )
            {
               return markToString(score_unit.mark());
            }
         }
            break;
         case Qt::TextAlignmentRole:
            return Qt::AlignCenter;
         case Qt::BackgroundRole:
         {
            if ( this->isScoreColumn(index.column()) )
            {
               return textBackgroundColorForMark(score_unit.mark());
            }
         }
            break;
         case Qt::ForegroundRole:
         {
            if ( this->isScoreColumn(index.column()) )
            {
               return textForegroundColorForMark(score_unit.mark());
            }
         }
            break;
         case Qt::DecorationRole:
         {
            if ( this->isViewColumn(index.column()) )
            {
               return iconForTargetMinimumScore(this->session()->targetMinimumScore());
            }
         }
            break;
         default:
            return QVariant();
      }
      return QVariant();
   }
   return QVariant();
}

bool Ends_Round_Model::hasChildren(QModelIndex const& /*parent*/) const
{
   return false;
}

QModelIndex Ends_Round_Model::index(int row,
                                     int column,
                                     QModelIndex const& /*parent*/) const
{
   return this->createIndex(row, column);
}

QVariant Ends_Round_Model::headerData(int section,
                                      Qt::Orientation orientation,
                                      int role) const
{
   if ( (role == Qt::DisplayRole) and (orientation == Qt::Vertical) and (section == this->rowCount()-1) )
   {
      return {};
   }
   return this->QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex Ends_Round_Model::parent(QModelIndex const& /*index*/) const
{
   return QModelIndex();
}

int Ends_Round_Model::rowCount(QModelIndex const& /*parent*/) const
{
   if ( this->session_ == nullptr )
   {
      return 1;
   }
   return static_cast<int>(this->ends_round->endsCount())
         + 1 // the last line: sums and stats
         ;
}
