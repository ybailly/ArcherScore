
#pragma once

#include "common_types.h"

#include <QtCore/QAbstractItemModel>

class Session;
class Ends_Round;

class Ends_Round_Model: public QAbstractItemModel
{
      Q_OBJECT
   public:
      explicit Ends_Round_Model(QObject* parent = nullptr);

      void setEndsRound(Session const* sd,
                        Round_Index ends_round_idx);
      Session const* session() const;
      Round_Index endsRoundIndex() const;
      Ends_Round* endsRound() const;
      void appendEnd();

      bool isScoreColumn(int col) const;
      bool isStatsColumn(int col) const;
      bool isViewColumn(int col) const;

      virtual int columnCount(QModelIndex const& parent = QModelIndex()) const override;
      virtual QVariant data(QModelIndex const& index,
                            int role = Qt::DisplayRole) const override;
      virtual bool hasChildren(QModelIndex const& parent = QModelIndex()) const override;
      virtual QModelIndex index(int row,
                                int column,
                                QModelIndex const& parent = QModelIndex()) const override;
      virtual QVariant headerData(int section,
                                  Qt::Orientation orientation,
                                  int role = Qt::DisplayRole) const override;
      virtual QModelIndex parent(QModelIndex const& index) const override;
      virtual int rowCount(QModelIndex const& parent = QModelIndex()) const override;

   private:
      Session const* session_{nullptr};
      Round_Index ends_round_index{0};
      Ends_Round* ends_round{nullptr};
      int nb_shot_columns{0};
      int stats_column{0};
      int view_column{0};

}; // class Score_Table_Model
