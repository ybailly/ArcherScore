
#include "front_page.h"
#include "overlay_choose_session_type.h"
#include "qt_utils.h"

#include <QtDebug>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QMenu>

Front_Page::Front_Page(QWidget* parent):
   QWidget(parent)
{
   this->setupUi(this);
   this->main_layout->setSpacing(2*qt::computeEM(this));
   this->logo_image->load(QStringLiteral(":/images/logo"));
   this->logo_image->setSizeBuddy(this);
   this->logo_image->setFillOrientation(Qt::Horizontal);
   this->layout()->setAlignment(this->logo_image, Qt::AlignCenter);

   struct button_info_t
   {
         Button_Label* button;
         QString svg;
   };
   button_info_t buttons_info[] {
      {this->bt_new_session, QStringLiteral(":/icons/add_session_simple")},
      {this->bt_prev_sessions, QStringLiteral(":/icons/previous_sessions_icon")},
      {this->bt_edit_bows, QStringLiteral(":/icons/bows_icon")},
      {this->bt_edit_arrows, QStringLiteral(":/icons/arrow_sets_icon")},
      {this->bt_settings, QStringLiteral(":/images/empty")},
      {this->bt_quit, QStringLiteral(":/icons/quit")},
   };
   for(button_info_t const& button_info: buttons_info)
   {
      button_info.button->loadLeftSvg(button_info.svg);
      button_info.button->setFont(qt::adjustFontSize(button_info.button->font(), 1.25));
      button_info.button->setSpacing(qRound(1.5*qt::computeEM(button_info.button)));
      button_info.button->setHorizontalMargin(qRound(1.5*qt::computeEM(button_info.button)));
      button_info.button->setVerticalMargin(qRound(0.5*qt::computeEX(button_info.button)));
   };

   QObject::connect(
            this->bt_new_session, &QPushButton::clicked,
            [this]()
   {
      Overlay_Choose_Session_Type* choose_session_type = new Overlay_Choose_Session_Type(this);
      QObject::connect(
               choose_session_type, &Overlay_Choose_Session_Type::sessionTypeChoosen,
               [choose_session_type,this](Session_Type st)
      {
         emit this->requestNewSession(st);
         choose_session_type->discard();
      });
      choose_session_type->run();
   });

   QObject::connect(
            this->bt_prev_sessions, &QPushButton::clicked,
            this, &Front_Page::requestPrevSessions);
   QObject::connect(
            this->bt_edit_bows, &QPushButton::clicked,
            this, &Front_Page::requestEditBows);
   QObject::connect(
            this->bt_edit_arrows, &QPushButton::clicked,
            this, &Front_Page::requestEditArrows);
   QObject::connect(
            this->bt_settings, &QPushButton::clicked,
            this, &Front_Page::requestSettings);
   QObject::connect(
            this->bt_quit, &QPushButton::clicked,
            this, &Front_Page::requestQuit);

   this->setFixedSize(this->parentWidget()->size());
   this->move(this->parentWidget()->width(), 0);
   this->parentWidget()->installEventFilter(this);
}

bool Front_Page::eventFilter(QObject* qo, QEvent* qe)
{
   QWidget* w = dynamic_cast<QWidget*>(qo);
   QResizeEvent* qre = dynamic_cast<QResizeEvent*>(qe);
   if ( (w == nullptr) or
        (w != this->parentWidget()) or
        (qre == nullptr) )
   {
      return this->QWidget::eventFilter(qo, qe);
   }
   this->setFixedSize(qre->size());
   if ( this->x() > 0 )
   {
      this->move(qre->size().width(), 0);
   }
   return false;
}

void Front_Page::resizeEvent(QResizeEvent* qre)
{
   this->QWidget::resizeEvent(qre);
#if 0
   QSize const logo_size = this->logo_image->sizeHint();
   double const ratio_h_on_w = static_cast<double>(logo_size.height()) / static_cast<double>(logo_size.width());
   double const curr_w = qRound(qre->size().width()*0.95);
   QSize const wanted_size{
     qRound(curr_w),
     qRound(curr_w*ratio_h_on_w)
   };
   this->logo_image->setFixedSize(wanted_size);
#endif
}
