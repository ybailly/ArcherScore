
#pragma once

#include <QtWidgets/QWidget>

#include "common_types.h"
#include "ui_front_page.h"

class Front_Page: public QWidget, private Ui::Front_Page
{
      Q_OBJECT
   public:
      explicit Front_Page(QWidget* parent = nullptr);

      virtual bool eventFilter(QObject* qo, QEvent* qe) override;

   signals:
      void requestNewSession(Session_Type);
      void requestPrevSessions();
      void requestEditBows();
      void requestEditArrows();
      void requestSettings();
      void requestQuit();

   protected:
      virtual void resizeEvent(QResizeEvent* qre) override;

}; // class Front_Page
