
#include "generic_number_input.h"

#include "qt_utils.h"

Generic_Number_Input_Base::Generic_Number_Input_Base(QWidget* parent):
   QWidget(parent)
{
   this->setupUi(this);
   QFont fnt = qt::adjustFontSize(QApplication::font(), 2.0);
   this->lbl_value->setFont(fnt);

   QObject::connect(
            this->bt_delete, &QPushButton::clicked,
            this, &Generic_Number_Input_Base::eraseLeft);
   QObject::connect(
            this->bt_erase_all, &QToolButton::clicked,
            [this]()
   {
      this->value_string = "0";
      this->updateDisplay();
   });

   for(QPushButton* bt: {this->bt_0, this->bt_1, this->bt_2, this->bt_3,
                         this->bt_4, this->bt_5, this->bt_6, this->bt_7,
                         this->bt_8, this->bt_9, this->bt_decimal_separator})
   {
      bt->setFont(fnt);
      QObject::connect(
               bt, &QPushButton::clicked,
               this, &Generic_Number_Input_Base::appendNumber);
   }
}

void Generic_Number_Input_Base::setPrefix(QString const& p)
{
   this->prefix_ = p;
   this->updateDisplay();
}

void Generic_Number_Input_Base::setSuffix(QString const& s)
{
   this->suffix_ = s;
   this->updateDisplay();
}

QString Generic_Number_Input_Base::displayedValue() const
{
   return this->lbl_value->text();
}

void Generic_Number_Input_Base::eraseLeft()
{
   if ( not this->value_string.isEmpty() )
   {
      this->value_string = this->value_string.left(this->value_string.length()-1);
   }
   if ( this->value_string.isEmpty() )
      this->value_string = "0";
   this->updateDisplay();
}

void Generic_Number_Input_Base::appendNumber()
{
   QPushButton* const button = dynamic_cast<QPushButton*>(this->sender());
   if ( button == nullptr )
      return;
   this->doAppendNumber(button->text()[0]);
}

void Generic_Number_Input_Base::updateDisplay()
{
   this->lbl_value->setText(
            QStringLiteral("%1%2%3")
            .arg(this->prefix_, this->value_string, this->suffix_));
}
