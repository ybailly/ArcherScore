
#pragma once

#include <type_traits>

#include <QtWidgets/QWidget>
#include <QtCore/QFutureWatcher>

#include "ui_generic_number_input.h"

class Generic_Number_Input_Base: public QWidget, protected Ui::Generic_Number_Input
{
      Q_OBJECT
   public:
      explicit Generic_Number_Input_Base(QWidget* parent = nullptr);

      void setPrefix(QString const& p);
      void setSuffix(QString const& s);
      
      QString displayedValue() const;

   signals:
      void valueChanged();

   protected:
      void eraseLeft();
      void appendNumber();
      void updateDisplay();
      virtual void doAppendNumber(QChar number) = 0;

      QString value_string;
      QString prefix_;
      QString suffix_;
}; // class Generic_Number_Input_Base

template<typename NUMBER_TYPE, bool IS_INTEGRAL = std::is_integral<NUMBER_TYPE>::value>
class Generic_Number_Input;

template<typename NUMBER_TYPE, bool IS_SIGNED = std::is_signed<NUMBER_TYPE>::value>
struct String_To_Integral_Value;

template<typename NUMBER_TYPE>
struct String_To_Integral_Value<NUMBER_TYPE, true>
{
      static NUMBER_TYPE value(QString const& s)
      { return static_cast<NUMBER_TYPE>(s.toLongLong()); }
};

template<typename NUMBER_TYPE>
struct String_To_Integral_Value<NUMBER_TYPE, false>
{
      static NUMBER_TYPE value(QString const& s)
      { return static_cast<NUMBER_TYPE>(s.toULongLong()); }
};

template<typename NUMBER_TYPE>
class Generic_Number_Input<NUMBER_TYPE, true>: public Generic_Number_Input_Base
{
   public:
      explicit Generic_Number_Input(QWidget* parent):
         Generic_Number_Input_Base(parent)
      {
         this->bt_decimal_separator->setVisible(false);
      }

      void setValue(NUMBER_TYPE i)
      {
         this->value_string = QStringLiteral("%1").arg(i);
         this->updateDisplay();
         emit this->valueChanged();
      }

      NUMBER_TYPE value() const
      {
         return String_To_Integral_Value<NUMBER_TYPE>::value(this->value_string);
      }

   protected:
      virtual void doAppendNumber(QChar number) override
      {
         if ( this->value() == 0 )
         {
            if ( number == '0' )
            {
               return;
            }
            else
            {
               this->value_string = number;
            }
         }
         else
         {
            this->value_string.append(number);
         }
         this->updateDisplay();
         emit this->valueChanged();
      }
}; // class Generic_Number_Input<NUMBER_TYPE>

template<typename NUMBER_TYPE>
class Generic_Number_Input<NUMBER_TYPE, false>: public Generic_Number_Input_Base
{
   public:
      explicit Generic_Number_Input(QWidget* parent):
         Generic_Number_Input_Base(parent)
      {
      }

      void setValue(NUMBER_TYPE d)
      {
         this->value_string = QStringLiteral("%1").arg(d, 0, 'f', this->double_decimals);
         while ( this->value_string.endsWith('0') )
         {
            this->value_string.chop(1);
         }
         if ( this->value_string.endsWith('.') )
         {
            this->value_string.chop(1);
         }
         this->updateDisplay();
         emit this->valueChanged();
      }

      NUMBER_TYPE value() const
      {
         return static_cast<NUMBER_TYPE>(this->value_string.toDouble());
      }

      void setDoubleDecimals(int d)
      {
         if ( d == this->double_decimals )
            return;
         this->double_decimals = d;
         this->setValue(this->value());
      }

   protected:
      virtual void doAppendNumber(QChar number) override
      {
         if ( this->value() < 1.0E-4 )
         {
            if ( number == '.' )
               this->value_string = "0.";
            else
               this->value_string = number;
            this->updateDisplay();
            emit this->valueChanged();
            return;
         }
         int const dot_pos = this->value_string.indexOf('.');
         if ( dot_pos >= 0 )
         {
            if ( number == '.' ) return;
            if ( (this->value_string.length() - dot_pos - 1) >= this->double_decimals ) return;
         }
         this->value_string.append(number);
         this->updateDisplay();
         emit this->valueChanged();
      }

   private:
      int double_decimals{2};
}; // class Generic_Number_Input<double>
