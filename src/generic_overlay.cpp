
#include "generic_overlay.h"

#include <QtDebug>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QApplication>

Generic_Overlay::Generic_Overlay(QWidget* parent):
   QWidget(parent)
{
   this->grid = new QGridLayout(this);
   this->contents_ = new QWidget(this);
   this->contents_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
   this->contents_->setAutoFillBackground(true);

   this->grid->addItem(new QSpacerItem(1,1,QSizePolicy::Minimum, QSizePolicy::Preferred), 0, 1);
   this->grid->addItem(new QSpacerItem(1,1,QSizePolicy::Minimum, QSizePolicy::Preferred), 2, 1);
   this->grid->addItem(new QSpacerItem(1,1,QSizePolicy::Preferred, QSizePolicy::Minimum), 1, 0);
   this->grid->addItem(new QSpacerItem(1,1,QSizePolicy::Preferred, QSizePolicy::Minimum), 1, 2);
   this->grid->addWidget(this->contents_, 1, 1, 1, 1);
   this->adjustMargins(3);

   this->move(0,0);
   this->resize(parent->size());

   QPalette pal = this->palette();
   QPalette const saved_pal = pal;
   pal.setColor(QPalette::Window, QColor(0,0,0,192));
   this->setPalette(pal);
   this->setAutoFillBackground(true);
   this->contents_->setPalette(saved_pal);
}

QWidget* Generic_Overlay::contents() const
{
   return this->contents_;
}

void Generic_Overlay::setContents(QWidget* w)
{
   if ( this->contents_ != nullptr )
   {
      delete this->contents_;
   }
   this->contents_ = w;
   if ( this->contents_ != nullptr )
   {
      this->contents_->setParent(this);
      this->grid->addWidget(this->contents_, 1, 1, 1, 1);
   }
}

void Generic_Overlay::adjustMargins(int f)
{
   this->adjustHorizontalMargins(f);
   this->adjustVerticalMargins(f);
}

void Generic_Overlay::adjustHorizontalMargins(int f)
{
   this->grid->setColumnStretch(0,1);
   this->grid->setColumnStretch(1,f);
   this->grid->setColumnStretch(2,1);
}

void Generic_Overlay::adjustVerticalMargins(int f)
{
   this->grid->setRowStretch(0,1);
   this->grid->setRowStretch(1,f);
   this->grid->setRowStretch(2,1);
}

void Generic_Overlay::run()
{
   this->show();
   this->raise();
}

void Generic_Overlay::discard()
{
   this->hide();
   this->deleteLater();
}
