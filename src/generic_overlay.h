
#pragma once

#include <QtWidgets/QWidget>

class QGridLayout;

class Generic_Overlay: public QWidget
{
      Q_OBJECT
   public:
      explicit Generic_Overlay(QWidget* parent = nullptr);

      QWidget* contents() const;
      void setContents(QWidget* w);

      void adjustMargins(int f);
      void adjustHorizontalMargins(int f);
      void adjustVerticalMargins(int f);

   public slots:
      void run();
      void discard();

   private:
      QWidget* contents_{nullptr};
      QGridLayout* grid{nullptr};
}; // class Generic_Overlay
