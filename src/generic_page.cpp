
#include "generic_page.h"

#include "qt_utils.h"

#include <QtDebug>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtGui/QKeyEvent>
#include <QtGui/QPainter>

Generic_Page::Generic_Page(QWidget* parent):
   QWidget(parent)
{
   QVBoxLayout* main_vbox = new QVBoxLayout(this);
   main_vbox->setMargin(0);
   main_vbox->setSpacing(0);

   QWidget* top_widget = new QWidget(this);
   top_widget->setStyleSheet(R"s(
QWidget { color: rgb(240,248,255); background-color: rgb(96,160,255); }
QPushButton { border: 1px solid rgb(96,160,255); padding-left: 0.25ex; padding-right: 0.25ex; }
QLabel { padding-left: 0ex; padding-right: 0ex; }
                             )s");
   top_widget->setAutoFillBackground(true);
   top_widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

   this->bt_back = new QPushButton(top_widget);
   this->bt_back->setIcon(qt::icon("back_arrow_white"));
   this->bt_back->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
   QObject::connect(this->bt_back, &QPushButton::clicked,
                    this, &Generic_Page::backRequested);
   this->bt_forward = new QPushButton(top_widget);
   this->bt_forward->setIcon(qt::icon("forward_arrow_white"));
   this->bt_forward->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
   QObject::connect(this->bt_forward, &QPushButton::clicked,
                    this, &Generic_Page::forwardRequested);

   QFont fnt = this->font();
   fnt.setItalic(true);
   this->lbl_subtitle = new QLabel(top_widget);
   this->lbl_subtitle->setWordWrap(false);
   this->lbl_subtitle->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
   this->lbl_subtitle->setFont(fnt);
   fnt.setItalic(false);
   fnt.setBold(true);
   this->lbl_title = new QLabel(top_widget);
   this->lbl_title->setWordWrap(false);
   this->lbl_title->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
   if ( fnt.pointSizeF() > 0.0 )
   {
      fnt.setPointSizeF(fnt.pointSizeF()*1.15);
   }
   else if ( fnt.pointSize() > 0 )
   {
      fnt.setPointSize(qRound(fnt.pointSize()*1.15));
   }
   else if ( fnt.pixelSize() > 0 )
   {
      fnt.setPixelSize(qRound(fnt.pixelSize()*1.15));
   }
   this->lbl_title->setFont(fnt);

   QHBoxLayout* top_hbox = new QHBoxLayout(top_widget);
   top_hbox->setMargin(0);
   top_hbox->setSpacing(0);
   top_hbox->addWidget(this->bt_back);
   QVBoxLayout* title_vbox = new QVBoxLayout;
   title_vbox->setMargin(0);
   title_vbox->setSpacing(0);
   title_vbox->addWidget(this->lbl_title);
   title_vbox->addWidget(this->lbl_subtitle);
   top_hbox->addLayout(title_vbox);
   top_hbox->addWidget(this->bt_forward);
   main_vbox->addWidget(top_widget);

   this->contents_ = new QWidget(this);
   this->contents_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   main_vbox->addWidget(this->contents_);

   this->setFixedSize(this->parentWidget()->size());
   this->move(this->parentWidget()->width(), 0);
   this->parentWidget()->installEventFilter(this);
}

void Generic_Page::setTitle(QString const& t)
{
   this->lbl_title->setText(t);
}

void Generic_Page::setSubTitle(QString const& st)
{
   this->lbl_subtitle->setText(st);
}

void Generic_Page::setBackVisible(bool v)
{
   this->bt_back->setVisible(v);
}

void Generic_Page::setForwardVisible(bool v)
{
   this->bt_forward->setVisible(v);
}

void Generic_Page::startSlide()
{
   this->is_sliding = true;
   this->slidingStart();
}

void Generic_Page::endSlide()
{
   this->is_sliding = false;
   this->slidingEnd();
}

bool Generic_Page::eventFilter(QObject* qo, QEvent* qe)
{
   QWidget* w = dynamic_cast<QWidget*>(qo);
   QResizeEvent* qre = dynamic_cast<QResizeEvent*>(qe);
   if ( (w == nullptr) or
        (w != this->parentWidget()) or
        (qre == nullptr) )
   {
      return this->QWidget::eventFilter(qo, qe);
   }
   this->setFixedSize(qre->size());
   if ( this->x() > 0 )
   {
      this->move(qre->size().width(), 0);
   }
   return false;
}

void Generic_Page::slidingStart()
{ }

void Generic_Page::slidingEnd()
{ }

QWidget* Generic_Page::contents() const
{
   return this->contents_;
}

void Generic_Page::keyPressEvent(QKeyEvent* qke)
{
   if ( (qke->key() == Qt::Key_Escape) or
        (qke->key() == Qt::Key_Back) )
   {
      qke->accept();
      emit this->backRequested();
   }
   else
   {
      this->QWidget::keyPressEvent(qke);
   }
}
