
#pragma once

#include <QtWidgets/QWidget>

class QLabel;
class QPushButton;

class Generic_Page: public QWidget
{
      Q_OBJECT
   public:
      explicit Generic_Page(QWidget* parent = nullptr);

      void setTitle(QString const& t);
      void setSubTitle(QString const& st);

      void setBackVisible(bool v);
      void setForwardVisible(bool v);

      void startSlide();
      void endSlide();

      virtual bool eventFilter(QObject* qo, QEvent* qe) override;

      virtual void slidingStart();
      virtual void slidingEnd();

   signals:
      void backRequested();
      void forwardRequested();

   protected:
      QWidget* contents() const;
      virtual void keyPressEvent(QKeyEvent* qke) override;

   private:
      QWidget* contents_{nullptr};
      QPushButton* bt_back{nullptr};
      QLabel* lbl_title{nullptr};
      QLabel* lbl_subtitle{nullptr};
      QPushButton* bt_forward{nullptr};
      bool is_sliding{false};

}; // class Generic_Page
