
#include "geometry_utils.h"

#include "miniball.h"

#include <glm/glm.hpp>

void computeGroup(Point_Set const& pts,
                  QPointF* grp_center,
                  double* grp_diam)
{
   if ( pts.empty() )
   {
      if ( grp_center != nullptr )
         *grp_center = QPointF(0.0,0.0);
      if ( grp_diam != nullptr )
         *grp_diam = -1.0;
      return;
   }
   if ( pts.size() == 1 )
   {
      if ( grp_center != nullptr )
         *grp_center = QPointF(pts[0].c[0], pts[0].c[1]);
      if ( grp_diam != nullptr )
         *grp_diam = 0.0;
      return;
   }
   using CoordAccesser = Miniball::CoordAccessor<std::vector<Pt2d>::const_iterator, double const*>;
   Miniball::Miniball<CoordAccesser> mb(2, pts.begin(), pts.end());
   if ( grp_center != nullptr )
   {
      *grp_center = QPointF(*(mb.center()), *(mb.center()+1));
   }
   if ( grp_diam != nullptr )
   {
      *grp_diam = 2.0 * std::sqrt(mb.squared_radius());
   }
}

QTransform distorsionFromCorners(std::array<QPointF const, 4> const& src_,
                                 std::array<QPointF const, 4> const& dst_)
{
   // borrowed from https://math.stackexchange.com/questions/296794/finding-the-transform-matrix-from-4-projected-points-with-javascript
   glm::dvec3 const src[4] {
      {src_[0].x(), src_[0].y(), 1.0},
      {src_[1].x(), src_[1].y(), 1.0},
      {src_[2].x(), src_[2].y(), 1.0},
      {src_[3].x(), src_[3].y(), 1.0},
   };
   glm::dvec3 const dst[4] {
      {dst_[0].x(), dst_[0].y(), 1.0},
      {dst_[1].x(), dst_[1].y(), 1.0},
      {dst_[2].x(), dst_[2].y(), 1.0},
      {dst_[3].x(), dst_[3].y(), 1.0},
   };
   glm::dmat3 const tmp_src(src[0], src[1], src[2]);
   glm::dvec3 const sf = glm::inverse(tmp_src) * src[3];
   glm::dmat3 const A(src[0]*sf.x, src[1]*sf.y, src[2]*sf.z);
   glm::dmat3 const tmp_dst(dst[0], dst[1], dst[2]);
   glm::dvec3 const df = glm::inverse(tmp_dst) * dst[3];
   glm::dmat3 const B(dst[0]*df.x, dst[1]*df.y, dst[2]*df.z);
   glm::dmat3 const C = B * glm::inverse(A);
   return
         QTransform(
            C[0][0], C[0][1], C[0][2],
            C[1][0], C[1][1], C[1][2],
            C[2][0], C[2][1], C[2][2]);
}
