
#pragma once

#include <QtCore/QPointF>
#include <QtGui/QTransform>

#include <vector>

struct Pt2d
{
      double c[2];
      operator double const*() const
      { return &(c[0]); }
}; // struct Pt2d;

using Point_Set = std::vector<Pt2d>;

void computeGroup(Point_Set const& pts,
                  QPointF* grp_center,
                  double* grp_diam);

QTransform distorsionFromCorners(std::array<QPointF const, 4> const& src,
                                 std::array<QPointF const, 4> const& dst);
