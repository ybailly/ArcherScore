
#include "hit_location_editor.h"

#include "common_types_utils.h"
#include "session.h"
#include "target_scene.h"
#include "settings.h"
#include "target_view.h"
#include "qt_utils.h"
#include "ui_utils.h"

#include <QtDebug>
#include <QtCore/QVariantAnimation>
#include <QtWidgets/QLabel>

Hit_Location_Editor::Hit_Location_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);

   QObject::connect(this->bt_radial_lines, &QPushButton::toggled,
                    [this](bool on)
   {
      if ( on ) this->target_view->showRadialLines();
      else this->target_view->hideRadialLines();
      Settings::Target_View::setShowRadialLines(on);
   });
   QObject::connect(
            this->bt_to_higher_ring, &QPushButton::clicked,
            this, &Hit_Location_Editor::onToHigherRing);
   QObject::connect(
            this->bt_to_lower_ring, &QPushButton::clicked,
            this, &Hit_Location_Editor::onToLowerRing);
   QObject::connect(
            this->bt_to_undefined, &QPushButton::clicked,
            this, &Hit_Location_Editor::onToUndefined);
   QObject::connect(
            this->bt_to_zero, &QPushButton::clicked,
            this, &Hit_Location_Editor::onToZero);

   QObject::connect(this->target_view, &Target_View::dynamicItemStart,
                    this, &Hit_Location_Editor::onDynamicItemStart);
   QObject::connect(this->target_view, &Target_View::dynamicItemMoved,
                    this, &Hit_Location_Editor::onDynamicItemMoved);
   QObject::connect(this->target_view, &Target_View::dynamicItemEnd,
                    this, &Hit_Location_Editor::onDynamicItemEnd);
   this->bt_radial_lines->setChecked(Settings::Target_View::showRadialLines());
}

void Hit_Location_Editor::setSession(Session const* sd)
{
   this->session_ = sd;
   int8_t const min_score = this->session_->targetMinimumScore();
   this->target_view->setSpotRadius(
            Radius(this->session_->arrowSizeSpecification().absoluteDiameter().value() /
                   (10.0*this->session_->targetDiameter().v1().value())));
   this->target_view->setupTarget(min_score);
   this->target_view->fitInView(this->target_view->scene()->sceneRect(), Qt::KeepAspectRatio);
}

Session const* Hit_Location_Editor::session() const
{
   return this->session_;
}

void Hit_Location_Editor::setHitLocations(std::vector<Score_Unit> const& hl)
{
   this->hit_locations = hl;
   if ( this->hit_locations.size() < this->hit_buttons.size() )
   {
      for(size_t idx = this->hit_locations.size();
          idx < this->hit_buttons.size();
          ++idx)
      {
         Hit_Button& hb = this->hit_buttons[idx];
         hb.hit_title->setVisible(false);
         hb.hit_button->setVisible(false);
         this->target_view->hideStaticSpot(idx);
      }
   }
   this->hit_buttons.resize(this->hit_locations.size());
   QFont title_font = qt::adjustFontSize(this->font(), 0.8);
   title_font.setBold(true);
   for(size_t ind_hit_loc = 0; ind_hit_loc < this->hit_locations.size(); ++ind_hit_loc)
   {
      Score_Unit const& su = this->hit_locations[ind_hit_loc];
      Hit_Button& hb = this->hit_buttons[ind_hit_loc];
      if ( hb.hit_title == nullptr )
      {
         hb.hit_title = new QLabel(tr("F%1").arg(ind_hit_loc+1), this->w_hit_buttons);
         hb.hit_title->setAlignment(Qt::AlignCenter);
         hb.hit_title->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
         hb.hit_title->setFont(title_font);
         hb.hit_title->setPalette(this->palette());
         hb.hit_title->setMargin(1);
         this->hit_buttons_layout->addWidget(hb.hit_title, 0, ind_hit_loc);
         hb.hit_button = new QPushButton(this->w_hit_buttons);
         hb.hit_button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
         hb.hit_button->setProperty("hit_index", static_cast<qulonglong>(ind_hit_loc));
         this->hit_buttons_layout->addWidget(hb.hit_button, 1, ind_hit_loc);
         QObject::connect(
                  hb.hit_button, &QPushButton::clicked,
                  this, &Hit_Location_Editor::onHitButtonClicked);

      }
      hb.hit_title->setVisible(true);
      hb.hit_button->setVisible(true);
      this->adjustHitButton(ind_hit_loc);
      this->target_view->addStaticSpot(ind_hit_loc, toQPointF(su.location()));
      this->target_view->setStaticSpotEnabled(ind_hit_loc, false);
      this->target_view->setStaticSpotVisible(
               ind_hit_loc,
               su.isLocationDefined() and (su.score() > 0));
   }
   if ( this->hit_locations.size() == 1 )
   {
      this->setActiveHitButton(Invalid_Score_Index.value());
      this->setActiveHitButton(0);
      this->hit_buttons[0].hit_title->hide();
   }
   else
   {
      this->setActiveHitButton(0);
      this->setActiveHitButton(Invalid_Score_Index.value());
   }
}

std::vector<Score_Unit> const& Hit_Location_Editor::hitLocations() const
{
   return this->hit_locations;
}

void Hit_Location_Editor::adjustHitButton(size_t index)
{
   if ( index >= this->hit_buttons.size() )
   {
      return;
   }
   Hit_Button& hb = this->hit_buttons[index];
   QPushButton* bt = hb.hit_button;
   Score_Unit const& su = this->hit_locations[index];
   bool const is_active = (index == this->active_hit_button);
   QString const score_str = markToString(su.mark());
   if ( is_active )
   {
      bt->setText(QStringLiteral("%1%2").arg(QChar(0x25CF)).arg(score_str));
   }
   else
   {
      bt->setText(score_str);
   }
   if ( su.isLocationDefined() )
   {
      QPalette pal = bt->palette();
      pal.setBrush(QPalette::Button, textBackgroundColorForMark(su.mark()));
      pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(su.mark()));
      bt->setPalette(pal);
   }
   else
   {
      QPalette pal = bt->palette();
      pal.setBrush(QPalette::Button, QColor(Qt::lightGray));
      pal.setBrush(QPalette::ButtonText, QColor(Qt::black));
      bt->setPalette(pal);
   }
   QLabel* lbl = hb.hit_title;
   QPalette pal = this->palette();
   if ( index == this->active_hit_button )
   {
      lbl->setAutoFillBackground(true);
      pal.setBrush(QPalette::Window, QColor(96,255,128));
   }
   else
   {
      lbl->setAutoFillBackground(false);
   }
   lbl->setPalette(pal);
   this->bt_to_undefined->setEnabled(su.isLocationDefined());
   this->bt_to_zero->setEnabled((not su.isLocationDefined()) or (su.score() > 0));
   if ( su.isLocationDefined() and (su.score() < 10) )
   {
      int8_t new_score = su.score()+1;
      QPalette pal = this->bt_to_lower_ring->palette();
      pal.setBrush(QPalette::Button, textBackgroundColorForMark(scoreToMark(new_score)));
      pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(scoreToMark(new_score)));
      this->bt_to_lower_ring->setPalette(pal);
      this->bt_to_lower_ring->setText(QStringLiteral("%1%2")
                                      .arg(QChar(0x2B06))
                                      .arg(new_score));
      this->bt_to_lower_ring->setEnabled(true);
   }
   else
   {
      this->bt_to_lower_ring->setText(QStringLiteral("%1").arg(QChar(0x2B06)));
      this->bt_to_lower_ring->setEnabled(false);
   }
   if ( su.isLocationDefined() and (su.score() > 1) )
   {
      int8_t new_score = su.score()-1;
      QPalette pal = this->bt_to_higher_ring->palette();
      pal.setBrush(QPalette::Button, textBackgroundColorForMark(scoreToMark(new_score)));
      pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(scoreToMark(new_score)));
      this->bt_to_higher_ring->setPalette(pal);
      this->bt_to_higher_ring->setText(QStringLiteral("%1%2")
                                      .arg(QChar(0x2B07))
                                      .arg(new_score));
      this->bt_to_higher_ring->setEnabled(true);
   }
   else
   {
      this->bt_to_higher_ring->setText(QStringLiteral("%1").arg(QChar(0x2B07)));
      this->bt_to_higher_ring->setEnabled(false);
   }
}

void Hit_Location_Editor::setActiveHitButton(size_t index)
{
   if ( index == this->active_hit_button )
   {
      return;
   }
   size_t const old_active_hit_button = this->active_hit_button;
   this->active_hit_button = index;
   this->adjustHitButton(old_active_hit_button);
   this->adjustHitButton(this->active_hit_button);
   if ( old_active_hit_button < this->hit_locations.size() )
   {
      this->target_view->disableStaticSpot(old_active_hit_button);
      Score_Unit const& su = this->hit_locations[old_active_hit_button];
      this->target_view->moveStaticSpot(
               old_active_hit_button,
               toQPointF(su.location()));
      this->target_view->setStaticSpotVisible(
               old_active_hit_button,
               su.isLocationDefined() and (su.score() > 0));
      this->target_view->disableStaticSpot(old_active_hit_button);
   }
   if ( this->active_hit_button < this->hit_locations.size() )
   {
      this->target_view->enableStaticSpot(this->active_hit_button);
      Score_Unit const& su = this->hit_locations[this->active_hit_button];
      if ( su.isLocationDefined() and (su.score() > 0) )
      {
         this->target_view->showStaticSpot(this->active_hit_button);
         this->target_view->activateDynamicSpot(toQPointF(su.location()));
         this->target_view->setDynamicSpotPosition(toQPointF(su.location()));
      }
      else
      {
         this->target_view->hideStaticSpot(this->active_hit_button);
         this->target_view->activateDynamicSpot({-1.0, 1.0});
      }
      this->bt_to_undefined->setEnabled(su.isLocationDefined());
      this->bt_to_zero->setEnabled((not su.isLocationDefined()) or (su.score() > 0));
      this->bt_to_lower_ring->setEnabled(su.isLocationDefined() and (su.score() < 10));
      this->bt_to_higher_ring->setEnabled(su.isLocationDefined() and (su.score() > 1));
      this->is_first_dynamic_hit = true;
   }
   else
   {
      this->target_view->deactivateDynamicSpot();
      this->bt_to_undefined->setEnabled(false);
      this->bt_to_zero->setEnabled(false);
      this->bt_to_lower_ring->setEnabled(false);
      this->bt_to_higher_ring->setEnabled(false);
   }
   this->target_view->zoomOnAll();
}

void Hit_Location_Editor::adjustScoreFromDynamicSpot()
{
   if ( this->active_hit_button >= this->hit_locations.size() )
   {
      return;
   }
   Score_Unit& su = this->hit_locations[this->active_hit_button];
   int8_t const score = this->target_view->scoreForPosition(this->target_view->dynamicSpotPosition());
   Hit_Location const hit_loc = toHitLocation(this->target_view->dynamicSpotPosition());
   if ( this->session_->is10Plus(hit_loc) )
   {
      su.set(Mark::TenPlus, hit_loc);
   }
   else
   {
      su.set(scoreToMark(score), hit_loc);
   }
   this->adjustHitButton(this->active_hit_button);
}

void Hit_Location_Editor::onHitButtonClicked()
{
   QPushButton* const bt = dynamic_cast<QPushButton*>(this->sender());
   if ( bt == nullptr )
   {
      return;
   }
   QVariant const var = bt->property("hit_index");
   if ( not var.isValid() )
   {
      return;
   }
   size_t const hit_index = var.toUInt();
   if ( hit_index == this->active_hit_button )
   {
      this->setActiveHitButton(Invalid_Score_Index.value());
   }
   else
   {
      this->setActiveHitButton(hit_index);
   }
}

void Hit_Location_Editor::onDynamicItemStart(QPointF const& p)
{
   this->target_view->setDynamicSpotPosition(p);
   if ( not this->is_first_dynamic_hit )
   {
      this->adjustScoreFromDynamicSpot();
   }
}

void Hit_Location_Editor::onDynamicItemMoved(QPointF const& p)
{
   this->target_view->setDynamicSpotPosition(p);
   if ( not this->is_first_dynamic_hit )
   {
      this->adjustScoreFromDynamicSpot();
   }
}

void Hit_Location_Editor::onDynamicItemEnd(QPointF const& p)
{
   if ( this->is_first_dynamic_hit )
   {
      double const margin = 2.5*this->target_view->ringWidth();
      QRectF const zoom_rect(p.x()-margin, p.y()-margin, 2.0*margin, 2.0*margin);
      this->target_view->zoomOnRect(zoom_rect);
      this->target_view->applyMouseOffset();
      this->is_first_dynamic_hit = false;
   }
   this->target_view->setDynamicSpotPosition(p);
   this->adjustScoreFromDynamicSpot();
}

void Hit_Location_Editor::onToHigherRing()
{
   if ( this->active_hit_button >= this->hit_locations.size() )
   {
      return;
   }
   Score_Unit& su = this->hit_locations[this->active_hit_button];
   if ( su.score() < 2 )
   {
      return;
   }
   QPointF const curr_dyn_pos = this->target_view->dynamicSpotPosition();
   QPointF const curr_dyn_vect = curr_dyn_pos / std::sqrt(QPointF::dotProduct(curr_dyn_pos,curr_dyn_pos));
   double const spot_radius = this->target_view->spotRadius().value();
   double const new_dist =
         this->target_view->ringWidth() * (11-su.score()) + 1.1*spot_radius;
   this->target_view->setDynamicSpotPosition(new_dist * curr_dyn_vect);
   this->adjustScoreFromDynamicSpot();
}

void Hit_Location_Editor::onToLowerRing()
{
   if ( this->active_hit_button >= this->hit_locations.size() )
   {
      return;
   }
   Score_Unit& su = this->hit_locations[this->active_hit_button];
   if ( su.score() >= 10 )
   {
      return;
   }
   QPointF const curr_dyn_pos = this->target_view->dynamicSpotPosition();
   QPointF const curr_dyn_vect = curr_dyn_pos / std::sqrt(QPointF::dotProduct(curr_dyn_pos,curr_dyn_pos));
   double const spot_radius = this->target_view->spotRadius().value();
   double const new_dist =
         this->target_view->ringWidth() * (10-su.score()) + 0.9*spot_radius;
   this->target_view->setDynamicSpotPosition(new_dist * curr_dyn_vect);
   this->adjustScoreFromDynamicSpot();
}

void Hit_Location_Editor::onToUndefined()
{
   if ( this->active_hit_button >= this->hit_locations.size() )
   {
      return;
   }
   Score_Unit& su = this->hit_locations[this->active_hit_button];
   su = {};
   this->target_view->setDynamicSpotPosition({-2.0,-2.0});
   this->adjustHitButton(this->active_hit_button);
   this->target_view->zoomOnAll();
   this->is_first_dynamic_hit = true;
}

void Hit_Location_Editor::onToZero()
{
   if ( this->active_hit_button >= this->hit_locations.size() )
   {
      return;
   }
   Score_Unit& su = this->hit_locations[this->active_hit_button];
   su.set(Mark::Zero);
   this->adjustHitButton(this->active_hit_button);
   this->target_view->zoomOnAll();
   this->is_first_dynamic_hit = true;
}
