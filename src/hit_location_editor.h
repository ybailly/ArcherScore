
#pragma once

#include "common_types.h"
#include "score_unit.h"

#include "generic_page.h"

#include "ui_hit_location_editor.h"

#include <array>

class Session;

class Hit_Location_Editor: public Generic_Page, private Ui::Hit_Location_Editor
{
      Q_OBJECT
   public:
      explicit Hit_Location_Editor(QWidget* parent = nullptr);

      void setSession(Session const* sd);
      Session const* session() const;

      void setHitLocations(std::vector<Score_Unit> const& hl);
      std::vector<Score_Unit> const& hitLocations() const;

   protected slots:
      void onHitButtonClicked();
      void onDynamicItemStart(QPointF const& p);
      void onDynamicItemMoved(QPointF const& p);
      void onDynamicItemEnd(QPointF const& p);
      void onToHigherRing();
      void onToLowerRing();
      void onToUndefined();
      void onToZero();

   protected:
      Mark selectedMark() const;
      Hit_Location selectedLocation() const;
      void adjustHitButton(size_t index);
      void setActiveHitButton(size_t index);
      void adjustScoreFromDynamicSpot();

   private:
      struct Hit_Button
      {
            QLabel* hit_title{nullptr};
            QPushButton* hit_button{nullptr};
      }; // struct Hit_Buttons
      std::vector<Score_Unit> hit_locations;
      std::vector<Hit_Button> hit_buttons;
      Session const* session_{nullptr};
      size_t active_hit_button{Invalid_Score_Index.value()};
      bool is_first_dynamic_hit{true};
}; // class Hit_Location_Editor
