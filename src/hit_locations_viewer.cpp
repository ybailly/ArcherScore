
#include "hit_locations_viewer.h"

#include "common_types_utils.h"
#include "session.h"
#include "ends_round.h"
#include "score_unit.h"
#include "target_scene.h"

#ifdef ANDROID
#include "android_sms.h"
#endif

#include <QtDebug>
#include <QtWidgets/QToolButton>

namespace
{
   QColor const Shown_End_Button_Back_Color(160,255,160);
}

Hit_Locations_Viewer::Hit_Locations_Viewer(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);

   QObject::connect(this->bt_show_groups, &QPushButton::toggled,
                    this, &Hit_Locations_Viewer::setShowGroups);
   QObject::connect(this->bt_show_global_group, &QPushButton::toggled,
                    this, &Hit_Locations_Viewer::setShowGlobalGroup);
}

void Hit_Locations_Viewer::setSession(Session const* sd)
{
   this->session_ = sd;
   this->target_view->setSpotRadius(
            Radius(
               this->session_->arrowSizeSpecification().absoluteDiameter().value() /
               (this->session_->targetDiameter().v1().value()*10.0)
               ));
   this->target_view->setupTarget(this->session_->targetMinimumScore());
   this->target_view->fitInView(this->target_view->scene()->sceneRect(), Qt::KeepAspectRatio);
}

Session const* Hit_Locations_Viewer::session() const
{
   return this->session_;
}

namespace
{
   int8_t find_min_score(Single_End const& se)
   {
      int8_t min_score = 8;
      for(Score_Unit const& su: se)
      {
         if ( (su.mark() != Mark::Undefined) and (su.score() > 0))
            min_score = std::min(su.score(), min_score);
      }
      return min_score;
   }
}

void Hit_Locations_Viewer::showEnd(Round_Index ends_round_index,
                                   End_Index end_index)
{
   this->target_view->removeAllStaticSpots();
   this->internalShowEnd(ends_round_index, end_index);
   this->shown_end = end_index;
   this->shown_ends_round = ends_round_index;
   this->bt_show_groups->setChecked(false);
   this->bt_show_global_group->setChecked(false);
   this->bt_show_groups->setText(tr("Groupe"));
   this->bt_show_global_group->setVisible(false);
   // zooming on end
   Single_End const& se = this->session_->endsRound(ends_round_index).singleEnd(end_index);
   int8_t const min_score = find_min_score(se);
   this->target_view->zoomOnRect(this->target_view->rectForScore(min_score).marginsAdded({0.05,0.05,0.05,0.05}));
}

namespace
{
   int8_t find_min_score(Ends_Round const& er)
   {
      int8_t min_score = 8;
      for(Single_End const& se: er)
      {
         min_score = std::min(find_min_score(se), min_score);
      }
      return min_score;
   }
}

void Hit_Locations_Viewer::showEndsRound(Round_Index ends_round_index)
{
   this->target_view->removeAllStaticSpots();
   this->internalShowEndsRound(ends_round_index);
   this->shown_end = Invalid_End_Index;
   this->shown_ends_round = ends_round_index;
   this->bt_show_groups->setChecked(false);
   this->bt_show_global_group->setChecked(false);
   this->bt_show_groups->setText(tr("Groupes\npar volée"));
   this->bt_show_global_group->setVisible(true);
   //
   Ends_Round const& round = this->session_->endsRound(ends_round_index);
   int8_t const min_score = find_min_score(round);
   this->target_view->zoomOnRect(this->target_view->rectForScore(min_score).marginsAdded({0.05,0.05,0.05,0.05}));
   //
   for(QPushButton*& bt: this->ends_buttons)
   {
      delete bt;
      bt = nullptr;
   }
   this->ends_buttons.resize(round.endsCount());
   this->ends_buttons_palettes.resize(round.endsCount());
   this->visible_ends.resize(round.endsCount());
   int row = 0;
   int col = 0;
   for(End_Index end_idx(0); end_idx < round.endsCount(); ++end_idx)
   {
      QPushButton* bt = new QPushButton(this->w_rounds_buttons);
      bt->setText(QStringLiteral("%1").arg(end_idx.value()+1));
      bt->setSizePolicy(QSizePolicy::Expanding, bt->sizePolicy().verticalPolicy());
      QPalette pal = bt->palette();
      this->ends_buttons_palettes[end_idx.value()] = pal;
      pal.setBrush(QPalette::Button, Shown_End_Button_Back_Color);
      bt->setPalette(pal);
      bt->setProperty("end_index", end_idx.value());
      QObject::connect(
               bt, &QPushButton::clicked,
               this, &Hit_Locations_Viewer::displayHideEnd);
      this->w_rounds_buttons_layout->addWidget(bt, row, col);
      this->ends_buttons[end_idx.value()] = bt;
      this->visible_ends[end_idx.value()] = 1;
      col += 1;
      if ( col >= 6 )
      {
         row += 1;
         col = 0;
      }
   }
}

Round_Index Hit_Locations_Viewer::shownRound() const
{
   return this->shown_ends_round;
}

End_Index Hit_Locations_Viewer::shownEnd() const
{
   return this->shown_end;
}

void Hit_Locations_Viewer::internalShowEnd(Round_Index ri,
                                           End_Index ei)
{
   Single_End const& se = this->session_->endsRound(ri).singleEnd(ei);
   Score_Index score_index{0};
   for(Score_Unit const& su: se)
   {
      if ( not su.isLocationDefined() )
      {
         continue;
      }
      Hit_Location const hit_loc = su.location();
      QPointF const pt = toQPointF(hit_loc);
      auto const id = makeId(Invalid_Session_Index, ri, ei, score_index);
      this->target_view->addStaticSpot(id, pt);
      this->target_view->setStaticSpotEnabled(id, su.isEnabledForGroup());
      ++score_index;
   }
}

void Hit_Locations_Viewer::internalShowEndsRound(Round_Index ri)
{
   Ends_Round const& er = this->session_->endsRound(ri);
   for(End_Index end_index{0}; end_index.value() < er.endsCount(); ++end_index)
   {
      this->internalShowEnd(ri, end_index);
   }
}

void Hit_Locations_Viewer::internalShowGroupForEnd(Round_Index ends_round_index,
                                                   End_Index end_index)
{
   Single_End const& se = this->session()->endsRound(ends_round_index).singleEnd(end_index);
   double const rad = se.groupDiameter().value() * 0.5;
   if ( rad > 0.0 )
   {
      QPointF const center = se.groupCenter();
      auto const id = makeId(Invalid_Session_Index, ends_round_index, end_index, Invalid_Score_Index);
      this->target_view->addDisc(id, center, rad, QColor(128,128,255));
      if ( this->visible_ends[end_index.value()] == 0 )
         this->target_view->hideDisc(id);
      else
         this->target_view->showDisc(id);
   }
}

void Hit_Locations_Viewer::internalShowGroups()
{
   if ( this->shown_end != Invalid_End_Index )
   {
      this->internalShowGroupForEnd(this->shown_ends_round, this->shown_end);
   }
   else if ( this->shown_ends_round != Invalid_Round_Index )
   {
      Ends_Round const& ends_round = this->session_->endsRound(this->shown_ends_round);
      for(End_Index end_index{0}; end_index.value() < ends_round.endsCount(); ++end_index)
      {
         this->internalShowGroupForEnd(this->shown_ends_round, end_index);
      }
   }
   else
   {
      for(Round_Index ends_round_index{0}; ends_round_index.value() < this->session_->endsRoundsCount(); ++ends_round_index)
      {
         Ends_Round const& ends_round = this->session_->endsRound(ends_round_index);
         for(End_Index end_index{0}; end_index.value() < ends_round.endsCount(); ++end_index)
         {
            this->internalShowGroupForEnd(ends_round_index, end_index);
         }
      }
   }
}

void Hit_Locations_Viewer::internalShowGlobalGroup()
{
   if ( this->shown_ends_round != Invalid_Round_Index )
   {
      Ends_Round const& ends_round = this->session_->endsRound(this->shown_ends_round);
      double const rad = ends_round.groupDiameterGlobal().value() *0.5;
      QPointF const center = ends_round.groupCenterGlobal();
      this->target_view->addDisc(
               makeId(Invalid_Session_Index, this->shown_ends_round, Invalid_End_Index, Invalid_Score_Index),
               center, rad, QColor(128,128,255));
   }
   else
   {
      Round_Index ends_round_index{0};
      for(Ends_Round const& ends_round: *this->session_)
      {
         double const rad = ends_round.groupDiameterGlobal().value() * 0.5;
         QPointF const center = ends_round.groupCenterGlobal();
         this->target_view->addDisc(
                  makeId(Invalid_Session_Index, ends_round_index, Invalid_End_Index, Invalid_Score_Index),
                  center, rad, QColor(128,128,255));
         ++ends_round_index;
      }
   }
   // what about a global global group?
}

void Hit_Locations_Viewer::displayHideEnd()
{
   QPushButton* bt = dynamic_cast<QPushButton*>(this->sender());
   if ( bt == nullptr )
      return;
   unsigned const end_idx = bt->property("end_index").toUInt();
   End_Index const end_index(end_idx);
   if ( this->visible_ends[end_idx] == 0 )
   {
      // displaying an end
      this->target_view->showDisc(makeId(Invalid_Session_Index, this->shown_ends_round, end_index, Invalid_Score_Index));
      for(Score_Index score_index(0); score_index < this->session_->shotCount(); ++score_index)
      {
         this->target_view->showStaticSpot(makeId(Invalid_Session_Index, this->shown_ends_round, end_index, score_index));
      }
      QPalette pal = this->ends_buttons_palettes[end_idx];
      pal.setBrush(QPalette::Button, Shown_End_Button_Back_Color);
      bt->setPalette(pal);
      this->visible_ends[end_idx] = 1;
   }
   else
   {
      // hiding an end
      this->target_view->hideDisc(makeId(Invalid_Session_Index, this->shown_ends_round, end_index, Invalid_Score_Index));
      for(Score_Index score_index(0); score_index < this->session_->shotCount(); ++score_index)
      {
         this->target_view->hideStaticSpot(makeId(Invalid_Session_Index, this->shown_ends_round, end_index, score_index));
      }
      bt->setPalette(this->ends_buttons_palettes[end_idx]);
      this->visible_ends[end_idx] = 0;
   }
}

void Hit_Locations_Viewer::setShowGroups(bool on)
{
   this->target_view->removeAllDiscs();
   if ( on )
   {
      this->target_view->fadeAll();
      this->internalShowGroups();
   }
   if ( this->bt_show_global_group->isChecked() )
   {
      this->target_view->fadeAll();
      this->internalShowGlobalGroup();
   }
   else if ( not on )
   {
      this->target_view->unfadeAll();
   }
}

void Hit_Locations_Viewer::setShowGlobalGroup(bool on)
{
   this->target_view->removeAllDiscs();
   if ( this->bt_show_groups->isChecked() )
   {
      this->target_view->fadeAll();
      this->internalShowGroups();
   }
   if ( on )
   {
      this->target_view->fadeAll();
      this->internalShowGlobalGroup();
   }
   else if ( not this->bt_show_groups->isChecked() )
   {
      this->target_view->unfadeAll();
   }
}
