
#pragma once

#include "generic_page.h"

#include "ui_hit_locations_viewer.h"

class QToolButton;
class Target_Scene;
class Session;
class Ends_Round;
class Single_End;

class Hit_Locations_Viewer: public Generic_Page, private Ui::Hit_Locations_Viewer
{
      Q_OBJECT
   public:
      explicit Hit_Locations_Viewer(QWidget* parent = nullptr);

      void setSession(Session const* sd);
      Session const* session() const;

      void showEnd(Round_Index ends_round_index,
                   End_Index end_index);
      void showEndsRound(Round_Index ends_round_index);

      Round_Index shownRound() const;
      End_Index shownEnd() const;

   public slots:
      void setShowGroups(bool on);
      void setShowGlobalGroup(bool on);

   protected:
      void internalShowEnd(Round_Index ri,
                           End_Index ei);
      void internalShowEndsRound(Round_Index ri);
      void internalShowGroupForEnd(Round_Index ends_round_index,
                                   End_Index end_index);
      void internalShowGroups();
      void internalShowGlobalGroup();

      void displayHideEnd();

   private:
      std::vector<QPushButton*> ends_buttons;
      std::vector<QPalette> ends_buttons_palettes;
      std::vector<uint8_t> visible_ends;
      Session const* session_{nullptr};
      Round_Index shown_ends_round{Invalid_Round_Index};
      End_Index shown_end{Invalid_End_Index};
}; // class Hit_Locations_Viewer
