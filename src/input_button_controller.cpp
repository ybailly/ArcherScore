
#include "input_button_controller.h"

Input_Button_Controller_Base::Input_Button_Controller_Base(QObject* parent):
   QObject(parent)
{ }

template class Input_Button_Controller<Distance_M_Yd>;
template class Input_Button_Controller<Diameter_Cm_In>;
template class Input_Button_Controller<Length_Cm_In>;
template class Input_Button_Controller<Weight_G_Oz>;
template class Input_Button_Controller<Weight_Kg_Lb>;
