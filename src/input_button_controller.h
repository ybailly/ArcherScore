
#pragma once

#include <QtCore/QObject>

#include "overlay_generic_number_input.h"
#include "overlay_tagged_value_input.h"
#include "overlay_tagged_value_pair_input.h"

class QAbstractButton;

class Input_Button_Controller_Base: public QObject
{
      Q_OBJECT
   public:
      explicit Input_Button_Controller_Base(QObject* parent = nullptr);

   signals:
      void valueChanged();

}; // class Input_Button_Controller_Base

//

template<typename VALUE_TYPE>
struct Overlay_For_Value_Type
{
      using value_type = VALUE_TYPE;
      using type = Overlay_Generic_Number_Input<VALUE_TYPE>;
}; // struct Overlay_For_Value_Type<>

#if 0
template<>
struct Overlay_For_Value_Type<int>
{
      using value_type = int;
      using type = Overlay_Generic_Number_Input<int>;
};

template<>
struct Overlay_For_Value_Type<uint8_t>
{
      using value_type = uint8_t;
      using type = Overlay_Generic_Number_Input<uint8_t>;
};

template<>
struct Overlay_For_Value_Type<double>
{
      using value_type = double;
      using type = Overlay_Generic_Number_Input<double>;
};
#endif

template<typename TYPE, typename DIM_TAG, typename UNIT_TAG>
struct Overlay_For_Value_Type<Tagged_Value<TYPE, DIM_TAG, UNIT_TAG>>
{
      using value_type = Tagged_Value<TYPE, DIM_TAG, UNIT_TAG>;
      using type = Overlay_Tagged_Value_Input<value_type>;
};

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
struct Overlay_For_Value_Type<Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2>>
{
      using value_type = Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2>;
      using type = Overlay_Tagged_Value_Pair_Input<value_type>;
};

//

template<typename VALUE_TYPE, typename OVERLAY_TYPE = typename Overlay_For_Value_Type<VALUE_TYPE>::type>
class Input_Button_Controller: public Input_Button_Controller_Base
{
   public:
      using Overlay_Type = OVERLAY_TYPE;
      explicit Input_Button_Controller(QObject* parent= nullptr):
         Input_Button_Controller_Base(parent)
      { }

      virtual ~Input_Button_Controller() override
      {
         if ( this->overlay_ != nullptr )
         {
            delete this->overlay_;
         }
      }

      void setOverlayMessage(QString const& msg)
      {
         this->overlay_->setMessage(msg);
      }

      void setValue(VALUE_TYPE val)
      {
         this->overlay_->setValue(val);
         this->button_->setText(this->overlay_->numberInput()->displayedValue());
      }

      VALUE_TYPE value() const
      {
         return this->overlay_->value();
      }

      void setButton(QAbstractButton* bt)
      {
         this->button_ = bt;
         if ( this->overlay_ == nullptr )
         {
            this->overlay_ = new Overlay_Type(this->button_->topLevelWidget());
            QObject::connect(
                     this->overlay_, &Overlay_Type::accepted,
                     [this]()
            {
               this->button_->setText(this->overlay_->numberInput()->displayedValue());
               this->overlay_->hide();
               emit this->valueChanged();
            });
            QObject::connect(
                     this->overlay_, &Overlay_Type::canceled,
                     [this]()
            {
               this->overlay_->hide();
            });
         }
         QObject::connect(
                  this->button_, &QAbstractButton::clicked,
                  [this]() { this->overlay_->run(); });
      }

   private:
      QAbstractButton* button_{nullptr};
      Overlay_Type* overlay_{nullptr};

}; // class Input_Button_Controller

extern template class Input_Button_Controller<Distance_M_Yd>;
extern template class Input_Button_Controller<Diameter_Cm_In>;
extern template class Input_Button_Controller<Length_Cm_In>;
extern template class Input_Button_Controller<Weight_G_Oz>;
extern template class Input_Button_Controller<Weight_Kg_Lb>;
