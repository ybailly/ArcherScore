
#include "keep_ratio_svg_widget.h"

#include <QtDebug>
#include <QtCore/QEvent>
#include <QtGui/QResizeEvent>

Keep_Ratio_Svg_Widget::Keep_Ratio_Svg_Widget(QWidget* parent):
   QSvgWidget(parent)
{
}

Keep_Ratio_Svg_Widget::Keep_Ratio_Svg_Widget(QString const& file, QWidget* parent):
   QSvgWidget(file, parent)
{
}

void Keep_Ratio_Svg_Widget::load(QByteArray const& contents)
{
   this->QSvgWidget::load(contents);
   this->have_svg = not contents.isEmpty();
   this->adjustSizeFromBuddySize();
}

void Keep_Ratio_Svg_Widget::load(QString const& file)
{
   this->QSvgWidget::load(file);
   this->have_svg = not file.isEmpty();
   this->adjustSizeFromBuddySize();
}

void Keep_Ratio_Svg_Widget::setSizeBuddy(QWidget* w)
{
   if ( this->size_buddy != nullptr )
   {
      this->size_buddy->removeEventFilter(this);
   }
   this->size_buddy = w;
   if ( this->size_buddy != nullptr )
   {
      this->adjustSizeFromBuddySize(this->size_buddy->size());
      this->size_buddy->installEventFilter(this);
   }
}

void Keep_Ratio_Svg_Widget::setFillOrientation(Qt::Orientation o)
{
   this->fill_orientation = o;
}

QSize Keep_Ratio_Svg_Widget::sizeHint() const
{
   if ( this->have_svg )
      return this->QSvgWidget::sizeHint();
   else
      return {0,0};
}

QSize Keep_Ratio_Svg_Widget::minimumSizeHint() const
{
   if ( this->have_svg )
      return this->QSvgWidget::minimumSizeHint();
   else
      return {0,0};
}

bool Keep_Ratio_Svg_Widget::eventFilter(QObject* qo, QEvent* qe)
{
   if ( not this->have_svg )
   {
      return false;
   }
   if ( qe->type() != QEvent::Resize )
   {
      return false;
   }
   QWidget* p = dynamic_cast<QWidget*>(qo);
   if ( p != this->size_buddy )
   {
      return false;
   }
   QResizeEvent* qre = dynamic_cast<QResizeEvent*>(qe);
   this->adjustSizeFromBuddySize(qre->size());
   return false;
}

void Keep_Ratio_Svg_Widget::adjustSizeFromBuddySize(QSize const& sz_)
{
   if ( not this->have_svg )
   {
      this->setFixedSize(0,0);
      return;
   }
   if ( ((sz_.width() == 0) and (this->fill_orientation == Qt::Horizontal)) or
        ((sz_.height() == 0) and (this->fill_orientation == Qt::Vertical)) )
   {
      this->setFixedSize(0,0);
      return;
   }
   this->setFixedSize({QWIDGETSIZE_MAX,QWIDGETSIZE_MAX});
   QSize const sz = [&sz_,this]() -> QSize
   {
      if ( sz_.width() <= 0 )
      {
         if ( this->size_buddy != nullptr )
            return this->size_buddy->size();
         else
            return {16,16};
      }
      return sz_;
   }();
   QSize const this_size = this->sizeHint();
   QSize const wanted_size = [&sz,&this_size,this]() -> QSize
   {
      switch ( this->fill_orientation )
      {
         case Qt::Horizontal:
         {
            double const ratio_h_on_w = static_cast<double>(this_size.height()) / static_cast<double>(this_size.width());
            double const curr_w = qRound(sz.width()*0.95);
            return QSize(curr_w, curr_w*ratio_h_on_w);
         }
            break;
         case Qt::Vertical:
         {
            double const ratio_w_on_h = static_cast<double>(this_size.width()) / static_cast<double>(this_size.height());
            double const curr_h = qRound(sz.height()*0.95);
            return QSize(curr_h*ratio_w_on_h, curr_h);
         }
            break;
      }
      return this_size;
   }();
   this->setFixedSize(wanted_size);
}
