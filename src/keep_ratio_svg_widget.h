
#pragma once

#include <QtSvg/QSvgWidget>

class Keep_Ratio_Svg_Widget: public QSvgWidget
{
      Q_OBJECT
   public:
      explicit Keep_Ratio_Svg_Widget(QWidget* parent = nullptr);
      explicit Keep_Ratio_Svg_Widget(QString const& file, QWidget* parent = nullptr);

      void load(QByteArray const& contents);
      void load(QString const& file);
      void setSizeBuddy(QWidget* w);
      void setFillOrientation(Qt::Orientation o);

      virtual QSize sizeHint() const override;
      virtual QSize minimumSizeHint() const override;

      virtual bool eventFilter(QObject* qo, QEvent* qe) override;

   protected:
      void adjustSizeFromBuddySize(QSize const& sz = QSize(-1,-1));

   private:
      QWidget* size_buddy{nullptr};
      Qt::Orientation fill_orientation{Qt::Horizontal};
      bool have_svg{false};
}; // class Keep_Ratio_Svg_Widget
