
#include <QtDebug>
#include <QtWidgets/QApplication>
#include <QtGui/QFont>
#include <QtCore/QStandardPaths>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtGui/QImage>

#include "main_win.h"
#include "style.h"
#include "bows.h"
#include "arrows.h"

#include <chrono>

int main(int argc, char *argv[])
{
#if 0
   std::chrono::high_resolution_clock::period period;
   qDebug() << period.num << "/" << period.den;
   auto const t0 = std::chrono::high_resolution_clock::now();
   auto t1 = std::chrono::high_resolution_clock::now();
   while ( t1 == t0 )
   {
      t1 = std::chrono::high_resolution_clock::now();
   }
   qDebug() << std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count();
#endif

   QApplication a(argc, argv);
   a.setStyle(new Style(QString("fusion")));
   a.setStyleSheet(
            R"s(
QPushButton {
   min-height: 2em;
}
            )s");

   readArrowSets();
   readBows();

   Main_Win mw;
   mw.show();

   return a.exec();
}
