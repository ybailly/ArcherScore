
#include "main_win.h"

#include "front_page.h"
#include "session.h"
#include "session_setup.h"
#include "ends_round_editor.h"
#include "sessions_list_model.h"
#include "sessions_list_delegate.h"
#include "tournament_editor.h"
#include "generic_overlay.h"
#include "hit_location_editor.h"
#include "hit_locations_viewer.h"
#include "settings.h"
#include "prev_sessions_page.h"
#include "edit_arrow_sets_page.h"
#include "arrow_set_editor.h"
#include "edit_bows_page.h"
#include "bow_editor.h"
#include "bow_sight_editor.h"
#include "bow_stabs_editor.h"
#include "bow_accessories_editor.h"
#include "bow_brands_models_editor.h"
#ifdef ANDROID
#include "camera_target.h"
#endif

#include <QtDebug>
#include <QtGui/QFontMetricsF>
#include <QtGui/QResizeEvent>
#include <QtCore/QPropertyAnimation>
#include <QtCore/QParallelAnimationGroup>

Main_Win::Main_Win(QWidget* parent):
   QWidget(parent)
{
   this->setupUi(this);
   this->createFrontPage();
   this->swipeFromRight(this->front_page);
}

Main_Win::~Main_Win() = default;

void Main_Win::requestedNewSession(Session_Type sess_type)
{
   if ( sess_type == Session_Type::Unknown )
   {
      return;
   }
   this->createSessionSetupPage();
   this->current_session = nullptr;
   switch ( sess_type )
   {
      case Session_Type::Unknown:
         break;
      case Session_Type::Simple:
         this->session_setup->setTitle(tr("Nouvelle session"));
         this->session_setup->setSubTitle(tr("Session simple"));
         break;
      case Session_Type::Mixed:
         this->session_setup->setTitle(tr("Nouveau tournoi"));
         this->session_setup->setSubTitle(tr("Session combinée"));
         break;
   }
   this->session_setup->prepareDefaultSession(sess_type);
   this->swipeFromRight(this->session_setup);
}

void Main_Win::requestedPreviousSessions()
{
   this->createPrevSessionsPage();
   this->swipeFromRight(this->prev_sessions_page);
}

void Main_Win::requestedEditBows()
{
   this->createEditBowsPage();
   readBows();
   this->swipeFromRight(this->edit_bows_page);
}

void Main_Win::requestedEditArrows()
{
   this->createEditArrowSetsPage();
   readArrowSets();
   this->swipeFromRight(this->edit_arrow_sets_page);
}

void Main_Win::restoreSession(QString const& basename)
{
   this->internalRestoreSession(basename);
}

void Main_Win::requestedSettings()
{

}

void Main_Win::requestedQuit()
{
   qApp->quit();
}

void Main_Win::requestedNewArrowSetEditor()
{
   this->createArrowSetEditor();
   this->arrow_set_editor->setCurrentArrowSet({});
   this->arrow_set_editor->setTitle(tr("Nouveau jeu de flèches"));
   this->arrow_set_editor->setSubTitle(QString());
   this->swipeFromRight(this->arrow_set_editor);
}

void Main_Win::requestedArrowSetEditor(Arrow_Set_Index asi)
{
   if ( asi == Invalid_Arrow_Set_Index )
      return;
   this->createArrowSetEditor();
   this->arrow_set_editor->setTitle(tr("Édition d'un jeu de flèches"));
   this->arrow_set_editor->setCurrentArrowSet(arrowSets().arrowSet(asi));
   this->edited_arrow_set_index = asi;
   this->swipeFromRight(this->arrow_set_editor);
}

void Main_Win::requestedNewBowEditor()
{
   this->createBowEditor();
   this->bow_editor->setCurrentBow({});
   this->bow_editor->setTitle(tr("Nouvel arc"));
   this->bow_editor->setSubTitle(QString());
   this->swipeFromRight(this->bow_editor);
}

void Main_Win::requestedBowEditor(Bow_Index bi)
{
   if ( bi == Invalid_Bow_Index )
      return;
   this->createBowEditor();
   this->bow_editor->setTitle(tr("Édition d'un arc"));
   this->bow_editor->setCurrentBow(bows().bow(bi));
   this->edited_bow_index = bi;
   this->swipeFromRight(this->bow_editor);
}

void Main_Win::requestedEditBowSight()
{
   this->createBowSightEditor();
   this->bow_sight_editor->fillFromBow(this->bow_editor->currentBow());
   this->swipeFromRight(this->bow_sight_editor);
}

void Main_Win::requestedEditBowStabs()
{
   this->createBowStabsEditor();
   this->bow_stabs_editor->fillFromBow(this->bow_editor->currentBow());
   this->swipeFromRight(this->bow_stabs_editor);
}

void Main_Win::requestedEditBowAccessories()
{
   this->createBowAccessoriesEditor();
   this->bow_accessories_editor->fillFromBow(this->bow_editor->currentBow());
   this->swipeFromRight(this->bow_accessories_editor);
}

void Main_Win::requestedEditBowBrandsModels()
{
   this->createBowBrandsModelsEditor();
   this->swipeFromRight(this->bow_brands_models_editor);
}

void Main_Win::sessionSetupRequestNewArrowSet()
{
   this->createArrowSetEditor();
   this->arrow_set_editor->setCurrentArrowSet({});
   this->arrow_set_editor->setTitle(tr("Nouveau jeu de flèches"));
   if ( this->current_session == nullptr )
   {
      this->arrow_set_editor->setSubTitle(tr("Pour nouvelle session"));
   }
   else
   {
      this->arrow_set_editor->setSubTitle(this->current_session->title());
   }
   this->arrow_set_editor_from_session_setup = true;
   this->swipeFromRight(this->arrow_set_editor);
}

void Main_Win::sessionSetupRequestArrowSetEditor()
{
   this->createArrowSetEditor();
   this->arrow_set_editor->setTitle(tr("Édition d'un jeu de flèches"));
   this->arrow_set_editor->setCurrentArrowSet(this->session_setup->arrowSet());
   this->arrow_set_editor_from_session_setup = true;
   this->swipeFromRight(this->arrow_set_editor);
}

void Main_Win::sessionSetupRequestNewBow()
{
   this->createBowEditor();
   this->bow_editor->setCurrentBow({});
   this->bow_editor->setTitle(tr("Nouvel arc"));
   if ( this->current_session == nullptr )
   {
      this->bow_editor->setSubTitle(tr("Pour nouvelle session"));
   }
   else
   {
      this->bow_editor->setSubTitle(this->current_session->title());
   }
   this->bow_editor_from_session_setup = true;
   this->swipeFromRight(this->bow_editor);
}

void Main_Win::sessionSetupRequestBowEditor()
{
   this->createBowEditor();
   this->bow_editor->setTitle(tr("Édition d'un arc"));
   this->bow_editor->setCurrentBow(this->session_setup->bow());
   this->bow_editor_from_session_setup = true;
   this->swipeFromRight(this->bow_editor);
}

void Main_Win::createFrontPage()
{
   if ( this->front_page != nullptr )
      return;
   this->front_page = new Front_Page(this);
   QObject::connect(
            this->front_page, &Front_Page::requestNewSession,
            this, &Main_Win::requestedNewSession);
   QObject::connect(
            this->front_page, &Front_Page::requestPrevSessions,
            this, &Main_Win::requestedPreviousSessions);
   QObject::connect(
            this->front_page, &Front_Page::requestEditBows,
            this, &Main_Win::requestedEditBows);
   QObject::connect(
            this->front_page, &Front_Page::requestEditArrows,
            this, &Main_Win::requestedEditArrows);
   QObject::connect(
            this->front_page, &Front_Page::requestSettings,
            this, &Main_Win::requestedSettings);
   QObject::connect(
            this->front_page, &Front_Page::requestQuit,
            this, &Main_Win::requestedQuit);
}

void Main_Win::createPrevSessionsPage()
{
   if ( this->prev_sessions_page != nullptr )
      return;
   this->prev_sessions_page = new Prev_Sessions_Page(this);
   QObject::connect(
            this->prev_sessions_page, &Prev_Sessions_Page::requestRestoreSession,
            this, &Main_Win::restoreSession);
   QObject::connect(
            this->prev_sessions_page, &Prev_Sessions_Page::backRequested,
            [this]()
   {
      this->swipeFromLeft(this->front_page);
   });
}

void Main_Win::createSessionSetupPage()
{
   if ( this->session_setup != nullptr )
      return;
   this->session_setup = new Session_Setup(this);
   QObject::connect(
            this->session_setup, &Session_Setup::accepted,
            this, &Main_Win::sessionSetupAccepted);
   QObject::connect(
            this->session_setup, &Session_Setup::canceled,
            [this]()
   {
      this->current_session = nullptr;
      this->swipeFromLeft(this->front_page);
   });
   QObject::connect(
            this->session_setup, &Session_Setup::requestArrowSetEditor,
            this, &Main_Win::sessionSetupRequestArrowSetEditor);
   QObject::connect(
            this->session_setup, &Session_Setup::requestNewArrowSet,
            this, &Main_Win::sessionSetupRequestNewArrowSet);
   QObject::connect(
            this->session_setup, &Session_Setup::requestBowEditor,
            this, &Main_Win::sessionSetupRequestBowEditor);
   QObject::connect(
            this->session_setup, &Session_Setup::requestNewBow,
            this, &Main_Win::sessionSetupRequestNewBow);
}

void Main_Win::createEditArrowSetsPage()
{
   if ( this->edit_arrow_sets_page != nullptr )
      return;
   this->edit_arrow_sets_page = new Edit_Arrow_Sets_Page(this);
   QObject::connect(
            this->edit_arrow_sets_page, &Edit_Arrow_Sets_Page::requestNewArrowSetEditor,
            this, &Main_Win::requestedNewArrowSetEditor);
   QObject::connect(
            this->edit_arrow_sets_page, &Edit_Arrow_Sets_Page::requestArrowSetEditor,
            this, &Main_Win::requestedArrowSetEditor);
   QObject::connect(
            this->edit_arrow_sets_page, &Edit_Arrow_Sets_Page::backRequested,
            [this]()
   {
      writeArrowSets();
      this->swipeFromLeft(this->front_page);
   });
}

void Main_Win::createArrowSetEditor()
{
   if ( this->arrow_set_editor != nullptr )
      return;
   this->arrow_set_editor = new Arrow_Set_Editor(this);
   QObject::connect(
            this->arrow_set_editor, &Arrow_Set_Editor::backRequested,
            this, &Main_Win::arrowSetEditorDone);
}

void Main_Win::createEditBowsPage()
{
   if ( this->edit_bows_page != nullptr )
      return;
   this->edit_bows_page = new Edit_Bows_Page(this);
   QObject::connect(
            this->edit_bows_page, &Edit_Bows_Page::requestNewBowEditor,
            this, &Main_Win::requestedNewBowEditor);
   QObject::connect(
            this->edit_bows_page, &Edit_Bows_Page::requestBowEditor,
            this, &Main_Win::requestedBowEditor);
   QObject::connect(
            this->edit_bows_page, &Edit_Bows_Page::backRequested,
            [this]()
   {
      writeBows();
      this->swipeFromLeft(this->front_page);
   });
}

void Main_Win::createBowEditor()
{
   if ( this->bow_editor != nullptr )
      return;
   this->bow_editor = new Bow_Editor(this);
   QObject::connect(
            this->bow_editor, &Bow_Editor::requestEditBowSight,
            this, &Main_Win::requestedEditBowSight);
   QObject::connect(
            this->bow_editor, &Bow_Editor::requestEditBowStabs,
            this, &Main_Win::requestedEditBowStabs);
   QObject::connect(
            this->bow_editor, &Bow_Editor::requestEditBowAccessories,
            this, &Main_Win::requestedEditBowAccessories);
   QObject::connect(
            this->bow_editor, &Bow_Editor::requestEditBowBrandsModels,
            this, &Main_Win::requestedEditBowBrandsModels);
   QObject::connect(
            this->bow_editor, &Bow_Editor::backRequested,
            this, &Main_Win::bowEditorDone);
}

void Main_Win::createBowSightEditor()
{
   if ( this->bow_sight_editor != nullptr )
      return;
   this->bow_sight_editor = new Bow_Sight_Editor(this);
   QObject::connect(
            this->bow_sight_editor, &Bow_Sight_Editor::backRequested,
            this, &Main_Win::bowSightEditorDone);
}

void Main_Win::createBowStabsEditor()
{
   if ( this->bow_stabs_editor != nullptr )
      return;
   this->bow_stabs_editor = new Bow_Stabs_Editor(this);
   QObject::connect(
            this->bow_stabs_editor, &Bow_Stabs_Editor::backRequested,
            this, &Main_Win::bowStabsEditorDone);
}

void Main_Win::createBowAccessoriesEditor()
{
   if ( this->bow_accessories_editor != nullptr )
      return;
   this->bow_accessories_editor = new Bow_Accessories_Editor(this);
   QObject::connect(
            this->bow_accessories_editor, &Bow_Accessories_Editor::backRequested,
            this, &Main_Win::bowAccessoriesEditorDone);
}

void Main_Win::createBowBrandsModelsEditor()
{
   if ( this->bow_brands_models_editor != nullptr )
      return;
   this->bow_brands_models_editor = new Bow_Brands_Models_Editor(this);
   QObject::connect(
            this->bow_brands_models_editor, &Bow_Brands_Models_Editor::backRequested,
            this, &Main_Win::bowBrandsModelsEditorDone);
}

void Main_Win::sessionSetupAccepted()
{
   this->session_setup->hide();
   if ( this->current_session == nullptr )
   {
      this->current_session = std::make_unique<Session>(
                                 this->session_setup->shotsPerEnd(),
                                 this->session_setup->sessionType());
      Settings::Previous::setTargetSize(this->session_setup->targetSize());
      Settings::Previous::setTargetMinimumScore(this->session_setup->targetMinimumScore());
      Settings::Previous::setTargetDistance(this->session_setup->targetDistance());
      Settings::Previous::setArrowSizeSpecification(this->session_setup->arrowSizeSpecification());
      Settings::Previous::setShotsPerEnd(this->session_setup->shotsPerEnd());
      Settings::Previous::setSessionLocation(this->session_setup->location());
      Settings::sync();
   }
   if ( this->session_setup->haveArrowSet() )
   {
      this->current_session->setArrowSet(this->session_setup->arrowSet());
   }
   if ( this->session_setup->haveBow() )
   {
      this->current_session->setBow(this->session_setup->bow());
   }
   this->current_session->setTargetDiameter(this->session_setup->targetSize());
   this->current_session->setTargetMinimumScore(this->session_setup->targetMinimumScore());
   this->current_session->setTargetDistance(this->session_setup->targetDistance());
   this->current_session->setArrowSizeSpecification(this->session_setup->arrowSizeSpecification());
   this->current_session->setTitle(this->session_setup->title());
   this->current_session->setDescription(this->session_setup->description());
   this->current_session->setLocation(this->session_setup->location());
   if ( this->current_session->endsRoundsCount() == 0 )
   {
      this->current_session->appendEndsRound();
      this->current_session->mutableEndsRound(Round_Index(0))->appendEnd();
   }
   this->current_session->saveToFile();
   if ( this->current_session->type() == Session_Type::Simple )
   {
      this->showEndsRoundEditor(Round_Index(0));
   }
   else
   {
      this->showTournamentEditor();
   }
}

void Main_Win::createEndsRoundEditor()
{
   if ( this->ends_round_editor != nullptr )
      return;
   this->ends_round_editor = new Ends_Round_Editor(this);
   QObject::connect(
            this->ends_round_editor, &Ends_Round_Editor::requestEditScore,
            this, &Main_Win::endsRoundEditorRequestEditScore);
   QObject::connect(
            this->ends_round_editor, &Ends_Round_Editor::requestViewEditEnd,
            this, &Main_Win::endsRoundEditorRequestViewEditEnd);
   QObject::connect(
            this->ends_round_editor, &Ends_Round_Editor::requestViewRound,
            this, &Main_Win::endsRoundEditorRequestViewRound);
   QObject::connect(
            this->ends_round_editor, &Ends_Round_Editor::done,
            this, &Main_Win::endsRoundEditorDone);
}

void Main_Win::createTournamentEditor()
{
   if ( this->tournament_editor != nullptr )
      return;
   this->tournament_editor = new Tournament_Editor(this);
   QObject::connect(
            this->tournament_editor, &Tournament_Editor::done,
            [this]()
   {
      this->current_session->saveToFile();
      this->tournament_editor->setSession(nullptr);
      this->current_session = nullptr;
      this->swipeFromLeft(this->front_page);
   });
   QObject::connect(
            this->tournament_editor, &Tournament_Editor::requestEditRound,
            this, &Main_Win::showEndsRoundEditor);
}

void Main_Win::createHitLocationEditor()
{
   if ( this->hit_location_editor != nullptr )
      return;
   this->hit_location_editor = new Hit_Location_Editor(this);
   QObject::connect(
            this->hit_location_editor, &Hit_Location_Editor::backRequested,
            this, &Main_Win::hitLocationEditorDone);
}

void Main_Win::createHitLocationViewer()
{
   if ( this->hit_locations_viewer != nullptr )
      return;
   this->hit_locations_viewer = new Hit_Locations_Viewer(this);
   QObject::connect(
            this->hit_locations_viewer, &Hit_Locations_Viewer::backRequested,
            this, &Main_Win::hitLocationsViewerDone);
}

void Main_Win::showEndsRoundEditor(Round_Index round_index)
{
   this->createEndsRoundEditor();
   this->ends_round_editor->setEndsRound(this->current_session.get(), round_index);
   switch ( this->current_session->type() )
   {
      case Session_Type::Simple:
         this->ends_round_editor->setTitle(this->current_session->title());
         this->ends_round_editor->setSubTitle(QString());
         break;
      case Session_Type::Mixed:
         if ( round_index.value() == 0 )
         {
            this->ends_round_editor->setTitle(this->current_session->title());
            this->ends_round_editor->setSubTitle(tr("Série principale"));
         }
         else
         {
            this->ends_round_editor->setTitle(this->current_session->title());
            this->ends_round_editor->setSubTitle(tr("Duel %1").arg(round_index.value()));
         }
         break;
      case Session_Type::Unknown:
         this->ends_round_editor->setTitle(QString());
         this->ends_round_editor->setSubTitle(QString());
   }

   this->swipeFromRight(this->ends_round_editor);
}

void Main_Win::showTournamentEditor()
{
   this->createTournamentEditor();
   this->tournament_editor->setSession(this->current_session.get());
   this->tournament_editor->setTitle(this->current_session->title());
   this->tournament_editor->setSubTitle(this->current_session->creationDate().toString(Qt::DefaultLocaleLongDate));
   this->swipeFromRight(this->tournament_editor);
}

void Main_Win::endsRoundEditorRequestEditScore(End_Index end_idx, Score_Index score_idx)
{
   this->createHitLocationEditor();
   Ends_Round* round = this->ends_round_editor->endsRound();
   this->hit_location_editor->setSession(this->current_session.get());
   this->hit_location_editor->setHitLocations({round->scoreUnit(end_idx, score_idx)});
   this->hit_location_editor->setTitle(tr("Édition d'un tir"));
   this->hit_location_editor->setSubTitle(
            QStringLiteral("Série %1 / Volée %2 / Flèche %3")
            .arg(this->ends_round_editor->endsRoundIndex().value())
            .arg(end_idx.value())
            .arg(score_idx.value()));
   this->edited_end_index = end_idx;
   this->edited_score_index = score_idx;
   this->swipeFromRight(this->hit_location_editor);
}

void Main_Win::endsRoundEditorRequestViewEditEnd(End_Index end_idx)
{
   this->createHitLocationEditor();
   this->hit_location_editor->setSession(this->current_session.get());
   Ends_Round* round = this->ends_round_editor->endsRound();
   Single_End const& end = round->singleEnd(end_idx);
   std::vector<Score_Unit> hit_locs;
   hit_locs.resize(end.nbShots());
   std::copy(end.begin(), end.end(), hit_locs.begin());
   this->hit_location_editor->setHitLocations(hit_locs);
   this->hit_location_editor->setTitle(tr("Édition d'une volée"));
   this->hit_location_editor->setSubTitle(
            QStringLiteral("Série %1 / Volée %2")
            .arg(this->ends_round_editor->endsRoundIndex().value())
            .arg(end_idx.value()));
   this->edited_end_index = end_idx;
   this->edited_score_index = Invalid_Score_Index;
   this->swipeFromRight(this->hit_location_editor);
}

void Main_Win::endsRoundEditorRequestViewRound()
{
   this->createHitLocationViewer();
   this->hit_locations_viewer->setSession(this->current_session.get());
   this->hit_locations_viewer->showEndsRound(this->ends_round_editor->endsRoundIndex());
   this->hit_locations_viewer->setTitle(this->current_session->title());
   this->hit_locations_viewer->setSubTitle(
            QStringLiteral("Série %1")
            .arg(this->ends_round_editor->endsRoundIndex().value()));
   this->swipeFromRight(this->hit_locations_viewer);
}

void Main_Win::hitLocationEditorDone()
{
   Ends_Round* round = this->ends_round_editor->endsRound();
   std::vector<Score_Unit> const& hit_locs = this->hit_location_editor->hitLocations();
   if ( this->edited_score_index != Invalid_Score_Index )
   {
      round->setScore(this->edited_end_index,
                      this->edited_score_index,
                      hit_locs.front());
   }
   else
   {
      Score_Index si{0};
      for(Score_Unit const& su: hit_locs)
      {
         round->setScore(this->edited_end_index, si, su);
         ++si;
      }
   }
   this->edited_end_index = Invalid_End_Index;
   this->edited_score_index = Invalid_Score_Index;
   this->swipeFromLeft(this->ends_round_editor);
   this->current_session->saveToFile();
}

void Main_Win::hitLocationsViewerDone()
{
   this->edited_end_index = Invalid_End_Index;
   this->edited_score_index = Invalid_Score_Index;
   this->swipeFromLeft(this->ends_round_editor);
}

void Main_Win::endsRoundEditorDone()
{
   switch ( this->ends_round_editor->session()->type() )
   {
      case Session_Type::Mixed:
         this->tournament_editor->refreshStats();
         this->swipeFromLeft(this->tournament_editor);
         break;
      case Session_Type::Simple:
         this->current_session = nullptr;
         this->swipeFromLeft(this->front_page);
   }
   this->ends_round_editor->setEndsRound(nullptr, {});
}

void Main_Win::arrowSetEditorDone()
{
   if ( this->arrow_set_editor_from_session_setup )
   {
      this->session_setup->setArrowSet(this->arrow_set_editor->currentArrowSet());
      if ( this->current_session != nullptr )
      {
         this->current_session->saveToFile();
      }
      this->swipeFromLeft(this->session_setup);
   }
   else
   {
      if ( this->edited_arrow_set_index == Invalid_Arrow_Set_Index )
      {
         addArrowSet(this->arrow_set_editor->currentArrowSet());
      }
      else
      {
         replaceArrowSet(this->edited_arrow_set_index, this->arrow_set_editor->currentArrowSet());
      }
      this->edited_arrow_set_index = Invalid_Arrow_Set_Index;
      writeArrowSets();
      this->swipeFromLeft(this->edit_arrow_sets_page);
   }
   this->arrow_set_editor_from_session_setup = false;
}

void Main_Win::bowEditorDone()
{
   if ( this->bow_editor_from_session_setup )
   {
      this->session_setup->setBow(this->bow_editor->currentBow());
      if ( this->current_session != nullptr )
      {
         this->current_session->saveToFile();
      }
      this->swipeFromLeft(this->session_setup);
   }
   else
   {
      if ( this->edited_bow_index == Invalid_Bow_Index )
      {
         addBow(this->bow_editor->currentBow());
      }
      else
      {
         replaceBow(this->edited_bow_index, this->bow_editor->currentBow());
      }
      this->edited_bow_index = Invalid_Bow_Index;
      writeBows();
      this->swipeFromLeft(this->edit_bows_page);
   }
   this->bow_editor_from_session_setup = false;
}   

void Main_Win::bowSightEditorDone()
{
   this->bow_editor->updateBowSight(this->bow_sight_editor);
   this->swipeFromLeft(this->bow_editor);
}

void Main_Win::bowStabsEditorDone()
{
   this->bow_editor->updateBowStabs(this->bow_stabs_editor);
   this->swipeFromLeft(this->bow_editor);
}

void Main_Win::bowAccessoriesEditorDone()
{
   this->bow_editor->updateBowAccessories(this->bow_accessories_editor);
   this->swipeFromLeft(this->bow_editor);
}

void Main_Win::bowBrandsModelsEditorDone()
{
   this->bow_editor->updateBowBrandsModels(this->bow_brands_models_editor);
   this->swipeFromLeft(this->bow_editor);
}

void Main_Win::internalRestoreSession(QString const& basename)
{
   this->createSessionSetupPage();
   this->current_session = std::make_unique<Session>(basename);
   this->session_setup->restoreSession(*this->current_session);
   switch ( this->current_session->type() )
   {
      case Session_Type::Simple:
         this->session_setup->setTitle(tr("Session simple"));
         this->session_setup->setSubTitle(
                  this->current_session->creationDate().toString(Qt::DefaultLocaleLongDate));
         break;
      case Session_Type::Mixed:
         this->session_setup->setTitle(tr("Session combinée"));
         this->session_setup->setSubTitle(
                  this->current_session->creationDate().toString(Qt::DefaultLocaleLongDate));
         break;
      case Session_Type::Unknown:
         this->session_setup->setTitle(QString());
         this->session_setup->setSubTitle(QString());
   }
   this->swipeFromRight(this->session_setup);
}

void Main_Win::swipeFromRight(QWidget* to)
{
   if ( to == nullptr )
   {
      return;
   }
   this->createSwipeAnims();
   this->startSwipe(
            this->width(),
            (this->currently_shown_page != nullptr) ? -this->currently_shown_page->width() : 0,
            to);
}

void Main_Win::swipeFromLeft(QWidget* to)
{
   if ( to == nullptr )
   {
      return;
   }
   this->createSwipeAnims();
   this->startSwipe(-to->width(), this->width(), to);
}

void Main_Win::startSwipe(int to_start_x, int from_end_x, QWidget* to)
{
   if ( to == nullptr )
   {
      return;
   }
   to->show();
   this->swipe_to_anim->setStartValue(QPoint(to_start_x, 0));
   this->swipe_to_anim->setEndValue(QPoint(0,0));
   if ( this->currently_shown_page == nullptr )
   {
      this->swipe_to_anim->setTargetObject(to);
      this->swipe_to_anim->start();
   }
   else
   {
      this->swipe_from_anim->setStartValue(QPoint(0,0));
      this->swipe_from_anim->setEndValue(QPoint(from_end_x, 0));
      this->swipe_from_anim->setTargetObject(this->currently_shown_page);
      this->swipe_to_anim->setTargetObject(to);
      this->swipe_anim->start();
   }
}

void Main_Win::createSwipeAnims()
{
   if ( this->swipe_anim != nullptr )
   {
      return;
   }
   this->swipe_anim = new QParallelAnimationGroup(this);
   this->swipe_from_anim = new QPropertyAnimation(this);
   this->swipe_to_anim = new QPropertyAnimation(this);
   for(QPropertyAnimation* anim: {this->swipe_from_anim, this->swipe_to_anim})
   {
      anim->setDuration(333);
      anim->setEasingCurve(QEasingCurve(QEasingCurve::OutQuad));
      anim->setPropertyName("pos");
   }
   this->swipe_from_anim->setStartValue(QPoint(0,0));
   this->swipe_from_anim->setEndValue(QPoint(-this->width(), 0));
   this->swipe_to_anim->setStartValue(QPoint(this->width(), 0));
   this->swipe_to_anim->setEndValue(QPoint(0,0));
   this->swipe_anim->addAnimation(this->swipe_from_anim);
   this->swipe_anim->addAnimation(this->swipe_to_anim);
   QObject::connect(
            this->swipe_anim, &QAbstractAnimation::stateChanged,
            [this](QAbstractAnimation::State newState, QAbstractAnimation::State)
   {
      Generic_Page* page_from = dynamic_cast<Generic_Page*>(this->swipe_from_anim->targetObject());
      Generic_Page* page_to = dynamic_cast<Generic_Page*>(this->swipe_to_anim->targetObject());
      bool const is_running = (newState == QAbstractAnimation::Running);
      auto func =
            is_running ?
               &Generic_Page::startSlide :
               &Generic_Page::endSlide;
      if ( page_from != nullptr )
      {
         page_from->setVisible(is_running);
         (page_from->*func)();
      }
      if ( page_to != nullptr )
      {
         page_to->setVisible(true);
         (page_to->*func)();
      }
   });
   QObject::connect(
            this->swipe_to_anim, &QPropertyAnimation::finished,
            [this]()
   {
      this->currently_shown_page = dynamic_cast<QWidget*>(this->swipe_to_anim->targetObject());
   });
}

void Main_Win::keyPressEvent(QKeyEvent* qke)
{
   if ( (qke->key() == Qt::Key_Escape) or
        (qke->key() == Qt::Key_Back) )
   {
      qke->accept();
   }
   else
   {
      this->QWidget::keyPressEvent(qke);
   }
}
