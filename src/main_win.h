
#pragma once

#include "ui_main_win.h"

#include "sessions_list.h"
#include "bows.h"

#include <QtWidgets/QWidget>

#include <memory>

class Session;
class Front_Page;
class Session_Setup;
class Prev_Sessions_Page;
class Edit_Bows_Page;
class Edit_Arrow_Sets_Page;
class Ends_Round_Editor;
class Tournament_Editor;
class Hit_Location_Editor;
class Hit_Locations_Viewer;
class QPropertyAnimation;
class QParallelAnimationGroup;
class Camera_Target;
class Arrow_Set_Editor;
class Bow_Editor;
class Bow_Sight_Editor;
class Bow_Stabs_Editor;
class Bow_Accessories_Editor;
class Bow_Brands_Models_Editor;

class Main_Win: public QWidget, private Ui::Main_Win
{
      Q_OBJECT
   public:
      explicit Main_Win(QWidget* parent = nullptr);
      virtual ~Main_Win() override;

   public slots:
      void restoreSession(QString const& basename);

   protected:
      // signals from front page
      void requestedNewSession(Session_Type sess_type);
      void requestedPreviousSessions();
      void requestedEditBows();
      void requestedEditArrows();
      void requestedSettings();
      void requestedQuit();

      void requestedNewArrowSetEditor();
      void requestedArrowSetEditor(Arrow_Set_Index asi);

      void requestedNewBowEditor();
      void requestedBowEditor(Bow_Index bi);
      void requestedEditBowSight();
      void requestedEditBowStabs();
      void requestedEditBowAccessories();
      void requestedEditBowBrandsModels();

      void sessionSetupRequestNewArrowSet();
      void sessionSetupRequestArrowSetEditor();
      void sessionSetupRequestNewBow();
      void sessionSetupRequestBowEditor();
      void sessionSetupAccepted();
      void showEndsRoundEditor(Round_Index round_index);
      void showTournamentEditor();
      void endsRoundEditorRequestEditScore(End_Index end_idx, Score_Index score_idx);
      void endsRoundEditorRequestViewEditEnd(End_Index end_idx);
      void endsRoundEditorRequestViewRound();
      void hitLocationEditorDone();
      void hitLocationsViewerDone();
      void endsRoundEditorDone();
      void arrowSetEditorDone();
      void bowEditorDone();
      void bowSightEditorDone();
      void bowStabsEditorDone();
      void bowAccessoriesEditorDone();
      void bowBrandsModelsEditorDone();

      void internalRestoreSession(QString const& basename);
      void swipeFromRight(QWidget* to);
      void swipeFromLeft(QWidget* to);
      void startSwipe(int to_start_x, int from_end_x, QWidget* to);

      void createSwipeAnims();
      void createFrontPage();
      void createPrevSessionsPage();
      void createSessionSetupPage();
      void createEditArrowSetsPage();
      void createArrowSetEditor();
      void createEditBowsPage();
      void createBowEditor();
      void createBowSightEditor();
      void createBowStabsEditor();
      void createBowAccessoriesEditor();
      void createBowBrandsModelsEditor();
      void createEditArrowsPage();
      void createSettingsPage();
      void createEndsRoundEditor();
      void createTournamentEditor();
      void createHitLocationEditor();
      void createHitLocationViewer();

      virtual void keyPressEvent(QKeyEvent* qke) override;

   private:
      Sessions_List sessions_list;
      std::unique_ptr<Session> current_session;

      QWidget* currently_shown_page{nullptr};
      Front_Page* front_page{nullptr};
      Prev_Sessions_Page* prev_sessions_page{nullptr};
      Edit_Arrow_Sets_Page* edit_arrow_sets_page{nullptr};
      Arrow_Set_Editor* arrow_set_editor{nullptr};
      Edit_Bows_Page* edit_bows_page{nullptr};
      Bow_Editor* bow_editor{nullptr};
      Bow_Sight_Editor* bow_sight_editor{nullptr};
      Bow_Stabs_Editor* bow_stabs_editor{nullptr};
      Bow_Accessories_Editor* bow_accessories_editor{nullptr};
      Bow_Brands_Models_Editor* bow_brands_models_editor{nullptr};
      Session_Setup* session_setup{nullptr};
      Ends_Round_Editor* ends_round_editor{nullptr};
      Tournament_Editor* tournament_editor{nullptr};
      Hit_Location_Editor* hit_location_editor{nullptr};
      Hit_Locations_Viewer* hit_locations_viewer{nullptr};
      QParallelAnimationGroup* swipe_anim{nullptr};
      QPropertyAnimation* swipe_from_anim{nullptr};
      QPropertyAnimation* swipe_to_anim{nullptr};
      Camera_Target* camera_target{nullptr};

      End_Index edited_end_index{Invalid_End_Index};
      Score_Index edited_score_index{Invalid_Score_Index};
      Bow_Index edited_bow_index{Invalid_Bow_Index};
      Arrow_Set_Index edited_arrow_set_index{Invalid_Arrow_Set_Index};
      bool bow_editor_from_session_setup{false};
      bool arrow_set_editor_from_session_setup{false};
}; // class Main_Win
