
#include "overlay_choose_bow_type.h"
#include "qt_utils.h"

#include <QtDebug>

Overlay_Choose_Bow_Type::Overlay_Choose_Bow_Type(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());
   qt::adjustToolButtonsIconSize(this->contents(), 4.0);

   std::pair<QToolButton*, Bow_Type> tb_types[] {
      {this->tb_traditional, Bow_Type::Traditional},
      {this->tb_traditional_hunt, Bow_Type::Traditional_Hunt},
      {this->tb_recurve, Bow_Type::Recurve},
      {this->tb_recurve_hunt, Bow_Type::Recurve_Hunt},
      {this->tb_recurve_modern, Bow_Type::Recurve_Modern},
      {this->tb_compound, Bow_Type::Compound}
   };
   for(auto const& tb_type: tb_types)
   {
      Bow_Type const bow_type = tb_type.second;
      QObject::connect(
               tb_type.first, &QToolButton::clicked,
               [bow_type,this]
      {
         emit this->bowTypeChoosen(bow_type);
      });
   }
}
