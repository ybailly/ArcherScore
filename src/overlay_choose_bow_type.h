
#pragma once

#include "generic_overlay.h"
#include "bows.h"

#include "ui_overlay_choose_bow_type.h"

class Overlay_Choose_Bow_Type: public Generic_Overlay, private Ui::Overlay_Choose_Bow_Type
{
      Q_OBJECT
   public:
      explicit Overlay_Choose_Bow_Type(QWidget* parent = nullptr);

   signals:
      void bowTypeChoosen(Bow_Type bt);

}; // class Overlay_Choose_Bow_Type
