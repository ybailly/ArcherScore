
#include "overlay_choose_location.h"
#include "qt_utils.h"

Overlay_Choose_Location::Overlay_Choose_Location(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());
   qt::adjustToolButtonsIconSize(this->contents(), 5.0);
   QObject::connect(
            this->tb_indoor, &QToolButton::clicked,
            [this]() { emit this->locationChoosen(Session_Location::Indoor) ;});
   QObject::connect(
            this->tb_outdoor, &QToolButton::clicked,
            [this]() { emit this->locationChoosen(Session_Location::Outdoor) ;});
}
