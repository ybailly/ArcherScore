
#pragma once

#include "generic_overlay.h"
#include "common_types.h"

#include "ui_overlay_choose_location.h"

class Overlay_Choose_Location: public Generic_Overlay, private Ui::Overlay_Choose_Location
{
      Q_OBJECT
   public:
      explicit Overlay_Choose_Location(QWidget* parent = nullptr);

   signals:
      void locationChoosen(Session_Location);

}; // class Overlay_Choose_Location
