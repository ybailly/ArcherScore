
#include "overlay_choose_score.h"

#include "qt_utils.h"
#include "ui_utils.h"
#include "common_types_utils.h"

Overlay_Choose_Score::Overlay_Choose_Score(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());

   QFont fnt = qt::adjustFontSize(QApplication::font(), 1.66);
   QList<QPushButton*> buttons = this->w_input->findChildren<QPushButton*>();
   for(QPushButton* button: buttons)
   {
      button->setFont(fnt);
      QObject::connect(
               button, &QPushButton::clicked,
               this, &Overlay_Choose_Score::onClicked);
      QPalette pal = button->palette();
      int const score = button->property("score").toInt();
      if ( score < 11 )
      {
         pal.setBrush(QPalette::Button, textBackgroundColorForMark(scoreToMark(score)));
         pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(scoreToMark(score)));
      }
      else
      {
         pal.setBrush(QPalette::Button, textBackgroundColorForMark(Mark::TenPlus));
         pal.setBrush(QPalette::ButtonText, textForegroundColorForMark(Mark::TenPlus));
      }
      button->setPalette(pal);
   }
}

void Overlay_Choose_Score::onClicked()
{
   int const score = this->sender()->property("score").toInt();
   emit this->scoreChoosen(static_cast<Mark>(score));
}
