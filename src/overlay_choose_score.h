
#pragma once

#include "generic_overlay.h"
#include "common_types.h"

#include "ui_overlay_choose_score.h"

class Overlay_Choose_Score: public Generic_Overlay, private Ui::Overlay_Choose_Score
{
      Q_OBJECT
   public:
      explicit Overlay_Choose_Score(QWidget* parent = nullptr);

   protected:
      void onClicked();

   signals:
      void scoreChoosen(Mark);
}; // class Overlay_Choose_Score
