
#include "overlay_choose_session_type.h"

#include "ui_utils.h"

Overlay_Choose_Session_Type::Overlay_Choose_Session_Type(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());
   struct session_type_info
   {
         Button_Label* button;
         QString text;
         Session_Type type;
   };
   session_type_info types[] {
      {this->bt_session_simple,
               tr("<b>Simple</b><br/>"
                  "Une unique série de volées<br/>"
                  "(entraînements, concours)"),
               Session_Type::Simple},
      {this->bt_session_tournament,
               tr("<b>Tournoi</b><br/>"
                  "Plusieurs séries de volées éliminatoires<br/>"
                  "(tournois)."),
               Session_Type::Tournament},
      {this->bt_session_mixed,
               tr("<b>Combinée</b><br/>"
                  "Une série de volées, suivie de plusieurs séries de volées éliminatoires<br/>"
                  "(tournois <i>\"spécial jeunes\"</i>)"),
               Session_Type::Mixed}
   };
   for(auto const& type: types)
   {
      type.button->setWordWrap(true);
      type.button->setText(type.text);
      type.button->loadLeftSvg(svgForSessionType(type.type));
      auto const stype = type.type;
      QObject::connect(
               type.button, &Button_Label::clicked,
               [this,stype]()
      {
         emit this->sessionTypeChoosen(stype);
      });
   }
}
