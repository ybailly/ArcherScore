
#pragma once

#include "generic_overlay.h"
#include "common_types.h"

#include "ui_overlay_choose_session_type.h"

class Overlay_Choose_Session_Type: public Generic_Overlay, private Ui::Overlay_Choose_Session_Type
{
      Q_OBJECT
   public:
      explicit Overlay_Choose_Session_Type(QWidget* parent = nullptr);

   signals:
      void sessionTypeChoosen(Session_Type bt);

}; // class Overlay_Choose_Session_Type
