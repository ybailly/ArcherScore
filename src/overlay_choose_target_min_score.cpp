
#include "overlay_choose_target_min_score.h"
#include "qt_utils.h"

#include <QtDebug>

Overlay_Choose_Target_Min_Score::Overlay_Choose_Target_Min_Score(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());
   qt::adjustToolButtonsIconSize(this->contents(), 4.0);

   for(QToolButton* bt: {this->bt_1, this->bt_5, this->bt_6, this->bt_7, this->bt_8})
   {
      QObject::connect(
               bt, &QToolButton::clicked,
               [bt,this]
      {
         emit this->minScoreChoosen(bt->property("min_mark").toUInt());
      });
   }
}
