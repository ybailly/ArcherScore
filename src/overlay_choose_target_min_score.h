
#pragma once

#include "generic_overlay.h"

#include "common_types.h"

#include "ui_overlay_choose_target_min_score.h"

class Overlay_Choose_Target_Min_Score: public Generic_Overlay, private Ui::Overlay_Choose_Target_Min_Score
{
      Q_OBJECT
   public:
      explicit Overlay_Choose_Target_Min_Score(QWidget* parent = nullptr);

   signals:
      void minScoreChoosen(int8_t);

}; // class Overlay_Choose_Target_Min_Score
