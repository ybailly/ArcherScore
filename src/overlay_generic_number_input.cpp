
#include "overlay_generic_number_input.h"

Overlay_Generic_Number_Input_Base::Overlay_Generic_Number_Input_Base(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());

   QObject::connect(
            this->bt_ok, &QPushButton::clicked,
            this, &Overlay_Generic_Number_Input_Base::accepted);
   QObject::connect(
            this->bt_cancel, &QPushButton::clicked,
            this, &Overlay_Generic_Number_Input_Base::canceled);
}

void Overlay_Generic_Number_Input_Base::setMessage(QString const& msg)
{
   this->lbl_message->setText(msg);
}

QWidget* Overlay_Generic_Number_Input_Base::numberInputContainer()
{
   return this->number_input_container;
}
