
#pragma once

#include "generic_overlay.h"

#include "ui_overlay_generic_number_input.h"

#include "generic_number_input.h"
#include "overlay_value_input_common.h"

#include <QtWidgets/QVBoxLayout>

class Overlay_Generic_Number_Input_Base: public Generic_Overlay, private Ui::Overlay_Generic_Number_Input_Base
{
      Q_OBJECT
   public:
      explicit Overlay_Generic_Number_Input_Base(QWidget* parent = nullptr);

      void setMessage(QString const& msg);

   protected:
      QWidget* numberInputContainer();

   signals:
      void accepted();
      void canceled();
}; // class Overlay_Generic_Number_Input_Base

template<typename NUMBER_TYPE>
class Overlay_Generic_Number_Input: public Overlay_Generic_Number_Input_Base
{
   public:
      explicit Overlay_Generic_Number_Input(QWidget* parent = nullptr):
         Overlay_Generic_Number_Input_Base(parent)
      {
         QVBoxLayout* input_layout = new QVBoxLayout(this->numberInputContainer());
         input_layout->setMargin(0);
         this->numberInputContainer()->setLayout(input_layout);
         this->number_input = new Generic_Number_Input<NUMBER_TYPE>(this->numberInputContainer());
         Adjust_Number_Input<NUMBER_TYPE>::adjust(this->number_input);
         input_layout->addWidget(this->number_input);
      }

      void setPrefix(QString const& p)
      {
         this->number_input->setPrefix(p);
      }

      void setSuffix(QString const& s)
      {
         this->number_input->setSuffix(s);
      }

      void setValue(NUMBER_TYPE val)
      {
         this->number_input->setValue(val);
      }

      NUMBER_TYPE value() const
      {
         return this->number_input->value();
      }

      Generic_Number_Input<NUMBER_TYPE>* numberInput() const
      {
         return this->number_input;
      }

   private:
      Generic_Number_Input<NUMBER_TYPE>* number_input{nullptr};
}; // class Overlay_Generic_Number_Input<>
