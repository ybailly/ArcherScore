
#include "overlay_message.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>

Overlay_Message::Overlay_Message(QWidget* parent):
   Generic_Overlay(parent)
{
}

void Overlay_Message::setQuestion(
      QString const& title,
      QString const& text,
      Overlay_Message::Button_Set buttons)
{
   QFont fnt = this->font();
   QLabel* lbl_title = new QLabel(title, this->contents());
   fnt.setBold(true);
   lbl_title->setFont(fnt);
   lbl_title->setWordWrap(true);
   QLabel* lbl_text = new QLabel(text, this->contents());
   lbl_text->setWordWrap(true);
   lbl_text->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

   QPushButton* bt_left = new QPushButton(this->contents());
   QPushButton* bt_right = new QPushButton(this->contents());

   QVBoxLayout* vbox = new QVBoxLayout(this->contents());
   vbox->setMargin(2);
   vbox->setSpacing(8);
   vbox->addWidget(lbl_title);
   vbox->addWidget(lbl_text);
   QHBoxLayout* hbox = new QHBoxLayout();
   hbox->setMargin(0);
   hbox->setSpacing(2);
   hbox->addWidget(bt_left);
   hbox->addWidget(bt_right);
   vbox->addLayout(hbox);

   switch ( buttons )
   {
      case Button_Set::OK:
         bt_left->setText(tr("OK"));
         QObject::connect(
                  bt_left, &QPushButton::clicked,
                  [this]() { emit this->clickedButton(Button::OK); });
         bt_right->hide();
         break;
      case Button_Set::Yes_No:
         bt_left->setText(tr("Oui"));
         bt_right->setText(tr("Non"));
         QObject::connect(
                  bt_left, &QPushButton::clicked,
                  [this]() { emit this->clickedButton(Button::Yes); });
         QObject::connect(
                  bt_right, &QPushButton::clicked,
                  [this]() { emit this->clickedButton(Button::No); });
         break;
      case Button_Set::OK_Cancel:
         bt_left->setText(tr("OK"));
         bt_right->setText(tr("Cancel"));
         QObject::connect(
                  bt_left, &QPushButton::clicked,
                  [this]() { emit this->clickedButton(Button::OK); });
         QObject::connect(
                  bt_left, &QPushButton::clicked,
                  [this]() { emit this->clickedButton(Button::Cancel); });
         break;
   }
}
