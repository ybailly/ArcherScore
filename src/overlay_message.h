
#pragma once

#include "generic_overlay.h"

class Overlay_Message: public Generic_Overlay
{
      Q_OBJECT
   public:
      explicit Overlay_Message(QWidget* parent = nullptr);

      enum class Button
      {
         Yes,
         No,
         OK,
         Cancel
      }; // enum class Button
      enum class Button_Set
      {
         OK,
         Yes_No,
         OK_Cancel
      };

      void setQuestion(
            QString const& title,
            QString const& text,
            Button_Set buttons = Button_Set::Yes_No);

   signals:
      void clickedButton(Button button);

}; // class Overlay_Message
