
#pragma once

#include "overlay_generic_number_input.h"

#include "units_ui.h"

template<typename TAGGED_VALUE_TYPE>
class Overlay_Tagged_Value_Input: public Overlay_Generic_Number_Input<typename TAGGED_VALUE_TYPE::type>
{
   public:
      explicit Overlay_Tagged_Value_Input(QWidget* parent = nullptr):
         Overlay_Generic_Number_Input<typename TAGGED_VALUE_TYPE::type>(parent)
      {
         this->setSuffix(Unit_Traits<typename TAGGED_VALUE_TYPE::unit_tag>::symbol());
         Adjust_Number_Input<TAGGED_VALUE_TYPE>::adjust(this->numberInput());
      }

      void setValue(TAGGED_VALUE_TYPE val)
      {
         this->Overlay_Generic_Number_Input<typename TAGGED_VALUE_TYPE::type>::setValue(val.value());
      }

      TAGGED_VALUE_TYPE value() const
      {
         return TAGGED_VALUE_TYPE(this->Overlay_Generic_Number_Input<typename TAGGED_VALUE_TYPE::type>::value());
      }

}; // class Overlay_Tagged_Value_Input<>
