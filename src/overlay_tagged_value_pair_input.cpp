
#include "overlay_tagged_value_pair_input.h"

Overlay_Tagged_Value_Pair_Input_Base::Overlay_Tagged_Value_Pair_Input_Base(QWidget* parent):
   Generic_Overlay(parent)
{
   this->setupUi(this->contents());

   QObject::connect(
            this->bt_ok, &QPushButton::clicked,
            this, &Overlay_Tagged_Value_Pair_Input_Base::accepted);
   QObject::connect(
            this->bt_cancel, &QPushButton::clicked,
            this, &Overlay_Tagged_Value_Pair_Input_Base::canceled);
}

void Overlay_Tagged_Value_Pair_Input_Base::setMessage(QString const& msg)
{
   this->lbl_message->setText(msg);
}

QWidget* Overlay_Tagged_Value_Pair_Input_Base::numberInputContainer()
{
   return this->number_input_container;
}

QPushButton* Overlay_Tagged_Value_Pair_Input_Base::buttonUseFirstUnit() const
{
   return this->bt_use_unit_1;
}

QPushButton* Overlay_Tagged_Value_Pair_Input_Base::buttonUseSecondUnit() const
{
   return this->bt_use_unit_2;
}

void Overlay_Tagged_Value_Pair_Input_Base::setNumberInput(Generic_Number_Input_Base* nib)
{
   if ( nib == this->generic_number_input )
      return;
   if ( this->generic_number_input != nullptr )
   {
      QObject::disconnect(
               this->generic_number_input, &Generic_Number_Input_Base::valueChanged,
               this, &Overlay_Tagged_Value_Pair_Input_Base::valueChanged);
   }
   this->generic_number_input = nib;
   if ( this->generic_number_input != nullptr )
   {
      QObject::connect(
               this->generic_number_input, &Generic_Number_Input_Base::valueChanged,
               this, &Overlay_Tagged_Value_Pair_Input_Base::valueChanged);
   }
}
