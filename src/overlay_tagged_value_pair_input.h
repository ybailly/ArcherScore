
#pragma once

#include "generic_overlay.h"

#include "ui_overlay_tagged_value_pair_input.h"

#include "generic_number_input.h"
#include "overlay_value_input_common.h"

#include <QtWidgets/QVBoxLayout>

class Overlay_Tagged_Value_Pair_Input_Base: public Generic_Overlay, private Ui::Overlay_Tagged_Value_Pair_Input_Base
{
      Q_OBJECT
   public:
      explicit Overlay_Tagged_Value_Pair_Input_Base(QWidget* parent = nullptr);

      void setMessage(QString const& msg);

   protected:
      QWidget* numberInputContainer();
      QPushButton* buttonUseFirstUnit() const;
      QPushButton* buttonUseSecondUnit() const;
      void setNumberInput(Generic_Number_Input_Base* nib);

   signals:
      void accepted();
      void canceled();
      void valueChanged();

   private:
      Generic_Number_Input_Base* generic_number_input{nullptr};
}; // class Overlay_Generic_Number_Input_Base

template<typename TAGGED_VALUE_PAIR_TYPE>
class Overlay_Tagged_Value_Pair_Input: public Overlay_Tagged_Value_Pair_Input_Base
{
   public:
      using type_1 = typename TAGGED_VALUE_PAIR_TYPE::type_1;
      using type_2 = typename TAGGED_VALUE_PAIR_TYPE::type_2;
      using value_pair_t = TAGGED_VALUE_PAIR_TYPE;
      using unit_1_traits = Unit_Traits<typename type_1::unit_tag>;
      using unit_2_traits = Unit_Traits<typename type_2::unit_tag>;

      explicit Overlay_Tagged_Value_Pair_Input(QWidget* parent = nullptr):
         Overlay_Tagged_Value_Pair_Input_Base(parent)
      {
         QVBoxLayout* input_layout = new QVBoxLayout(this->numberInputContainer());
         input_layout->setMargin(0);
         this->numberInputContainer()->setLayout(input_layout);
         this->number_input = new Generic_Number_Input<typename type_1::type>(this->numberInputContainer());
         Adjust_Number_Input<type_1>::adjust(this->number_input);
         input_layout->addWidget(this->number_input);
         this->setNumberInput(this->number_input);

         this->buttonUseFirstUnit()->setText(unit_1_traits::name());
         this->buttonUseSecondUnit()->setText(unit_2_traits::name());

         QObject::connect(
                  this->buttonUseFirstUnit(), &QPushButton::clicked,
                  this, &Overlay_Tagged_Value_Pair_Input::applyUseFirstUnit);
         QObject::connect(
                  this->buttonUseSecondUnit(), &QPushButton::clicked,
                  this, &Overlay_Tagged_Value_Pair_Input::applyUseSecondUnit);
      }

      void setValue(value_pair_t val)
      {
         if ( val.preferred() == 0 )
         {
            this->buttonUseFirstUnit()->setText(QStringLiteral("%1%2").arg(QChar(0x25CF), unit_1_traits::name()));
            this->buttonUseFirstUnit()->setChecked(true);
            this->buttonUseSecondUnit()->setText(unit_2_traits::name());
            this->buttonUseSecondUnit()->setChecked(false);
            this->number_input->setSuffix(unit_1_traits::symbol());
            Adjust_Number_Input<type_1>::adjust(this->number_input);
         }
         else
         {
            this->buttonUseFirstUnit()->setText(unit_1_traits::name());
            this->buttonUseFirstUnit()->setChecked(false);
            this->buttonUseSecondUnit()->setText(QStringLiteral("%1%2").arg(QChar(0x25CF), unit_2_traits::name()));
            this->buttonUseSecondUnit()->setChecked(true);
            this->number_input->setSuffix(unit_2_traits::symbol());
            Adjust_Number_Input<type_2>::adjust(this->number_input);
         }
         this->preferred_unit = val.preferred();
         this->number_input->setValue(val.rawPreferredValue());
      }

      value_pair_t value() const
      {
         if ( this->preferred_unit == 0 )
         {
            return value_pair_t(type_1(this->number_input->value()));
         }
         else
         {
            return value_pair_t(type_2(this->number_input->value()));
         }
      }

      Generic_Number_Input<typename type_1::type>* numberInput() const
      {
         return this->number_input;
      }

   protected:
      void applyUseFirstUnit()
      {
         if ( this->preferred_unit == 0 )
            return;
         this->setValue(value_pair_t(this->value().v1()));
      }

      void applyUseSecondUnit()
      {
         if ( this->preferred_unit == 1 )
            return;
         this->setValue(value_pair_t(this->value().v2()));
      }

   private:
      Generic_Number_Input<typename type_1::type>* number_input{nullptr};
      uint8_t preferred_unit{0};

}; // class Overlay_Tagged_Value_Pair_Input<>
