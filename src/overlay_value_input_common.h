
#pragma once

#include "units_ui.h"
#include "generic_number_input.h"

template<typename NUMBER_TYPE>
struct Is_Integral
{
      static constexpr bool value = std::is_integral<NUMBER_TYPE>::value;
};

template<typename TYPE, typename DIM_TAG, typename UNIT_TAG>
struct Is_Integral<Tagged_Value<TYPE, DIM_TAG, UNIT_TAG>>
{
      static constexpr bool value = std::is_integral<TYPE>::value;
};

template<typename NUMBER_TYPE, bool IS_INTEGRAL = Is_Integral<NUMBER_TYPE>::value>
struct Adjust_Number_Input;

template<typename NUMBER_TYPE>
struct Adjust_Number_Input<NUMBER_TYPE, true>
{
      using number_input_type = Generic_Number_Input<NUMBER_TYPE, true>;
      static void adjust(number_input_type*) { }
};

template<typename NUMBER_TYPE>
struct Adjust_Number_Input<NUMBER_TYPE, false>
{
      using number_input_type = Generic_Number_Input<NUMBER_TYPE, false>;
      static void adjust(number_input_type* ni)
      {
         ni->setDoubleDecimals(3);
      }
};

template<typename TYPE, typename DIM_TAG, typename UNIT_TAG>
struct Adjust_Number_Input<Tagged_Value<TYPE, DIM_TAG, UNIT_TAG>, true>
{
      using number_input_type = Generic_Number_Input<TYPE, true>;
      static void adjust(number_input_type*) { }
};

template<typename TYPE, typename DIM_TAG, typename UNIT_TAG>
struct Adjust_Number_Input<Tagged_Value<TYPE, DIM_TAG, UNIT_TAG>, false>
{
      using number_input_type = Generic_Number_Input<TYPE, false>;
      using unit_traits = Unit_Traits<UNIT_TAG>;
      static void adjust(number_input_type* ni)
      {
         ni->setDoubleDecimals(unit_traits::decimalsForShort());
      }
};
