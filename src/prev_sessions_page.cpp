
#include "prev_sessions_page.h"

#include "sessions_list_model.h"
#include "sessions_list_delegate.h"
#include "overlay_message.h"
#include "qt_utils.h"

#include <QtDebug>

Prev_Sessions_Page::Prev_Sessions_Page(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setForwardVisible(false);
   this->setTitle(tr("Sessions précédentes"));

   this->sessions_list.refresh();
   auto sessions_list_model = new Sessions_List_Model(this);
   auto sessions_list_delegate = new Sessions_List_Delegate(this);
   this->lv_prev_sessions->setModel(sessions_list_model);
   this->lv_prev_sessions->setItemDelegate(sessions_list_delegate);
   sessions_list_model->setSessionsList(&this->sessions_list);
   sessions_list_delegate->setSessionsList(&this->sessions_list);
   this->refreshSessionsList();

   QObject::connect(
            this->lv_prev_sessions, &QListView::doubleClicked,
            [this](QModelIndex const& idx)
   {
      if ( not idx.isValid() )
      {
         return;
      }
      Session_Info const& session_info = this->sessions_list.sessionInfo(idx.row());
      emit this->requestRestoreSession(session_info.basename);
   });
   QObject::connect(
            this->lv_prev_sessions, &QListView::clicked,
            [this](QModelIndex const&)
   {
      QItemSelectionModel* selection = this->lv_prev_sessions->selectionModel();
      QModelIndexList selected = selection->selectedIndexes();
      this->bt_erase_session->setVisible(not selected.empty());
      this->bt_restore_session->setVisible(selected.size() == 1);
   });
   QObject::connect(
            this->bt_erase_session, &QPushButton::clicked,
            this, &Prev_Sessions_Page::deleteSelectedSessions);
   QObject::connect(
            this->bt_restore_session, &QPushButton::clicked,
            this, &Prev_Sessions_Page::restoreSelectedSession);
}

void Prev_Sessions_Page::refreshSessionsList()
{
   this->sessions_list.refresh();
   this->lv_prev_sessions->clearSelection();
   this->bt_erase_session->setVisible(false);
   this->bt_restore_session->setVisible(false);
}

void Prev_Sessions_Page::deleteSelectedSessions()
{
   QItemSelectionModel* selection = this->lv_prev_sessions->selectionModel();
   QModelIndexList selected = selection->selectedIndexes();
   if ( selected.empty() )
   {
      return;
   }
   Overlay_Message* msg = new Overlay_Message(this->topLevelWidget());
   msg->setQuestion(tr("Suppression"),
                    tr("Êtes-vous <i>sûr</i> de vouloir effacer ces <b>%1</b> sessions ?")
                    .arg(selected.count()));
   QObject::connect(
            msg, &Overlay_Message::clickedButton,
            [selected,msg,this](Overlay_Message::Button bt)
   {
      if ( bt == Overlay_Message::Button::Yes )
      {
         for(QModelIndex const& idx: selected)
         {
            Session_Info const& session_info = this->sessions_list.sessionInfo(idx.row());
            QFileInfo session_fi(qt::documentsDir(), session_info.basename);
            if ( QFile::remove(session_fi.absoluteFilePath()) )
            {
               qDebug() << "Removed" << QDir::toNativeSeparators(session_fi.absoluteFilePath());
            }
            else
            {
               qDebug() << "*** Failed to remove" << QDir::toNativeSeparators(session_fi.absoluteFilePath());
            }
         }
         this->refreshSessionsList();
      }
      msg->hide();
      msg->deleteLater();
   });
   msg->raise();
   msg->show();
}

void Prev_Sessions_Page::restoreSelectedSession()
{
   QItemSelectionModel* selection = this->lv_prev_sessions->selectionModel();
   QModelIndexList selected = selection->selectedIndexes();
   if ( selected.count() != 1 )
   {
      return;
   }
   Session_Info const& session_info = this->sessions_list.sessionInfo(selected.front().row());
   this->lv_prev_sessions->clearSelection();
   emit this->requestRestoreSession(session_info.basename);
}
