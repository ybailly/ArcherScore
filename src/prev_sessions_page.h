
#pragma once

#include "sessions_list.h"

#include "generic_page.h"

#include "ui_prev_sessions_page.h"

class Prev_Sessions_Page: public Generic_Page, private Ui::Prev_Sessions_Page
{
      Q_OBJECT
   public:
      explicit Prev_Sessions_Page(QWidget* parent = nullptr);

   public slots:
      void refreshSessionsList();
      void deleteSelectedSessions();
      void restoreSelectedSession();

   signals:
      void requestDeleteSession();
      void requestRestoreSession(QString const&);

   private:
      Sessions_List sessions_list;

}; // class Prev_Sessions_Page
