
#include "qt_utils.h"

#include <QtDebug>
#include <QtCore/QHash>
#include <QtCore/QSaveFile>
#include <QtWidgets/QWidget>
#include <QtGui/QFontMetrics>
#include <QtWidgets/QApplication>
#include <QtGui/QIcon>
#include <QtWidgets/QToolButton>
#include <QtCore/QStandardPaths>
#include <QtGui/QPen>
#include <QtGui/QPainter>
#include <QtWidgets/QLayout>
#include <QtWidgets/QLayoutItem>
#include <QtCore/QJsonDocument>
#ifdef ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#endif // ANDROID

namespace qt
{

   QString simplifyDoubleShortString(double value, int decimals)
   {
      QString str = QStringLiteral("%1").arg(value, 0, 'f', decimals);
      if ( str.contains('.') )
      {
         while ( str.endsWith('0') )
         {
            str.chop(1);
         }
         if ( str.endsWith('.') )
         {
            str.chop(1);
         }
      }
      return str;
   }

   int computeEM(QWidget const* widget)
   {
      QFontMetrics fmt =
            (widget != nullptr) ?
               widget->fontMetrics() :
               qApp->fontMetrics();
      return fmt.boundingRect(QChar('M')).size().width();
   }

   int computeEX(QWidget const* widget)
   {
      QFontMetrics fmt =
            (widget != nullptr) ?
               widget->fontMetrics() :
               qApp->fontMetrics();
      return fmt.boundingRect(QChar('x')).size().height();
   }

   QFont adjustFontSize(QFont const& fnt, double ratio)
   {
      QFont f = fnt;
      if ( f.pointSizeF() > 1.0 )
      {
         f.setPointSizeF(f.pointSizeF()*ratio);
         return f;
      }
      if ( f.pointSize() > 1 )
      {
         f.setPointSize(qRound(f.pointSize()*ratio));
         return f;
      }
      if ( f.pixelSize() > 1 )
      {
         f.setPixelSize(qRound(f.pixelSize()*ratio));
         return f;
      }
      return f;
   }

   QIcon const& icon(QString const& name)
   {
      static QHash<QString, QIcon> icons_cache;
      QIcon& the_icon = icons_cache[name];
      if ( the_icon.isNull() )
      {
         the_icon = QIcon(QStringLiteral(":/icons/%1").arg(name));
      }
      return the_icon;
   }

   void adjustToolButtonsIconSize(QWidget* w,
                                  double scale_1em)
   {
      QFontMetricsF fmt(w->font());
      double const em = fmt.boundingRect('M').width();
      int const icon_size = qRound(scale_1em*em);
      QList<QToolButton*> tbs = w->findChildren<QToolButton*>();
      for(QToolButton* tb: tbs)
      {
         tb->setIconSize(QSize(icon_size, icon_size));
      }
   }

   QDir const& documentsDir()
   {
      static QDir docs_dir;
      static bool have_docs_dir{false};
      if ( not have_docs_dir )
      {
   #ifdef ANDROID
         QAndroidJniObject jni_DIRECTORY_DOCUMENTS = QAndroidJniObject::getStaticObjectField<jstring>("android/os/Environment", "DIRECTORY_DOCUMENTS");
         QAndroidJniObject jni_ext_store_dir =
               QAndroidJniObject::callStaticObjectMethod(
                  "android/os/Environment",
                  "getExternalStoragePublicDirectory",
                  "(Ljava/lang/String;)Ljava/io/File;",
                  jni_DIRECTORY_DOCUMENTS.object<jstring>());
         QAndroidJniObject jni_ext_store_path = jni_ext_store_dir.callObjectMethod( "getAbsolutePath", "()Ljava/lang/String;" );
         QString ext_store_path = jni_ext_store_path.toString();
         docs_dir = QDir(ext_store_path);
         docs_dir.mkpath(ext_store_path);
   #else
       #ifdef WIN32
         docs_dir = QDir(QCoreApplication::applicationDirPath());
       #else
         docs_dir = QDir(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first());
       #endif // WIN32
   #endif // ANDROID
         docs_dir.mkpath(".");
         if ( not docs_dir.absolutePath().endsWith("ArcherScore") )
         {
            if ( not docs_dir.mkpath("ArcherScore") )
            {
               qDebug() << "*** Failed to create \"ArcherScore\" in" << docs_dir;
            }
            if ( not docs_dir.cd("ArcherScore") )
            {
               qDebug() << "*** Failed to cd into" << docs_dir;
            }
         }
         qDebug() << "Using documents' dir" << docs_dir;
         have_docs_dir = true;
      }
      return docs_dir;
   }

   bool writeJsonDocument(QJsonDocument const& doc,
                         QString const& basename)
   {
      QFileInfo json_fi(documentsDir(), basename);
      QSaveFile json_save(json_fi.absoluteFilePath());
      if ( json_save.open(QIODevice::WriteOnly) )
      {
         json_save.write(doc.toJson(QJsonDocument::Indented));
         if ( json_save.commit() )
         {
            qDebug() << "Wrote" << QDir::toNativeSeparators(json_save.fileName());
            return false;
         }
         else
         {
            qDebug() << "*** Failed to write" << QDir::toNativeSeparators(json_save.fileName());
            return false;
         }
      }
      else
      {
         qDebug() << "*** Failed to open" << QDir::toNativeSeparators(json_fi.absoluteFilePath()) << "for writing";
         return false;
      }
      return true;
   }

   QJsonDocument readJsonDocument(QString const& basename)
   {
      QFileInfo json_fi(documentsDir(), basename);
      if ( not (json_fi.exists() and json_fi.isReadable()) )
      {
         return {};
      }
      QFile json_f(json_fi.absoluteFilePath());
      if ( not json_f.open(QIODevice::ReadOnly) )
      {
         qDebug() << "*** Failed to read" << json_f.fileName();
         return {};
      }
      qDebug() << "Reading" << QDir::toNativeSeparators(json_f.fileName());
      QJsonDocument json = QJsonDocument::fromJson(json_f.readAll());
      json_f.close();
      return json;
   }

   void paintLayout(QWidget* w)
   {
      QPainter p(w);
      QPen pen(Qt::red, 1);
      pen.setCosmetic(true);
      p.setPen(pen);
      p.setBrush(Qt::NoBrush);
      QLayout* layout = w->layout();
      if ( layout == nullptr )
      {
         return;
      }
      for(int li = 0; li < layout->count(); ++li)
      {
         QLayoutItem* item = layout->itemAt(li);
         p.drawRect(item->geometry());
      }
      p.setPen(Qt::green);
      p.drawRect(layout->geometry());
   }

} // namespace qt
