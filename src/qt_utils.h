
#pragma once

#include <QtCore/QString>
#include <QtGui/QFont>
#include <QtCore/QDir>

class QJsonDocument;
class QWidget;

namespace qt
{
   QString simplifyDoubleShortString(double value, int decimals);

   int computeEM(QWidget const* w);
   int computeEX(QWidget const* w);

   QFont adjustFontSize(QFont const& fnt, double ratio);

   QIcon const& icon(QString const& name);

   void adjustToolButtonsIconSize(QWidget* w,
                                  double scale_1em);

   QDir const& documentsDir();

   bool writeJsonDocument(QJsonDocument const& doc,
                          QString const& basename);
   QJsonDocument readJsonDocument(QString const& basename);

   //! To see the layouts - to be called only from QWidget::paintEvent()!
   void paintLayout(QWidget* w);

} // namespace qt
