
#include "score_unit.h"
#include "common_types_utils.h"

#include <QtDebug>
#include <QtCore/QJsonObject>

Score_Unit::Score_Unit(Mark m)
{
   this->set(m);
}

Score_Unit::Score_Unit(Mark s,
                       Hit_Location loc)
{
   this->set(s, loc);
}

Score_Unit::Score_Unit(QJsonObject const& json,
                       int version)
{
   if ( version > 3 )
   {
      qDebug() << "*** Unknown version" << version << "while reading score unit";
   }
   if ( version <= 2 )
   {
      bool is_defined = json["is_defined"].toBool();
      if ( is_defined )
      {
         this->the_mark = static_cast<Mark>(json["mark"].toInt());
         this->hit_loc = {static_cast<float>(json["location_x"].toDouble()),
                          static_cast<float>(json["location_y"].toDouble())};
         if ( version == 1 )
         {
            this->hit_loc.setY(-this->hit_loc.y());
         }
         this->loc_is_defined = true;
         this->is_enabled_for_group = json["is_enabled"].toBool();
      }
      else
      {
         this->the_mark = Mark::Undefined;
         this->loc_is_defined = false;
         this->is_enabled_for_group = false;
         this->hit_loc = {-2.0,-2.0};
      }
      this->loc_is_defined = json["is_defined"].toBool();
   }
   if ( version >= 3 )
   {
      this->the_mark = markFromString(json["mark"].toString());
      this->is_enabled_for_group = json["is_enabled"].toBool();
      this->loc_is_defined = json["location_is_defined"].toBool();
      if ( this->loc_is_defined )
      {
         this->hit_loc = {static_cast<float>(json["location_x"].toDouble()),
                          static_cast<float>(json["location_y"].toDouble())};
      }
      else
      {
         this->hit_loc = {-2.0,-2.0};
      }
   }
}

Mark Score_Unit::mark() const
{
   return this->the_mark;
}

int8_t Score_Unit::score() const
{
   return markToScore(this->the_mark);
}

Hit_Location Score_Unit::location() const
{
   return this->hit_loc;
}

Hit_Location const& Score_Unit::locationRef() const
{
   return this->hit_loc;
}

void Score_Unit::set(Mark m)
{
   this->the_mark = m;
   this->hit_loc = {-1.0,-1.0};
   this->loc_is_defined = false;
}

void Score_Unit::set(Mark m, Hit_Location const& l)
{
   int8_t const score = markToScore(m);
   if ( (0 < score) and (score < 11) )
   {
      this->the_mark = m;
      this->hit_loc = l;
      this->loc_is_defined = true;
   }
   else
   {
      this->set(m);
   }
}

bool Score_Unit::isLocationDefined() const
{
   return this->loc_is_defined;
}

bool Score_Unit::isEnabledForGroup() const
{
   return this->is_enabled_for_group and this->loc_is_defined;
}

void Score_Unit::setEnabledForGroup(bool e)
{
   this->is_enabled_for_group = e;
}

QJsonObject Score_Unit::toJson() const
{
   QJsonObject score_unit_object;
   score_unit_object.insert("mark", markToString(this->the_mark));
   score_unit_object.insert("location_is_defined", this->loc_is_defined);
   if ( this->loc_is_defined )
   {
      score_unit_object.insert("location_x", this->hit_loc.x());
      score_unit_object.insert("location_y", this->hit_loc.y());
   }
   score_unit_object.insert("is_enabled", this->is_enabled_for_group);
   return score_unit_object;
}

void appendToPointSet(std::vector<Score_Unit> const& from,
                      Point_Set* ps)
{
   if ( ps == nullptr )
   {
      return;
   }
   ps->reserve(ps->size()+from.size());
   for(Score_Unit const& su: from)
   {
      if ( su.isLocationDefined() and su.isEnabledForGroup() and (su.score() > 0) )
      {
         ps->push_back({su.location().x(), su.location().y()});
      }
   }
}

