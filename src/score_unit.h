
#pragma once

#include "common_types.h"
#include "geometry_utils.h"

#include <vector>

class QJsonObject;

class Score_Unit
{
   public:
      explicit Score_Unit(Mark m);
      Score_Unit(Mark s,
                 Hit_Location loc);
      Score_Unit(QJsonObject const& json,
                 int version);

      Score_Unit() = default;
      Score_Unit(Score_Unit const&) = default;
      Score_Unit(Score_Unit&&) = default;
      ~Score_Unit() = default;

      Score_Unit& operator=(Score_Unit const&) = default;
      Score_Unit& operator=(Score_Unit&&) = default;

      Mark mark() const;
      int8_t score() const;
      Hit_Location location() const;
      Hit_Location const& locationRef() const;

      void set(Mark m);
      void set(Mark m, Hit_Location const& l);

      bool isLocationDefined() const;
      bool isEnabledForGroup() const;

      void setEnabledForGroup(bool e);

      QJsonObject toJson() const;

   private:
      Mark the_mark{Mark::Undefined};
      Hit_Location hit_loc{0.0f, 0.0f};
      bool loc_is_defined{false};
      bool is_enabled_for_group{true};
}; // class Score_Unit

void appendToPointSet(std::vector<Score_Unit> const& from,
                      Point_Set* ps);

