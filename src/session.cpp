
#include "session.h"

#include "ends_round.h"
#include "common_types_utils.h"
#include "units_ui.h"
#include "qt_utils.h"
#include "ui_utils.h"

#include <QtDebug>
#include <QtCore/QDateTime>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonValue>
#include <QtCore/QFileInfo>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include <QtCore/QBuffer>
#include <QtGui/QTextDocument>
#include <QtGui/QTextFrame>
#include <QtGui/QPdfWriter>
#include <QtGui/QPainter>
#include <QtGui/QTextCursor>
#include <QtGui/QTextTable>
#include <QtGui/QTextTableCell>
#include <QtGui/QImageWriter>
#include <QtGui/QStaticText>
#include <QtSvg/QSvgRenderer>

#include <array>
#include <vector>
#include <algorithm>

Session::Session(uint8_t shot_count,
                 Session_Type sess_type):
   creation_date(QDateTime::currentDateTimeUtc()),
   shot_count(shot_count),
   session_type(sess_type)
{
   this->doc_file_basename =
         QString("ArcherScore_%1.as")
         .arg(this->creation_date.toString(QStringLiteral("yyyyMMdd-HHmmsst")));
}

Session::Session(QString const& basename):
      doc_file_basename(basename)
{
   this->loadFromFile();
}

Session_Type Session::type() const
{
   return this->session_type;
}

void Session::setArrowSet(Arrow_Set const& as)
{
   this->arrow_set = as;
   this->have_arrow_set = true;
}

void Session::setBow(Bow const& b)
{
   this->bow_ = b;
   this->have_bow = true;
}

void Session::setTargetDiameter(Diameter_Cm_In td)
{
   this->target_diameter = td;
}

void Session::setTargetMinimumScore(int8_t ms)
{
   this->target_minimum_score = ms;
}

void Session::setTargetDistance(Distance_M_Yd d)
{
   this->target_distance = d;
}

void Session::setArrowSizeSpecification(Arrow_Size_Spec const& ass)
{
   this->arrow_size_spec = ass;
}

void Session::setLocation(Session_Location loc)
{
   this->session_location = loc;
}

void Session::setTitle(QString const& titl)
{
   this->title_ = titl;
}

void Session::setDescription(QString const& descr)
{
   this->description_ = descr;
}

bool Session::haveArrowSet() const
{
   return this->have_arrow_set;
}

Arrow_Set const& Session::arrowSet() const
{
   return this->arrow_set;
}

bool Session::haveBow() const
{
   return this->have_bow;
}

Bow const& Session::bow() const
{
   return this->bow_;
}

QDateTime const& Session::creationDate() const
{
   return this->creation_date;
}

Diameter_Cm_In Session::targetDiameter() const
{
   return this->target_diameter;
}

int8_t Session::targetMinimumScore() const
{
   return this->target_minimum_score;
}

Distance_M_Yd Session::targetDistance() const
{
   return this->target_distance;
}

Arrow_Size_Spec const& Session::arrowSizeSpecification() const
{
   return this->arrow_size_spec;
}

Session_Location Session::location() const
{
   return this->session_location;
}

QString const& Session::title() const
{
   return this->title_;
}

QString const& Session::description() const
{
   return this->description_;
}

uint8_t Session::shotCount() const
{
   return this->shot_count;
}

void Session::appendEndsRound()
{
   this->rounds.emplace_back(this->shot_count);
}

size_t Session::endsRoundsCount() const
{
   return this->rounds.size();
}

Ends_Round const& Session::endsRound(Round_Index index) const
{
   return this->rounds.at(index.value());
}

Ends_Round* Session::mutableEndsRound(Round_Index index) const
{
   return &(this->rounds[index.value()]);
}

bool Session::is10Plus(Score_Unit const& su) const
{
   if ( su.score() < 10 )
      return false;
   else
      return this->is10Plus(su.location());
}

bool Session::is10Plus(Hit_Location const& hl) const
{
   // 10+ radius is half 10's radius, so 0.05
   // normalized arrows' radius
   double const norm_arrows_radius =
         (this->arrow_size_spec.absoluteDiameter().value()) /
         (this->targetDiameter().v1().value() * 10.0);
   return std::sqrt(static_cast<double>(Hit_Location::dotProduct(hl,hl))) - norm_arrows_radius < 0.05;
}

uint16_t Session::n10Plus(Round_Index rid) const
{
   Ends_Round const& round = this->endsRound(rid);
   return
      std::accumulate(
               round.begin(),
               round.end(),
               uint16_t(0),
               [this](uint16_t acc_r, Single_End const& se)
      {
         return
               std::accumulate(
                  se.begin(),
                  se.end(),
                  acc_r,
                  [this](uint16_t acc_e, Score_Unit const& su)
         {
            return this->is10Plus(su) ? (acc_e+1) : acc_e;
         });
      });
}

Session::iterator Session::begin()
{
   return this->rounds.begin();
}

Session::iterator Session::end()
{
   return this->rounds.end();
}

Session::const_iterator Session::begin() const
{
   return this->rounds.begin();
}

Session::const_iterator Session::end() const
{
   return this->rounds.end();
}

Session::const_iterator Session::cbegin() const
{
   return this->rounds.cbegin();
}

Session::const_iterator Session::cend() const
{
   return this->rounds.cend();
}

QFileInfo Session::file() const
{
   return QFileInfo(qt::documentsDir(), this->doc_file_basename);
}

namespace
{
   struct Session_Json
   {
         static QString const version;
         static QString const type;
         static QString const title;
         static QString const description;
         static QString const date_utc;
         static QString const arrow_set;
         static QString const bow;
         static QString const target_diameter;
         static QString const distance;
         static QString const arrows_diameter_spec;
         static QString const target_minimum_score;
         static QString const shots_count;
         static QString const location;
         static QString const rounds;
   };
   QString const Session_Json::version{QStringLiteral("version")};
   QString const Session_Json::type{QStringLiteral("type")};
   QString const Session_Json::title{QStringLiteral("title")};
   QString const Session_Json::description{QStringLiteral("description")};
   QString const Session_Json::date_utc{QStringLiteral("date_utc")};
   QString const Session_Json::arrow_set{QStringLiteral("arrow_set")};
   QString const Session_Json::bow{QStringLiteral("bow")};
   QString const Session_Json::target_diameter{QStringLiteral("target_diameter")};
   QString const Session_Json::distance{QStringLiteral("distance")};
   QString const Session_Json::arrows_diameter_spec{QStringLiteral("arrows_diameter_spec")};
   QString const Session_Json::target_minimum_score{QStringLiteral("target_minimum_score")};
   QString const Session_Json::shots_count{QStringLiteral("shots_count")};
   QString const Session_Json::location{QStringLiteral("location")};
   QString const Session_Json::rounds{QStringLiteral("rounds")};
}

void Session::saveToFile() const
{
   QJsonDocument doc;
   QJsonObject root;
   root.insert(Session_Json::version, 3);
   root.insert(Session_Json::type, sessionTypeToString(this->session_type));
   root.insert(Session_Json::title, this->title());
   root.insert(Session_Json::description, this->description());
   root.insert(Session_Json::date_utc, this->creation_date.toString(Qt::ISODate));
   if ( this->have_arrow_set )
   {
      root.insert(Session_Json::arrow_set, this->arrow_set.toJson());
   }
   if ( this->have_bow )
   {
      root.insert(Session_Json::bow, this->bow_.toJson());
   }
   root.insert(Session_Json::target_diameter, toLongString(this->target_diameter));
   root.insert(Session_Json::distance, toLongString(this->target_distance));
   root.insert(Session_Json::arrows_diameter_spec, this->arrowSizeSpecification().toString());
   root.insert(Session_Json::target_minimum_score, this->targetMinimumScore());
   root.insert(Session_Json::shots_count, static_cast<int>(this->shotCount()));
   root.insert(Session_Json::location, sessionLocationToString(this->session_location));

   QJsonArray rounds_array;
   for(Ends_Round const& round: this->rounds)
   {
      rounds_array.append(round.toJson());
   }
   root.insert(Session_Json::rounds, rounds_array);
   doc.setObject(root);
   qt::writeJsonDocument(doc, this->doc_file_basename);
}

void Session::loadFromFile()
{
   QJsonDocument doc = qt::readJsonDocument(this->doc_file_basename);
   QJsonObject root = doc.object();
   int const version = root[Session_Json::version].toInt();
   if ( version <= 3 )
   {
      if ( version <= 1 )
      {
         if ( root[Session_Json::type].toString() == "tournament_youths_special" )
            this->session_type = Session_Type::Mixed;
         else
            this->session_type = sessionTypeFromString(root[Session_Json::type].toString());
      }
      if ( version >= 2 )
      {
         this->session_type = sessionTypeFromString(root[Session_Json::type].toString());
      }
      this->title_ = root[Session_Json::title].toString();
      this->description_ = root[Session_Json::description].toString();
      this->creation_date = QDateTime::fromString(root[Session_Json::date_utc].toString(), Qt::ISODate);
      this->arrow_size_spec = Arrow_Size_Spec(root[Session_Json::arrows_diameter_spec].toString());
      this->shot_count = root[Session_Json::shots_count].toInt();
      this->session_location = sessionLocationFromString(root[Session_Json::location].toString());

      QJsonArray rounds_array = root[Session_Json::rounds].toArray();
      this->rounds.reserve(rounds_array.size());
      for(QJsonValue const& round_value: rounds_array)
      {
         this->rounds.emplace_back(round_value.toObject(), version);
      }
      if ( version <= 1 )
      {
         this->target_diameter = Diameter_Cm_In(Diameter_CM(root[QStringLiteral("target_diameter_cm")].toDouble()));
         this->target_distance = Distance_M_Yd(Distance_M(root[QStringLiteral("distance_m")].toDouble()));

      }
      if ( version >= 2 )
      {
         fillFromString(this->target_diameter, root[Session_Json::target_diameter].toString());
         fillFromString(this->target_distance, root[Session_Json::distance].toString());
         auto iter = root.find(Session_Json::bow);
         if ( iter != root.end() )
         {
            this->bow_.fromJson(iter.value().toObject());
            this->have_bow = true;
         }
         iter = root.find(Session_Json::arrow_set);
         if ( iter != root.end() )
         {
            this->arrow_set.fromJson(iter.value().toObject());
            this->have_arrow_set = true;
         }
      }
      if ( version <= 2 )
      {
         this->target_minimum_score = root[QStringLiteral("target_minimum_mark")].toInt();
         // adjusting marks for 10+, which where not recorded before
         for(Ends_Round& ends_round: this->rounds)
         {
            for(End_Index ei{0}; ei < ends_round.endsCount(); ++ei)
            {
               Single_End const& single_end = ends_round.singleEnd(ei);
               for(Score_Index si{0}; si < single_end.nbShots(); ++si)
               {
                  Score_Unit su = single_end.scoreUnit(si);
                  if ( this->is10Plus(su.location()) )
                  {
                     su.set(Mark::TenPlus, su.location());
                     ends_round.setScore(ei, si, su);
                  }
               }
            }
         }
      }
      if ( version >= 3 )
      {
         this->target_minimum_score = root[Session_Json::target_minimum_score].toInt();
      }
   }
   else
   {
      qDebug() << "*** Unknown file version";
   }
}

void sessionToPdf(Session const& s)
{
   QFileInfo fi = s.file();
   fi = QFileInfo(fi.absoluteDir(), fi.completeBaseName()+".pdf");
   QPdfWriter pdf(fi.absoluteFilePath());
   pdf.setResolution(300);
   pdf.setPageSize(QPageSize(QPageSize::A4));
   if ( (s.type() == Session_Type::Simple) or (s.endsRoundsCount() <= 1) )
   {
      pdf.setPageOrientation(QPageLayout::Portrait);
   }
   else
   {
      pdf.setPageOrientation(QPageLayout::Landscape);
   }
   pdf.setPageMargins(QMarginsF(10.0, 10.0, 10.0, 10.0), QPageLayout::Millimeter);
   pdf.setTitle(s.title());
   double const pdf_width = static_cast<double>(pdf.width());
   double const pdf_height = static_cast<double>(pdf.height());
   QPainter p(&pdf);
   QFont const base_fnt = []()
   {
      QFont f("sans-serif");
      f.setPointSizeF(10.0);
      return f;
   }();
   p.setFont(base_fnt);
   auto draw_text = [&p](QPointF pos, QString const& s) -> QSizeF
   {
      QTextOption options;
      options.setWrapMode(QTextOption::NoWrap);
      QStaticText stext;
      stext.setTextFormat(Qt::RichText);
      stext.setTextOption(options);
      stext.setText(s);
      p.drawStaticText(pos, stext);
      return stext.size();
   };
   QString const location_str = [&s]()
   {
      switch ( s.location() )
      {
         case Session_Location::Indoor:
            return QStringLiteral("En intérieur");
            break;
         case Session_Location::Outdoor:
            return QStringLiteral("En extérieur");
            break;
      }
      return QString();
   }();
   QSizeF text_size =
         draw_text(
            QPointF(0.0,0.0),
            QString("<big><b>%1</b></big><br/>%2 - %3")
            .arg(s.title())
            .arg(s.creationDate().date().toString(Qt::SystemLocaleLongDate))
            .arg(location_str));
   {
      // logo
      QSvgRenderer svg(QStringLiteral(":/images/logo"));
      double const w_on_h_ratio =
            static_cast<double>(svg.defaultSize().width()) /
            static_cast<double>(svg.defaultSize().height());
      double const wanted_w = w_on_h_ratio * text_size.height();
      svg.render(&p,
                 QRectF(pdf_width-wanted_w, 0.0, wanted_w, text_size.height()));
   }
   p.translate(0.0, text_size.height()+2.0);
   p.setPen(QPen(Qt::darkGray, 1.0));
   p.drawLine(QPointF(0.0, 0.0), QPointF(pdf.width(), 0.0));
   p.setPen(QPen(Qt::black));
   QString details_str =
         QStringLiteral("<i>Cible : </i>%1 à %2")
         .arg(toShortString(s.targetDiameter()))
         .arg(toShortString(s.targetDistance()));
   if ( s.haveBow() and (s.bow().type != Bow_Type::Unknown) )
   {
      details_str.append(
               QStringLiteral(" | <i>Arc : </i>%1, %2 à l'allonge %3")
               .arg(textForBowType(s.bow().type))
               .arg(toShortString(s.bow().power_at_draw))
               .arg(toShortString(s.bow().draw_length))
               );
   }
   if ( s.haveArrowSet() )
   {
      Arrow_Set const& as = s.arrowSet();
      details_str.append(
               QStringLiteral(" | <i>Flèches : </i>%1%2, %3, spine %4")
               .arg(QChar(0x2300))
               .arg(toShortString(as.diameter))
               .arg(toShortString(as.length))
               .arg(toShortString<void>(as.spine)));

   }
   text_size = draw_text(QPointF(0.0, 2.0), details_str);
   p.translate(0.0, text_size.height()+32.0);
   double const results_top_y = p.transform().dy();
   text_size = draw_text(QPointF(0.0, 0.0), "<b>Série principale</b>");
   p.translate(0.0, text_size.height()+8.0);
   auto round_summary = [&](Ends_Round const& round, bool with_cut) -> QSizeF
   {
      QRectF const r(0.0,0.0,1000.0,1000.0);
      int const flags = Qt::AlignCenter bitor Qt::TextSingleLine;
      QString const n10_str = QString("%1").arg(round.n10());
      QString const n10plus_str = QString("%1").arg(round.n10Plus());
      QString const mark_avg = QString("%1").arg(round.scoreAverage(), 0, 'f', 2);
      QString const cc_avg = QString("%1").arg(round.concentrationCoefficientAverage().value(), 0, 'f', 2);
      QString const mark_sum = QString("%1").arg(round.scoreSum());
      QString const cut_str = (with_cut ? QString("%1").arg(round.cut()) : QString());
      // n9 | n10 | x̄ | c̅ | Σ | cut
      std::array<QSizeF, 12> const sizes = {
         p.boundingRect(r, flags, "10").size(),
         p.boundingRect(r, flags, "10+").size(),
         p.boundingRect(r, flags, "x̄").size(),
         p.boundingRect(r, flags, "c̅").size(),
         p.boundingRect(r, flags, "Σ").size(),
         with_cut ? p.boundingRect(r, flags, "cut").size() : QSizeF(0.0,0.0),
         p.boundingRect(r, flags, n10_str).size(),
         p.boundingRect(r, flags, n10plus_str).size(),
         p.boundingRect(r, flags, mark_avg).size(),
         p.boundingRect(r, flags, cc_avg).size(),
         p.boundingRect(r, flags, mark_sum).size(),
         with_cut ? p.boundingRect(r, flags, cut_str).size() : QSizeF(0.0,0.0)
      };
      double const max_w =
            std::max_element(sizes.begin(),
                             sizes.end(),
                             [](QSizeF const& s1, QSizeF const& s2)
                             { return s1.width() < s2.width(); })->width();
      double const max_h =
            std::max_element(sizes.begin(),
                             sizes.end(),
                             [](QSizeF const& s1, QSizeF const& s2)
                             { return s1.height() < s2.height(); })->height();
      double const good_w = max_w*1.6;
      double const good_h = max_h*1.3;
      QRectF const cell_rect(0.0, 0.0, good_w, good_h);
      size_t const nb_cols = (with_cut ? 6 : 5);
      QSizeF const total_size(good_w*nb_cols, good_h*2.0);
      p.fillRect(QRectF(0.0, 0.0, nb_cols*good_w, good_h), Qt::lightGray);
      QFont fnt = base_fnt;
      fnt.setBold(true);
      p.setFont(fnt);
      QTransform saved_transform = p.transform();
      for(char const* s: {"10", "10+", "x̄", "c̅", "Σ"})
      {
         p.drawText(cell_rect, Qt::AlignCenter, s);
         p.translate(good_w, 0.0);
      }
      if ( with_cut )
      {
         p.drawText(cell_rect, Qt::AlignCenter, "cut");
      }
      p.setTransform(saved_transform);
      p.translate(0.0, good_h);
      p.setFont(base_fnt);
      p.drawText(cell_rect, Qt::AlignCenter, n10_str);
      p.translate(good_w, 0.0);
      p.drawText(cell_rect, Qt::AlignCenter, n10plus_str);
      p.translate(good_w, 0.0);
      p.drawText(cell_rect, Qt::AlignCenter, mark_avg);
      p.translate(good_w, 0.0);
      p.setPen(colorForConcentrationCoefficient(round.concentrationCoefficientAverage()));
      p.drawText(cell_rect, Qt::AlignCenter, cc_avg);
      p.setPen(Qt::black);
      p.translate(good_w, 0.0);
      p.drawText(cell_rect, Qt::AlignCenter, mark_sum);
      p.translate(good_w, 0.0);
      if ( with_cut )
      {
         p.fillRect(cell_rect,
                    (round.cut() < round.scoreSum()) ?
                       QBrush(QColor(192,255,192)) :
                       QBrush(QColor(255,216,216)));
         p.drawText(cell_rect, Qt::AlignCenter, cut_str);
      }
      p.setTransform(saved_transform);
      p.setPen(QPen(Qt::black, 1.0));
      for(size_t r = 0; r < 3; ++r)
      {
         double const y = r * good_h;
         p.drawLine(QPointF(0.0, y), QPointF(total_size.width(), y));
      }
      for(size_t c = 0; c < nb_cols+1; ++c)
      {
         double const x = c * good_w;
         p.drawLine(QPointF(x, 0.0), QPointF(x, total_size.height()));
      }
      return total_size;
   }; // auto round_summary
   Ends_Round const& main_round = s.endsRound(Round_Index(0));
   QSizeF summary_size = round_summary(main_round, false);
   p.translate(0.0, summary_size.height()+8.0);
   auto round_table = [&](Ends_Round const& round) -> QSizeF
   {
      QRectF const r(0.0,0.0,1000.0,1000.0);
      int const flags = Qt::AlignCenter bitor Qt::TextSingleLine;
      size_t const nb_cols = round.shotCountByEnd() + 2;
      size_t const nb_rows = round.endsCount()+1;
      std::vector<std::vector<QString>> strs;
      strs.reserve(nb_rows);
      {
         std::vector<QString> header;
         header.reserve(nb_cols);
         for(size_t shot = 0; shot < round.shotCountByEnd(); ++shot)
         {
            header.push_back(QString("F%1").arg(shot+1));
         }
         header.push_back("c");
         header.push_back("Σ");
         strs.push_back(std::move(header));
      }
      for(Single_End const& end: round)
      {
         std::vector<QString> values;
         values.reserve(nb_cols);
         for(Score_Unit const& su: end)
         {
            values.push_back(markToString(su.mark()));
         }
         values.push_back(QString("%1").arg(end.concentrationCoefficient().value(), 0, 'f', 2));
         values.push_back(QString("%1").arg(end.scoreSum()));
         strs.push_back(std::move(values));
      }
      std::vector<QSizeF> sizes;
      sizes.reserve(strs.size()*nb_cols);
      for(auto const& row: strs)
      {
         for(QString const& v: row)
         {
            sizes.push_back(p.boundingRect(r, flags, v).size());
         }
      }
      double const max_w =
            std::max_element(sizes.begin(),
                             sizes.end(),
                             [](QSizeF const& s1, QSizeF const& s2)
                             { return s1.width() < s2.width(); })->width();
      double const max_h =
            std::max_element(sizes.begin(),
                             sizes.end(),
                             [](QSizeF const& s1, QSizeF const& s2)
                             { return s1.height() < s2.height(); })->height();
      double const good_w = max_w*1.6;
      double const good_h = max_h*1.3;
      QRectF const cell_rect(0.0, 0.0, good_w, good_h);
      QSizeF const total_size(good_w*nb_cols, good_h*nb_rows);
      p.fillRect(QRectF(0.0, 0.0, total_size.width(), good_h), Qt::lightGray);
      QTransform const saved_transform = p.transform();
      QFont fnt = base_fnt;
      fnt.setBold(true);
      p.setFont(fnt);
      for(QString const& s: strs.front())
      {
         p.drawText(cell_rect, Qt::AlignCenter, s);
         p.translate(good_w, 0.0);
      }
      p.setFont(base_fnt);
      p.translate(-total_size.width(), good_h);
      auto row_iter = strs.begin() + 1;
      for(Single_End const& end: round)
      {
         auto str_iter = row_iter->begin();
         for(Score_Unit const& su: end)
         {
            p.fillRect(cell_rect, textBackgroundColorForMark(su.mark()));
            p.drawText(cell_rect, Qt::AlignCenter, *str_iter);
            p.translate(good_w, 0.0);
            ++str_iter;
         }
         p.setPen(colorForConcentrationCoefficient(end.concentrationCoefficient()));
         p.drawText(cell_rect, Qt::AlignCenter, *str_iter);
         p.translate(good_w, 0.0);
         p.setPen(Qt::black);
         ++str_iter;
         p.drawText(cell_rect, Qt::AlignCenter, *str_iter);
         p.translate(good_w, 0.0);
         p.translate(-total_size.width(), good_h);
         ++row_iter;
      }
      p.setTransform(saved_transform);
      p.setPen(QPen(Qt::black, 1.0));
      for(size_t r = 0; r < nb_rows+1; ++r)
      {
         double const y = r * good_h;
         p.drawLine(QPointF(0.0, y), QPointF(total_size.width(), y));
      }
      for(size_t c = 0; c < nb_cols+1; ++c)
      {
         double const x = c * good_w;
         p.drawLine(QPointF(x, 0.0), QPointF(x, total_size.height()));
      }
      return total_size;
   }; // auto round_table
   QSizeF table_size = round_table(main_round);
   p.translate(0.0, table_size.height()+8.0);
   double target_width = std::min(pdf_height-p.transform().dy(), table_size.width());
   QRectF target_rect = QRectF(
                           0.0, 0.0,
                           std::max(target_width, table_size.width()),
                           std::max(target_width, pdf_height-p.transform().dy()));
   auto draw_target = [&](Ends_Round const& round, QRectF in_rect)
   {
      double const ref_width = std::min(in_rect.width(), in_rect.height());
      int8_t min_score = 10;
      for(Single_End const& end: round)
      {
         for(Score_Unit const& su: end)
         {
            if ( su.isLocationDefined() and (su.score() > 0) )
            {
               min_score = std::min(min_score, su.score());
            }
         }
      }
      size_t nb_rings = 11 - min_score;
      double const ring_radius = (ref_width*0.5) / nb_rings;
      QTransform saved_transform = p.transform();
      p.translate(in_rect.left()+in_rect.width()*0.5, in_rect.top()+in_rect.height()*0.5);
      p.setPen(QPen(Qt::darkGray, 1.0));
      for(size_t ring_id = 0; ring_id < nb_rings; ++ring_id)
      {
         Mark const ring_mark = scoreToMark(min_score+ring_id);
         p.setBrush(targetFadeColorForMark(ring_mark ));
         double const radius = ring_radius*(nb_rings-ring_id);
         p.drawEllipse(QPointF(0.0,0.0), radius, radius);
      }
      p.setPen(QPen(Qt::lightGray, 1.0));
      p.drawEllipse(QPointF(0.0,0.0), ring_radius*0.5, ring_radius*0.5);
      double const cross_radius = ring_radius*0.1;
      p.drawLine(-cross_radius, 0.0, cross_radius, 0.0);
      p.drawLine(0.0, -cross_radius, 0.0, cross_radius);
      double const spot_radius = 1.1*
         (ref_width * 0.5 * s.arrowSizeSpecification().absoluteDiameter().value()) /
         (s.targetDiameter().v1().value()*nb_rings);
      p.setPen(Qt::NoPen);
      p.setBrush(Qt::black);
      double const scale = 10.0 / nb_rings;
      for(Single_End const& end: round)
      {
         for(Score_Unit const& su: end)
         {
            if ( (not su.isLocationDefined()) or (su.score() < 1) )
            {
               continue;
            }
            QPointF loc(su.location().x()*scale, -su.location().y()*scale);
            p.drawEllipse(loc*ref_width*0.5, spot_radius, spot_radius);
         }
      }
      p.setBrush(Qt::NoBrush);
      p.setPen(Qt::black);
      p.setTransform(saved_transform);
   }; // auto draw_target
   draw_target(main_round, target_rect);

   // now the duels
   p.resetTransform();
   p.translate(std::max(summary_size.width(), table_size.width())+64.0, results_top_y);
   p.setPen(QPen(Qt::black, 1.0));
   p.drawLine(QPointF(-32.0, 0.0), QPointF(-32.0, pdf_height-p.transform().dy()));
   Round_Index round_index(1);
   while ( round_index.value() < s.endsRoundsCount() )
   {
      QTransform const saved_transform = p.transform();
      text_size = draw_text(QPointF(0.0, 0.0), QString("<b>Duel %1</b>").arg(round_index.value()));
      p.translate(0.0, text_size.height()+8.0);
      Ends_Round const& round = s.endsRound(round_index);
      summary_size = round_summary(round, true);
      p.translate(0.0, summary_size.height()+8.0);
      table_size = round_table(round);
      double duel_bottom_y = p.transform().dy() + table_size.height() - saved_transform.dy();
      double duel_right_x = std::max(std::max(text_size.width(), summary_size.width()), table_size.width())+8.0;
      p.setTransform(saved_transform);
      target_width = std::min(duel_bottom_y, pdf_width-p.transform().dx()-duel_right_x);
      target_rect = QRectF(
                       duel_right_x, 0.0,
                       target_width,
                       std::max(target_width, duel_bottom_y));
      draw_target(round, target_rect);
      p.translate(0.0, duel_bottom_y+32.0);
      ++round_index;
   }

   qDebug() << "Saving to" << fi.absoluteFilePath();
}
