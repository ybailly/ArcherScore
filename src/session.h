
#pragma once

#include "units.h"
#include "ends_round.h"
#include "bows.h"
#include "arrows.h"
#include "arrow_size_spec.h"

#include <QtCore/QDateTime>
#include <QtCore/QFileInfo>

#include <vector>

class Session
{
   public:
      Session() = delete;
      Session(Session const&) = default;
      Session(Session&&) = default;
      explicit Session(uint8_t shot_count,
                       Session_Type sess_type);
      explicit Session(QString const& basename);

      Session_Type type() const;

      void setArrowSet(Arrow_Set const& as);
      void setBow(Bow const& b);
      void setTargetDiameter(Diameter_Cm_In td);
      void setTargetMinimumScore(int8_t ms);
      void setTargetDistance(Distance_M_Yd d);
      void setArrowSizeSpecification(Arrow_Size_Spec const& ass);
      void setLocation(Session_Location loc);
      void setTitle(QString const& titl);
      void setDescription(QString const& descr);

      bool haveArrowSet() const;
      Arrow_Set const& arrowSet() const;
      bool haveBow() const;
      Bow const& bow() const;
      QDateTime const& creationDate() const;
      Diameter_Cm_In targetDiameter() const;
      int8_t targetMinimumScore() const;
      Distance_M_Yd targetDistance() const;
      Arrow_Size_Spec const& arrowSizeSpecification() const;
      Session_Location location() const;
      QString const& title() const;
      QString const& description() const;

      uint8_t shotCount() const;

      void appendEndsRound();
      size_t endsRoundsCount() const;
      Ends_Round const& endsRound(Round_Index index) const;
      Ends_Round* mutableEndsRound(Round_Index index) const;

      bool is10Plus(Score_Unit const& su) const;
      bool is10Plus(Hit_Location const& hl) const;
      uint16_t n10Plus(Round_Index rid) const;

      QFileInfo file() const;
      void saveToFile() const;
      void loadFromFile();
      void exportToText() const;
      void exportToPDF() const;

      // iterators
      using Ends_Rounds_Container = std::vector<Ends_Round>;
      using iterator = Ends_Rounds_Container::iterator;
      using const_iterator = Ends_Rounds_Container::const_iterator;
      iterator begin();
      iterator end();
      const_iterator begin() const;
      const_iterator end() const;
      const_iterator cbegin() const;
      const_iterator cend() const;

   private:
      mutable Ends_Rounds_Container rounds;

      Arrow_Set arrow_set;
      Bow bow_;
      QDateTime creation_date;
      QString title_;
      QString description_;
      Arrow_Size_Spec arrow_size_spec;
      QString doc_file_basename;
      Diameter_Cm_In target_diameter;
      Distance_M_Yd target_distance;
      uint8_t shot_count{0};
      int8_t target_minimum_score{1};
      Session_Type session_type{Session_Type::Simple};
      Session_Location session_location{Session_Location::Indoor};
      bool have_arrow_set{false};
      bool have_bow{false};
}; // class Session

void sessionToPdf(Session const& s);

