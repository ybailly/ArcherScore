
#include "session_setup.h"

#include "qt_utils.h"
#include "settings.h"
#include "session.h"
#include "overlay_choose_target_min_score.h"
#include "overlay_choose_location.h"
#include "overlay_get_arrow_size_spec.h"
#include "bows_list_widget.h"
#include "arrow_sets_list_widget.h"
#include "ui_utils.h"

#include <QtDebug>

Session_Setup::Session_Setup(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   QObject::connect(this, &Session_Setup::forwardRequested,
                    this, &Session_Setup::accepted);
   QObject::connect(this, &Session_Setup::backRequested,
                    this, &Session_Setup::canceled);
   this->setBackVisible(true);
   this->setForwardVisible(true);
   qt::adjustToolButtonsIconSize(this->contents(), 4.0);

   this->target_size_controller = new Input_Button_Controller<Diameter_Cm_In>(this);
   this->target_size_controller->setButton(this->tb_target_diameter);
   this->target_size_controller->setOverlayMessage(tr("Diamètre de la cible en centimètres: "));

   QObject::connect(
            this->tb_target_shape, &QToolButton::clicked,
            [this]()
   {
      Overlay_Choose_Target_Min_Score* min_mark = new Overlay_Choose_Target_Min_Score(this->topLevelWidget());
      QObject::connect(
               min_mark, &Overlay_Choose_Target_Min_Score::minScoreChoosen,
               [min_mark,this](int8_t s)
      {
         this->setTargetMinimumScore(s);
         min_mark->discard();
      });
      min_mark->run();
   });

   this->target_distance_controller = new Input_Button_Controller<Distance_M_Yd>(this);
   this->target_distance_controller->setButton(this->tb_target_distance);
   this->target_distance_controller->setOverlayMessage(tr("Distance à la cible en mètres :"));

   QObject::connect(
            this->tb_arrows_diameter, &QToolButton::clicked,
            [this]()
   {
      Overlay_Get_Arrow_Size_Spec* get_arrow_size = new Overlay_Get_Arrow_Size_Spec(this->topLevelWidget());
      get_arrow_size->setArrowSizeSpec(this->arrowSizeSpecification());
      QObject::connect(
               get_arrow_size, &Overlay_Get_Arrow_Size_Spec::accepted,
               [get_arrow_size,this]()
      {
         this->setArrowSizeSpecification(get_arrow_size->arrowSizeSpec());
         get_arrow_size->discard();
      });
      QObject::connect(
               get_arrow_size, &Overlay_Get_Arrow_Size_Spec::canceled,
               get_arrow_size, &Overlay_Get_Arrow_Size_Spec::discard);
      get_arrow_size->run();
   });

   this->shots_count_controller = new Input_Button_Controller<int>(this);
   this->shots_count_controller->setButton(this->tb_shots_count);
   this->shots_count_controller->setOverlayMessage(tr("Nombre de flèches par volée :"));

   QObject::connect(
            this->tb_location, &QToolButton::clicked,
            [this]()
   {
      Overlay_Choose_Location* choose_loc = new Overlay_Choose_Location(this->topLevelWidget());
      QObject::connect(
               choose_loc, &Overlay_Choose_Location::locationChoosen,
               [choose_loc,this](Session_Location loc)
      {
         this->setSessionLocation(loc);
         choose_loc->discard();
      });
      choose_loc->run();
   });

   QObject::connect(
            this->bt_bow, &Bow_Push_Button::clicked,
            [this]()
   {
      if ( this->bt_bow->haveBow() )
      {
         emit this->requestBowEditor();
      }
      else
      {
         Generic_Overlay* overlay = new Generic_Overlay(this);
         Bows_List_Widget* bows_list = new Bows_List_Widget(nullptr);
         bows_list->refreshBowList();
         overlay->setContents(bows_list);
         overlay->adjustMargins(8);
         QObject::connect(
                  bows_list, &Bows_List_Widget::requestNewBow,
                  [bows_list,this]()
         {
            emit this->requestNewBow();
            bows_list->refreshBowList();
         });
         QObject::connect(
                  bows_list, &Bows_List_Widget::selectedBow,
                  [overlay,this](Bow_Index bi)
         {
            this->bow_ = bows().bow(bi);
            this->bt_bow->displayBow(this->bow_);
            overlay->discard();
         });
         overlay->run();
      }
   });

   QObject::connect(
            this->bt_arrow_set, &Bow_Push_Button::clicked,
            [this]()
   {
      if ( this->bt_arrow_set->haveArrowSet() )
      {
         emit this->requestArrowSetEditor();
      }
      else
      {
         Generic_Overlay* overlay = new Generic_Overlay(this);
         Arrow_Sets_List_Widget* arrow_sets_list = new Arrow_Sets_List_Widget(nullptr);
         arrow_sets_list->refreshArrowSetList();
         overlay->setContents(arrow_sets_list);
         overlay->adjustMargins(8);
         QObject::connect(
                  arrow_sets_list, &Arrow_Sets_List_Widget::requestNewArrowSet,
                  [arrow_sets_list,this]()
         {
            emit this->requestNewArrowSet();
            arrow_sets_list->refreshArrowSetList();
         });
         QObject::connect(
                  arrow_sets_list, &Arrow_Sets_List_Widget::selectedArrowSet,
                  [overlay,this](Arrow_Set_Index asi)
         {
            this->arrow_set = arrowSets().arrowSet(asi);
            this->bt_arrow_set->displayArrowSet(this->arrow_set);
            overlay->discard();
         });
         overlay->run();
      }
   });

   QObject::connect(
            this->bt_to_pdf, &QPushButton::clicked,
            [this]()
   {
      if ( this->restored_session == nullptr )
      {
         return;
      }
      sessionToPdf(*(this->restored_session));
   });
}

void Session_Setup::prepareDefaultSession(Session_Type sess_type)
{
   this->restored_session = nullptr;
   this->session_type = sess_type;
   if ( arrowSets().size() == 1 )
   {
      this->arrow_set = arrowSets().arrowSet(Arrow_Set_Index(0));
      this->bt_arrow_set->displayArrowSet(this->arrow_set);
   }
   else
   {
      this->bt_arrow_set->setArrowSetIndex(Invalid_Arrow_Set_Index);
   }
   if ( bows().size() == 1 )
   {
      this->bow_ = bows().bow(Bow_Index(0));
      this->bt_bow->displayBow(this->bow_);
   }
   else
   {
      this->bt_bow->setBowIndex(Invalid_Bow_Index);
   }
   this->setTargetSize(Settings::Previous::targetSize());
   this->setTargetMinimumScore(Settings::Previous::targetMinimumScore());
   this->setTargetDistance(Settings::Previous::targetDistance());
   this->setArrowSizeSpecification(Settings::Previous::arrowSizeSpecification());
   this->setShotsPerEnd(Settings::Previous::shotsPerEnd());
   this->setSessionLocation(Settings::Previous::sessionLocation());
   this->le_title->clear();
   this->pte_description->clear();
   this->bt_to_pdf->setVisible(false);
}

void Session_Setup::restoreSession(Session const& s)
{
   this->restored_session = &s;
   this->session_type = s.type();
   if ( s.haveArrowSet() )
   {
      this->arrow_set = s.arrowSet();
      this->bt_arrow_set->displayArrowSet(this->arrow_set);
   }
   else
   {
      this->bt_arrow_set->setArrowSetIndex(Invalid_Arrow_Set_Index);
   }
   if ( s.haveBow() )
   {
      this->bow_ = s.bow();
      this->bt_bow->displayBow(this->bow_);
   }
   else
   {
      this->bt_bow->setBowIndex(Invalid_Bow_Index);
   }
   this->setTargetSize(s.targetDiameter());
   this->setTargetMinimumScore(s.targetMinimumScore());
   this->setTargetDistance(s.targetDistance());
   this->setArrowSizeSpecification(s.arrowSizeSpecification());
   this->setShotsPerEnd(s.shotCount());
   this->setSessionLocation(s.location());
   this->le_title->setText(s.title());
   this->pte_description->setPlainText(s.description());
   this->bt_to_pdf->setVisible(true);
}

Session_Type Session_Setup::sessionType() const
{
   return this->session_type;
}

bool Session_Setup::haveArrowSet() const
{
   return this->bt_arrow_set->haveArrowSet();
}

Arrow_Set const& Session_Setup::arrowSet() const
{
   return this->arrow_set;
}

bool Session_Setup::haveBow() const
{
   return this->bt_bow->haveBow();
}

Bow const& Session_Setup::bow() const
{
   return this->bow_;
}

Diameter_Cm_In Session_Setup::targetSize() const
{
   return this->target_size_controller->value();
}

int8_t Session_Setup::targetMinimumScore() const
{
   return this->min_score;
}

Distance_M_Yd Session_Setup::targetDistance() const
{
   return this->target_distance_controller->value();
}

Arrow_Size_Spec Session_Setup::arrowSizeSpecification() const
{
   return Arrow_Size_Spec(this->tb_arrows_diameter->text());
}

size_t Session_Setup::shotsPerEnd() const
{
   return static_cast<size_t>(this->shots_count_controller->value());
}

Session_Location Session_Setup::location() const
{
   return this->session_location;
}

QString Session_Setup::title() const
{
   return this->le_title->text();
}

QString Session_Setup::description() const
{
   return this->pte_description->toPlainText();
}

void Session_Setup::setArrowSet(Arrow_Set const& as)
{
   this->arrow_set = as;
   this->bt_arrow_set->displayArrowSet(as);
}

void Session_Setup::setBow(Bow const& b)
{
   this->bow_ = b;
   this->bt_bow->displayBow(b);
}

void Session_Setup::setTargetSize(Diameter_Cm_In ts)
{
   this->target_size_controller->setValue(ts);
}

void Session_Setup::setTargetMinimumScore(int8_t s)
{
   this->min_score = s;
   this->tb_target_shape->setIcon(iconForTargetMinimumScore(s));
   switch ( s )
   {
      case 1:
      case 2:
      case 3:
      case 4:
         this->tb_target_shape->setText(tr("Complète"));
         break;
      case 5:
         this->tb_target_shape->setText(tr("Réduite\nau 5"));
         break;
      case 6:
         this->tb_target_shape->setText(tr("Réduite\nau 6"));
         break;
      case 7:
         this->tb_target_shape->setText(tr("Réduite\nau 7"));
      case 8:
      case 9:
      case 10:
         this->tb_target_shape->setText(tr("Réduite\nau 8"));
         break;
      default:
         this->tb_target_shape->setText(tr("Complète"));
   }
}

void Session_Setup::setTargetDistance(Distance_M_Yd dist)
{
   this->target_distance_controller->setValue(dist);
}

void Session_Setup::setArrowSizeSpecification(Arrow_Size_Spec const& ass)
{
   this->tb_arrows_diameter->setText(ass.toString());
}

void Session_Setup::setShotsPerEnd(size_t spe)
{
   this->shots_count_controller->setValue(spe);
}

void Session_Setup::setSessionLocation(Session_Location loc)
{
   this->session_location = loc;
   switch ( loc )
   {
      case Session_Location::Indoor:
         this->tb_location->setText(tr("Intérieur"));
         break;
      case Session_Location::Outdoor:
         this->tb_location->setText(tr("Extérieur"));
         break;
   }
   this->tb_location->setIcon(iconForSessionLocation(loc));
}
