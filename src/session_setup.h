
#pragma once

#include "generic_page.h"
#include "arrow_size_spec.h"
#include "input_button_controller.h"
#include "bows.h"
#include "arrows.h"

#include "ui_session_setup.h"

class Session;

class Session_Setup: public Generic_Page, private Ui::Session_Setup
{
      Q_OBJECT
   public:
      explicit Session_Setup(QWidget* parent = nullptr);

      void prepareDefaultSession(Session_Type sess_type);
      void restoreSession(Session const& s);

      Session_Type sessionType() const;
      bool haveArrowSet() const;
      Arrow_Set const& arrowSet() const;
      bool haveBow() const;
      Bow const& bow() const;
      Diameter_Cm_In targetSize() const;
      int8_t targetMinimumScore() const;
      Distance_M_Yd targetDistance() const;
      Arrow_Size_Spec arrowSizeSpecification() const;
      size_t shotsPerEnd() const;
      Session_Location location() const;
      QString title() const;
      QString description() const;

      void setArrowSet(Arrow_Set const& as);
      void setBow(Bow const& b);

   signals:
      void requestArrowSetEditor();
      void requestNewArrowSet();
      void requestBowEditor();
      void requestNewBow();
      void accepted();
      void canceled();

   protected:
      void setTargetSize(Diameter_Cm_In);
      void setTargetMinimumScore(int8_t);
      void setTargetDistance(Distance_M_Yd);
      void setArrowSizeSpecification(Arrow_Size_Spec const&);
      void setShotsPerEnd(size_t);
      void setSessionLocation(Session_Location loc);

   private:
      Arrow_Set arrow_set;
      Bow bow_;
      Input_Button_Controller<int>* shots_count_controller{nullptr};
      Input_Button_Controller<Diameter_Cm_In>* target_size_controller{nullptr};
      Input_Button_Controller<Distance_M_Yd>* target_distance_controller{nullptr};
      Session_Type session_type{Session_Type::Simple};
      int8_t min_score{1};
      Session_Location session_location{Session_Location::Indoor};
      Arrow_Size_Spec arrow_size_spec;
      Session const* restored_session{nullptr};

}; // class Session_Setup
