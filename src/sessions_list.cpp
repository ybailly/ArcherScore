
#include "sessions_list.h"
#include "qt_utils.h"

#include <QtDebug>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonParseError>

#include <algorithm>

Sessions_List::Sessions_List():
   QObject(nullptr)
{
}

void Sessions_List::refresh()
{
   this->sessions.clear();
   QDir dir = qt::documentsDir();
   QFileInfoList files = dir.entryInfoList(QStringList() << "*.as", QDir::Files bitor QDir::Readable);
   for(QFileInfo const& fi: files)
   {
      QFile f(fi.absoluteFilePath());
      if ( not f.open(QIODevice::ReadOnly))
      {
         qDebug() << "*** failed to open" << fi.absoluteFilePath();
         continue;
      }
      qDebug() << "Found" << fi.absoluteFilePath();
      QByteArray ba = f.readAll();
      f.close();
      if ( ba.isEmpty() )
      {
         qDebug() << "***" << fi.absoluteFilePath() << "is empty";
         continue;
      }
      QJsonParseError errors;
      QJsonDocument doc = QJsonDocument::fromJson(ba, &errors);
      if(errors.error != QJsonParseError::NoError)
      {
         qDebug() << "Error reading" << QDir::toNativeSeparators(fi.absoluteFilePath()) << "at" << errors.offset << ":";
         qDebug() << errors.errorString();
         continue;
      }
      QJsonObject root = doc.object();
      auto const iter_title = root.find("title");
      if ( iter_title == root.end() )
      {
         qDebug() << "No title in" << QDir::toNativeSeparators(fi.absoluteFilePath());
         continue;
      }
      auto const iter_date = root.find("date_utc");
      if ( iter_date == root.end() )
      {
         qDebug() << "No date in" << QDir::toNativeSeparators(fi.absoluteFilePath());
         continue;
      }
      this->sessions.push_back({fi.fileName(), QDateTime::fromString(iter_date->toString(), Qt::ISODate), iter_title->toString()});
   }
   std::sort(
         this->sessions.begin(),
         this->sessions.end(),
         [](Session_Info const& si1, Session_Info const& si2)
         { return si2.date < si1.date; });
   emit this->listHasChanged();
}

size_t Sessions_List::count() const
{
   return this->sessions.size();
}

Session_Info const& Sessions_List::sessionInfo(size_t idx) const
{
   return this->sessions.at(idx);
}
