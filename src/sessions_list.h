
#pragma once

#include <QtCore/QFileInfo>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QObject>

#include <vector>

struct Session_Info
{
      QString basename;
      QDateTime date;
      QString title;
}; // Session_Info

class Sessions_List: public QObject
{
      Q_OBJECT
   public:
      Sessions_List();
      void refresh();

      size_t count() const;
      Session_Info const& sessionInfo(size_t idx) const;

   signals:
      void listHasChanged();

   private:
      std::vector<Session_Info> sessions;
}; // class Sessions_List
