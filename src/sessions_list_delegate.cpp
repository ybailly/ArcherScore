
#include "sessions_list_delegate.h"

#include <QtDebug>
#include <QtWidgets/QLabel>
#include <QtGui/QPainter>

#include "sessions_list.h"

Sessions_List_Delegate::Sessions_List_Delegate(QObject* parent):
   QStyledItemDelegate(parent)
{
}

void Sessions_List_Delegate::setSessionsList(Sessions_List* sl)
{
   this->sessions_list = sl;
}

Sessions_List* Sessions_List_Delegate::sessionsList() const
{
   return this->sessions_list;
}

void Sessions_List_Delegate::paint(QPainter* painter,
                                   QStyleOptionViewItem const& option,
                                   QModelIndex const& index) const
{
   QAbstractItemModel const* model = index.model();
   if ( (this->sessions_list == nullptr) or (model == nullptr) )
   {
      this->QStyledItemDelegate::paint(painter, option, index);
      return;
   }
   QLabel* label = this->qLabel();
   QPalette pal = option.palette;
   if ( (option.state bitand QStyle::State_Selected) != 0 )
   {
      pal.setBrush(QPalette::Text, pal.brush(QPalette::HighlightedText));
      pal.setBrush(QPalette::Window, pal.brush(QPalette::Highlight));
   }
   else if ( (option.features bitand QStyleOptionViewItem::Alternate) != 0 )
   {
      pal.setBrush(QPalette::Window, pal.brush(QPalette::AlternateBase));
   }
   else
   {
      pal.setBrush(QPalette::Window, pal.brush(QPalette::Base));
   }
   label->setPalette(pal);
   label->setText(model->data(index).toString());
   label->setGeometry(option.rect);
   painter->save();
   painter->translate(option.rect.topLeft());
   label->render(painter);
   painter->restore();
}

QSize Sessions_List_Delegate::sizeHint(QStyleOptionViewItem const& option,
                                       QModelIndex const& index) const
{
   QAbstractItemModel const* model = index.model();
   if ( (this->sessions_list == nullptr) or (model == nullptr) )
   {
      return this->QStyledItemDelegate::sizeHint(option, index);
   }
   QLabel* label = this->qLabel();
   label->setText(model->data(index).toString());
   label->adjustSize();
   return label->sizeHint();
}

QLabel* Sessions_List_Delegate::qLabel() const
{
   if ( this->q_label == nullptr )
   {
      this->q_label = new QLabel;
      this->q_label->setTextFormat(Qt::RichText);
      this->q_label->setAutoFillBackground(true);
      this->q_label->setWordWrap(false);
   }
   return this->q_label;
}
