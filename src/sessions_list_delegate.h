
#pragma once

#include <QtWidgets/QStyledItemDelegate>

class Sessions_List;
class QLabel;

class Sessions_List_Delegate: public QStyledItemDelegate
{
      Q_OBJECT
   public:
      explicit Sessions_List_Delegate(QObject* parent = nullptr);

      void setSessionsList(Sessions_List* sl);
      Sessions_List* sessionsList() const;

      virtual void paint(QPainter* painter,
                         QStyleOptionViewItem const& option,
                         QModelIndex const& index) const override;

      virtual QSize sizeHint(QStyleOptionViewItem const& option,
                             QModelIndex const& index) const override;

   private:
      QLabel* qLabel() const;
      Sessions_List* sessions_list{nullptr};
      mutable QLabel* q_label{nullptr};
}; // class Sessions_List_Delegate
