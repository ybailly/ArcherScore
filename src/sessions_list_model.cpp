
#include "sessions_list_model.h"

#include "sessions_list.h"

Sessions_List_Model::Sessions_List_Model(QObject* parent):
   QAbstractItemModel(parent)
{
}

void Sessions_List_Model::setSessionsList(Sessions_List* sl)
{
   this->beginResetModel();
   this->sessions_list = sl;
   this->endResetModel();
   if ( this->sessions_list != nullptr )
   {
      QObject::connect(this->sessions_list, &Sessions_List::listHasChanged,
                       [this]() { this->beginResetModel(); this->endResetModel(); });
   }
}

Sessions_List* Sessions_List_Model::sessionsList() const
{
   return this->sessions_list;
}

int Sessions_List_Model::columnCount(QModelIndex const& /*parent*/) const
{
   return 1;
}

QVariant Sessions_List_Model::data(QModelIndex const& index,
                                   int role) const
{
   if ( this->sessions_list == nullptr )
   {
      return {};
   }
   if ( (not index.isValid()) or (index.column() > 0) )
   {
      return {};
   }
   Session_Info const& session_info = this->sessions_list->sessionInfo(index.row());
   switch ( role )
   {
      case Qt::DisplayRole:
         return QStringLiteral("<p>%1<br/><small>%2</small></p>")
               .arg(session_info.title.isEmpty() ? tr("<i>Pas de titre</i>") : session_info.title)
               .arg(session_info.date.toLocalTime().toString());
      default:
         return {};
   }
}

bool Sessions_List_Model::hasChildren(QModelIndex const& /*parent*/) const
{
   return false;
}

QModelIndex Sessions_List_Model::index(int row,
                                       int column,
                                       QModelIndex const& /*parent*/) const
{
   return this->createIndex(row, column);
}

QModelIndex Sessions_List_Model::parent(QModelIndex const& /*index*/) const
{
   return {};
}

int Sessions_List_Model::rowCount(QModelIndex const& /*parent*/) const
{
   if ( this->sessions_list == nullptr )
   {
      return 0;
   }
   return static_cast<int>(this->sessions_list->count());
}
