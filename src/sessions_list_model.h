
#pragma once

#include <QtCore/QAbstractItemModel>

class Sessions_List;

class Sessions_List_Model: public QAbstractItemModel
{
      Q_OBJECT
   public:
      explicit Sessions_List_Model(QObject* parent = nullptr);

      void setSessionsList(Sessions_List* sl);
      Sessions_List* sessionsList() const;

      virtual int columnCount(QModelIndex const& parent = QModelIndex()) const override;
      virtual QVariant data(QModelIndex const& index,
                            int role = Qt::DisplayRole) const override;
      virtual bool hasChildren(QModelIndex const& parent = QModelIndex()) const override;
      virtual QModelIndex index(int row,
                                int column,
                                QModelIndex const& parent = QModelIndex()) const override;
      virtual QModelIndex parent(QModelIndex const& index) const override;
      virtual int rowCount(QModelIndex const& parent = QModelIndex()) const override;

   private:
      Sessions_List* sessions_list{nullptr};
}; // class Sessions_List_Model
