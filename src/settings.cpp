
#include "settings.h"
#include "units_ui.h"
#include "cpp_utils.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>

namespace
{
   struct Settings_Keys
   {
         struct Target_View
         {
               static QString const show_radial_lines;
         }; // struct Settings_Keys::Target_View

         struct Previous
         {
               static QString const target_size;
               static QString const target_min_score;
               static QString const distance;
               static QString const arrows_size_spec;
               static QString const shots_per_end;
               static QString const session_location;
         }; // struct Settings_Keys::Previous
   }; // struct Settings_Keys
   QString const Settings_Keys::Target_View::show_radial_lines("target/show_radial_lines");
   QString const Settings_Keys::Previous::target_size{"prev/target_diameter_cm"};
   QString const Settings_Keys::Previous::target_min_score{"prev/target_minimum_score"};
   QString const Settings_Keys::Previous::distance{"prev/distance"};
   QString const Settings_Keys::Previous::arrows_size_spec{"prev/arrows_size_spec"};
   QString const Settings_Keys::Previous::shots_per_end{"prev/shots_per_end"};
   QString const Settings_Keys::Previous::session_location{"prev/session_location"};

   QSettings& settings()
   {
#ifdef WIN32
      static QSettings* const setts =
            new QSettings(QFileInfo(documentsDir(), "ArcherScore.ini").absoluteFilePath(),
                          QSettings::IniFormat,
                          qApp);
#else
      static QSettings* const setts =
            new QSettings(QSettings::IniFormat,
                          QSettings::UserScope,
                          "ybailly.org",
                          "ArcherScore",
                          QCoreApplication::instance());
#endif
      return *setts;
   }

} // namespace

// static
bool Settings::Target_View::showRadialLines()
{
   return settings().value(Settings_Keys::Target_View::show_radial_lines, true).toBool();
}

// static
void Settings::Target_View::setShowRadialLines(bool srl)
{
   settings().setValue(Settings_Keys::Target_View::show_radial_lines, srl);
}

// static
Diameter_Cm_In Settings::Previous::targetSize()
{
   return fromString<Diameter_Cm_In>(settings().value(Settings_Keys::Previous::target_size, "80cm").toString());
}

// static
int8_t Settings::Previous::targetMinimumScore()
{
   return cpp::clamp(settings().value(Settings_Keys::Previous::target_min_score, 1).toInt(), 1, 10);
}

// static
Distance_M_Yd Settings::Previous::targetDistance()
{
   return fromString<Distance_M_Yd>(settings().value(Settings_Keys::Previous::distance, "15m").toString());
}

// static
Arrow_Size_Spec Settings::Previous::arrowSizeSpecification()
{
   return Arrow_Size_Spec{settings().value(Settings_Keys::Previous::arrows_size_spec, QStringLiteral("14/64")).toString()};
}

// static
size_t Settings::Previous::shotsPerEnd()
{
   return settings().value(Settings_Keys::Previous::shots_per_end, 3).toUInt();
}

// static
Session_Location Settings::Previous::sessionLocation()
{
   QString const sess_loc = settings().value(Settings_Keys::Previous::session_location, "indoor").toString();
   if ( QString::compare(sess_loc, QStringLiteral("indoor"), Qt::CaseInsensitive) == 0 )
   {
      return Session_Location::Indoor;
   }
   if ( QString::compare(sess_loc, QStringLiteral("outdoor"), Qt::CaseInsensitive) == 0 )
   {
      return Session_Location::Outdoor;
   }
   return Session_Location::Indoor;
}

// static
void Settings::Previous::setTargetSize(Diameter_Cm_In d)
{
   settings().setValue(Settings_Keys::Previous::target_size, toLongString(d));
}

// static
void Settings::Previous::setTargetMinimumScore(int8_t m)
{
   settings().setValue(Settings_Keys::Previous::target_min_score, cpp::clamp(m, {1}, {10}));
}

// static
void Settings::Previous::setTargetDistance(Distance_M_Yd d)
{
   settings().setValue(Settings_Keys::Previous::distance, toLongString(d));
}

// static
void Settings::Previous::setArrowSizeSpecification(Arrow_Size_Spec const& ass)
{
   settings().setValue(Settings_Keys::Previous::arrows_size_spec, ass.toString());
}

// static
void Settings::Previous::setShotsPerEnd(size_t spe)
{
   settings().setValue(Settings_Keys::Previous::shots_per_end, static_cast<qulonglong>(spe));
}

// static
void Settings::Previous::setSessionLocation(Session_Location loc)
{
   switch ( loc )
   {
      case Session_Location::Indoor:
         settings().setValue(Settings_Keys::Previous::session_location, QStringLiteral("indoor"));
         break;
      case Session_Location::Outdoor:
         settings().setValue(Settings_Keys::Previous::session_location, QStringLiteral("outdoor"));
         break;
   }
}

// static
void Settings::sync()
{
   settings().sync();
}
