
#pragma once

#include "common_types.h"
#include "units_pairs.h"
#include "arrow_size_spec.h"

struct Settings
{
      struct Target_View
      {
         static bool showRadialLines();

         static void setShowRadialLines(bool srl);
      };
      struct Previous
      {
            static Diameter_Cm_In targetSize();
            static int8_t targetMinimumScore();
            static Distance_M_Yd targetDistance();
            static Arrow_Size_Spec arrowSizeSpecification();
            static size_t shotsPerEnd();
            static Session_Location sessionLocation();

            static void setTargetSize(Diameter_Cm_In);
            static void setTargetMinimumScore(int8_t);
            static void setTargetDistance(Distance_M_Yd);
            static void setArrowSizeSpecification(Arrow_Size_Spec const&);
            static void setShotsPerEnd(size_t);
            static void setSessionLocation(Session_Location);
      };
      static void sync();
}; // struct Settings

