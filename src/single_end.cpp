
#include "single_end.h"
#include "common_types_utils.h"

#include <QtDebug>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

Single_End::Single_End(uint8_t nb_shots):
   scores(nb_shots)
{
}

Single_End::Single_End(QJsonObject const& json,
                       int version)
{
   if ( version > 3 )
   {
      qDebug() << "*** Unknown version" << version << "while reading end";
   }
   QJsonArray score_units_array = json["score_units"].toArray();
   this->scores.reserve(score_units_array.count());
   for(QJsonValue score_unit_value: score_units_array)
   {
      QJsonObject score_unit_object = score_unit_value.toObject();
      this->scores.emplace_back(score_unit_object, version);
   }
}

uint8_t Single_End::nbShots() const
{
   return this->scores.size();
}

uint8_t Single_End::nbDefinedShots() const
{
   if ( this->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->nb_defined;
}

void Single_End::setScore(Score_Index shot,
                          Score_Unit const& su)
{
   this->scores[shot.value()] = su;
   this->mark_stats_dirty = true;
   this->group_stats_dirty = true;
}

void Single_End::setScoreEnabledForGroup(Score_Index shot,
                                         bool is_enabled_for_group)
{
   this->scores[shot.value()].setEnabledForGroup(is_enabled_for_group);
   this->group_stats_dirty = true;
}

Score_Unit const& Single_End::scoreUnit(Score_Index shot) const
{
   return this->scores[shot.value()];
}

uint16_t Single_End::scoreSum() const
{
   if ( this->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->mark_sum;
}

double Single_End::scoreAverage() const
{
   if ( this->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->mark_average;
}

uint8_t Single_End::n10() const
{
   if ( this->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->nb_10;
}

uint8_t Single_End::n10plus() const
{
   if ( this->mark_stats_dirty )
   {
      this->updateStats();
   }
   return this->nb_10_plus;
}

Diameter Single_End::groupDiameter() const
{
   if ( this->group_stats_dirty )
   {
      this->updateStats();
   }
   return this->group_diameter;
}

QPointF Single_End::groupCenter() const
{
   if ( this->group_stats_dirty )
   {
      this->updateStats();
   }
   return this->group_center;
}

Concentration_Coefficient Single_End::concentrationCoefficient() const
{
   return normalizedDiameterToConcentrationCoefficient(this->groupDiameter());
}

void Single_End::appendToPointSet(Point_Set* ps) const
{
   ::appendToPointSet(this->scores, ps);
}

QJsonObject Single_End::toJson() const
{
   QJsonArray score_units_array;
   for(auto const& score_unit: this->scores)
   {
      score_units_array.append(score_unit.toJson());
   }
   QJsonObject end_object;
   end_object.insert("score_units", score_units_array);
   return end_object;
}

Single_End::iterator Single_End::begin()
{
   return this->scores.begin();
}

Single_End::iterator Single_End::end()
{
   return this->scores.end();
}

Single_End::const_iterator Single_End::begin() const
{
   return this->scores.begin();
}

Single_End::const_iterator Single_End::end() const
{
   return this->scores.end();
}

Single_End::const_iterator Single_End::cbegin() const
{
   return this->scores.cbegin();
}

Single_End::const_iterator Single_End::cend() const
{
   return this->scores.cend();
}

void Single_End::updateStats() const
{
   if ( this->mark_stats_dirty )
   {
      this->mark_sum = 0;
      this->mark_average = 0.0;
      this->nb_defined = 0;
      this->nb_10 = 0;
      this->nb_10_plus = 0;
      for(Score_Unit const& su: this->scores)
      {
         if ( su.mark() == Mark::Undefined )
            continue;
         this->mark_sum += su.score();
         this->nb_defined += 1;
         if ( su.mark() == Mark::Ten ) this->nb_10 += 1;
         else if ( su.mark() == Mark::TenPlus ) this->nb_10_plus += 1;
      }
      if ( this->nb_defined > 0 )
      {
         this->mark_average =
               static_cast<double>(this->mark_sum) /
               static_cast<double>(this->nb_defined);
      }
      this->mark_stats_dirty = false;
   }
   if ( this->group_stats_dirty )
   {
      Point_Set pts;
      ::appendToPointSet(this->scores, &pts);
      double group_diameter;
      computeGroup(pts,
                   &this->group_center,
                   &group_diameter);
      this->group_diameter = Diameter(group_diameter);
      this->group_stats_dirty = false;
   }
}
