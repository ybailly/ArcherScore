
#pragma once

#include "score_unit.h"
#include "units.h"

#include <vector>

class QJsonObject;

class Single_End
{
   public:
      Single_End() = delete;
      Single_End(Single_End const&) = default;
      Single_End(Single_End&&) = default;

      Single_End& operator=(Single_End const&) = default;
      Single_End& operator=(Single_End&&) = default;

      explicit Single_End(uint8_t nb_shots);
      explicit Single_End(QJsonObject const& json,
                          int version);

      uint8_t nbShots() const;
      uint8_t nbDefinedShots() const;

      void setScore(Score_Index shot,
                    Score_Unit const& su);
      void setScoreEnabledForGroup(Score_Index,
                                   bool is_enabled_for_group);

      Score_Unit const& scoreUnit(Score_Index shot) const;
      uint16_t scoreSum() const;
      double scoreAverage() const;
      uint8_t n10() const;
      uint8_t n10plus() const;
      Diameter groupDiameter() const;
      QPointF groupCenter() const;
      Concentration_Coefficient concentrationCoefficient() const;

      void appendToPointSet(Point_Set* ps) const;

      QJsonObject toJson() const;

      // iterators
      using Score_Units_Container = std::vector<Score_Unit>;
      using iterator = Score_Units_Container::iterator;
      using const_iterator = Score_Units_Container::const_iterator;
      iterator begin();
      iterator end();
      const_iterator begin() const;
      const_iterator end() const;
      const_iterator cbegin() const;
      const_iterator cend() const;

   private:
      void updateStats() const;
      std::vector<Score_Unit> scores;
      mutable QPointF group_center{0.0,0.0};
      mutable double mark_average{0.0};
      mutable Diameter group_diameter{0.0};
      mutable uint16_t mark_sum{0};
      mutable uint8_t nb_defined{0};
      mutable uint8_t nb_10{0};
      mutable uint8_t nb_10_plus{0};
      mutable bool mark_stats_dirty{true};
      mutable bool group_stats_dirty{true};

}; // class Single_End
