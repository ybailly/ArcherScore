
#include "style.h"
#include "qt_utils.h"

#include <QtDebug>
#include <QtGui/QFontMetrics>
#include <QtWidgets/QWidget>
#include <QtWidgets/QApplication>

Style::Style(QString const& key):
   QProxyStyle(key)
{
}

int Style::pixelMetric(QStyle::PixelMetric metric,
                       QStyleOption const* option,
                       QWidget const* widget) const
{
   switch ( metric )
   {
      case QStyle::PM_ButtonMargin:
         return qRound(this->QProxyStyle::pixelMetric(metric, option, widget)*0.75);
         break;
      case QStyle::PM_SmallIconSize:
         return qRound(Style::Em_To_Small_Icon_Size*qt::computeEM(widget));
         break;
      case QStyle::PM_LargeIconSize:
         return qRound(Style::Em_To_Large_Icon_Size*qt::computeEM(widget));
         break;
      case QStyle::PM_ButtonIconSize:
         return qRound(Style::Em_To_Button_Icon_Size*qt::computeEM(widget));
         break;
      default:
         break;
   }
   return this->QProxyStyle::pixelMetric(metric, option, widget);
}

