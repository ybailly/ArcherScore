
#pragma once

#include <QtWidgets/QProxyStyle>

class Style: public QProxyStyle
{
      Q_OBJECT
   public:
      static constexpr double Em_To_Small_Icon_Size = 1.5;
      static constexpr double Em_To_Large_Icon_Size = 3.0;
      static constexpr double Em_To_Button_Icon_Size = 2.5;

      explicit Style(QString const& key);

      virtual int pixelMetric(PixelMetric metric,
                              QStyleOption const* option = nullptr,
                              QWidget const* widget = nullptr) const override;
}; // class Style
