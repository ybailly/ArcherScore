
#pragma once

#include <ciso646>
#include <utility>
#include <cstdint>

class No_Dim_Tag;
class Any_Dim_Tag;

class No_Unit_Tag;
class Any_Unit_Tag;

template<typename TYPE, typename DIM_TAG, typename UNIT_TAG>
class Tagged_Value
{
   public:
      using type = TYPE;
      using dimension_tag = DIM_TAG;
      using unit_tag = UNIT_TAG;

      explicit constexpr Tagged_Value(TYPE v): val(v) { }
      Tagged_Value() = default;
      Tagged_Value(Tagged_Value const&) = default;
      Tagged_Value(Tagged_Value&&) = default;
      Tagged_Value& operator=(Tagged_Value const&) = default;
      Tagged_Value& operator=(Tagged_Value&&) = default;

      constexpr TYPE value() const { return this->val; }

      Tagged_Value& operator+=(Tagged_Value tv)
      { val += tv.val; return *this; }

   private:
      TYPE val;
}; // class Tagged_Value<>

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator+(Tagged_Value<T,D,U> v1, Tagged_Value<T,D,U> v2)
{ return Tagged_Value<T,D,U>{static_cast<T>(v1.value()+v2.value())}; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator-(Tagged_Value<T,D,U> v1, Tagged_Value<T,D,U> v2)
{ return Tagged_Value<T,D,U>{static_cast<T>(v1.value()-v2.value())}; }

template<typename T, typename D, typename U>
bool operator<(Tagged_Value<T,D,U> v1, Tagged_Value<T,D,U> v2)
{ return v1.value() < v2.value(); }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator-(Tagged_Value<T,D,U> v)
{ return Tagged_Value<T,D,U>{-v.value()}; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator*(T v1, Tagged_Value<T,D,U> v2)
{ return {v1*v2.value()}; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator*(Tagged_Value<T,D,U> v1, T v2)
{ return {v1.value()*v2}; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator/(T v1, Tagged_Value<T,D,U> v2)
{ return {v1/v2.value()}; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U> operator/(Tagged_Value<T,D,U> v1, T v2)
{ return Tagged_Value<T,D,U>{v1.value()/v2}; }

template<typename T, typename D, typename U>
bool operator==(Tagged_Value<T,D,U> v1, Tagged_Value<T,D,U> v2)
{ return v1.value() == v2.value(); }

template<typename T, typename D, typename U>
bool operator!=(Tagged_Value<T,D,U> v1, Tagged_Value<T,D,U> v2)
{ return v1.value() != v2.value(); }

template<typename T, typename D, typename U>
bool operator<(T v1, Tagged_Value<T,D,U> v2)
{ return v1<v2.value(); }

template<typename T, typename D, typename U>
bool operator<(Tagged_Value<T,D,U> v1, T v2)
{ return v1.value()<v2; }

template<typename T, typename D, typename U>
bool operator<=(T v1, Tagged_Value<T,D,U> v2)
{ return v1<=v2.value(); }

template<typename T, typename D, typename U>
bool operator<=(Tagged_Value<T,D,U> v1, T v2)
{ return v1.value()<=v2; }

template<typename T, typename D, typename U>
bool operator>(T v1, Tagged_Value<T,D,U> v2)
{ return v1>v2.value(); }

template<typename T, typename D, typename U>
bool operator>(Tagged_Value<T,D,U> v1, T v2)
{ return v1.value()>v2; }

template<typename T, typename D, typename U>
bool operator>=(T v1, Tagged_Value<T,D,U> v2)
{ return v1>=v2.value(); }

template<typename T, typename D, typename U>
bool operator>=(Tagged_Value<T,D,U> v1, T v2)
{ return v1.value()>=v2; }

template<typename T, typename D, typename U>
Tagged_Value<T,D,U>& operator++(Tagged_Value<T,D,U>& v)
{ return (v = Tagged_Value<T,D,U>(v.value()+1)); }

