
#pragma once

#include "tagged_value.h"

template<typename BASE_TYPE, typename UNIT_TAG_FROM, typename UNIT_TAG_TO>
struct Unit_Converter;

template<typename TV_FROM, typename TV_TO>
TV_TO convert(TV_FROM from)
{
   static_assert(std::is_same<typename TV_FROM::type, typename TV_TO::type>::value,
                 "same underlying type required");
   static_assert(std::is_same<typename TV_FROM::dimension_tag, typename TV_TO::dimension_tag>::value,
                 "same dimension required");
   using unit_converter = Unit_Converter<typename TV_FROM::type, typename TV_FROM::unit_tag, typename TV_TO::unit_tag>;
   return TV_TO{unit_converter::convert(from.value())};
}

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
class Tagged_Value_Pair
{
   public:
      using base_type = BASE_TYPE;
      using dimension_tag = DIM_TAG;
      using unit_tag_1 = UNIT_TAG_1;
      using unit_tag_2 = UNIT_TAG_2;

      using type_1 = Tagged_Value<base_type, dimension_tag, unit_tag_1>;
      using type_2 = Tagged_Value<base_type, dimension_tag, unit_tag_2>;

      Tagged_Value_Pair() = default;
      Tagged_Value_Pair(Tagged_Value_Pair const&) = default;
      Tagged_Value_Pair& operator=(Tagged_Value_Pair const&) = default;
      explicit Tagged_Value_Pair(type_1 v1_)
      { this->setV1(v1_); }
      explicit Tagged_Value_Pair(type_2 v2_)
      { this->setV2(v2_); }

      type_1 v1() const { return this->values.first; }
      type_2 v2() const { return this->values.second; }

      void setV1(type_1 v1_)
      {
         this->values.first = v1_;
         this->values.second = convert<type_1, type_2>(v1_);
         this->preferred_ = 0;
      }

      void setV2(type_2 v2_)
      {
         this->values.first = convert<type_2, type_1>(v2_);
         this->values.second = v2_;
         this->preferred_ = 1;
      }

      uint8_t preferred() const { return this->preferred_; }
      BASE_TYPE rawPreferredValue() const
      {
         if ( this->preferred_ == 0 )
            return this->values.first.value();
         else
            return this->values.second.value();
      }

   private:
      std::pair<type_1, type_2> values{type_1{static_cast<base_type>(0)}, type_2{static_cast<base_type>(0)}};
      uint8_t preferred_{0};
}; // class Tagged_Value_Pair<>

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
bool operator< (Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2> const& l,
                Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2> const& r)
{ return l.v1() < r.v1(); }
