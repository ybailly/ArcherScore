
#include "target_scene.h"

#include "common_types_utils.h"
#include "ui_utils.h"

#include <QtDebug>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsRectItem>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QGraphicsLineItem>
#include <QtWidgets/QGraphicsPathItem>
#include <QtGui/QPainterPath>

constexpr double Default_Rings_Width = 0.1;
constexpr double Default_Rings_Border_Width = 0.0025;
constexpr double Default_Radial_Lines_Width = 0.0015;
QColor const Default_Ring_Borders_Color{16,16,16};
QColor const Default_Radial_Lines_Color{48,48,48};
QColor const Dynamic_Spot_Border{0,64,128};
QBrush const Dynamic_Spot_Fill{QColor{0,224,224,255}};
QColor const Enabled_Static_Spot_Border{0,128,64};
QBrush const Enabled_Static_Spot_Fill{QColor{0,224,0,255}};
QColor const Disabled_Static_Spot_Border{128,128,128};
QBrush const Disabled_Static_Spot_Fill{QColor{208,208,208,255}};

Target_Scene::Target_Scene(QObject* parent):
   QGraphicsScene(parent)
{
}

double Target_Scene::ringWidth() const
{
   return Default_Rings_Width;
}

void Target_Scene::setupTarget(int8_t min_score_)
{
   this->min_score = min_score_;

   // cleanup
   // the rings: from inside to outside
   constexpr double ring_border_width = Default_Rings_Border_Width;
   QPen rings_border_pen(Default_Ring_Borders_Color, ring_border_width);
   for(int ring_idx = 0; ring_idx < 10; ++ring_idx)
   {
      int8_t const score = 10-ring_idx;
      if ( score < this->min_score )
      {
         if ( this->rings[ring_idx] != nullptr )
         {
            this->rings[ring_idx]->setVisible(false);
         }
         if ( this->rings_borders[ring_idx] != nullptr )
         {
            this->rings_borders[ring_idx]->setVisible(false);
         }
         continue;
      }
      double const ring_radius = Default_Rings_Width * (11-score);
      if ( this->rings[ring_idx] == nullptr )
      {
         this->rings[ring_idx] =
            this->addEllipse(
                     -ring_radius, -ring_radius,
                     2.0*ring_radius, 2.0*ring_radius,
                     Qt::NoPen,
                     QBrush(targetVividColorForMark(scoreToMark(score))));
         this->rings[ring_idx]->setZValue(0.1*score);
      }
      this->rings[ring_idx]->setVisible(true);
      if ( this->rings_borders[ring_idx] == nullptr )
      {
         this->rings_borders[ring_idx] =
            this->addEllipse(
                     -ring_radius+ring_border_width*0.5, -ring_radius+ring_border_width*0.5,
                     2.0*ring_radius-ring_border_width, 2.0*ring_radius-ring_border_width,
                     rings_border_pen,
                     Qt::NoBrush);
         this->rings_borders[ring_idx]->setZValue(0.1*score+0.04);
      }
      this->rings_borders[ring_idx]->setVisible(true);
   }
   // adding an extra inner ring and cross
   QGraphicsEllipseItem* inner_center =
      this->addEllipse(-Default_Rings_Width*0.5, -Default_Rings_Width*0.5,
                       Default_Rings_Width, Default_Rings_Width,
                       QPen(Default_Radial_Lines_Color, Default_Radial_Lines_Width),
                       Qt::NoBrush);
   inner_center->setZValue(1.04);
   QGraphicsLineItem* vline =
         this->addLine(0.0, -0.01, 0.0, 0.01,
                       QPen(Default_Radial_Lines_Color, Default_Radial_Lines_Width));
   vline->setZValue(1.04);
   QGraphicsLineItem* hline =
         this->addLine(-0.01, 0.0, 0.01, 0.0,
                       QPen(Default_Radial_Lines_Color, Default_Radial_Lines_Width));
   hline->setZValue(1.04);
   //
   this->deactivateDynamicSpot();
   this->unfadeAll();
   this->removeAllStaticSpots();
   this->removeAllDiscs();
   this->setSceneRect(this->rectForScore(0));
}

int8_t Target_Scene::minimumScore() const
{
   return this->min_score;
}

QRectF Target_Scene::rectForScore(int8_t score) const
{
   size_t const ref_ring_id =
         (score <= this->minimumScore()) ?
            10 - this->minimumScore() :
            ( (score >= 9) ? 1 : 10-score );
   QRectF const r = this->rings[ref_ring_id]->boundingRect();
   double const margin = 2.1*this->dyn_spot_radius.value();
   return r.marginsAdded({margin, margin, margin, margin});
}

void Target_Scene::showRadialLines()
{
   size_t idx = 0;
   size_t const nb_rings = 11-this->minimumScore();
   double const half_length = nb_rings * Default_Rings_Width;
   QPen lines_pen(Default_Radial_Lines_Color, Default_Radial_Lines_Width);
   for(QGraphicsLineItem*& rad_line: this->radial_lines)
   {
      if ( rad_line == nullptr )
      {
         rad_line = new QGraphicsLineItem(Default_Rings_Width, 0.0, half_length, 0.0);
         rad_line->setPen(lines_pen);
         rad_line->setRotation(idx*30.0);
         rad_line->setZValue(1.5);
         this->addItem(rad_line);
      }
      rad_line->setVisible(true);
      ++idx;
   }
}

void Target_Scene::hideRadialLines()
{
   for(QGraphicsLineItem* rad_line: this->radial_lines)
   {
      if ( rad_line != nullptr )
      {
         rad_line->setVisible(false);
      }
   }
}

void Target_Scene::setSpotRadius(Radius r)
{
   this->dyn_spot_radius = r;
   if ( this->dyn_spot != nullptr )
   {
      QPointF const curr_dyn_spot_pos = this->dynamicSpotPosition();
      bool const dyn_spot_active = this->isDynamicSpotActive();
      this->deactivateDynamicSpot();
      delete this->dyn_spot;
      this->dyn_spot = this->makeSpot(Dynamic_Spot_Border, Dynamic_Spot_Fill);
      if ( dyn_spot_active )
      {
         this->activateDynamicSpot(curr_dyn_spot_pos);
      }
      else
      {
         this->dyn_spot->setPos(curr_dyn_spot_pos);
         this->dyn_spot->setVisible(false);
      }
   }
}

Radius Target_Scene::spotRadius() const
{
   return this->dyn_spot_radius;
}

int8_t Target_Scene::scoreForPosition(QPointF const& p) const
{
   double const dist = std::max(std::sqrt(QPointF::dotProduct(p,p)) - this->dyn_spot_radius.value(), 0.0);
   int const ring = 10 - static_cast<int>(std::floor(10.0*dist));
   return std::max(ring, 0);
}

void Target_Scene::fadeScore(int8_t score)
{
   if ( score == 0 )
   {
      return;
   }
   size_t const ref_ring_id =
         (score <= this->minimumScore()) ?
            10 - this->minimumScore() :
            ( (score >= 9) ? 1 : 10-score );
   QGraphicsEllipseItem* ring = this->rings[ref_ring_id];
   if ( ring != nullptr )
   {
      ring->setBrush(targetFadeColorForMark(scoreToMark(score)));
   }
}

void Target_Scene::fadeAll()
{
   int8_t score = 10;
   for(QGraphicsEllipseItem* ring: this->rings)
   {
      if ( ring != nullptr )
      {
         ring->setBrush(targetFadeColorForMark(scoreToMark(score)));
      }
      --score;
   }
}

void Target_Scene::unfadeAll()
{
   int8_t score = 10;
   for(QGraphicsEllipseItem* ring: this->rings)
   {
      if ( ring != nullptr )
      {
         ring->setBrush(targetVividColorForMark(scoreToMark(score)));
      }
      --score;
   }
}

void Target_Scene::unfadeScore(int8_t score)
{
   if ( score == 0 )
   {
      return;
   }
   size_t const ref_ring_id =
         (score <= this->minimumScore()) ?
            10 - this->minimumScore() :
            ( (score >= 9) ? 1 : 10-score );
   QGraphicsEllipseItem* ring = this->rings[ref_ring_id];
   if ( ring != nullptr )
   {
      ring->setBrush(targetVividColorForMark(scoreToMark(score)));
   }
}

void Target_Scene::activateDynamicSpot(QPointF const& p)
{
   if ( this->is_dyn_spot_active )
   {
      return;
   }
   this->deactivateSelector();
   if ( this->dyn_spot == nullptr )
   {
      this->dyn_spot = this->makeSpot(Dynamic_Spot_Border, Dynamic_Spot_Fill);
      this->dyn_spot->setZValue(this->dyn_spot->zValue()+0.5);
   }
   this->dyn_spot->setPos(p);
   this->dyn_spot->setVisible(true);
   this->is_dyn_spot_active = true;
}

bool Target_Scene::isDynamicSpotActive() const
{
   return this->is_dyn_spot_active;
}

void Target_Scene::setDynamicSpotPosition(QPointF const& p)
{
   if ( this->dyn_spot == nullptr )
   {
      this->dyn_spot = this->makeSpot(Dynamic_Spot_Border, Dynamic_Spot_Fill);
   }
   this->dyn_spot->setPos(p);
}

QPointF Target_Scene::dynamicSpotPosition() const
{
   if ( this->dyn_spot != nullptr )
   {
      return this->dyn_spot->pos();
   }
   return {0.0,0.0};
}

QRectF Target_Scene::dynamicSpotRect() const
{
   if ( this->dyn_spot != nullptr )
   {
      QPointF const pos = this->dyn_spot->pos();
      double const margin = this->dyn_spot_radius.value() + 2.0*Default_Rings_Width;
      return QRectF(pos.x()-margin, pos.y()-margin,
                    2.0*margin, 2.0*margin);
   }
   return {};
}

void Target_Scene::deactivateDynamicSpot()
{
   if ( this->dyn_spot != nullptr )
   {
      this->dyn_spot->setVisible(false);
   }
   this->is_dyn_spot_active = false;
}

void Target_Scene::activateSelector(QPointF const& p)
{
   if ( this->is_selector_active )
   {
      return;
   }
   this->deactivateDynamicSpot();
   if ( this->selector == nullptr )
   {
      QGraphicsRectItem* sel_rect =
            new QGraphicsRectItem(
               -1.5*Default_Rings_Width, -1.5*Default_Rings_Width,
               3.0*Default_Rings_Width, 3.0*Default_Rings_Width);
      sel_rect->setPen(QPen(QColor(128,128,128), 0.75*Default_Rings_Border_Width));
      sel_rect->setBrush(QColor(192,192,192,128));
      sel_rect->setZValue(2.0);
      this->selector = sel_rect;
      this->addItem(sel_rect);
   }
   this->selector->setVisible(true);
   this->selector->setPos(p);
   this->is_selector_active = true;
}

bool Target_Scene::isSelectorActive() const
{
   return this->is_selector_active;
}

void Target_Scene::setSelectorPosition(QPointF const& p)
{
   if ( this->selector != nullptr )
   {
      this->selector->setPos(p);
   }
}

QRectF Target_Scene::selectorRect() const
{
   return this->selector->sceneBoundingRect();
}

void Target_Scene::deactivateSelector()
{
   if ( this->selector != nullptr )
   {
      this->selector->setVisible(false);
   }
   this->is_selector_active = false;
}

void Target_Scene::addStaticSpot(uint64_t id,
                                 QPointF const& pos,
                                 QString const& label)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot != nullptr )
   {
      return;
   }
   static_spot.spot = this->makeSpot(Enabled_Static_Spot_Border, Enabled_Static_Spot_Fill);
   static_spot.spot->setPos(pos);
   static_spot.label = label;
   this->enableStaticSpot(id);
}

void Target_Scene::moveStaticSpot(uint64_t id,
                                  QPointF const& new_pos)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** moveStaticSpot(): invalid static spot id" << id;
      return;
   }
   static_spot.spot->setPos(new_pos);
}

void Target_Scene::enableStaticSpot(uint64_t id)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** enableStaticSpot(): invalid static spot id" << id;
      return;
   }
   this->adjustSpotColors(static_spot.spot,
                          Enabled_Static_Spot_Border,
                          Enabled_Static_Spot_Fill);
   static_spot.is_enabled = true;
}

void Target_Scene::disableStaticSpot(uint64_t id)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** disableStaticSpot(): invalid static spot id" << id;
      return;
   }
   this->adjustSpotColors(static_spot.spot,
                          Disabled_Static_Spot_Border,
                          Disabled_Static_Spot_Fill);
   static_spot.is_enabled = false;
}

void Target_Scene::setStaticSpotEnabled(uint64_t id,
                                        bool enabled)
{
   if ( enabled )
   {
      this->enableStaticSpot(id);
   }
   else
   {
      this->disableStaticSpot(id);
   }
}

void Target_Scene::showStaticSpot(uint64_t id)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** showStaticSpot(): invalid static spot id" << id;
      return;
   }
   static_spot.spot->setVisible(true);
}

void Target_Scene::hideStaticSpot(uint64_t id)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** hideStaticSpot(): invalid static spot id" << id;
      return;
   }
   static_spot.spot->setVisible(false);
}

void Target_Scene::setStaticSpotVisible(uint64_t id,
                                        bool visible)
{
   if ( visible )
      this->showStaticSpot(id);
   else
      this->hideStaticSpot(id);
}

void Target_Scene::removeStaticSpot(uint64_t id)
{
   Static_Spot& static_spot = this->static_spots[id];
   if ( static_spot.spot == nullptr )
   {
      qDebug() << "*** removeStaticSpot(): invalid static spot id" << id;
      return;
   }
   delete static_spot.spot;
   static_spot.spot = nullptr;
}

void Target_Scene::removeAllStaticSpots()
{
   for(auto& static_spot: this->static_spots)
   {
      if ( static_spot.spot != nullptr )
      {
         delete static_spot.spot;
      }
   }
   this->static_spots.clear();
}

void Target_Scene::addDisc(uint64_t id,
                           QPointF const& p,
                           double radius,
                           QColor base_color)
{
   QGraphicsEllipseItem*& disc = this->discs[id];
   if ( disc == nullptr )
   {
      QColor const pen_color(
               qRound(0.75*base_color.red()),
               qRound(0.75*base_color.green()),
               qRound(0.75*base_color.blue()));
      QColor const fill_color(base_color.red(), base_color.green(), base_color.blue(), 128);
      disc = new QGraphicsEllipseItem(-radius, -radius, 2.0*radius, 2.0*radius);
      disc->setPen(QPen(pen_color, Default_Rings_Border_Width));
      disc->setBrush(QBrush(fill_color));
      this->addItem(disc);
      disc->setPos(p);
      disc->setZValue(2.1);
      disc->setVisible(true);
   }
}

void Target_Scene::showDisc(uint64_t id)
{
   QGraphicsEllipseItem*& disc = this->discs[id];
   if ( disc != nullptr )
   {
      disc->setVisible(true);
   }
}

void Target_Scene::hideDisc(uint64_t id)
{
   QGraphicsEllipseItem*& disc = this->discs[id];
   if ( disc != nullptr )
   {
      disc->setVisible(false);
   }
}

void Target_Scene::removeDisc(uint64_t id)
{
   QGraphicsEllipseItem*& disc = this->discs[id];
   if ( disc != nullptr )
   {
      delete disc;
      disc = nullptr;
   }
}

void Target_Scene::removeAllDiscs()
{
   for(auto& disc_val: this->discs)
   {
      if ( disc_val != nullptr )
      {
         delete disc_val;
      }
   }
   this->discs.clear();
}

void Target_Scene::setMouseOffset(QPointF const& offset)
{
   this->mouse_offset = offset;
}

QPointF Target_Scene::mouseOffset() const
{
   return this->mouse_offset;
}

QGraphicsItem* Target_Scene::makeSpot(QColor const& border_color,
                                      QBrush const& fill_brush)
{
   static constexpr double inside = Default_Rings_Border_Width*1.2;
   static constexpr double outside = Default_Rings_Width * 0.3;
   static QPointF const points[] = {
      {outside, inside}, {outside, -inside}, {inside, -inside},
      {inside, -outside}, {-inside, -outside}, {-inside, -inside},
      {-outside, -inside}, {-outside, inside}, {-inside, inside},
      {-inside, outside}, {inside, outside},
   };
   QPainterPath path;
   path.moveTo(inside, inside);
   for(auto const& p: points)
   {
      path.lineTo(p);
   }
   path.closeSubpath();

   QGraphicsPathItem* cross = new QGraphicsPathItem(path);
   cross->setPen(QPen(border_color, Default_Rings_Border_Width*0.1));
//   cross->setPen(Qt::NoPen);
   cross->setBrush(fill_brush);
   cross->setRotation(45.0);

   QGraphicsEllipseItem* circle =
         new QGraphicsEllipseItem(
            -this->dyn_spot_radius.value(), -this->dyn_spot_radius.value(),
            2.0*this->dyn_spot_radius.value(), 2.0*this->dyn_spot_radius.value(),
            cross);
   circle->setPen(QPen(border_color, Default_Rings_Border_Width*0.1));
//   circle->setPen(Qt::NoPen);
   circle->setBrush(fill_brush);

   cross->setZValue(2.0);
   this->addItem(cross);
   return cross;
}

void Target_Scene::adjustSpotColors(QGraphicsItem* s,
                                    QColor const& border_color,
                                    QBrush const& fill_brush) const
{
   QAbstractGraphicsShapeItem* shape = dynamic_cast<QAbstractGraphicsShapeItem*>(s);
   if ( shape == nullptr )
   {
      return;
   }
   shape->setPen(QPen(border_color, Default_Rings_Border_Width*0.8));
   shape->setBrush(fill_brush);
   for(QGraphicsItem* child: shape->childItems())
   {
      QAbstractGraphicsShapeItem* child_shape = dynamic_cast<QAbstractGraphicsShapeItem*>(child);
      if ( child_shape == nullptr )
      {
         continue;
      }
      child_shape->setPen(QPen(border_color, Default_Rings_Border_Width*0.8));
      child_shape->setBrush(fill_brush);
   }
}

void Target_Scene::mousePressEvent(QGraphicsSceneMouseEvent* qgsme)
{
   if ( this->isDynamicSpotActive() or this->isSelectorActive() )
   {
      emit this->dynamicItemStart(qgsme->scenePos()+this->mouse_offset);
   }
}

void Target_Scene::mouseMoveEvent(QGraphicsSceneMouseEvent* qgsme)
{
   if ( this->isDynamicSpotActive() or this->isSelectorActive() )
   {
      emit this->dynamicItemMoved(qgsme->scenePos()+this->mouse_offset);
   }
}

void Target_Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent* qgsme)
{
   if ( this->isDynamicSpotActive() or this->isSelectorActive() )
   {
      emit this->dynamicItemEnd(qgsme->scenePos()+this->mouse_offset);
   }
}
