
#pragma once

#include "common_types.h"
#include "units.h"

#include <QtCore/QHash>
#include <QtWidgets/QGraphicsScene>

#include <array>

constexpr double Default_Spot_Radius = 0.03;

class Target_Scene: public QGraphicsScene
{
      Q_OBJECT
   public:
      explicit Target_Scene(QObject* parent = nullptr);

      double ringWidth() const;

      void setupTarget(int8_t min_score_);
      int8_t minimumScore() const;
      QRectF rectForScore(int8_t score) const;

      void showRadialLines();
      void hideRadialLines();

      void setSpotRadius(Radius r);
      Radius spotRadius() const;
      int8_t scoreForPosition(QPointF const& p) const;

      void fadeScore(int8_t score);
      void fadeAll();
      void unfadeAll();
      void unfadeScore(int8_t score);

      void activateDynamicSpot(QPointF const& p = {0.0,0.0});
      bool isDynamicSpotActive() const;
      void setDynamicSpotPosition(QPointF const& p);
      QPointF dynamicSpotPosition() const;
      QRectF dynamicSpotRect() const;
      void deactivateDynamicSpot();

      void activateSelector(QPointF const& p = {0.0,0.0});
      bool isSelectorActive() const;
      void setSelectorPosition(QPointF const& p);
      QRectF selectorRect() const;
      void deactivateSelector();

      void addStaticSpot(uint64_t id,
                         QPointF const& pos,
                         QString const& label = QString());
      void moveStaticSpot(uint64_t id,
                          QPointF const& new_pos);
      void enableStaticSpot(uint64_t id);
      void disableStaticSpot(uint64_t id);
      void setStaticSpotEnabled(uint64_t id,
                                bool enabled);
      void showStaticSpot(uint64_t id);
      void hideStaticSpot(uint64_t id);
      void setStaticSpotVisible(uint64_t id,
                                bool visible);
      void removeStaticSpot(uint64_t id);
      void removeAllStaticSpots();

      void addDisc(uint64_t id,
                   QPointF const& p,
                   double radius,
                   QColor base_color);
      void showDisc(uint64_t id);
      void hideDisc(uint64_t id);
      void removeDisc(uint64_t id);
      void removeAllDiscs();

      void setMouseOffset(QPointF const& offset);
      QPointF mouseOffset() const;

   signals:
      void dynamicItemStart(QPointF const& p);
      void dynamicItemMoved(QPointF const& p);
      void dynamicItemEnd(QPointF const& p);

   protected:
      QGraphicsItem* makeSpot(QColor const& border_color,
                              QBrush const& fill_brush);
      void adjustSpotColors(QGraphicsItem* s,
                            QColor const& border_color,
                            QBrush const& fill_brush) const;
      virtual void mousePressEvent(QGraphicsSceneMouseEvent* qgsme) override;
      virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* qgsme) override;
      virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* qgsme) override;

   private:
      QPointF mouse_offset{0.0,0.0};
      std::array<QGraphicsEllipseItem*, 10> rings{{nullptr,}};
      std::array<QGraphicsEllipseItem*, 10> rings_borders{{nullptr,}};
      std::array<QGraphicsLineItem*, 12> radial_lines{{nullptr,}};
      QGraphicsItem* dyn_spot{nullptr};
      QGraphicsItem* selector{nullptr};
      Radius dyn_spot_radius{Default_Spot_Radius};
      struct Static_Spot
      {
            QGraphicsItem* spot{nullptr};
            QString label;
            bool is_enabled{true};
      }; // struct Static_Spot
      QHash<uint64_t, Static_Spot> static_spots;
      QHash<uint64_t, QGraphicsEllipseItem*> discs;
      int8_t min_score{1};
      bool is_dyn_spot_active{false};
      bool is_selector_active{false};
}; // class Target_Scene
