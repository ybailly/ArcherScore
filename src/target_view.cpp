
#include "target_view.h"
#include "target_scene.h"

#include <QtDebug>
#include <QtGui/QScreen>
#include <QtGui/QBackingStore>

Target_View::Target_View(QWidget* parent):
   QGraphicsView(parent)
{
   this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   this->target_scene = new Target_Scene(this);
   this->setScene(this->target_scene);

   QObject::connect(this->target_scene, &Target_Scene::dynamicItemStart,
                    this, &Target_View::dynamicItemStart);
   QObject::connect(this->target_scene, &Target_Scene::dynamicItemMoved,
                    this, &Target_View::dynamicItemMoved);
   QObject::connect(this->target_scene, &Target_Scene::dynamicItemEnd,
                    this, &Target_View::dynamicItemEnd);
   this->scale(1.0,-1.0);
}

QPointF Target_View::computeMouseOffset() const
{
#ifdef ANDROID
   QScreen* scr = this->backingStore()->window()->screen();
   if ( scr != nullptr )
   {
      // offset upward
      double const dpi_y = std::min(static_cast<double>(scr->logicalDotsPerInchY()),
                                    static_cast<double>(scr->physicalDotsPerInchY()));
      QPointF const origin = this->mapToScene(QPoint{0,0});
      QPointF const dpi_y_pos = this->mapToScene({0, qRound(dpi_y)});
      QPointF const vect = dpi_y_pos - origin;
      double const vect_length = std::sqrt(QPointF::dotProduct(vect,vect));
      QPointF const offset{0.0,vect_length*0.75};
      return offset;
   }
#endif // ANDROID
   return {0.0,0.0};
}

void Target_View::applyMouseOffset()
{
   this->target_scene->setMouseOffset(this->computeMouseOffset());
}

void Target_View::resetMouseOffset()
{
   this->target_scene->setMouseOffset({0.0,0.0});
}

void Target_View::zoomOnRect(QRectF const& r)
{
   this->zoomed_rect = r;
   this->fitInView(this->zoomed_rect, Qt::KeepAspectRatio);
}

void Target_View::zoomOnAll()
{
   this->zoomOnRect(this->rectForScore(this->minimumScore()));
   this->resetMouseOffset();
}

double Target_View::ringWidth() const
{
   return this->target_scene->ringWidth();
}

void Target_View::setupTarget(int8_t min_score)
{
   this->target_scene->setupTarget(min_score);
}

int8_t Target_View::minimumScore() const
{
   return this->target_scene->minimumScore();
}

QRectF Target_View::rectForScore(int8_t s) const
{
   return this->target_scene->rectForScore(s);
}

void Target_View::showRadialLines()
{
   this->target_scene->showRadialLines();
}

void Target_View::hideRadialLines()
{
   this->target_scene->hideRadialLines();
}

void Target_View::setSpotRadius(Radius r)
{
   this->target_scene->setSpotRadius(r);
}

Radius Target_View::spotRadius() const
{
   return this->target_scene->spotRadius();
}

int8_t Target_View::scoreForPosition(QPointF const& p) const
{
   return this->target_scene->scoreForPosition(p);
}

void Target_View::fadeScore(int8_t s)
{
   this->target_scene->fadeScore(s);
}

void Target_View::fadeAll()
{
   this->target_scene->fadeAll();
}

void Target_View::unfadeAll()
{
   this->target_scene->unfadeAll();
}

void Target_View::unfadeScore(int8_t s)
{
   this->target_scene->unfadeScore(s);
}

void Target_View::activateDynamicSpot(QPointF const& p)
{
   this->target_scene->activateDynamicSpot(p);
}

bool Target_View::isDynamicSpotActive() const
{
   return this->target_scene->isDynamicSpotActive();
}

void Target_View::setDynamicSpotPosition(QPointF const& p)
{
   this->target_scene->setDynamicSpotPosition(p);
}

QPointF Target_View::dynamicSpotPosition() const
{
   return this->target_scene->dynamicSpotPosition();
}

QRectF Target_View::dynamicSpotRect() const
{
   return this->target_scene->dynamicSpotRect();
}

void Target_View::deactivateDynamicSpot()
{
   this->target_scene->deactivateDynamicSpot();
}

void Target_View::activateSelector(QPointF const& p)
{
   this->target_scene->activateSelector(p);
}

bool Target_View::isSelectorActive() const
{
   return this->target_scene->isSelectorActive();
}

void Target_View::setSelectorPosition(QPointF const& p)
{
   this->target_scene->setSelectorPosition(p);
}

QRectF Target_View::selectorRect() const
{
   return this->target_scene->selectorRect();
}

void Target_View::deactivateSelector()
{
   this->target_scene->deactivateSelector();
}

void Target_View::addStaticSpot(uint64_t id,
                                QPointF const& pos,
                                QString const& label)
{
   this->target_scene->addStaticSpot(id, pos, label);
}

void Target_View::moveStaticSpot(uint64_t id,
                                 QPointF const& new_pos)
{
   this->target_scene->moveStaticSpot(id, new_pos);
}

void Target_View::enableStaticSpot(uint64_t id)
{
   this->target_scene->enableStaticSpot(id);
}

void Target_View::disableStaticSpot(uint64_t id)
{
   this->target_scene->disableStaticSpot(id);
}

void Target_View::setStaticSpotEnabled(uint64_t id,
                                       bool enabled)
{
   this->target_scene->setStaticSpotEnabled(id, enabled);
}

void Target_View::showStaticSpot(uint64_t id)
{
   this->target_scene->showStaticSpot(id);
}

void Target_View::hideStaticSpot(uint64_t id)
{
   this->target_scene->hideStaticSpot(id);
}

void Target_View::setStaticSpotVisible(uint64_t id,
                                       bool visible)
{
   this->target_scene->setStaticSpotVisible(id, visible);
}

void Target_View::removeStaticSpot(uint64_t id)
{
   this->target_scene->removeStaticSpot(id);
}

void Target_View::removeAllStaticSpots()
{
   this->target_scene->removeAllStaticSpots();
}

void Target_View::addDisc(uint64_t id,
                          QPointF const& p,
                          double radius,
                          QColor base_color)
{
   this->target_scene->addDisc(id, p, radius, base_color);
}

void Target_View::showDisc(uint64_t id)
{
   this->target_scene->showDisc(id);
}

void Target_View::hideDisc(uint64_t id)
{
   this->target_scene->hideDisc(id);
}

void Target_View::removeDisc(uint64_t id)
{
   this->target_scene->removeDisc(id);
}

void Target_View::removeAllDiscs()
{
   this->target_scene->removeAllDiscs();
}

void Target_View::resizeEvent(QResizeEvent* qre)
{
   this->QGraphicsView::resizeEvent(qre);
   if ( this->target_scene == nullptr )
   {
      return;
   }
   this->fitInView(this->zoomed_rect, Qt::KeepAspectRatio);
}
