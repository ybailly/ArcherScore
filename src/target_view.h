
#pragma once

#include "common_types.h"
#include "units.h"

#include <QtWidgets/QGraphicsView>

class Target_Scene;

class Target_View: public QGraphicsView
{
      Q_OBJECT
   public:
      explicit Target_View(QWidget* parent = nullptr);

      QPointF computeMouseOffset() const;
      void applyMouseOffset();
      void resetMouseOffset();

      void zoomOnRect(QRectF const& r);
      void zoomOnAll();

      double ringWidth() const;

      void setupTarget(int8_t min_score);
      int8_t minimumScore() const;
      QRectF rectForScore(int8_t s) const;

      void showRadialLines();
      void hideRadialLines();

      void setSpotRadius(Radius r);
      Radius spotRadius() const;
      int8_t scoreForPosition(QPointF const& p) const;

      void fadeScore(int8_t s);
      void fadeAll();
      void unfadeAll();
      void unfadeScore(int8_t s);

      void activateDynamicSpot(QPointF const& p = {0.0,0.0});
      bool isDynamicSpotActive() const;
      void setDynamicSpotPosition(QPointF const& p);
      QPointF dynamicSpotPosition() const;
      QRectF dynamicSpotRect() const;
      void deactivateDynamicSpot();

      void activateSelector(QPointF const& p = {0.0,0.0});
      bool isSelectorActive() const;
      void setSelectorPosition(QPointF const& p);
      QRectF selectorRect() const;
      void deactivateSelector();

      void addStaticSpot(uint64_t id,
                         QPointF const& pos,
                         QString const& label = QString());
      void moveStaticSpot(uint64_t id,
                          QPointF const& new_pos);
      void enableStaticSpot(uint64_t id);
      void disableStaticSpot(uint64_t id);
      void setStaticSpotEnabled(uint64_t id,
                                bool enabled);
      void showStaticSpot(uint64_t id);
      void hideStaticSpot(uint64_t id);
      void setStaticSpotVisible(uint64_t id,
                                bool visible);
      void removeStaticSpot(uint64_t id);
      void removeAllStaticSpots();

      void addDisc(uint64_t id,
                   QPointF const& p,
                   double radius,
                   QColor base_color);
      void showDisc(uint64_t id);
      void hideDisc(uint64_t id);
      void removeDisc(uint64_t id);
      void removeAllDiscs();

   signals:
      void dynamicItemStart(QPointF const& p);
      void dynamicItemMoved(QPointF const& p);
      void dynamicItemEnd(QPointF const& p);

   protected:
      virtual void resizeEvent(QResizeEvent* qre) override;

   private:
      Target_Scene* target_scene{nullptr};
      QRectF zoomed_rect{-1.1,-1.1,2.2,2.2};
}; // class Target_View
