
#include "tournament_editor.h"

#include "session.h"
#include "tournament_rounds_model.h"
#include "tournament_rounds_delegate.h"
#include "overlay_generic_number_input.h"

#include <QtDebug>

Tournament_Editor::Tournament_Editor(QWidget* parent):
   Generic_Page(parent)
{
   this->setupUi(this->contents());
   this->setBackVisible(true);
   this->setForwardVisible(false);

   QObject::connect(
            this, &Generic_Page::backRequested,
            this, &Tournament_Editor::done);
   QObject::connect(
            this->bt_edit_main_round, &QPushButton::clicked,
            [this]() { emit this->requestEditRound(Round_Index(0)); });
   QObject::connect(
            this->bt_new_duel_session, &QPushButton::clicked,
            this, &Tournament_Editor::appendRound);
   QObject::connect(
            this->tv_duel_rounds, &QTableView::clicked,
            this, &Tournament_Editor::roundClicked);
   QObject::connect(
            this->bt_to_pdf, &QPushButton::clicked,
            [this]()
   {
      if ( this->session_ == nullptr )
      {
         return;
      }
      sessionToPdf(*(this->session_));
   });
}

void Tournament_Editor::setSession(Session* sess)
{
   if ( this->rounds_model == nullptr )
   {
      this->rounds_model = new Tournament_Rounds_Model(this);
      this->tv_duel_rounds->setModel(this->rounds_model);
   }
   if ( this->rounds_delegate == nullptr )
   {
      this->rounds_delegate = new Tournament_Rounds_Delegate(this);
      this->tv_duel_rounds->setItemDelegate(this->rounds_delegate);
   };
   this->session_ = sess;
   this->rounds_model->setSession(this->session_, Round_Index(1));
   this->tv_duel_rounds->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
   this->tv_duel_rounds->clearSelection();
   this->tv_duel_rounds->resizeColumnsToContents();
   this->tv_duel_rounds->resizeRowsToContents();
   this->refreshStats();
}

Session* Tournament_Editor::session() const
{
   return this->session_;
}

void Tournament_Editor::appendRound()
{
   this->rounds_model->appendRound();
//   this->tv_duel_rounds->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
   this->tv_duel_rounds->clearSelection();
//   this->tv_duel_rounds->resizeColumnsToContents();
   this->tv_duel_rounds->resizeRowsToContents();
}

void Tournament_Editor::refreshStats()
{
   if ( this->session_ == nullptr )
   {
      return;
   }
   Ends_Round const& main_round = this->session_->endsRound(Round_Index(0));
   this->lbl_n10->setNum(main_round.n10());
   this->lbl_n10plus->setNum(main_round.n10Plus());
   this->lbl_mean_x->setText(QString("%1").arg(main_round.scoreAverage(), 0, 'f', 2));
   this->lbl_mean_cc->setText(QString("%1").arg(main_round.concentrationCoefficientAverage().value(), 0, 'f', 2));
   this->lbl_sum->setNum(main_round.scoreSum());
   this->rounds_model->setSession(this->session_, Round_Index(1));
}

void Tournament_Editor::roundClicked(QModelIndex const& idx)
{
   if ( idx.column() == static_cast<int>(Tournament_Rounds_Model::Columns::status) )
   {
      Overlay_Generic_Number_Input<int>* cut_input = new Overlay_Generic_Number_Input<int>(this->topLevelWidget());
      cut_input->setMessage(tr("Choisissez le <i>cut</i> pour valider le duel :"));
      cut_input->setValue(this->rounds_model->endsRound(idx).cut());
      QObject::connect(
               cut_input, &Overlay_Generic_Number_Input<int>::accepted,
               [cut_input,idx,this]()
      {
         this->rounds_model->setData(idx, cut_input->value());
         cut_input->hide();
         cut_input->deleteLater();
      });
      QObject::connect(
               cut_input, &Overlay_Generic_Number_Input<int>::canceled,
               [cut_input,this]()
      {
         cut_input->hide();
         cut_input->deleteLater();
      });
      cut_input->show();
      cut_input->raise();
      this->tv_duel_rounds->clearFocus();
      this->tv_duel_rounds->clearSelection();
      return;
   }
   emit this->requestEditRound(Round_Index(idx.row()+1));
}
