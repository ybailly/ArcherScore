
#pragma once

#include "generic_page.h"
#include "common_types.h"

#include "ui_tournament_editor.h"

class Session;
class Tournament_Rounds_Model;
class Tournament_Rounds_Delegate;

class Tournament_Editor: public Generic_Page, private Ui::Tournament_Editor
{
      Q_OBJECT
   public:
      explicit Tournament_Editor(QWidget* parent = nullptr);

      void setSession(Session* sess);
      Session* session() const;

      void appendRound();
      void refreshStats();

   signals:
      void requestEditRound(Round_Index rid);
      void done();

   protected slots:
      void roundClicked(QModelIndex const& idx);

   private:
      Session* session_{nullptr};
      Tournament_Rounds_Model* rounds_model{nullptr};
      Tournament_Rounds_Delegate* rounds_delegate{nullptr};

}; // class Tournament_Editor
