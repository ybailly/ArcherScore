
#include "tournament_rounds_delegate.h"

#include "tournament_rounds_model.h"
#include "ends_round.h"

#include <QtGui/QFontMetrics>
#include <QtGui/QPainter>

Tournament_Rounds_Delegate::Tournament_Rounds_Delegate(QObject* parent):
   QStyledItemDelegate(parent)
{
}

/*
void Tournament_Rounds_Delegate::paint(QPainter* painter,
                                       QStyleOptionViewItem const& option,
                                       QModelIndex const& index) const
{
   if ( index.column() != static_cast<int>(Tournament_Rounds_Model::Columns::status) )
   {
      this->QStyledItemDelegate::paint(painter, option, index);
      return;
   }
   Tournament_Rounds_Model const* const model = dynamic_cast<Tournament_Rounds_Model const*>(index.model());
   if ( model == nullptr)
   {
      this->QStyledItemDelegate::paint(painter, option, index);
      return;
   }
   Ends_Round const& ends_round = model->endsRound(index);
   QIcon const& icon = iconRoundStatus(ends_round.status());
   painter->save();
   int const margin_x = qRound(option.rect.width()*0.15);
   int const margin_y = qRound(option.rect.height()*0.15);
   icon.paint(painter, option.rect.adjusted(margin_x, margin_y, -margin_x, -margin_y), Qt::AlignCenter);
   painter->restore();
}
*/

QSize Tournament_Rounds_Delegate::sizeHint(QStyleOptionViewItem const& option,
                                           QModelIndex const& index) const
{
   QSize const base_size = this->QStyledItemDelegate::sizeHint(option, index);
   int const m_height = option.fontMetrics.boundingRect('M').height();
   return QSize(base_size.width(), 4*m_height);
}
