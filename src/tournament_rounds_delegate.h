
#pragma once

#include <QtWidgets/QStyledItemDelegate>
#include <QtWidgets/QLabel>

#include "session.h"

class Tournament_Rounds_Delegate: public QStyledItemDelegate
{
      Q_OBJECT
   public:
      explicit Tournament_Rounds_Delegate(QObject* parent = nullptr);

/*      virtual void paint(QPainter* painter,
                         QStyleOptionViewItem const& option,
                         QModelIndex const& index) const override;
*/
      virtual QSize sizeHint(QStyleOptionViewItem const& option,
                             QModelIndex const& index) const;

}; // class Tournament_Rounds_Delegate
