
#include "tournament_rounds_model.h"

#include "session.h"
#include "ui_utils.h"

Tournament_Rounds_Model::Tournament_Rounds_Model(QObject* parent):
   QAbstractItemModel(parent)
{
}

void Tournament_Rounds_Model::setSession(Session* sd,
                                         Round_Index minimum_round_index)
{
   this->beginResetModel();
   this->session_ = sd;
   this->min_round_index = minimum_round_index;
   this->endResetModel();
}

Session* Tournament_Rounds_Model::session() const
{
   return this->session_;
}

Round_Index Tournament_Rounds_Model::minimumRoundIndex() const
{
   return this->min_round_index;
}

void Tournament_Rounds_Model::appendRound()
{
   if ( this->session_ == nullptr )
   {
      return;
   }
   this->beginInsertRows({}, this->rowCount(), this->rowCount());
   this->session_->appendEndsRound();
   this->session_->mutableEndsRound(Round_Index(this->session_->endsRoundsCount()-1))->appendEnd();
   this->endInsertRows();
}

Ends_Round const& Tournament_Rounds_Model::endsRound(QModelIndex const& idx) const
{
   return this->session_->endsRound(Round_Index(idx.row())+this->min_round_index);
}

Ends_Round* Tournament_Rounds_Model::mutableEndsRound(QModelIndex const& idx)
{
   return this->session_->mutableEndsRound(Round_Index(idx.row())+this->min_round_index);
}

int Tournament_Rounds_Model::columnCount(QModelIndex const&) const
{
   return static_cast<int>(Columns::NB_COLUMNS);
}

QVariant Tournament_Rounds_Model::data(QModelIndex const& index,
                                       int role) const
{
   if ( (not index.isValid()) or (this->session_ == nullptr) )
   {
      return {};
   }
   if ( role == Qt::TextAlignmentRole )
   {
      return Qt::AlignCenter;
   }
   Columns const column = static_cast<Columns>(index.column());
   Ends_Round const& end_round = this->endsRound(index);
   if ( role == Qt::BackgroundRole )
   {
      if ( (column == Columns::n10) and (end_round.n10() > 0) )
      {
         return textBackgroundColorForMark(Mark::Ten);
      }
      if ( (column == Columns::n10plus) and (end_round.n10Plus() > 0) )
      {
         return textBackgroundColorForMark(Mark::TenPlus);
      }
      return {};
   }
   if ( role == Qt::DecorationRole )
   {
      if ( column == Columns::status )
      {
         return iconForRoundStatus(end_round.status());
      }
      return {};
   }
   if ( role == Qt::DisplayRole )
   {
      switch ( column )
      {
         case Columns::n10:
            return end_round.n10();
            break;
         case Columns::n10plus:
            return end_round.n10Plus();
            break;
/*
         case Columns::mean_x:
            return QString("%1").arg(end_round.markAverage(), 0, 'f', 2);
            break;
*/
         case Columns::mean_cc:
            return QString("%1").arg(end_round.concentrationCoefficientAverage().value(), 0, 'f', 2);
            break;
         case Columns::sum:
            return end_round.scoreSum();
            break;
         case Columns::status:
            return end_round.cut();
         default:
            return {};
      }
   }
   return {};
}

/*
Qt::ItemFlags Tournament_Rounds_Model::flags(QModelIndex const& index) const
{
   if ( index.column() == static_cast<int>(Columns::status) )
   {
      return this->QAbstractItemModel::flags(index) bitor Qt::ItemIsEditable;
   }
   return this->QAbstractItemModel::flags(index);
}
*/

bool Tournament_Rounds_Model::hasChildren(QModelIndex const&) const
{
   return false;
}

QModelIndex Tournament_Rounds_Model::index(int row,
                                           int column,
                                           QModelIndex const& /*parent*/) const
{
   return this->createIndex(row, column);
}

QVariant Tournament_Rounds_Model::headerData(int section,
                                             Qt::Orientation orientation,
                                             int role) const
{
   if ( this->session_ == nullptr )
   {
      return {};
   }
   if ( (role != Qt::DisplayRole) or (orientation != Qt::Horizontal) )
   {
      return this->QAbstractItemModel::headerData(section, orientation, role);
   }
   switch ( static_cast<Columns>(section) )
   {
      case Columns::n10:
         return QStringLiteral("10");
         break;
      case Columns::n10plus:
         return QStringLiteral("10+");
         break;
      case Columns::mean_cc:
         return QStringLiteral("c̅");
         break;
      case Columns::sum:
         return QStringLiteral("Σ");
         break;
      case Columns::status:
         return QStringLiteral(" Cut ");
         break;
      default:
         return {};
   }
   return {};
}

QModelIndex Tournament_Rounds_Model::parent(QModelIndex const&) const
{
   return {};
}

int Tournament_Rounds_Model::rowCount(QModelIndex const& /*parent*/) const
{
   if ( this->session_ == nullptr )
   {
      return 0;
   }
   return static_cast<int>(this->session_->endsRoundsCount()) -
          static_cast<int>(this->min_round_index.value());
}

bool Tournament_Rounds_Model::setData(QModelIndex const& index,
                                      QVariant const& value,
                                      int role)
{
   if ( this->session_ == nullptr )
   {
      return true;
   }
   if ( (Columns(index.column()) == Columns::status) and (role == Qt::EditRole) )
   {
      this->mutableEndsRound(index)->setCut(static_cast<uint16_t>(value.toUInt()));
      emit this->dataChanged(index, index);
      return true;
   }
   return this->QAbstractItemModel::setData(index, value, role);
}
