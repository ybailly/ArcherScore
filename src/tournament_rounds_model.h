
#pragma once

#include "common_types.h"

#include <QtCore/QAbstractItemModel>

class Session;
class Ends_Round;

class Tournament_Rounds_Model: public QAbstractItemModel
{
      Q_OBJECT
   public:
      enum class Columns: int
      {
         n10 = 0,
         n10plus,
//         mean_x,
         mean_cc,
         sum,
         status,
         NB_COLUMNS
      };

      explicit Tournament_Rounds_Model(QObject* parent = nullptr);

      void setSession(Session* sd,
                      Round_Index minimum_round_index);
      Session* session() const;
      Round_Index minimumRoundIndex() const;

      void appendRound();
      Ends_Round const& endsRound(QModelIndex const& idx) const;
      Ends_Round* mutableEndsRound(QModelIndex const& idx);

      virtual int columnCount(QModelIndex const& parent = QModelIndex()) const override;
      virtual QVariant data(QModelIndex const& index,
                            int role = Qt::DisplayRole) const override;
      // virtual Qt::ItemFlags flags(QModelIndex const& index) const override;
      virtual bool hasChildren(QModelIndex const& parent = QModelIndex()) const override;
      virtual QModelIndex index(int row,
                                int column,
                                QModelIndex const& parent = QModelIndex()) const override;
      virtual QVariant headerData(int section,
                                  Qt::Orientation orientation,
                                  int role = Qt::DisplayRole) const override;
      virtual QModelIndex parent(QModelIndex const& index) const override;
      virtual int rowCount(QModelIndex const& parent = QModelIndex()) const override;
      virtual bool setData(QModelIndex const& index,
                           QVariant const& value,
                           int role = Qt::EditRole) override;

   private:
      Session* session_{nullptr};
      Round_Index min_round_index{0};

}; // class Tournament_Rounds_Model
