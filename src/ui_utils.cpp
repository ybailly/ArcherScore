
#include "ui_utils.h"

#include "cpp_utils.h"
#include "qt_utils.h"
#include "arrows.h"

#include <QtCore/QCoreApplication>
#include <QtSvg/QSvgGenerator>
#include <QtGui/QPainter>
#include <QtCore/QBuffer>

QColor targetVividColorForMark(Mark mark)
{
   switch ( mark )
   {
      case Mark::One:
      case Mark::Two:
         return QColor(Qt::white);
      case Mark::Three:
      case Mark::Four:
         return QColor(64,64,64);
      case Mark::Five:
      case Mark::Six:
         return QColor(96,96,255);
      case Mark::Seven:
      case Mark::Height:
         return QColor(255,80,80);
      case Mark::Nine:
      case Mark::Ten:
         return QColor(255,255,80);
      case Mark::TenPlus:
         return QColor(255,255,0);
      default:
         return QColor(Qt::lightGray);
   }
}

QColor targetFadeColorForMark(Mark mark)
{
   switch ( mark )
   {
      case Mark::One:
      case Mark::Two:
         return QColor(240,240,240);
      case Mark::Three:
      case Mark::Four:
         return QColor(216,216,216);
      case Mark::Five:
      case Mark::Six:
         return QColor(216,216,255);
      case Mark::Seven:
      case Mark::Height:
         return QColor(255,216,216);
      case Mark::Nine:
      case Mark::Ten:
         return QColor(255,255,216);
      case Mark::TenPlus:
         return QColor(255,255,160);
      default:
         return QColor(Qt::lightGray);
   }
}

QColor textForegroundColorForMark(Mark mark)
{
   switch ( mark )
   {
      case Mark::One:
      case Mark::Two:
         return QColor(Qt::black);
      case Mark::Three:
      case Mark::Four:
         return QColor(Qt::white);
      case Mark::Five:
      case Mark::Six:
      case Mark::Seven:
      case Mark::Height:
      case Mark::Nine:
      case Mark::Ten:
      case Mark::TenPlus:
      default:
         return QColor(Qt::black);
   }
}

QColor textBackgroundColorForMark(Mark mark)
{
   switch ( mark )
   {
      case Mark::One:
      case Mark::Two:
         return Qt::white;
      case Mark::Three:
      case Mark::Four:
         return QColor(120,120,120);
      case Mark::Five:
      case Mark::Six:
         return QColor(176,176,255);
      case Mark::Seven:
      case Mark::Height:
         return QColor(255,192,192);
      case Mark::Nine:
      case Mark::Ten:
         return QColor(255,255,192);
      case Mark::TenPlus:
         return QColor(255,255,128);
      default:
         return QColor(Qt::lightGray);
   }
}

QIcon const& iconForTargetMinimumScore(int8_t m)
{
   switch ( m )
   {
      case 1:
      case 2:
      case 3:
      case 4:
         return qt::icon(QStringLiteral("target_1"));
      case 5:
         return qt::icon(QStringLiteral("target_5"));
      case 6:
         return qt::icon(QStringLiteral("target_6"));
      case 7:
         return qt::icon(QStringLiteral("target_7"));
      case 8:
      case 9:
      case 10:
         return qt::icon(QStringLiteral("target_8"));
      default:
         return qt::icon(QStringLiteral("target_1"));
   }
}

QIcon const& iconForSessionType(Session_Type t)
{
   static QIcon const empty;
   return empty;
}

QString const& svgForSessionType(Session_Type t)
{
   static QString const empty;
   return empty;
}

QIcon const& iconForSessionLocation(Session_Location loc)
{
   switch ( loc )
   {
      case Session_Location::Indoor:
         return qt::icon(QStringLiteral("location_indoor"));
      case Session_Location::Outdoor:
         return qt::icon(QStringLiteral("location_outdoor"));
   }
   return qt::icon(QStringLiteral("location_indoor"));
}

QIcon const& iconForRoundStatus(Round_Status rs)
{
   static QString const icons[] = {
      QStringLiteral("round_status_unknown"),
      QStringLiteral("round_status_irrelevant"),
      QStringLiteral("round_status_won"),
      QStringLiteral("round_status_lost")
   };
   return qt::icon(icons[static_cast<size_t>(rs)]);
}

QColor colorForConcentrationCoefficient(Concentration_Coefficient cc)
{
   static std::pair<double, QColor> const thresholds[] = {
      {0.001, QColor(Qt::lightGray)},  // null/invalid
      {0.660, QColor(Qt::green)},      // smaller than "10" ring
      {1.000, QColor(Qt::darkGreen)},  // smaller than 1.5*"10" ring
      {1.330, QColor(Qt::black)},      // smaller than "9" ring
      {2.000, QColor(Qt::darkRed)}     // smaller than "8" ring
   };
   for(auto const& thres: thresholds)
   {
      if ( cc.value() < thres.first ) return thres.second;
   }
   return QColor(Qt::red); // greater than "8" ring
}

QString svgResourceForBowType(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Unknown:
         return QStringLiteral(":/icons/bow_unknown_type");
         break;
      case Bow_Type::Traditional:
         return QStringLiteral(":/icons/bow_traditional");
         break;
      case Bow_Type::Traditional_Hunt:
         return QStringLiteral(":/icons/bow_traditional_hunt");
         break;
      case Bow_Type::Recurve:
         return QStringLiteral(":/icons/bow_recurve");
         break;
      case Bow_Type::Recurve_Hunt:
         return QStringLiteral(":/icons/bow_recurve_hunt");
         break;
      case Bow_Type::Recurve_Modern:
         return QStringLiteral(":/icons/bow_recurve_modern");
         break;
      case Bow_Type::Compound:
         return QStringLiteral(":/icons/bow_compound");
         break;
   }
   return {};
}

QIcon const& iconForBowType(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Unknown:
         return qt::icon(QStringLiteral("bow_unknown_type"));
         break;
      case Bow_Type::Traditional:
         return qt::icon(QStringLiteral("bow_traditional"));
         break;
      case Bow_Type::Traditional_Hunt:
         return qt::icon(QStringLiteral("bow_traditional_hunt"));
         break;
      case Bow_Type::Recurve:
         return qt::icon(QStringLiteral("bow_recurve"));
         break;
      case Bow_Type::Recurve_Hunt:
         return qt::icon(QStringLiteral("bow_recurve_hunt"));
         break;
      case Bow_Type::Recurve_Modern:
         return qt::icon(QStringLiteral("bow_recurve_modern"));
         break;
      case Bow_Type::Compound:
         return qt::icon(QStringLiteral("bow_compound"));
         break;
   }
   static QIcon empty_icon;
   return empty_icon;
}

QString textForBowType(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Unknown:
         return QCoreApplication::translate("bow type text", "Inconnu");
         break;
      case Bow_Type::Traditional:
         return QCoreApplication::translate("bow type text", "Traditionnel");
         break;
      case Bow_Type::Traditional_Hunt:
         return QCoreApplication::translate("bow type text", "Traditionnel (chasse)");
         break;
      case Bow_Type::Recurve:
         return QCoreApplication::translate("bow type text", "Classique (nu)");
         break;
      case Bow_Type::Recurve_Hunt:
         return QCoreApplication::translate("bow type text", "Classique (chasse)");
         break;
      case Bow_Type::Recurve_Modern:
         return QCoreApplication::translate("bow type text", "Classique (moderne)");
         break;
      case Bow_Type::Compound:
         return QCoreApplication::translate("bow type text", "Poulies");
         break;
   }
   return {};
}

QString buttonTextForBowType(Bow_Type bt)
{
   switch ( bt )
   {
      case Bow_Type::Unknown:
         return QCoreApplication::translate("bow type text", "Inconnu");
         break;
      case Bow_Type::Traditional:
         return QCoreApplication::translate("bow type text", "Traditionnel");
         break;
      case Bow_Type::Traditional_Hunt:
         return QCoreApplication::translate("bow type text", "Traditionnel\n(chasse)");
         break;
      case Bow_Type::Recurve:
         return QCoreApplication::translate("bow type text", "Classique\n(nu)");
         break;
      case Bow_Type::Recurve_Hunt:
         return QCoreApplication::translate("bow type text", "Classique\n(chasse)");
         break;
      case Bow_Type::Recurve_Modern:
         return QCoreApplication::translate("bow type text", "Classique\n(moderne)");
         break;
      case Bow_Type::Compound:
         return QCoreApplication::translate("bow type text", "Poulies");
         break;
   }
   return {};
}

QByteArray generateSvgFromFletching(Fletching const& f)
{
   QByteArray svg_data;
   QSvgGenerator svg;
   QBuffer buff(&svg_data);
   buff.open(QIODevice::WriteOnly);
   svg.setOutputDevice(&buff);
   constexpr qreal sz = 512.0;
   svg.setSize({qRound(sz),qRound(sz)});
   svg.setViewBox(QRectF{0.0,0.0,sz,sz});
   constexpr qreal margin = sz*0.05;
   constexpr qreal spacing = sz*0.02;
   constexpr qreal item_width = (sz-2.0*margin-2.0*spacing)/3.0;
   constexpr qreal item_height = sz-2.0*margin;
   QPainterPath feather;
   feather.moveTo(item_width, 0.0);
   feather.cubicTo({-item_width*0.5, item_height*0.20},
                   {item_width*0.5, item_height*0.75},
                   {item_width, item_height});
   feather.closeSubpath();
   QPainter p;
   p.begin(&svg);
   p.setPen(QPen(Qt::black, sz*0.02, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
   p.translate(margin, margin);
   p.setBrush(QBrush(f.colors[0]));
   p.drawPath(feather);
   p.translate(spacing+item_width, 0.0);
   p.setBrush(QBrush(f.colors[1]));
   p.drawPath(feather);
   p.translate(spacing+item_width, 0.0);
   p.setBrush(QBrush(f.colors[2]));
   p.drawPath(feather);
   p.end();
   buff.close();
   return svg_data;
}
