
#pragma once

#include "common_types.h"

#include <QtCore/QString>
#include <QtGui/QColor>
#include <QtGui/QIcon>

struct Fletching;

QColor targetVividColorForMark(Mark mark);
QColor targetFadeColorForMark(Mark mark);
QColor textForegroundColorForMark(Mark mark);
QColor textBackgroundColorForMark(Mark mark);

QIcon const& iconForTargetMinimumScore(int8_t m);

QIcon const& iconForSessionType(Session_Type t);
QString const& svgForSessionType(Session_Type t);

QIcon const& iconForSessionLocation(Session_Location loc);

QIcon const& iconForRoundStatus(Round_Status rs);

QColor colorForConcentrationCoefficient(Concentration_Coefficient cc);

QString svgResourceForBowType(Bow_Type bt);
QIcon const& iconForBowType(Bow_Type bt);
QString textForBowType(Bow_Type bt);
QString buttonTextForBowType(Bow_Type bt);

QByteArray generateSvgFromFletching(Fletching const& f);
