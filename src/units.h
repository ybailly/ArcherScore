
#pragma once

#include "tagged_value.h"

class Radius_Tag;
class Diameter_Tag;
class Distance_Tag;
class Length_Tag;
class Weight_Tag;
class Thickness_Tag;

class IN64th_Tag;
class IN1Kth_Tag;
class MM_Tag;
class CM_Tag;
class M_Tag;
class IN_Tag;
class YD_Tag;
class GR_Tag;
class MG_Tag;
class OZ_Tag;
class G_Tag;
class LB_Tag;
class KG_Tag;

using Radius = Tagged_Value<double, Radius_Tag, Any_Unit_Tag>;
using Radius_MM = Tagged_Value<double, Radius_Tag, MM_Tag>;
using Radius_CM = Tagged_Value<double, Radius_Tag, CM_Tag>;
using Radius_iCM = Tagged_Value<int32_t, Radius_Tag, CM_Tag>;
using Diameter = Tagged_Value<double, Diameter_Tag, Any_Unit_Tag>;
using Diameter_MM = Tagged_Value<double, Diameter_Tag, MM_Tag>;
using Diameter_CM = Tagged_Value<double, Diameter_Tag, CM_Tag>;
using Diameter_iCM = Tagged_Value<int32_t, Diameter_Tag, CM_Tag>;
using Diameter_64thIn = Tagged_Value<double, Diameter_Tag, IN64th_Tag>;

using Distance = Tagged_Value<double, Distance_Tag, Any_Unit_Tag>;
using Distance_M = Tagged_Value<double, Distance_Tag, M_Tag>;
using Distance_iM = Tagged_Value<int32_t, Distance_Tag, M_Tag>;
using Distance_YD = Tagged_Value<double, Distance_Tag, YD_Tag>;
using Distance_iYD = Tagged_Value<int32_t, Distance_Tag, YD_Tag>;

using Thickness_MM = Tagged_Value<double, Thickness_Tag, MM_Tag>;
using Thickness_1KthIn = Tagged_Value<double, Thickness_Tag, IN1Kth_Tag>;

constexpr Radius operator"" _radius(long double d) { return Radius(d); }
constexpr Diameter operator"" _diameter(long double d) { return Diameter(d); }

using Length = Tagged_Value<double, Length_Tag, Any_Unit_Tag>;
using Length_IN = Tagged_Value<double, Length_Tag, IN_Tag>;
using Length_CM = Tagged_Value<double, Length_Tag, CM_Tag>;

using Weight = Tagged_Value<double, Weight_Tag, Any_Unit_Tag>;
using Weight_GR = Tagged_Value<double, Weight_Tag, GR_Tag>;
using Weight_MG = Tagged_Value<double, Weight_Tag, MG_Tag>;
using Weight_OZ = Tagged_Value<double, Weight_Tag, OZ_Tag>;
using Weight_G = Tagged_Value<double, Weight_Tag, G_Tag>;
using Weight_LB = Tagged_Value<double, Weight_Tag, LB_Tag>;
using Weight_KG = Tagged_Value<double, Weight_Tag, KG_Tag>;
