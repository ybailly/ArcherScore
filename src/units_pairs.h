
#pragma once

#include "units.h"
#include "tagged_value_pair.h"

template<>
struct Unit_Converter<double, IN_Tag, CM_Tag>
{ static constexpr double convert(double in) { return in*2.54; } };

template<>
struct Unit_Converter<double, CM_Tag, IN_Tag>
{ static constexpr double convert(double cm) { return cm/2.54; } };

template<>
struct Unit_Converter<double, M_Tag, YD_Tag>
{ static constexpr double convert(double m) { return m/0.9144; } };

template<>
struct Unit_Converter<double, YD_Tag, M_Tag>
{ static constexpr double convert(double yd) { return yd*0.9144; } };

template<>
struct Unit_Converter<double, GR_Tag, MG_Tag>
{ static constexpr double convert(double gr) { return gr*64.79891; } };

template<>
struct Unit_Converter<double, MG_Tag, GR_Tag>
{ static constexpr double convert(double mg) { return mg/64.79891; } };

template<>
struct Unit_Converter<double, OZ_Tag, G_Tag>
{ static constexpr double convert(double oz) { return oz*28.349523125; } };

template<>
struct Unit_Converter<double, G_Tag, OZ_Tag>
{ static constexpr double convert(double g) { return g/28.349523125; } };

template<>
struct Unit_Converter<double, LB_Tag, KG_Tag>
{ static constexpr double convert(double lb) { return lb*0.45359237; } };

template<>
struct Unit_Converter<double, KG_Tag, LB_Tag>
{ static constexpr double convert(double kg) { return kg/0.45359237; } };

template<>
struct Unit_Converter<double, IN64th_Tag, MM_Tag>
{ static constexpr double convert(double in64th) { return in64th*0.15625 /*25.4/64*/; } };

template<>
struct Unit_Converter<double, MM_Tag, IN64th_Tag>
{ static constexpr double convert(double mm) { return mm/0.15625 /*25.4/64*/; } };

template<>
struct Unit_Converter<double, IN1Kth_Tag, MM_Tag>
{ static constexpr double convert(double in1kth) { return in1kth*0.0254 /*25.4/1000*/; } };

template<>
struct Unit_Converter<double, MM_Tag, IN1Kth_Tag>
{ static constexpr double convert(double mm) { return mm/0.0254 /*25.4/1000*/; } };

template<typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
using Tagged_Value_Pair_Double = Tagged_Value_Pair<double, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2>;

using Distance_M_Yd  = Tagged_Value_Pair_Double<Distance_Tag, M_Tag, YD_Tag>;
using Diameter_Cm_In = Tagged_Value_Pair_Double<Diameter_Tag, CM_Tag, IN_Tag>;
using Length_Cm_In   = Tagged_Value_Pair_Double<Length_Tag, CM_Tag, IN_Tag>;
using Weight_Mg_Gr   = Tagged_Value_Pair_Double<Weight_Tag, MG_Tag, GR_Tag>;
using Weight_G_Oz    = Tagged_Value_Pair_Double<Weight_Tag, G_Tag, OZ_Tag>;
using Weight_Kg_Lb   = Tagged_Value_Pair_Double<Weight_Tag, KG_Tag, LB_Tag>;
using Diameter_Mm_64thIn = Tagged_Value_Pair_Double<Diameter_Tag, MM_Tag, IN64th_Tag>;
using Thickness_Mm_1KthIn = Tagged_Value_Pair_Double<Thickness_Tag, MM_Tag, IN1Kth_Tag>;
