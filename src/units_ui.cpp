
#include "units_ui.h"

QString const& Unit_Traits<MM_Tag>::name() { static const QString n("millimètre"); return n; }
QString const& Unit_Traits<MM_Tag>::symbol() { static const QString s("mm"); return s; }
uint8_t Unit_Traits<MM_Tag>::decimalsForShort() { return 2; }

QString const& Unit_Traits<CM_Tag>::name() { static const QString n("centimètre"); return n; }
QString const& Unit_Traits<CM_Tag>::symbol() { static const QString s("cm"); return s; }
uint8_t Unit_Traits<CM_Tag>::decimalsForShort() { return 2; }

QString const& Unit_Traits<M_Tag>::name() { static const QString n("mètre"); return n; }
QString const& Unit_Traits<M_Tag>::symbol() { static const QString s("m"); return s; }
uint8_t Unit_Traits<M_Tag>::decimalsForShort() { return 3; }

QString const& Unit_Traits<IN64th_Tag>::name() { static const QString n("pouce/64"); return n; }
QString const& Unit_Traits<IN64th_Tag>::symbol() { static const QString s("in/64"); return s; }
uint8_t Unit_Traits<IN64th_Tag>::decimalsForShort() { return 1; }

QString const& Unit_Traits<IN1Kth_Tag>::name() { static const QString n("pouce/1000"); return n; }
QString const& Unit_Traits<IN1Kth_Tag>::symbol() { static const QString s("in/1000"); return s; }
uint8_t Unit_Traits<IN1Kth_Tag>::decimalsForShort() { return 1; }

QString const& Unit_Traits<IN_Tag>::name() { static const QString n("pouce"); return n; }
QString const& Unit_Traits<IN_Tag>::symbol() { static const QString s("in"); return s; }
uint8_t Unit_Traits<IN_Tag>::decimalsForShort() { return 3; }

QString const& Unit_Traits<YD_Tag>::name() { static const QString n("yard"); return n; }
QString const& Unit_Traits<YD_Tag>::symbol() { static const QString s("yd"); return s; }
uint8_t Unit_Traits<YD_Tag>::decimalsForShort() { return 3; }

QString const& Unit_Traits<GR_Tag>::name() { static const QString n("grain"); return n; }
QString const& Unit_Traits<GR_Tag>::symbol() { static const QString s("gr"); return s; }
uint8_t Unit_Traits<GR_Tag>::decimalsForShort() { return 2; }

QString const& Unit_Traits<MG_Tag>::name() { static const QString n("milligramme"); return n; }
QString const& Unit_Traits<MG_Tag>::symbol() { static const QString s("mg"); return s; }
uint8_t Unit_Traits<MG_Tag>::decimalsForShort() { return 1; }

QString const& Unit_Traits<OZ_Tag>::name() { static const QString n("once"); return n; }
QString const& Unit_Traits<OZ_Tag>::symbol() { static const QString s("oz"); return s; }
uint8_t Unit_Traits<OZ_Tag>::decimalsForShort() { return 3; }

QString const& Unit_Traits<G_Tag>::name() { static const QString n("gramme"); return n; }
QString const& Unit_Traits<G_Tag>::symbol() { static const QString s("g"); return s; }
uint8_t Unit_Traits<G_Tag>::decimalsForShort() { return 1; }

QString const& Unit_Traits<LB_Tag>::name() { static const QString n("livre"); return n; }
QString const& Unit_Traits<LB_Tag>::symbol() { static const QString s("lb"); return s; }
uint8_t Unit_Traits<LB_Tag>::decimalsForShort() { return 2; }

QString const& Unit_Traits<KG_Tag>::name() { static const QString n("kilogramme"); return n; }
QString const& Unit_Traits<KG_Tag>::symbol() { static const QString s("kg"); return s; }
uint8_t Unit_Traits<KG_Tag>::decimalsForShort() { return 3; }

template QString toShortString<IN64th_Tag, double>(double);
template QString toShortString<IN1Kth_Tag, double>(double);
template QString toShortString<MM_Tag, double>(double);
template QString toShortString<CM_Tag, double>(double);
template QString toShortString<M_Tag, double>(double);
template QString toShortString<IN_Tag, double>(double);
template QString toShortString<YD_Tag, double>(double);
template QString toShortString<OZ_Tag, double>(double);
template QString toShortString<G_Tag, double>(double);
template QString toShortString<LB_Tag, double>(double);
template QString toShortString<KG_Tag, double>(double);

template QString toLongString<IN64th_Tag, double>(double);
template QString toLongString<IN1Kth_Tag, double>(double);
template QString toLongString<MM_Tag, double>(double);
template QString toLongString<CM_Tag, double>(double);
template QString toLongString<M_Tag, double>(double);
template QString toLongString<IN_Tag, double>(double);
template QString toLongString<YD_Tag, double>(double);
template QString toLongString<OZ_Tag, double>(double);
template QString toLongString<G_Tag, double>(double);
template QString toLongString<LB_Tag, double>(double);
template QString toLongString<KG_Tag, double>(double);

template QString toShortStringSymbol<IN64th_Tag, double>(double);
template QString toShortStringSymbol<IN1Kth_Tag, double>(double);
template QString toShortStringSymbol<MM_Tag, double>(double);
template QString toShortStringSymbol<CM_Tag, double>(double);
template QString toShortStringSymbol<M_Tag, double>(double);
template QString toShortStringSymbol<IN_Tag, double>(double);
template QString toShortStringSymbol<YD_Tag, double>(double);
template QString toShortStringSymbol<OZ_Tag, double>(double);
template QString toShortStringSymbol<G_Tag, double>(double);
template QString toShortStringSymbol<LB_Tag, double>(double);
template QString toShortStringSymbol<KG_Tag, double>(double);

template QString toLongStringSymbol<IN64th_Tag, double>(double);
template QString toLongStringSymbol<IN1Kth_Tag, double>(double);
template QString toLongStringSymbol<MM_Tag, double>(double);
template QString toLongStringSymbol<CM_Tag, double>(double);
template QString toLongStringSymbol<M_Tag, double>(double);
template QString toLongStringSymbol<IN_Tag, double>(double);
template QString toLongStringSymbol<YD_Tag, double>(double);
template QString toLongStringSymbol<OZ_Tag, double>(double);
template QString toLongStringSymbol<G_Tag, double>(double);
template QString toLongStringSymbol<LB_Tag, double>(double);
template QString toLongStringSymbol<KG_Tag, double>(double);

template QString toShortString<double, Distance_Tag, M_Tag, YD_Tag>(Distance_M_Yd const&);
template QString toShortString<double, Diameter_Tag, CM_Tag, IN_Tag>(Diameter_Cm_In const&);
template QString toShortString<double, Diameter_Tag, MM_Tag, IN64th_Tag>(Diameter_Mm_64thIn const&);
template QString toShortString<double, Length_Tag, CM_Tag, IN_Tag>(Length_Cm_In const&);
template QString toShortString<double, Weight_Tag, G_Tag, OZ_Tag>(Weight_G_Oz const&);
template QString toShortString<double, Weight_Tag, KG_Tag, LB_Tag>(Weight_Kg_Lb const&);
template QString toShortString<double, Thickness_Tag, MM_Tag, IN1Kth_Tag>(Thickness_Mm_1KthIn const&);

template double fromString<double>(QString const&);
template Distance_M_Yd fromString<Distance_M_Yd>(QString const&);
template Diameter_Cm_In fromString<Diameter_Cm_In>(QString const&);
template Diameter_Mm_64thIn fromString<Diameter_Mm_64thIn>(QString const&);
template Length_Cm_In fromString<Length_Cm_In>(QString const&);
template Weight_G_Oz fromString<Weight_G_Oz>(QString const&);
template Weight_Kg_Lb fromString<Weight_Kg_Lb>(QString const&);
template Thickness_Mm_1KthIn fromString<Thickness_Mm_1KthIn>(QString const&);
