
#pragma once

#include "units.h"
#include "units_pairs.h"
#include "qt_utils.h"

template<typename UNIT_TAG>
struct Unit_Traits
{
      static QString const& name() { return QString(); }
      static QString const& symbol() { return QString(); }
      static uint8_t decimalsForShort() { return 0; }
};

template<typename BASE_TYPE, bool IS_INTEGRAL = std::is_integral<BASE_TYPE>::value>
struct String_To_Value;

template<typename BASE_TYPE>
struct String_To_Value<BASE_TYPE, true>
{
      static BASE_TYPE fromString(QString const& s)
      { return static_cast<BASE_TYPE>(s.toLongLong()); }
      static BASE_TYPE fromString(QStringRef const& s)
      { return static_cast<BASE_TYPE>(s.toLongLong()); }
};

template<typename BASE_TYPE>
struct String_To_Value<BASE_TYPE, false>
{
      static BASE_TYPE fromString(QString const& s)
      { return static_cast<BASE_TYPE>(s.toDouble()); }
      static BASE_TYPE fromString(QStringRef const& s)
      { return static_cast<BASE_TYPE>(s.toDouble()); }
};

template<typename TAGGED_VALUE_TYPE>
struct String_To_Tagged_Value
{
      static TAGGED_VALUE_TYPE fromString(QString const& s)
      { return String_To_Value<TAGGED_VALUE_TYPE>::fromString(s); }
      static TAGGED_VALUE_TYPE fromString(QStringRef const& s)
      { return String_To_Value<TAGGED_VALUE_TYPE>::fromString(s); }
};

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG>
struct String_To_Tagged_Value<Tagged_Value<BASE_TYPE, DIM_TAG, UNIT_TAG>>
{
      static constexpr bool is_integral = std::is_integral<BASE_TYPE>::value;
      using value_t = Tagged_Value<BASE_TYPE, DIM_TAG, UNIT_TAG>;
      static value_t fromString(QString const& s)
      {
         return String_To_Tagged_Value::fromString(QStringRef(&s));
      }
      static value_t fromString(QStringRef const& s)
      {
         QString const& symbol = Unit_Traits<UNIT_TAG>::symbol();
         if ( s.endsWith(symbol) )
         {
            QStringRef const value_ref = s.left(s.length()-symbol.length());
            return value_t(String_To_Value<BASE_TYPE, is_integral>::fromString(value_ref));
         }
         return value_t(String_To_Value<BASE_TYPE>::fromString(s));
      }
};

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
struct String_To_Tagged_Value<Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2>>
{
      static constexpr bool is_integral = std::is_integral<BASE_TYPE>::value;
      using value_t = Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2>;
      using type_1 = typename value_t::type_1;
      using type_2 = typename value_t::type_2;
      static value_t fromString(QString const& s)
      { return String_To_Tagged_Value::fromString(QStringRef(&s)); }
      static value_t fromString(QStringRef const& s)
      {
         QString const& symbol_1 = Unit_Traits<UNIT_TAG_1>::symbol();
         if ( s.endsWith(symbol_1) )
         {
            QStringRef const value_ref = s.left(s.length()-symbol_1.length());
            return value_t(type_1(String_To_Value<BASE_TYPE, is_integral>::fromString(value_ref)));
         }
         QString const& symbol_2 = Unit_Traits<UNIT_TAG_2>::symbol();
         if ( s.endsWith(symbol_2) )
         {
            QStringRef const value_ref = s.left(s.length()-symbol_2.length());
            return value_t(type_2(String_To_Value<BASE_TYPE, is_integral>::fromString(value_ref)));
         }
         return value_t(String_To_Tagged_Value<type_1>::fromString(s));
      }
};

template<typename BASE_TYPE, typename UNIT_TAG, bool IS_INTEGRAL = std::is_integral<BASE_TYPE>::value>
struct Value_To_String;

template<typename BASE_TYPE, typename UNIT_TAG>
struct Value_To_String<BASE_TYPE, UNIT_TAG, true>
{
      static QString toShortString(BASE_TYPE value)
      {
         return QStringLiteral("%1").arg(value);
      }
      static QString toShortStringSymbol(BASE_TYPE value)
      {
         return Value_To_String::toShortString(value) + Unit_Traits<UNIT_TAG>::symbol();
      }
      static QString toLongString(BASE_TYPE value)
      {
         return Value_To_String::toShortString(value);
      }
      static QString toLongStringSymbol(BASE_TYPE value)
      {
         return Value_To_String::toShortStringSymbol(value);
      }
};

template<typename BASE_TYPE, typename UNIT_TAG>
struct Value_To_String<BASE_TYPE, UNIT_TAG, false>
{
      static QString toShortString(BASE_TYPE value)
      {
         return qt::simplifyDoubleShortString(value, Unit_Traits<UNIT_TAG>::decimalsForShort());
      }
      static QString toShortStringSymbol(BASE_TYPE value)
      {
         return Value_To_String::toShortString(value) + Unit_Traits<UNIT_TAG>::symbol();
      }
      static QString toLongString(BASE_TYPE value)
      {
         return QStringLiteral("%1").arg(value, 0, 'E', 12);
      }
      static QString toLongStringSymbol(BASE_TYPE value)
      {
         return Value_To_String::toLongString(value) + Unit_Traits<UNIT_TAG>::symbol();
      }
};

template<typename UNIT_TAG, typename BASE_TYPE>
QString toShortString(BASE_TYPE value)
{
   return Value_To_String<BASE_TYPE, UNIT_TAG>::toShortString(value);
}

template<typename UNIT_TAG, typename BASE_TYPE>
QString toLongString(BASE_TYPE value)
{
   return Value_To_String<BASE_TYPE, UNIT_TAG>::toLongString(value);
}

template<typename UNIT_TAG, typename BASE_TYPE>
QString toShortStringSymbol(BASE_TYPE value)
{
   return Value_To_String<BASE_TYPE, UNIT_TAG>::toShortStringSymbol(value);
}

template<typename UNIT_TAG, typename BASE_TYPE>
QString toLongStringSymbol(BASE_TYPE value)
{
   return Value_To_String<BASE_TYPE, UNIT_TAG>::toLongStringSymbol(value);
}

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG>
QString toShortString(Tagged_Value<BASE_TYPE, DIM_TAG, UNIT_TAG> value)
{
   return toShortStringSymbol<UNIT_TAG>(value.value());
}

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG>
QString toLongString(Tagged_Value<BASE_TYPE, DIM_TAG, UNIT_TAG> value)
{
   return toLongStringSymbol<UNIT_TAG>(value.value());
}

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
QString toShortString(Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2> const& value)
{
   return
         (value.preferred() == 0) ?
            toShortString(value.v1()) :
            toShortString(value.v2());
}

template<typename BASE_TYPE, typename DIM_TAG, typename UNIT_TAG_1, typename UNIT_TAG_2>
QString toLongString(Tagged_Value_Pair<BASE_TYPE, DIM_TAG, UNIT_TAG_1, UNIT_TAG_2> const& value)
{
   return
         (value.preferred() == 0) ?
            toLongString(value.v1()) :
            toLongString(value.v2());
}

template<typename VALUE_TYPE>
VALUE_TYPE fromString(QString const& s)
{
   return String_To_Tagged_Value<VALUE_TYPE>::fromString(s);
}

template<typename VALUE_TYPE>
void fillFromString(VALUE_TYPE& val, QString const& s)
{
   val = fromString<VALUE_TYPE>(s);
}

template<>
struct Unit_Traits<MM_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<CM_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<M_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<IN64th_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<IN1Kth_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<IN_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<YD_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<GR_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<MG_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<OZ_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<G_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<LB_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

template<>
struct Unit_Traits<KG_Tag>
{
      static QString const& name();
      static QString const& symbol();
      static uint8_t decimalsForShort();
};

extern template QString toShortString<IN64th_Tag, double>(double);
extern template QString toShortString<IN1Kth_Tag, double>(double);
extern template QString toShortString<MM_Tag, double>(double);
extern template QString toShortString<CM_Tag, double>(double);
extern template QString toShortString<M_Tag, double>(double);
extern template QString toShortString<IN_Tag, double>(double);
extern template QString toShortString<YD_Tag, double>(double);
extern template QString toShortString<OZ_Tag, double>(double);
extern template QString toShortString<G_Tag, double>(double);
extern template QString toShortString<LB_Tag, double>(double);
extern template QString toShortString<KG_Tag, double>(double);

extern template QString toLongString<IN64th_Tag, double>(double);
extern template QString toLongString<IN1Kth_Tag, double>(double);
extern template QString toLongString<MM_Tag, double>(double);
extern template QString toLongString<CM_Tag, double>(double);
extern template QString toLongString<M_Tag, double>(double);
extern template QString toLongString<IN_Tag, double>(double);
extern template QString toLongString<YD_Tag, double>(double);
extern template QString toLongString<OZ_Tag, double>(double);
extern template QString toLongString<G_Tag, double>(double);
extern template QString toLongString<LB_Tag, double>(double);
extern template QString toLongString<KG_Tag, double>(double);

extern template QString toShortStringSymbol<IN64th_Tag, double>(double);
extern template QString toShortStringSymbol<IN1Kth_Tag, double>(double);
extern template QString toShortStringSymbol<CM_Tag, double>(double);
extern template QString toShortStringSymbol<M_Tag, double>(double);
extern template QString toShortStringSymbol<IN_Tag, double>(double);
extern template QString toShortStringSymbol<YD_Tag, double>(double);
extern template QString toShortStringSymbol<OZ_Tag, double>(double);
extern template QString toShortStringSymbol<G_Tag, double>(double);
extern template QString toShortStringSymbol<LB_Tag, double>(double);
extern template QString toShortStringSymbol<KG_Tag, double>(double);

extern template QString toLongStringSymbol<IN64th_Tag, double>(double);
extern template QString toLongStringSymbol<IN1Kth_Tag, double>(double);
extern template QString toLongStringSymbol<MM_Tag, double>(double);
extern template QString toLongStringSymbol<CM_Tag, double>(double);
extern template QString toLongStringSymbol<M_Tag, double>(double);
extern template QString toLongStringSymbol<IN_Tag, double>(double);
extern template QString toLongStringSymbol<YD_Tag, double>(double);
extern template QString toLongStringSymbol<OZ_Tag, double>(double);
extern template QString toLongStringSymbol<G_Tag, double>(double);
extern template QString toLongStringSymbol<LB_Tag, double>(double);
extern template QString toLongStringSymbol<KG_Tag, double>(double);

extern template QString toShortString<double, Distance_Tag, M_Tag, YD_Tag>(Distance_M_Yd const&);
extern template QString toShortString<double, Diameter_Tag, CM_Tag, IN_Tag>(Diameter_Cm_In const&);
extern template QString toShortString<double, Diameter_Tag, MM_Tag, IN64th_Tag>(Diameter_Mm_64thIn const&);
extern template QString toShortString<double, Length_Tag, CM_Tag, IN_Tag>(Length_Cm_In const&);
extern template QString toShortString<double, Weight_Tag, G_Tag, OZ_Tag>(Weight_G_Oz const&);
extern template QString toShortString<double, Weight_Tag, KG_Tag, LB_Tag>(Weight_Kg_Lb const&);
extern template QString toShortString<double, Thickness_Tag, MM_Tag, IN1Kth_Tag>(Thickness_Mm_1KthIn const&);

extern template double fromString<double>(QString const&);
extern template Distance_M_Yd fromString<Distance_M_Yd>(QString const&);
extern template Diameter_Cm_In fromString<Diameter_Cm_In>(QString const&);
extern template Diameter_Mm_64thIn fromString<Diameter_Mm_64thIn>(QString const&);
extern template Length_Cm_In fromString<Length_Cm_In>(QString const&);
extern template Weight_G_Oz fromString<Weight_G_Oz>(QString const&);
extern template Weight_Kg_Lb fromString<Weight_Kg_Lb>(QString const&);
extern template Thickness_Mm_1KthIn fromString<Thickness_Mm_1KthIn>(QString const&);
